<?php
/*
 * Konfiguration
 */

//extern
define("intern",false);
include("funktionen/constant.php");

if (file_exists('serverconfig.php') && is_readable('serverconfig.php')) {
  include('serverconfig.php');
} else { die("Die Serverkonfiguration kann nicht gelesen werden."); }

include("funktionen/loadclasses.php");

UserConfig::getObj();

    error_reporting(E_ALL);
$GLOBALS['interface'] = 'frontend';

$GLOBALS['cms_root'] = $GLOBALS['cms_roothtml'];


cms\session::getObj();
if ( isset($_GET['act']) && $_GET['act'] == 'shortlink' ) {

    if ( !isset($_GET["str"]) || empty($_GET["str"])) {
        header('HTTP/1.0 404 Not Found');
        die("nope");
    }

    $links = UserConfig::getObj()->getRedirects();

    if ( isset($links[ $_GET["str"] ]) ) {
        header("Location: {$links[ $_GET["str"] ]}");
        die();
    }


    header('HTTP/1.0 404 Not Found');
    die();
}

if(isset($_GET['act']) && $_GET['act'] == 'sitemap'){
    header('Content-Type: application/xml; charset=utf-8');
    include("klassen/sitemap.php");
    $map = new sitemap();
    echo $map->getSitemap();
    die();
}
if(isset($_GET['act']) && $_GET['act'] == 'getrssfeed'){
	include('klassen/RssFeeds.php');
	header('Content-Type: application/xml; charset=utf-8');
	$c = new RssFeeds();
	echo $c->xmlOutPut();
	exit();
}
if(isset($_GET['tag']) && $_GET['tag'] != ''){
	addWhere('title_intern', '=',  $_GET['tag'], 's');
	select('tags', 'showin,title');
	$row = getRow();
	$str = $row['showin'];
	$GLOBALS['akt_tag'] = $row;
	addWhere('title_intern', '=',  $_GET['tag'], 's');
	updateArray('tags', array('prio' => 'prio+1'));
}elseif(isset($_GET['str']) && $_GET['str'] != '') $str = $_GET['str'];
else $str = '';

//var_dump($str);
//die();

$GLOBALS['seitenid'] = getMenuid($str);

preparation();
if(isset($GLOBALS['akt_menuepunkt']['useTpl']) && $GLOBALS['akt_menuepunkt']['useTpl'] != '') $tplFile = $GLOBALS['akt_menuepunkt']['useTpl'];
else $tplFile = 'index';
$tpl = new tpl('tpl/'.$tplFile.'.html');
$tpl->wenndann['ADMIN'] = false;
if($str == "" || $GLOBALS['akt_menuepunkt']['startpage'] == '1') $tpl->wenndann['START'] = true;
else $tpl->wenndann['START'] = false;

if (frontendSession::getObj()->getUserId() > 0){
	$tpl->wenndann['LOGGEDIN'] = true;
}else{
	$tpl->wenndann['LOGGEDIN'] = false;
}
echo $tpl->preParse();

//cleanup
if (cms\session::sessionExists()) cms\session::getObj()->__destruct();
//nice
?>