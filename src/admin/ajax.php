<?php

    define("intern",true);
    define("ajax",true);
    include("../funktionen/constant.php");

$GLOBALS['interface'] = 'ajax';
$GLOBALS['seitenid'] = '';

//echo $_SERVER['QUERY_STRING'].'<br>';
include('../serverconfig.php');

$GLOBALS['cms_root'] = $GLOBALS['cms_roothtml'].'admin/';

include("../funktionen/loadclasses.php");
UserConfig::getObj();

new login();
new \cms\session();
$namespace = '';

include('../funktionen/include.php');

if(isset($_GET['kl']) && $_GET['kl'] != ''){
	if($_GET['kl'] != 'DatabaseConnector' && $_GET['kl'] != 'dateimanager')
		include_once('../klassen/'.$_GET['kl'].'.php');
	if($_GET['kl'] == 'User') $klasse = new \cms\User ();
	else $klasse = new $_GET['kl']();
	if(method_exists($klasse, 'getHtml'))
		echo $klasse->getHtml();
	else
		echo $klasse->__toString();
}
if(isset($_GET['klasse']) && $_GET['klasse'] != ''){
	if($_GET['klasse'] != 'dateimanager')
		include('../klassen/'.$_GET['klasse'].'.php');
	if($_GET['klasse'] == 'User') $klasse = new \cms\User ();
	else $klasse = new $_GET['klasse']();
    $klasse->{$_GET['act']}();
}elseif(isset($_POST['klasse']) && $_POST['klasse'] != ''){
	if($_POST['klasse'] != 'dateimanager')
		include('../klassen/'.$_POST['klasse'].'.php');
	if($_POST['klasse'] == 'User') $klasse = new \cms\User ();
	else $klasse = new $_POST['klasse']();
    $klasse->{$_POST['act']}();
}elseif(isset($_GET['el']) && $_GET['el'] != ''){
	if(isset($_GET['m'])) $GLOBALS['seitenid'] = $_GET['m'];
	if($_GET['el'] == 'container'){
		include('../klassen/container.php');
	}
	elseif($_GET['el'] == 'menuepunkte' || (isset($_GET['get']) && $_GET['get'] != '')){
		include('../klassen/tpl.php');
		tpl::getCVARS();
	}
	if(($_GET['el'] == 'container' || $_GET['el'] == 'Accordion') && ($_GET['act'] == 'formedit' || $_GET['act'] == 'formNew')){
//                new dBug($GLOBALS['elements']);
		echo '<div id="addElements">Einf&uuml;gen von:'; // <a href="#">Inhalt</a>
		echo "</div>";
        echo "<div class='createContentContainer'>";
		foreach($GLOBALS['elements'] as $name => $el){
			$id = ($_GET['el'] == 'container')? $_GET['c']: $_GET['id'];
			$mainid = ($_GET['el'] == 'container')? 0 : $_GET['id'];

			echo ' <a class="createContent" href="#" id="addElement'.$name.'" onclick="elementaction(\''.$name.'\',\'New\',\''.$id.'\',\''.$_GET['c'].'\',\'\',\''.$_GET['parentid'].'\',[],\''.$mainid.'\')">'.$name.'</a>'; //'.$_GET['parentid'].'
		}
        echo "</div>";
	}
	if(isset($_GET['id']) && $_GET['id'] != '') $id = $_GET['id'];
	else $id = 0;
	/*
	 * Inhalt blocken
	 */
	if($id > 0 && (!isSet($_POST) || count($_POST) == 0)){
		$username = \cms\session::getObj()->getUserName();
		addWhere('id', '=', $id);
		select("elements",'blockedTime,blockedBy');
		$row = getRow();
		if($row['blockedTime']+5*60 > time() && $username != $row['blockedBy']){
			echo '<div class="warning">Achtung: Ein anderer Mitarbeiter ('.$row['blockedBy'].') bearbeitet gerade diesen Inhalt</div>';
		}else{
			addWhere('id', '=', $id);
			updateArray("elements", array(
				'blockedTime' => time(),
				'blockedBy' => $username
			),"is");
		}
   	}
	/*
	 * Rechte überprüfen
	 */
	if(!PageRights::getObj()->isAllowed()){
		echo '<div class="warning">Sie haben keine Berrechtigung für diesen Bereich</div>';
		exit();
	}

   	if(isset($_GET['get']) && $_GET['get'] != ''){
		/*
		 * Todo: PHP 5.3
		 * $klasse = $_GET['el']::getSub($id);
		 */
		$klasse = call_user_func_array(array($_GET['el'], 'getSub'),array($id));
		$id = 0;
	}elseif($_GET['el'] == 'DatabaseConnector'){ // && $_GET['act'] == 'formedit'
		$klasse = new DatabaseConnector($_GET['c'],0,$id);
	}elseif($_GET['el'] == 'Comments'){
		include('../klassen/Comments.php');
		$klasse = new Comments();
	}elseif(isset($_GET['mainid']) && $_GET['mainid'] > 0){
		$klasse = new $_GET['el']($id,array(),'', $_GET['mainid']);
	}else{
		$klasse = new $_GET['el']($id);
	}
	if($_GET['act'] == 'formPost'){
		$GLOBALS['seitenid'] = getMenuid('', $_GET['m']);
		preparation();
		$src = $klasse->formPost();
		include_once('../klassen/tpl.php');
		$tpl = new tpl('', $src);
		$tpl->parseLinks();
		echo $tpl->getSrcString();
	}else
        echo $klasse->{$_GET['act']}();
}elseif(isset($_GET['act'])){
	if($_GET['act'] == 'menulist'){
		addWhere('visible', '!=', 'a');
		$arr = buildSelectArray('menuepunkte', array('title_intern','title'));
		$ret = array();
		foreach($arr as $link => $val){
			$ret[] = array($val,$link);
		}
		echo json_encode($ret);
	}elseif($_GET['act'] == 'dblist'){
		$dbs = new DatabaseConnector();
		$arr = $dbs->getDatabases('id,name');
		$ret = array(); //array('','[Bitte ausw&auml;hlen]')
		foreach($arr as $link => $val){
			$ret[] = array($val,$val);
		}
		echo json_encode($ret);
	}elseif($_GET['act'] == 'rsslist'){
		$sqlobj = mys::getObj()->cleanup();
        $res = $sqlobj->query("SELECT r.id, r.name, d.`name` AS dbname FROM rss AS r INNER JOIN `databases` AS d ON r.`database` = d.id");
		$ret = array();
		 if ($res !== false) {
			while($row = $res->fetch_array()) {
				$ret[] = array($row['name'],$row['dbname'].'/'.$row['name']);
			}
		 }
		echo json_encode($ret);
	}elseif($_GET['act'] == 'ajaxList'){
		addWhere('verlinkung', '=', 'ajaxLink');
		$arr = buildSelectArray('menuepunkte', array('title_intern','title'));
		$arr = $arr + array('newAjaxSite' => 'Neue AjaxSeite');
		$ret = array();
		foreach($arr as $link => $val){
			$ret[] = array($val,$link);
		}
		echo json_encode($ret);
	}elseif($_GET['act'] == 'ajaxLang'){
		$ret = array();
		$ret[] = array("","Standard");

		if(isSet($GLOBALS['languages'])){
			foreach($arr as $link => $val){
				$ret[] = array($val,$link);
			}
		}
		echo json_encode($ret);
	}elseif($_GET['act'] == 'ajaxCropEig'){
		$ret = UserConfig::getObj()->getCropProperties();

		echo json_encode($ret);
	}elseif($_GET['act'] == 'tpl_datalist'){
		$ret = array();

		if(isset($_GET['dbid'])){
			$id = $_GET['dbid'];
	//		addWhere('dbid', '=', $id, 'i');
	//		$arr = buildSelectArray('datatemplates', array('id','name'));
	//		foreach($arr as $link => $val){
	//			$ret[0][] = array($val,$link);
	//		}

			$dbs = new DatabaseConnector();
			addWhere('name', '=', $id, 's');
			$db = array_pop($dbs->getDatabases('name,fields,fieldid'));
			$what = $dbs->getFieldId();
			$arr = $dbs->getData($db['name'],'id,'.$what);
			foreach($arr as $val){
				$ret[] = array($dbs->getFieldId($val),$val['id']);
			}
			echo json_encode($ret);
		}else echo json_encode(array());
	}elseif($_GET['act'] == 'newajax'){
		addWhere('SUBSTRING(title_intern,1,4)', '=', 'ajax');
		addWhere('verlinkung', '=', 'ajaxLink');
		setLimit('1');
//		select('menuepunkte','title_intern','SUBSTRING(title_intern,5)+0 DESC',true,true);
		select('menuepunkte','title_intern','',true,true);
		$rows = getRows();
		if(isSet($rows[0])) $z = substr($rows[0]['title_intern'],4);
		else $z = 0;
		do{
			$z++;
			addWhere('title_intern', '=', 'ajax'.$z);
		}while(dbCheck('menuepunkte'));
		$menuepunkt = array(
			'title' => 'ajax'.$z,
			'title_intern' => 'ajax'.$z,
			'mainid' => 0,
			'ordernr' => 0,
			'level' => 0,
			'visible' => 'a',
			'erstellt' => time(),
			'lastmod' => time(),
			'parent' => -1,
			'verlinkung' => 'ajaxLink'
		);
		insertArray('menuepunkte', $menuepunkt, 'ssiiisiiis');
		echo 'ajax'.$z;
	}elseif($_GET['act'] == 'disableBlock'){
		if(isset($_GET['id']) && $_GET['id'] != '') $id = $_GET['id'];
		else $id = 0;
		if($id > 0){
			addWhere('id', '=', $id);
			addWhere('blockedBy', '=', \cms\session::getObj()->getUserName());
			updateArray("elements", array(
				'blockedTime' => '0',
				'blockedBy' => ''
			),"is");
		}
		echo "Ok";
	}
	elseif($_GET['act'] == 'getFormBackup'){
		$id = $_GET['id'];
		$ret = array();
		addWhere('id', '=', $_GET['id']);
		select('formBackup');
		$row = getRow();
		$ret = unserialize($row['data']);
		if(isSet($ret['eldata'])){
			$add = unserialize($ret['eldata']);
			foreach($add as $i => $d){
				$ret[$i] = $d;
			}
		}
		echo json_encode($ret);
	}
	elseif($_GET['act'] == 'dbVars'){
		$dbId = $_GET['dbid'];
		$ret = array();
		$ret = array();
		addWhere('id', '=', $dbId);
		select('databases','name,fields');
		$row = getRow();
		$fields = unserialize($row['fields']);
		foreach($fields['fieldname'] as $f){
			$ret[] = array($row['name'].': '.$f,'[DB_'.$f.']');
		}

		echo json_encode($ret);
	}
	elseif($_GET['act'] == 'help'){
		mys::getObj()->changeDatabase('help');
		addWhere('element', '=', $_GET['helpEl']);
		select('help`.`elementhelp');
		$row = getRow();
		mys::getObj()->changeDatabase();
		echo '<h1>Hilfe</h1>';

		$attr = unserialize($row['attributes']);

		$overview = $row['description'];

		if(is_array($attr) && count($attr) > 0){
			foreach($attr['name'] as $i => $name){
				$overview .= '<p>';
				$overview .= '<b>'.$name.'</b>: ';
				$overview .= $attr['desc'][$i];
				$overview .= '</p>';
			}
		}

		$form = new formular();
		$form->useTab('Übersicht', $overview);
		$form->addHtml($overview);
		$form->useTab('Videos', $row['video']);
		$form->addHtml($row['video']);
		echo $form;
	}
	elseif($_GET['act'] == 'dmSearch' && isSet($_GET['term']) && $_GET['term'] != ''){
                $files = \cms\file::find($_GET['term']);
                $ret = array();
                foreach($files as $file){
                    $label = str_replace($_GET['term'], '<b>'.$_GET['term'].'</b>', $file['path'].'/'.$file['name']);
                    $label = $file['path'].'/'.$file['name'];
                    $ret[] = array(
                        'value' => $file['path'].'/'.$file['name'],
                        'label' => $label
                    );
                }

		echo json_encode($ret);
	}
}
//file_put_contents($_SERVER['DOCUMENT_ROOT'].$_GLOBALS['cms_rootdir']."dateien/ajax.log",
//		'Url:'.$_SERVER['REQUEST_URI']."\n".
//		'Post:'.print_r($_POST,1)."\n".
//		'Get:'.print_r($_GET,1)."\n".
//		'Files:'.print_r($_FILES,1)."\n".
//		'------------------------------------------',FILE_APPEND);
?>