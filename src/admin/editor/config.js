/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	mWidth = mWidth || 0;
	var path = '../';
	if($('#cmsroot').length > 0){
	  path = $('#cmsroot').html()+'admin/';
	}
	config.filebrowser_browse_url = path+'index.php?mm=dateimanager&maxwidth='+mWidth;
	config.filebrowserWindowFeatures = 'resizable=yes,scrollbars=yes';

	config.defaultLanguage = 'de';
	config.toolbar = 'Inhalt';
	config.extraPlugins = "cmsvideo,linkcms,cmsinstantcrop,cmsinstantzoom,cmsGMap";

//	config.emailProtection = 'mt(NAME,DOMAIN,SUBJECT,BODY)';
	config.bodyId = 'inhalt';
	config.resize_enabled = false;
	config.format_tags = 'p;h1;h2;h3;h4';
	config.contentsCss = ['/tpl/css/style.css', '/admin/css/interface.css',"/tpl/css/style.css"];
	
	
        config.fillEmptyBlocks = function (element) {
                return true; // DON'T DO ANYTHING!!!!!
        };
        config.allowedContent = true; // don't filter my data

	config.toolbar_Inhalt = [
		['Source','-','Preview'],
		['PasteText','PasteFromWord'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link', 'IntLink','Unlink','Anchor'],
		['Image','cmsGMap','instantCrop','instantZoom','CmsVideo','Flash','Table','HorizontalRule','SpecialChar'],
		'/',
		['Styles','Format'],
		['TextColor'],
		['Maximize', 'ShowBlocks']
	];
	config.toolbar_FrontEnd = [
		['PasteText','PasteFromWord'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['HorizontalRule','SpecialChar'],
		'/',
		['Styles','Format'],
		['TextColor'],
		['Maximize']
	];
	config.toolbar_Template = [
		['Source','-','Preview'],
		['PasteText','PasteFromWord'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link', 'IntLink','Unlink','Anchor'],
		['Image','instantCrop','instantZoom','cmsGMap','CmsVideo','Flash','Table','HorizontalRule','SpecialChar'],
		'/',
		['Styles','Format'],
		['TextColor'],
		['Maximize', 'ShowBlocks'],
		['CmsVars']
	];
	config.toolbar_Pdf = [
		['Source'],
		['Undo','Redo'],
		['Bold','Italic','Underline','-','Link', 'Unlink'],
		['FontSize','Blockquote', 'TextColor', 'Image'],
		['SelectAll', 'RemoveFormat'],
		['Maximize']
	] ;
	config.toolbar_BBCUser = [
		['Source','CharCount','Undo','Redo'],
		['Bold','Italic','Underline'],
		['FontSize', 'TextColor'],
		['SelectAll', 'RemoveFormat'],
		['Maximize']
	] ;
	config.toolbar_Newsletter = [
		['Source','-','Preview'],
		['PasteText','PasteFromWord'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link', 'IntLink','Unlink','Anchor'],
		['Image','Table','HorizontalRule','SpecialChar'],
		'/',
		['Styles','Format', 'Font','FontSize', 'TextColor'],
		['Maximize', 'ShowBlocks']
	];
    //Full Toolbar: http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar
};
