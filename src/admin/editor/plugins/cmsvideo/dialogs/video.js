/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.dialog.add( 'videointegration', function( editor )
{
	// Handles the event when the "Type" selection box is changed.
	var playerTypeChanged = function()
	{
		var dialog = this.getDialog(),
		partIds = [ 'mini', 'maxi'], //, 'multi'
		typeValue = this.getValue(),
		videoFile,
		videoFolder;

		videoFile = dialog.getContentElement( 'info', 'videoFile');
		videoFolder = dialog.getContentElement( 'info', 'videoFolder');
	//		if(typeValue == 'multi'){
	//			if (videoFile )
	//				videoFile.hide();
	//			if (videoFolder )
	//				videoFolder.show();
	//		}else{
	//			if (videoFile )
	//				videoFile.show();
	//			if (videoFolder )
	//				videoFolder.hide();
	//		}
	};

	// Loads the parameters in a selected link to the link dialog fields.
	var playerFolder = '/js/player/';

	var parseSelection = function( editor, element )
	{
		var width = element ? ( element.getAttribute( 'width' )) : '',
		height = element ? ( element.getAttribute( 'height' )) : '',
		data = element ? ( element.getAttribute( 'data' )) : '',
		retval = {},
		value,val,vars,i,j,check=false;

		retval.type = data.substr(data.length-8, 4);
		retval.file = {};
		retval.file.width = width;
		retval.file.height = height;
//		retval.file.vidurl = false;

		if(Math.round((height/width)*100) == 75) retval.file.format = 'normalScreen';
		else if(Math.round((height/width)*10000) == 5625) retval.file.format = 'wideScreen';
		else retval.file.format = 'cusom';
		
		if(element){
			for ( i = 0 ; i < element.children.length ; i++ ){
				var child =  element.children[ i ];
				if ( element.children[ i ].nodeName == 'CKE:PARAM' )
				{
					value = element.children[i].getAttribute('value');
					vars = value.split('&');
					for (j = 0 ; j < vars.length ; j++ ){
						val = vars[j].split('=');
						if(val[0] == 'flv'){
							retval.file.vidurl = val[1];
							check = true;
							break;
						}
					}
					if(check != false)
						break;
				}
			}
		}
		/**
		 *param name="FlashVars" value="flv=/21cms/dateien/KyodaiNoGilga.flv&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;showfullscreen=1" /
		 **/

		return retval;
	};
		
	var playerTypes =
	[
	[ 'minimaler Player', 'mini' ],
	[ 'Player mit vollem funktionsumfang', 'maxi' ]
	//			,[ 'Player für mehrere Videos', 'multi' ]
	];

	return {
		title : editor.lang.link.title,
		minWidth : 350,
		minHeight : 230,
		contents : [
		{
			id : 'info',
			label : editor.lang.link.info,
			title : editor.lang.link.info,
			elements :
			[
			{
				id : 'playerType',
				type : 'select',
				label : editor.lang.link.type,
				'default' : 'url',
				items : playerTypes,
				onChange : playerTypeChanged,
				setup : function( data )
				{
					if ( data.type )
						this.setValue( data.type );
				},
				commit : function( data )
				{
					data.type = this.getValue();
				}
			},
			{
				type : 'vbox',
				id : 'videoFile',
				children :
				[
				{
					type : 'hbox',
					widths : [ '75%', '25%' ],
					children :
					[
					{
						type : 'text',
						id : 'vidurl',
						label : 'VideoDatei',
						setup : function( data )
						{
							this.allowOnChange = false;
							if ( data.file )
								this.setValue( data.file.vidurl );
							this.allowOnChange = true;
						},
						commit : function( data )
						{
							if ( !data.file )
								data.file = {};

							data.file.vidurl = this.getValue();
							this.allowOnChange = false;
						}
					}
					]
				},
				{
					type : 'button',
					id : 'browse',
					hidden : 'true',
					filebrowser : {
						action : 'Browse',
						url : 'index.php?mm=Dateimanager&filter=flv',
						target : 'info:vidurl'
					},
					label : editor.lang.common.browseServer
				}
				]
			},{
				type : 'vbox',
				id : 'resolution',
				children :
				[
				{
					type : 'select',
					id : 'vidFormat',
					label : 'Format',
					items: [["normal (4:3)", "normalScreen"], ["Breitbild (16:9)", "wideScreen"], ["Benutzerdefiniert", "custom"]],
					onChange: function(data){
						var dialog = this.getDialog();
						this.allowOnChange = false;
						var format = this.getValue();
						//							if(!data.file) data.file = {};
						if(format == 'custom') return;


						var widthEl = dialog.getContentElement( 'info', 'vidWidth');
						var width = widthEl.getValue();
						var height = 0;

						if(format == "normalScreen")
							height = Math.round(width*3/4);
						else if(format == "wideScreen")
							height = Math.round(width*9/16);
							

						var heightEl = dialog.getContentElement( 'info', 'vidHeight');
						heightEl.allowOnChange = false;
						heightEl.setValue(height);
						heightEl.allowOnChange = true;
						this.allowOnChange = true;
					},
					setup : function( data )
					{
						this.allowOnChange = false;
						if ( data.file.format )
							this.setValue( data.file.format );
						this.allowOnChange = true;
					}
				},
				{
					type : 'hbox',
					widths : [ '50%', '50%' ],
					label : 'Auflösung',
					children :
					[
					{
						type : 'text',
						id : 'vidWidth',
						label : 'Breite in px',
						onLoad : function(){
							mWidth = mWidth || 0;
							if(mWidth == 0)
								this.setValue('320');
							else
								this.setValue(mWidth);
						},
						setup : function( data )
						{
							this.allowOnChange = false;
							if ( data.file)
								this.setValue( data.file.width );
							else{
								mWidth = mWidth || 0;
								if(mWidth == 0)
									this.setValue('320');
								else
									this.setValue(mWidth);
							this.allowOnChange = true;
							}

						},
						commit : function( data )
						{
							if ( !data.file )
								data.file = {};

							data.file.width = this.getValue();

						//							heightEl.setValue(data.file.height);
						},
						onChange : function( data )
						{
							if ( !data.file )
								data.file = {};
							data.file.width = this.getValue();
							
							var dialog = this.getDialog();
							var formatEl = dialog.getContentElement( 'info', 'vidFormat');
							//							formatEl.fire( 'onChange' );
							formatEl.onChange();
						//							formatEl.allowOnChange = true;
						//							this.allowOnChange = true;
						}
					},
					{
						type : 'text',
						id : 'vidHeight',
						label : 'Höhe in px',
						setup : function( data )
						{
							this.allowOnChange = false;
							if ( data.file )
								this.setValue( data.file.height );
							else
								this.setValue('240');
							this.allowOnChange = true;
						},
						commit : function( data )
						{
							if ( !data.file )
								data.file = {};

							data.file.height = this.getValue();

						//							heightEl.setValue(data.file.height);
						},
						onChange : function(data){
							if(!this.allowOnChange || this.getValue() == "") return;

							this.allowOnChange = false;
							var dialog = this.getDialog();
							var formatEl = dialog.getContentElement( 'info', 'vidFormat');
							formatEl.setValue('custom');
							this.allowOnChange = true;
						}
					}
					]
				}
				]
			}
			]
		}
		],
		onShow : function()
		{
			// Clear previously saved elements.
			this.fakeImage = this.objectNode = null;

			// Try to detect any embed or object tag that has Flash parameters.
			var fakeImage = this.getSelectedElement();
			if ( fakeImage && fakeImage.getAttribute( '_cke_real_element_type' ) && fakeImage.getAttribute( '_cke_real_element_type' ) == 'flash' )
			{
				this.fakeImage = fakeImage;

				var realElement = editor.restoreRealElement( fakeImage ),
				objectNode = null, embedNode = null, paramMap = {};
				if ( realElement.getName() == 'cke:object' ){
					objectNode = realElement;
				}
				this.objectNode = objectNode;
				this.objectParent = objectNode.parentNode;

				this.setupContent( parseSelection.apply( this, [ editor, objectNode.$ ] ) );
			}
		},
		onOk : function()
		{

			var objectNode = null,
				paramMap = null,
				editor = this.getParentEditor(),
				player,
				data, attributes = {
					type : 'application/x-shockwave-flash'
				};
			data = attributes;
			this.commitContent( data );

			if(data.type == 'mini')
				player = 'js/player/player_flv_mini.swf';
			else if(data.type == 'maxi')
				player = 'js/player/player_flv_maxi.swf';
			else
				player = 'js/player/player_flv_multi.swf';

			if($('#cmsroot').length > 0) player = $('#cmsroot').html()+player;
			else player = '/'+player;
			attributes = {
				type : 'application/x-shockwave-flash',
				data: player,
				width:data.file.width,
				height: data.file.height
			};
			if ( !this.fakeImage )
			{
				objectNode = CKEDITOR.dom.element.createFromHtml( '<cke:object></cke:object>', editor.document );
			}
			else
			{
				objectNode = this.objectNode;
			}
			objectNode.setAttributes( attributes );

			// Produce the paramMap if there's an object tag.
			if ( objectNode )
			{
				var paramList = objectNode.getElementsByTag( 'param', 'cke' ),
					name,
					dontAdd = {},
					i,param;
					
				paramMap = {
					'width' : data.file.width,
					'height' : data.file.height,
					'movie' : player,
					'allowFullScreen' : 'true',
					'FlashVars' : 'flv='+data.file.vidurl+'&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;showfullscreen=1'
				};
				for (i = 0, length = paramList.count() ; i < length ; i++ ){
					name = paramList.getItem( i ).getAttribute( 'name' );
					if(paramMap[name]){
						dontAdd[name] = true;
						paramList.getItem( i ).setAttribute( 'name', paramMap[name]);
					}
				}
				for(name in paramMap) {
					if(dontAdd[name] != true ){
						param = CKEDITOR.dom.element.createFromHtml( '<cke:param name="'+name+'" value="'+paramMap[name]+'"></cke:param>', editor.document);
						objectNode.append(param);
					}
				}
			}

			// A subset of the specified attributes/styles
			// should also be applied on the fake element to
			// have better visual effect. (#5240)
			var extraStyles = {
				width:data.file.width+"px",
				height:data.file.height+"px"
				}, extraAttributes = {
				width:data.file.width,
				height:data.file.height
				};
			//				this.commitContent( objectNode, paramMap, extraStyles, extraAttributes );

			// Refresh the fake image.
			var newFakeImage = editor.createFakeElement( objectNode, 'cke_flash', 'flash', true );
			newFakeImage.setAttributes( extraAttributes );
			newFakeImage.setStyles( extraStyles );
			if ( this.fakeImage )
			{
				newFakeImage.replace( this.fakeImage );
				editor.getSelection().selectElement( newFakeImage );
			}
			else
				editor.insertElement( newFakeImage );

			return;
//			var attributes,params,videofile,
//			data,player,
//			editor = this.getParentEditor();

			attributes = {
				type : 'application/x-shockwave-flash',
				data: '/player_flv_multi.swf'
			}
			data = attributes;

			this.commitContent( data );

			if(data.type == 'mini')
				player = '/js/player/player_flv_mini.swf';
			else if(data.type == 'maxi')
				player = '/js/player/player_flv_maxi.swf';
			else
				player = '/js/player/player_flv_multi.swf';

			attributes = {
				type : 'application/x-shockwave-flash',
				data: player
			}

			videofile = data.file.vidurl;

			params = '<object type="application/x-shockwave-flash" data="'+player+'" width="'+data.file.width+'" height="'+data.file.height+'">';
			params += '<param name="width" value="'+data.file.width+'" />';
			params += '<param name="height" value="'+data.file.height+'" />';
			params += '<param name="movie" value="'+player+'" />';
			params += '<param name="allowFullScreen" value="true" />';
			params += '<param name="FlashVars" value="flv='+videofile+'&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;showfullscreen=1" />';
			/*
    <param name="showvolume" value="1"/>
    <param name="showtime" value="1"/>*/
			params += '<param name="allowFullScreen" value="true" />';
			params += '</object>';
			//			params = '<div><span style="color:red">test</span></div>';
			var text = new CKEDITOR.dom.element.createFromHtml(params, editor.document);
			if (!this.getSelectedElement() )
			{
				// Create element if current selection is collapsed.
				var selection = editor.getSelection(),
				ranges = selection.getRanges();
				if ( ranges.length == 1 && ranges[0].collapsed )
				{
					ranges[0].insertNode( text );
					ranges[0].selectNodeContents( text );
					selection.selectRanges( ranges );
				}
			}
			else
			{
				//				var selection = editor.getSelection(),
				//				ranges = selection.getRanges();
				//				var range = ranges[0];
				////				if ( ranges.length == 1 && ranges[0].collapsed )
				////				{
				//					ranges[0].insertNode( text );
				//					ranges[0].selectNodeContents( text );
				//					selection.selectRanges( ranges );
				////					this.objectNode.g
				////				}
				////				element.insertBefore(text);
				//				delete this.objectNode;


				objectNode = this.objectNode;
				embedNode = this.embedNode;

				if ( objectNode )
				{
					paramMap = {};
					var paramList = objectNode.getElementsByTag( 'param', 'cke' );
					for ( var i = 0, length = paramList.count() ; i < length ; i++ )
						paramMap[ paramList.getItem( i ).getAttribute( 'name' ) ] = paramList.getItem( i );
				}

				var extraStyles = {}, extraAttributes = {};
				this.commitContent( objectNode, embedNode, paramMap, extraStyles, extraAttributes );

				// Refresh the fake image.
				var newFakeImage = editor.createFakeElement( objectNode || embedNode, 'cke_flash', 'flash', true );
				newFakeImage.setAttributes( extraAttributes );
				newFakeImage.setStyles( extraStyles );
				if ( this.fakeImage )
				{
					newFakeImage.replace( this.fakeImage );
					editor.getSelection().selectElement( newFakeImage );
				}
				else
					editor.insertElement( newFakeImage );

			// IE BUG: Setting the name attribute to an existing link doesn't work.
			// Must re-create the link from weired syntax to workaround.
			//				if ( CKEDITOR.env.ie && attributes.name != element.getAttribute( 'name' ) )
			//				{
			//					selection = editor.getSelection();
			//
			//					element.moveChildren( text );
			//					element.copyAttributes( text, {
			//						name : 1
			//					} );
			//					text.replace( element );
			//					element = text;
			//
			//					selection.selectElement( element );
			//				}
			//
			//				element.setAttributes( attributes );
			//				element.removeAttributes( removeAttributes );
			//				delete this._.selectedElement;
			}
		}
	};
});