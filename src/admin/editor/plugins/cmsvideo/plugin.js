CKEDITOR.plugins.add( 'cmsvideo',
	{
		init : function( editor )
		{
			// Add the link and unlink buttons.
			editor.addCommand( 'videointegration', new CKEDITOR.dialogCommand( 'videointegration' ) );
		
			editor.ui.addButton( 'CmsVideo',
			{
				label : 'Video einf&uuml;gen',
				icon: this.path + 'cms_video.gif',
				command : 'videointegration'
			} );
			CKEDITOR.dialog.add( 'videointegration', this.path + 'dialogs/video.js' );
//			editor.addCss('img.cke_flash{background-image: url('+CKEDITOR.getUrl(this.path+'images/placeholder.png')+');'+'background-position: center center;'+'background-repeat: no-repeat;'+'border: 1px solid #a9a9a9;'+'width: 80px;'+'height: 80px;'+'}');
			if(editor.addMenuItems)editor.addMenuItems({
				flash:{
					label:editor.lang.flash.properties,
					command:'flash',
					group:'flash'
				}
			});
		//		if(f.contextMenu)f.contextMenu.addListener(function(g,h){
		//			if(g&&g.is('img')&&g.getAttribute('_cke_real_element_type')=='flash')return{
		//				flash:CKEDITOR.TRISTATE_OFF
		//				};
		//
		//		});
		},
//		afterInit : function( editor ){
//			var dataProcessor = editor.dataProcessor,
//			dataFilter = dataProcessor && dataProcessor.dataFilter;
//
//			if ( dataFilter )
//			{
//				dataFilter.addRules(
//				{
//					elements :
//					{
//						'cke:object' : function( element )
//						{
//							var attributes = element.attributes,
//							classId = attributes.classid && String( attributes.classid ).toLowerCase();
//
//							if ( !classId )
//							{
//								// Look for the inner <embed>
//								for ( var i = 0 ; i < element.children.length ; i++ )
//								{
//									if ( element.children[ i ].name == 'cke:embed' )
//									{
//										if ( !isFlashEmbed( element.children[ i ] ) )
//											return null;
//
//										return createFakeElement( editor, element );
//									}
//								}
//								return createFakeElement( editor, element );
//							}
//
//							return createFakeElement( editor, element );
//						}
//					}
//				},
//				5);
//			}
//		},

		requires : [ 'fakeobjects' ]
	} );
