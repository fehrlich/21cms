CKEDITOR.plugins.add( 'cmsvars',{
	init : function( editor ){
		editor.addCommand( 'addCmsVars', new CKEDITOR.dialogCommand( 'addCmsVars' ) );
		
		editor.ui.addButton( 'CmsVars',{
			label : 'Variable einf&uuml;gen',
			icon: this.path + 'vars.gif',
			command : 'addCmsVars'
		});
	}
});

CKEDITOR.dialog.add( 'addCmsVars', function( editor ){
		var vars =
			[[ 'aktuelle Nummer', '[NR]' ],
			[ 'interner Titel', '[DB_title_intern]' ],
			[ 'Kommentare', '[COMMENTSYS]' ],
			[ 'Anzahl an Kommentaren', '[COMMENT_COUNT]' ],
			[ 'In den Warenkorb (Link)', '<a id="addToCart" href="[ADDTOCART]">Zum Warenkorb hinzuf&uuml;gen</a>' ],
			[ 'IF-Statement', '[IF(condition)]value[ELSE]else[/IF]' ]];
		var dbId = $('select[name=dbid]').val();
		var txt = $.ajax({
			async: false,
			url: $('#cmsroot').html()+'admin/ajax.php',
			data: {
				act:'dbVars',
				dbid:dbId,
				t:Math.random()
			},
			dataType: 'json'
		}).responseText;
		var dbVars = eval(txt);

		vars = vars.concat(dbVars);

		return {
			title : editor.lang.link.title,
			minWidth : 350,
			minHeight : 230,
			contents : [{
				id : 'info',
				label : editor.lang.link.info,
				title : editor.lang.link.info,
				elements :[{
					id : 'var',
					type : 'select',
					label : 'Variable einfügen',
					'default' : 'id',
					items : vars,
					commit : function( data ){
						if(!data) data = new Object();
						data.name = this.getValue();
					}
				}]
			}],
			onOk : function(){
				var data = {};
				this.commitContent( data );
				editor.insertText(data.name);

			}
		};
	});