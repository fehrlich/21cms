CKEDITOR.plugins.add( 'cmsinstantzoom',{
	init : function( editor ){
		var thisPath = this.path;
		editor.addCommand( 'instantZoom',{
			exec : function( editor )
			{
				var selection, element,img;
				selection = editor.getSelection();
				if(selection.getType() == CKEDITOR.SELECTION_ELEMENT){
					element = selection.getSelectedElement();
					img = $(element.$);
					if(img.hasClass('lightbox')){
						img.removeClass('lightbox');
						$('.cke_button_instantZoom .cke_icon').css('backgroundImage', 'url('+thisPath+'zoom.gif)');					  
					}
					else {
						img.addClass('lightbox');
						if(img.parent('a').length == 0) img.wrap('<a href="'+img.attr('src')+'">');
						$('.cke_button_instantZoom .cke_icon').css('backgroundImage', 'url('+thisPath+'zoomOff.gif)');					  
					}
				}else{
					alert('Bitte Bild auswählen');
				}
			}
		});
		
		editor.ui.addButton( 'instantZoom',{
			label : 'Bild-vergrößer-funktion einfügen',
			icon: this.path + 'zoom.gif',
			command : 'instantZoom'
		});

		editor.on( 'selectionChange', function( evt ){
			var command = editor.getCommand( 'instantZoom' ),
			element = evt.data.path.lastElement && evt.data.path.lastElement.getAscendant( 'img', true ),
			check = (element.$.naturalWidth > element.$.width || element.$.naturalHeight > element.$.height);
			if(element.hasClass('lightbox'))
			  $('.cke_button_instantZoom .cke_icon').css('backgroundImage', 'url('+thisPath+'zoomOff.gif)');		
			else
			  $('.cke_button_instantZoom .cke_icon').css('backgroundImage', 'url('+thisPath+'zoom.gif)');	
			if ( element && check && element.getName() == 'img' && element.getAttribute( 'src' ) )
				command.setState( CKEDITOR.TRISTATE_OFF  );
			else
				command.setState( CKEDITOR.TRISTATE_DISABLED);
		} );
	}
});