CKEDITOR.addStylesSet('custom',[
	{
		name:'Variable',
		element:'var'
	},{
		name:'Zitat',
		element:'q'
	},{
		name:'Image on Left',
		element:'img',
		attributes:{
			style:'margin-right: 5px;margin-bottom: 5px',
			align:'left'
		}
	},{
		name:'Image on Right',
		element:'img',
		attributes:{
			style:'margin-right: 5px;margin-bottom: 5px',
			align:'right'
		}
	}
]);