﻿/**
 * QR CODES plugin for CKEditor 3.x 
 * @Author: Cedric Dugas, http://www.position-absolute.com
 * @Copyright Cakemail
 * @version:	 1.0
 */


var geocoder;
var map;
var address;
var showBubble = true;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var myOptions = {
	zoom: 15,
	center: latlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP,

    mapTypeControl: false,
    navigationControl: true,
    navigationControlOptions: {
        style: google.maps.NavigationControlStyle.ZOOM_PAN,
        position: google.maps.ControlPosition.TOP_LEFT
    },
    scaleControl: false
  }
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

}

function codeAddress() {
//  var address = document.getElementById("address").value;
  if (geocoder) {
	geocoder.geocode( {
	  'address': address
	}, function(results, status) {
	  if (status == google.maps.GeocoderStatus.OK) {
		map.setCenter(results[0].geometry.location);
		var marker = new google.maps.Marker({
		  map: map,
		  position: results[0].geometry.location
		});
		var bubble = new google.maps.InfoWindow({
		  position: results[0].geometry.location,
		  content: address
		});
		google.maps.event.addListener(bubble, 'closeclick', function() {
		  showBubble = false;
		});

		bubble.open(map);
	  } else {
		alert("Die Position konnte nicht bestimmt werden. Fehler: " + status);
	  }
	});
  }
}

CKEDITOR.dialog.add("cmsGMap",function(e){
	
  return{
	title:e.lang.cmsGMap.title,
	resizable : CKEDITOR.DIALOG_RESIZE_BOTH,
	minWidth:180,
	minHeight:160,
	onShow:function(){
	},
	onLoad:function(){
	  this.setupContent();
	  initialize();
	},
	onOk:function(){
	  var zoom = map.getZoom();
	  var bounds = map.getBounds();
	  var l1 = bounds.getCenter().lat();
	  var l2 = bounds.getCenter().lng();
	  var l3 = bounds.getSouthWest().lat();
	  var l4 = bounds.getSouthWest().lng();


	  if(showBubble) bubbleStr = '&amp;iwloc=A';
	  else bubbleStr = '';

	  mWidth = mWidth || 320;

//	  var sInsert = '<iframe width="'+mWidth+'" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.de/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q='+address+'&amp;aq=&amp;sll='+l1+','+l2+'&amp;sspn='+l3+','+l4+'&amp;ie=UTF8&amp;hq=&amp;hnear='+address+'&amp;ll='+l1+','+l2+'&amp;spn='+l3+','+l4+'&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.de/maps?f=q&amp;source=embed&amp;hl=de&amp;geocode=&amp;q='+address+'&amp;aq=&amp;sll='+l1+','+l2+'&amp;sspn='+l3+','+l4+'&amp;ie=UTF8&amp;hq=&amp;hnear='+address+'&amp;ll=51.033564,13.77541&amp;spn=0.018892,0.036478&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Größere Kartenansicht</a></small>';
	  var sInsert = '<iframe width="'+mWidth+'" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.de/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q='+address+'&amp;aq=&amp;sll='+l1+','+l2+'&amp;sspn='+l3+','+l4+'&amp;ie=UTF8&amp;hq=&amp;hnear='+address+'&amp;ll='+l1+','+l2+'&amp;spn='+l3+','+l4+'&amp;z='+zoom+bubbleStr+'&amp;output=embed"></iframe><br /><small><a href="http://maps.google.de/maps?f=q&amp;source=embed&amp;hl=de&amp;geocode=&amp;q='+address+'&amp;aq=&amp;sll='+l1+','+l2+'&amp;sspn='+l3+','+l4+'&amp;ie=UTF8&amp;hq=&amp;hnear='+address+'&amp;ll=51.033564,13.77541&amp;spn=0.018892,0.036478&amp;z='+zoom+bubbleStr+'" style="color:#0000FF;text-align:left">Größere Kartenansicht</a></small>';

	  if ( sInsert.length > 0 )
		e.insertHtml(sInsert);
	},
	contents:[
	{
	  id:"info",
	  name:'info',
	  label:e.lang.cmsGMap.commonTab,
	  elements:[
	  {
		type:'html',
		html:'<div style="padding-bottom:5px;">'+e.lang.cmsGMap.HelpInfo+'</div>'
	  },
	  {
		type:'vbox',
		padding:0,
		children:[
		{
		  type:'hbox',
		  children:[{
			type:'text',
			id:'address',
			label: 'Addresse',
			text: 'Dresden, An der Pikardie 10'
		  },{
			type:'button',
			id:'search',
			label: 'Suchen',
			onClick:function(){
			  address = this.getDialog().getContentElement( 'info', 'address' ).getValue();
			  codeAddress();
			}
		  }]
		},
		{
		  type:'html',
		  html:'<div id="map_canvas" style="width: 480px; height: 320px;"></div>'
		}
		]
	  }]
	}
	]
  };
});


	