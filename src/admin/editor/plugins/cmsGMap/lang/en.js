﻿CKEDITOR.plugins.setLang("cmsGMap","en",{
  cmsGMap:
    {
	 title:'Google Map location',
	 commonTab:'Info',
	 HelpInfo:'Insert an address and receive an image of your location',
	 Width:'Width:',
	 Height:'Height:',
	 Near:'Closest',
	 Far:'Furthest',
	 Zoom:'Zoom:'
	}
});