CKEDITOR.plugins.add( 'cmsinstantcrop',{
	init : function( editor ){
		editor.addCommand( 'instantCrop',{
			exec : function( editor )
			{
				var selection, element,img,imgeig;
				selection = editor.getSelection();
				if(selection.getType() == CKEDITOR.SELECTION_ELEMENT){
					element = selection.getSelectedElement();
					img = $(element.$);
					imgeig = {
						"aktuelles Bild":[
						img.width(),
						img.height(),
						false
						]
					}
					
					cropImage(img,imgeig,true,"msg",$(".ajaxwindow2"));
				}else{
					alert('Bitte Bild auswählen');
				}
			}
		});
		
		editor.ui.addButton( 'instantCrop',{
			label : 'Bild anpassen',
			icon: this.path + 'crop.gif',
			command : 'instantCrop'
		});

		editor.on( 'selectionChange', function( evt ){
			var command = editor.getCommand( 'instantCrop' ),
			element = evt.data.path.lastElement && evt.data.path.lastElement.getAscendant( 'img', true ),
			check = (element.$.naturalWidth > element.$.width || element.$.naturalHeight > element.$.height);
			if ( element && check && element.getName() == 'img' && element.getAttribute( 'src' ) )
				command.setState( CKEDITOR.TRISTATE_OFF  );
			else
				command.setState( CKEDITOR.TRISTATE_DISABLED);
		} );
	}
});