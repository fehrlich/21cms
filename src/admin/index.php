<?php
/*
 * Konfiguration
 */

//für spätere headermanipulation!
ob_start();


//define some vars
    include("../funktionen/constant.php");
    define("intern", true);


$GLOBALS['interface'] = 'admin';
if (file_exists('../install.sql')) {
	$_GET['mm'] = 'install';
	$install = true;
} else {
	$install = false;
	include('../serverconfig.php');
	$GLOBALS['cms_root'] = $GLOBALS['cms_roothtml'] . 'admin/';
}

include("../funktionen/loadclasses.php");

UserConfig::getObj();

cms\session::getObj();
$login = new login();

include('../funktionen/include.php');
$tplname = 'admin/tpl/index.html';
$tpl = new tpl('admin/tpl/index.html');
if (!isset($_GET['mm']))
	$_GET['mm'] = '';
$tpl->addJS(array(
    'jquery-2.1.3.min',
    'jQueryUI/jquery-ui.min',
    'jquery.tablesorter.min'
));
$toolbox = true;

$menu_array = array('DatabaseConnector', 'SendedForms','Comments','BugReport','StatsSearch','PdfTemplate');

$adminMenuId = getAdminMenuId($_GET['mm']);
$rights = \cms\session::getObj()->getAdminMenuRights();
if(!rights::getObj()->isSuperAdmin() && ($_GET['mm'] != '' && $_GET['mm'] != 'Webseite') && (!isSet($rights['allow' . $adminMenuId]) || $rights['allow' . $adminMenuId] != '1')) {
	$c = 'Sie haben leider nicht die benötigten Berechtigungen';
	$tpl->setVar('ADMINCONTENT', $c);
}
elseif (in_array($_GET['mm'], $menu_array)) {
	$class = $_GET['mm'];

	include_once '../klassen/'.$class.'.php';
	$c = new $class();
	$tpl->setVar('ADMINCONTENT', $c->__toString());
}else
	switch ($_GET['mm']) {
		case 'servinfo':
			$tpl->setVar('ADMINCONTENT', phpinfo());
			exit();
			break;
		case 'servstatus':
			$c = new config();
			$tpl->setVar('ADMINCONTENT', $c->htmlStatus());
			break;
                case 'addresses' :
                        $c = new addresses();
                        $tpl->setVar('ADMINCONTENT', $c->__toString());
                        break;
                case 'NewsletterQueue' :
                        $c = new NewsletterQueue();
                        $tpl->setVar('ADMINCONTENT', $c->__toString());
                        break;
                case 'sendNewsletter' :
                        $c = new sendNewsletter();
                        $tpl->setVar('ADMINCONTENT', $c->__toString());
                        break;
                case 'addressGroups' :
                        $c = new addressGroups();
                        $tpl->setVar('ADMINCONTENT', $c->__toString());
                        break;
                case 'newsletterTemplates' :
                        $c = new newsletterTemplates();
                        $tpl->setVar('ADMINCONTENT', $c->__toString());
                        break;

		case 'server':
			$c = new config();
			$tpl->setVar('ADMINCONTENT', $c->htmlStatus() . $c);
			break;
		case 'servereinst':
			$c = new config();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'Dateimanager':
			include('../klassen/dateimanager.php');
			$tpl->setSrcPath('admin/tpl/dateimanager.html');
			$tpl->addJS(array(
//				'admin/upload/' => 'swfobject',
				'admin/' => 'upload/jquery.uploadify.v2.1.4.min',
				'jquery.Jcrop',
//				'jquery-migrate-1.2.1.min',
				'jquery.fineuploader-3.0.min',
//				'jquery.tooltip.min',
                                'knockout-2.0.0',
                                'dm',
                                'dmViewModel',
			));
			$tpl->addCSS(array(
                            'admin/upload/' => 'uploadify',
                            'jquery.Jcrop',
//                            'jquery.tooltip',
                            'admin/' => 'upload/uploadify'
                        ));
			$c = new dateimanager();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			if ((isset($_GET['retid']) && $_GET['retid'] != '')
					|| isSet($_GET['CKEditorFuncNum']) && $_GET['CKEditorFuncNum'] != '')
				$toolbox = false;
			break;
//	case 'DatabaseConnector':
//		$c = new DatabaseConnector();
//		$tpl->setVar('ADMINCONTENT', $c->__toString());
//		break;
		case 'User':
			$c = new \cms\User();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'Gruppen':
			$c = new Gruppen();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'GruppenDbRechte':
			$c = new GruppenDbRechte();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'PageRights':
			$c = new PageRights();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'TagSystem':
			$c = new TagSystem();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'DataTemplate':
			$c = new DataTemplate();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'RssFeeds':
			include('../klassen/RssFeeds.php');
			$c = new RssFeeds();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'FrontEndUser':
			include('../klassen/FrontEndUser.php');
			$c = new FrontEndUser();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'FrontEndGroups':
			include('../klassen/FrontEndGroups.php');
			$c = new FrontEndGroups();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'Languages':
			$c = new Languages();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'MessageSystem':
			$c = MessageSystem::getObj();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'install':
			if ($install) {
				$c = new config(true);
				echo $c;
			}
			exit();
			break;
			break;
		case 'fixes':
			include('../klassen/fixes.php');
			$c = new fixes();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		case 'logout':
			ob_clean();
			\cms\session::getObj()->destroySession();
			header("Location: " . $GLOBALS['cms_roothtml'] . "admin/");
			die();
		case 'upgrade':
			$c = new upgrade();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
                case 'stats':
			$c = new stats();
			$tpl->setVar('ADMINCONTENT', $c->__toString());
			break;
		default:
			#if (!rights::getObj()->isAdmin()) { header("Location: ".$GLOBALS['cms_roothtml']."admin/index.php?mm=DatabaseConnector"); die(); }
			if (isset($_GET['tag']) && $_GET['tag'] != '') {
				addWhere('title_intern', '=', $_GET['tag'], 's');
				select('tags', 'showin,title');
				$row = getRow();
				$str = $row['showin'];
				$_GET['str'] = $str;
				$GLOBALS['akt_tag'] = $row;
				addWhere('title_intern', '=', $_GET['tag'], 's');
				updateArray('tags', array('prio' => 'prio+1'));
			} elseif (isset($_GET['str']) && $_GET['str'] != '')
				$str = $_GET['str'];
			else
				$str = '';
			$GLOBALS['seitenid'] = getMenuid($str);
			preparation();
			if (isSet($GLOBALS['akt_menuepunkt']['useTpl']) && $GLOBALS['akt_menuepunkt']['useTpl'] != '')
				$tplFile = $GLOBALS['akt_menuepunkt']['useTpl'];
			else
				$tplFile = 'index';
			$tpl->addToolBar = true;
			$tpl->setSrcPath('tpl/' . $tplFile . '.html');
	}

if ($toolbox)
	$tpl->setVar('TOOLBAR', buildToolbar());
else
	$tpl->setVar('TOOLBAR', '<img src="'.$GLOBALS['cms_rootdir'].'admin/images/new/logo.gif" id="cms_logo" alt="21 CMS" />');
$tpl->wenndann['ADMIN'] = true;
if (!isSet($str) || $str == "" || $GLOBALS['akt_menuepunkt']['startpage'] == '1')
	$tpl->wenndann['START'] = true;
else
	$tpl->wenndann['START'] = false;
if (frontendSession::getObj()->getUserId() > 0){
	$tpl->wenndann['LOGGEDIN'] = true;
}else{
	$tpl->wenndann['LOGGEDIN'] = false;
}
$tpl->setHtmlVar('messages', MessageSystem::getObj()->getMessages());
$tpl->addJS(array( 'querystr', 'global', 'jquery.tablednd_0_5', 'utf', 'phpserializer', 'jquery.tabby', 'jquery.menu', 'selectToUISlider.jQuery',
//	'jquery.tooltip.min',
	'jquery.stickytableheaders',
	'jQuery.tableColDelete',
	'jQuery.combobox',
	'admin/editor/' => 'ckeditor',
	'admin/editor/adapters/' => 'jquery',
	'admin/jsTree/' => 'jquery.jstree',
	'jQuery.multiselect',
	'http://maps.google.com/maps/api/js',
	'js/CodeMirror-master/lib/' => 'codemirror',
	'js/CodeMirror-master/mode/xml/' => 'xml'
));
$tpl->addCSS(array(
    'interface',
    '../js/jQueryUI/jquery-ui.structure.min',
    '../js/jQueryUI/jquery-ui.theme.min',
//    '21cms/jquery-ui-1.10.3.custom.min',
    'ui.slider.extras',
    'jquery.Jcrop',
    'jquery.multiselect',
    'js/CodeMirror-master/lib/' => 'codemirror',
    'js/CodeMirror-master/theme/' => 'monokai'
));

$html = $tpl->preParse(true);
$debug = ob_get_contents ();
if($debug !== false && $debug != ''){
	$debug .= 'PostData:'.print_r($_GET,true);
	$debug .= 'GetData:'.print_r($_GET,true);
	$html = str_replace('[DEBUG]',$debug,$html);
	ob_clean();
}else $html = str_replace('[DEBUG]','',$html);
echo $html;
?>
