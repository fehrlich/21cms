var dateienarchiv = new Array();
var returnFile = '';
var selectType = 'file';
var ignoreUploadify = false;
var dmListView = false;
var viewModel;

function deleteFolder(folderEl){
    var datei = folderEl || $('.selected');
    var dialog = $("#dialog").dialog('option', 'title', 'Verzeichniseigenschaften &auml;ndern');
    $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), function(d){
        dialog.html(d);
        dialog.dialog('option', 'buttons', {
            'Speichern': function() {
                var postdata = dialog.find('form').getFormValues();
                $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), postdata,function(d2){
                    dialog.dialog('close');
                    if(d2 != 'Ok')
                        debug(d2);

                });

            }
        });
    });
    $("#dialog").dialog('open');    
}


function hasFlash(){
    if($('#fileInput').length == 1 && !$('#fileInput').hasClass('uploadify'))
        return false;
    if(typeof(navigator.plugins["Shockwave Flash"]) == "object") return true;
    else if(typeof(ActiveXObject) == "function") return true
	
    return false;
}

function str_replace(search, replace, subject) {
    for(i=0;i<search.length;i++)
        subject = subject.split(search[i]).join(replace[i]);
    return subject;
}
function updateStaticUpload(folder){
    $('#fileInputDiv').html('<form action="ajax.php?klasse=dateimanager&act=uploadPost&sessionid'+sessionid+'&folder='+folder+'&noflash=1" method="post" enctype="multipart/form-data"><input id="fileInput" name="Filedata" type="file" /><input type="submit" name="upload" id="NonFlashUpload" value="Hochladen" /></form>');
}

function message(html){
    $('#messages').show();
    $('#messages').html(html);
    $('#messages').dialog({
        bgiframe: true,
        modal: false
    });
    $('#messages').dialog('option', 'buttons', {
        Ok: function() {
            $(this).dialog('close');
        }
    });
    $('#messages').dialog('open');
    $('#messages').dialog('width', 500);
    $('#messages').dialog('height', 250);
}


function delete_dateilink(dateiel){
    var lastIndex = dateiel.attr('id').lastIndexOf("/");
    if(lastIndex != 0) lastIndex++;
    var name = dateiel.attr('id').substring(lastIndex);
    var dirid = $(".selected").attr('id');
    if(typeof dateienarchiv[dirid] != "undefined"){
        var dateien = dateienarchiv[dirid];
        for (var i in dateien){
            if(dateien[i]['name'] == name){
                delete dateienarchiv[dirid][i];
            }
        }
    }
}

function checkImage(img,mwidth,mheight){
    var width = img[0].naturalWidth;
    var height = img[0].naturalHeight
    var strictheight = false;
    if((mwidth != 0 && width > mwidth) || (mheight != 0 && height > mheight)){
        if(mheight == '') mheight = 0;
        else strictheight = true;
        if(mwidth == '') mwidth = 0;
        cropImage(img,{
            'Imagesize':  new Array(mwidth,mheight,strictheight)
        }, true, '<span style="color:red">Das Bild entspricht nicht der geforderten gr&ouml;&szlig;e. Bitte markieren sie den gew&uuml;nschten Bereich.');
		
        return false;
    }

    return true;
}

function dateibuild(id){
    var dateien = dateienarchiv[id];
    var html = '';
    if(id == 'root') id = '';
    for (var i in dateien){
        var showThumbOnFileName = '';
        var addStyle = '';
        var realImg = 'src';
        if(dateien[i]['art'] == 'img'){
            showThumbOnFileName = ' showImage';
        }
        if(id == '')html += '<div class="datei" id="'+dateien[i]['name']+'">';
        else html += '<div class="datei" id="'+id+'/'+dateien[i]['name']+'">';
        if(dmListView){
            addStyle = ' style="display:none"';
            realImg = 'src="" fakesrc';
        }
        html += '<div class="dateiimg"'+addStyle+'>';
        if(dateien[i]['art'] == 'img'){
            html += '<img '+realImg+'="../dateien/'+(str_replace(new Array('%', '#'), new Array('%25', '%23'), dateien[i]['pfad']))+'" alt="Bild" class="dateithumb" />';
        }
        html += '</div>';

        html += '<span class="dateiname'+showThumbOnFileName+'">'+dateien[i]['name']+'</span>';
        html += '<div class="fileactions">';
//        console.log("PFAD", dateien[i]['pfad']);
//        console.log(dateien, (str_replace(new Array('%', '#'), new Array('%25', '%23'), dateien[i]['pfad'])));
        html += '<a target="_blank" href="../dateien/'+(str_replace(new Array('%', '#'), new Array('%25', '%23'), dateien[i]['pfad']))+'" class="act"><img src="images/icons/download.png" alt="[d]" title="Datei downloaden" /><span class="desc">Download</span></a>';
        html += '<span class="act actren"><img src="images/icons/rename.png" alt="[r]" title="Datei umbenennen" /><span class="desc">Umbenennen</span></span>';
        if(dateien[i]['art'] == 'img')
            html += '<span class="act actedit" title="Bild bearbeiten"><img src="images/icons/imgedit.png" alt="[e]" /><span class="desc">Foto bearbeiten</span></span>';
        html += '<span class="act actoptedit" title="Datei bearbeiten"><img src="images/icons/fileedit.png" alt="[e]" /><span class="desc">Datei bearbeiten</span></span>';
        html += '<span class="act actdel" title="Datei l&ouml;schen"><img src="images/icons/filedelete.png" alt="[x]" /><span class="desc">L&ouml;schen</span></span>';
        html += '</div>';
        html += '</div>';
    }
    $('#dateien,#fileList').html(html);
    $("#dateien").delegate(".actren", "click",function(event){
        var datei = $(this).parent().parent();
        var indexend = datei.attr('id').lastIndexOf('.')+1;
        var name = datei.attr('id').substring(datei.attr('id').lastIndexOf('/')+1,indexend-1);
        var endung = datei.attr('id').substring(indexend);
        $("#dialog").dialog('option', 'title', 'Datei umbenennen');
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
		
        $("#dialog").html('Name: <input type="text" id="newname" value="'+name+'" />.'+endung+fileOccurrence);
        activateOccurenceSeachButton(datei.attr('id'));
        $("#dialog").dialog('option', 'buttons', {
            'Umbenennen': function() {
                $.post('ajax.php?klasse=dateimanager&act=rename', {
                    'src': datei.attr('id'),
                    'newname': $("#dialog #newname").val(),
                    'endung':endung
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        var dirid = $(".selected").attr('id');
                        var dateien = dateienarchiv[dirid];
                        var fullname = name+'.'+endung;
                        for (var i in dateien){
                            if(dateien[i]['name'] == fullname){
                                dateien[i]['name'] = $("#dialog #newname").val()+'.'+endung;
                            }
                        }
                        dateibuild(dirid);
                    }
					
                });
                $(this).dialog('close');
            },
            'Sicheres Umbenennen (Beta)': function() {
                $.post('ajax.php?klasse=dateimanager&act=rename&opt=save', {
                    'src': datei.attr('id'),
                    'newname': $("#dialog #newname").val(),
                    'endung':endung
                }, function(d){
                    message(d);
                    var dirid = $(".selected").attr('id');
                    var dateien = dateienarchiv[dirid];
                    var fullname = name+'.'+endung;
                    for (var i in dateien){
                        if(dateien[i]['name'] == fullname){
                            dateien[i]['name'] = $("#dialog #newname").val()+'.'+endung;
                        }
                    }
                    dateibuild(dirid);
					
                });
                $(this).dialog('close');
            },
            'Abbrechen': function() {
                $(this).dialog('close');
            }
        });
        $("#dialog").dialog('open');
    });
    $("#dateien").delegate(".actedit", "click",function(event){
        var img = $(this).parent().parent().find('.dateithumb'),
        imgeig = new Array();
                  
        imgeig = eval("("+$.ajax({
            async: false,
            url: $('#cmsroot').html()+'admin/ajax.php',
            data: {
                act:'ajaxCropEig'
            },
            dataType: 'json'
        }).responseText+")");
        cropImage(img, imgeig);
    });
    $("#dateien").delegate(".actoptedit", "click",function(event){
        var datei = $(this).parent().parent();
        var dialog = $("#dialog").dialog('option', 'title', 'Dateieigenschaften &auml;ndern');
        $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), function(d){
            dialog.html(d);
            dialog.dialog('option', 'buttons', {
                'Speichern': function() {
                    var postdata = dialog.find('form').getFormValues();
                    $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), postdata,function(d2){
                        dialog.dialog('close');
                        if(d2 != 'Ok')
                            debug(d2);

                    });

                }
            });
        });
        $("#dialog").dialog('open');
	  
    });
    $("#dateien").delegate(".actdel", "click",function(event){
        var datei = $(this).parent().parent();
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
					
        $("#dialog").dialog('option', 'title', 'Datei l&ouml;schen');
        $("#dialog").html('Sind Sie sicher das sie die Datei \''+datei.attr('id')+'\' l&ouml;schen m&ouml;chten?'+fileOccurrence);
        activateOccurenceSeachButton(datei.attr('id'));
                
        $("#dialog").dialog('option', 'buttons', {
            'Ja': function() {
                $.post('ajax.php?klasse=dateimanager&act=delete', {
                    src: datei.attr('id')
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        datei.hide();
                        delete_dateilink(datei);
                    }
                });
                $(this).dialog('close');
            },
            'Nein': function() {
                $(this).dialog('close');

            },
            'Abbrechen': function() {
                $(this).dialog('close');
            }
        });
        $("#dialog").dialog('open');
    });

    $("#dateien .datei,#dateien .dateiBig,#fileList .datei").draggable({
        delay: 10,
        revert: true,
        opacity: 0.5,
        helper: function(event) {
            return $('<div class="ui-widget-header">'+$(this).attr('id').substring($(this).attr('id').lastIndexOf("/"))+'</div>');
        },
        cursorAt: {
            top: -12,
            left: -25
        }
    });
    //	$("#dateien .datei .dateithumb").tooltip({
    //		delay: 0,
    //		showURL: false,
    //		track: true,
    //		positionRight: true,
    //		bodyHandler: function() {
    //			return '<h2>'+$(this).parent().parent().find('.dateiname').html()+'</h2><img src="'+$(this).attr('src')+'" alt="" />';
    //		}
    //	});
    //	$("#dateien .datei .showImage").tooltip({
    //		delay: 0,
    //		showURL: false,
    //		track: true,
    //		positionRight: true,
    //		bodyHandler: function() {
    //			return '<h2>'+$(this).html()+'</h2><img src="'+$('#cmsroot').html()+'dateien/'+$(this).parent().attr('id')+'" alt="" />';
    //		}
    //	});	
    if(!dmListView){
        var val = $("#imageSizeSlider").slider('option', 'value');
        var height = val*15/11;
        $(".datei").css('width', val+'px');
        $(".datei").css('height', height+'px');
        $(".dateiimg").css('height', (val-10)+'px');
    }
    $(".datei").dblclick(function(){
        return madeSelection($(this), true);
    });
        
    $(".datei").click(function(){
        $(this).parent().children('.selected').removeClass('selected');
        $(this).addClass('selected');
        var html = '<div id="previewImage"><div class="dateiBig"  id="'+$(this).attr('id')+'">'+$(this).html()+'</div></div>';
            
        $('#previewDiv #dateien').html(html);
    });
}

function madeSelection(el,isFile){

    var check = true,mw,mh;
    if(selectType == 'folder' && isFile) return false;
    if(selectType == 'file' && !isFile) return false;
    if(selectType == 'none') return false;

    if(isFile){
        el.addClass('selectedFile');
        mw = $.query.get('maxwidth');
        mh = $.query.get('maxheight');
        if(mh == '' || mh === true) mh = 0;
        if(mw == '' || mh === true) mw = 0;
        if(mw != 0 || mh != 0){
            check = checkImage(el.find('img:first'),mw,mh);
        }
    }
    createCookie('cmsLastDMSelection',el.attr('id'),30);
    if(check){
        if(isFile) {
            returnToForm(el.attr('id'),getURLParameter("absoluteLinks"));
        }
        else returnToForm(el.attr('id')+'/');
    }
    return true;
}

function get_foldercontent(parent, isroot,reload){
    var filter = $.query.get('filter'),
    folders,
    filteradd = '';
    if(filter != '') filteradd = '&filter='+filter;
    parent.addClass('load');
    isroot = isroot || false;

    var d=new Date();


    $.post('ajax.php?klasse=dateimanager&act=get_foldercontents' + '&' + d.getTime() +filteradd, {
        pfad: parent.attr('id')
    }, function(d){
        dateienarchiv[parent.attr('id')] = new Array();
        parent_str = '<ul>';
        $(d).find('el').each(function(){
            if($(this).attr('isdir') == 1){
                var hoveraction = '<div class="hoverAction">\n\
                                    <img id="folderEdit" src="'+$('#cmsroot').html()+'admin/images/icons/appedit.png" alt="[e]" />\n\
                                    <img id="folderDelete" src="'+$('#cmsroot').html()+'admin/images/icons/appdel.png" alt="[X]" />\n\
                                </div>';
                parent_str += '<li id="'+$(this).attr('pfad')+'">'+hoveraction+'<a href="#">'+($(this).attr('name'))+'</a></li>';
            //$.tree.focused().create({ data : $(this).attr('name') }, '#'.$(this).attr('pfad')); //, 2
            }else{
                var datei = new Array();
                datei['name'] = ($(this).attr('name'));
                datei['art'] = ($(this).attr('art'));
                datei['pfad'] = ($(this).attr('pfad'));
                dateienarchiv[parent.attr('id')].push(datei);
            }
        });
        dateibuild(parent.attr('id'));
        parent.find('ul').remove();
        parent_str += '<ul>';
        parent.html(parent.html()+parent_str);
        parent.unbind('click');
        folders = parent.find('li');
        folders.click(function(event){
            var parent = $(this);
            $(".selected").removeClass('selected');
            $(this).addClass('selected');
            if(!ignoreUploadify){
                /*FIX: ERROR UPLOADIFY 3*/
                if(hasFlash())
                    $('#fileInput').uploadifySettings('folder', $(this).attr('id'));
                else
                    updateStaticUpload($(this).attr('id'));
            }
            get_foldercontent(parent);
            event.stopPropagation();
        });
        folders.dblclick(function(e){
            e.stopPropagation();
            return madeSelection($(this), false);
        });


        //TODO: Mit Root #+id li,#root
        parent.find('li').add('#root,#delfolderdiv').droppable({ //parent.find('li')
            hoverClass: 'drophover',
            greedy: true,
            drop: function(event, ui) {
                event.stopPropagation();
                var len = ui.draggable.attr('id').lastIndexOf("/");
                if(len == -1) len = 0;
                if(($(this).attr('id') != ui.draggable.attr('id').substring(0,len)) && !($(this).attr('id') == 'root' && ui.draggable.attr('id').substring(0,len) == '')){
                    var dropel = $(this);
                    var newel = ui.draggable.clone();
                    if($(this).attr('id') == 'delfolderdiv'){
                        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
                        if(ui.draggable.hasClass('datei')){
                            $("#dialog").dialog('option', 'title', 'Datei L&ouml;schen');
                            $("#dialog").html('M&ouml;chten Sie die Datei "'+ui.draggable.attr('id')+'" wirklich löschen?'+fileOccurrence);
                        }else{
                            $("#dialog").dialog('option', 'title', 'Verzeichnis L&ouml;schen');
                            $("#dialog").html('M&ouml;chten Sie das Verzeichnis "'+ui.draggable.attr('id')+'" wirklich löschen?'+fileOccurrence);
                        }
                        activateOccurenceSeachButton(ui.draggable.attr('id'));
                        $("#dialog").dialog('option', 'buttons', {
                            'Löschen': function() {
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=remDir',{
                                    src: ui.draggable.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            delete dateienarchiv[$(".selected").attr('id')];
                                            ui.draggable.hide();
                                            ui.draggable.removeClass('selected');
                                            $("#root").addClass('selected');
											
											
                                            if(hasFlash())
                                                $('#fileInput').uploadifySettings('folder', '../');
                                            else
                                                updateStaticUpload('../');
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    delete dateien[i];
                                                }
                                            }
                                            ui.draggable.hide();
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Abbrechen': function() {
                                $(this).dialog('close');
                            }
                        });

                    }else{  
                        var fileOccurrence = '<div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
                        if(ui.draggable.hasClass('datei')){
                            $("#dialog").dialog('option', 'title', 'Datei Verschieben/Kopieren');
                            $("#dialog").html('M&ouml;chten Sie die Datei "'+ui.draggable.attr('id')+'" kopieren oder verschieben.<br /><span style="color: #f00">Achtung: Wenn Sie die Dateien verschieben k&ouml;nnen verlinkungen verloren gehen</span><br />'+fileOccurrence);
                        }else{
                            $("#dialog").dialog('option', 'title', 'Verzeichnis Verschieben/Kopieren');
                            $("#dialog").html('M&ouml;chten Sie das Verzeichnis "'+ui.draggable.attr('id')+'" kopieren oder verschieben.<br /><span style="color: #f00">Achtung: Wenn Sie das Verzeichnis verschieben k&ouml;nnen verlinkungen verloren gehen</span><br />'+fileOccurrence);
                        }      
                        activateOccurenceSeachButton(ui.draggable.attr('id'));
                        $("#dialog").dialog('option', 'buttons', {
                            'Kopieren': function() {
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=dircopy',{
                                    src: ui.draggable.attr('id'),
                                    to: dropel.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable.clone();
                                            //TODO: Check ob neuberechnung noetig ist
                                            var len = newel.attr('id').lastIndexOf("/");
                                            if(len == -1) len = 0;
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.wrap("<ul></ul>");
                                            newel.parent().appendTo(dropel);
                                            newel.wrap("<ul></ul>");
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                var dateien = dateienarchiv[dirid];
                                                var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                                for (var i in dateien){
                                                    if(dateien[i]['name'] == name){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Verschieben': function() {
                                var oldid = ui.draggable.attr('id');
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=dirmove',{
                                    src: oldid,
                                    to:dropel.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable;
                                            var len = newel.attr('id').lastIndexOf("/")+1;
                                            newel.wrap("<ul></ul>");
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                delete dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.parent().appendTo(dropel);
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var delid = -1;
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                    delete dateien[i];
                                                    ui.draggable.hide();
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'sicher Verschieben (Beta)': function() {
                                var oldid = ui.draggable.attr('id');
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=savedirmove',{
                                    src: oldid,
                                    to:dropel.attr('id')
                                }, function(d){
                                    if(d.substr(0,6) == 'Error:'){
                                        debug(d)
                                    }
                                    else{
                                        message(d);
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable;
                                            var len = newel.attr('id').lastIndexOf("/")+1;
                                            newel.wrap("<ul></ul>");
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                delete dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.parent().appendTo(dropel);
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var delid = -1;
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                    delete dateien[i];
                                                    ui.draggable.hide();
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Abbrechen': function() {
                                $(this).dialog('close');
                            }
                        });
                    }
                    $("#dialog").dialog('open');
                }
            }
        });
        parent.find('li').draggable({
            revert: true,
            delay: 250
        });
        
        parent.find('.folderDelete').click(function(){
            deleteFolder
        });

        //parent.find('li').prepend('[+]');
        parent.click(function(e){
            if(!isroot){
                if(hasFlash()){
                    $('#fileInput').uploadifySettings('folder', $(this).attr('id'));
                }else
                    updateStaticUpload($(this).attr('id'));
                $(this).find('ul').toggle();
            /*if($(this).find('ul').css('display') == 'none'){
					$(this).prepend('[+]');
				}else{
					$(this).prepend('[-]');
				}*/
            }else{
                if(hasFlash())
                    $('#fileInput').uploadifySettings('folder', '');
                else
                    updateStaticUpload($(this).attr('id'));
            }
            $(".selected").removeClass('selected');
            $(this).addClass('selected');
            dateibuild($(this).attr('id'));
            e.stopPropagation();
        });
		
        parent.removeClass('load');
    });
}

$(document).ready(function(){
    var txt = "",note = "",absolutepath = '../';
//    if($('#fileInput').hasClass('uploadify') && !hasFlash()){
////        $.pnotify({
////            pnotify_title: "Kein Flash",
////            pnotify_text: "Sie haben kein Flash in ihrem Browser installiert. F&uuml;r eine bessere Benutzung des Dateimanager empfehlen wir Flash zu installieren: <a href=\"http://get.adobe.com/de/flashplayer/\">zum Download</a>",
////            pnotify_type: 'error', 						
////            pnotify_hide: 90
////        });
//    }
    $("#imageSizeSlider").slider({
        min: 60,
        max: 220,
        value: 110,
        step: 10,
        slide: function(event, ui) {
            var height = ui.value*15/11;
            $(".datei").css('width', ui.value+'px');
            $(".datei").css('height', height+'px');
            $(".dateiimg").css('height', (ui.value-10)+'px');
        }
    });
    if($('.listview').length > 0)
        dmListView = true;
    else if(readCookie('cmsDMListView') == '1'){
        dmListView = true;
        $('#dateimanager').addClass('listview');
    }
    var toggleToList = 'Zur Listenansicht wechseln';
    var toggleToView = 'Zur Vorschauansicht wechseln';
    var toggleTxt = '';
    if(dmListView){
        toggleTxt = toggleToView;
    }else toggleTxt = toggleToList;

    $('#toggleListView').val(toggleTxt).click(function(){
        if(dmListView){
            $(this).val(toggleToList);
            $('#dateimanager').removeClass('listview');
            $('.dateiimg').show();
            $('.dateithumb').each(function(){
                if($(this).attr('fakesrc') == '') return;
                $(this).attr('src',$(this).attr('fakesrc'));
                $(this).attr('fakesrc', '');
            });
            createCookie('cmsDMListView', '0', 712);
        }else{
            $(this).val(toggleToView);
            $('#dateimanager').addClass('listview');
            $('.dateiimg').hide();
            createCookie('cmsDMListView', '1', 712);
            $('.datei').attr('style','');
        }
        dmListView = !dmListView;
    });

//    if($.query.get('art') != '')selectType = $.query.get('art');
//    if(selectType == 'folder') txt = "Bitte w&auml;hlen Sie ein Verzeichnis auf der linken Seite mit einem Doppelklick aus";
//    else if(selectType == 'file') txt = "Bitte w&auml;hlen Sie eine Datei auf der rechten Seite mit einem Doppelklick aus";
//    else if(selectType == 'hybrid') txt = "Bitte w&auml;hlen Sie eine Datei oder ein Verzeichnis mit einem Doppelklick aus";
//    txt = "TEST";
//    if(txt != ""){
//        $.jGrowl(txt, { sticky: true });
//        $.sticky(txt, {
//            classList: 'info'
//        });
//        $.pnotify({
//            pnotify_title: "Auswahl",
//            pnotify_text: txt,
////            pnotify_type: 'notice',
////            pnotify_hide: 0
//        });
//        new PNotify({
//            title: 'Auswahl',
//            text: "txt"
//	});
//    }
//    note = $.query.get('note');
//    if(note != ""){
//        
//        $.sticky(note, {
//            classList: 'info'
//        });
//////        $.pnotify({
//////            pnotify_title: "Hinweis",
//////            pnotify_text: note,
//////            pnotify_type: 'notice',
//////            pnotify_hide: 0
//////        });
////        new PNotify({
////		title: 'Hinweis',
////		text: note
////	});
//    }
    var cookiePath = readCookie('cmsLastDMSelection');
    var getRoot = true;
    var openAbsolutePath = "";

    if(window.opener){
        var retInp;
        if($.query.get('retid') != ''){
            retInp = window.opener.$('#'+$.query.get('retid')).val();
        }else{
            if(window.opener.CKEDITOR.dialog.getCurrent().getContentElement("info", 'txtUrl'))
                retInp = window.opener.CKEDITOR.dialog.getCurrent().getContentElement("info", 'txtUrl').getValue();
            else if(window.opener.CKEDITOR.dialog.getCurrent().getContentElement("info", 'url'))
                retInp = window.opener.CKEDITOR.dialog.getCurrent().getContentElement("info", 'url').getValue();
        }
        console.log(retInp);
        if(retInp && retInp.length && retInp.length > 8 && retInp.substr(0, 9) == '/dateien/'){
            if(getAbselutePath(retInp.substr(9))){
                absolutepath = retInp.substr(9);
                getRoot = false;
                openAbsolutePath = retInp.substr(9);
            }
        }

    }

    if (cookiePath && getRoot) {
        openAbsolutePath = cookiePath;
        if (getAbselutePath(cookiePath)) {
            getRoot = false
            absolutepath = cookiePath;
        } else {
            getRoot = true;
            openAbsolutePath = "";
        }

    }

    if(getRoot){
        get_foldercontent($('#root'),true);
    }
    $('body').append('<div id="messages" class="ajaxwindow2"></div>');
    $('<img>').attr('src', 'images/ajaxload.gif');
    $('#newfolder').click(function(){
        var foldername = prompt('Bitte Verzeichnisnamen eingeben');
        if(foldername != null && foldername != ""){
            $.post('ajax.php?klasse=dateimanager&act=mkdir', {
                'pfad': $(".selected").attr('id'),
                'name':foldername
            }, function(d){
                if(d != 'Ok') debug(d);
                if( $(".selected").attr('id') == 'root')
                    get_foldercontent($('#root'),true);
                else
                    get_foldercontent($(".selected"));
            });
        }
    });
    $('#editFolder').click(function(event){
        var datei = $('.selected');
        var dialog = $("#dialog").dialog('option', 'title', 'Verzeichniseigenschaften &auml;ndern');
        $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), function(d){
            dialog.html(d);
            dialog.dialog('option', 'buttons', {
                'Speichern': function() {
                    var postdata = dialog.find('form').getFormValues();
                    $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+(datei.attr('id')).replace('+','%2B').replace('&','%26'), postdata,function(d2){
                        dialog.dialog('close');
                        if(d2 != 'Ok')
                            debug(d2);

                    });

                }
            });
        });
        $("#dialog").dialog('open');
    });
    $('#renameFolder').click(function(event){
        var folder = $(".selected"),
        indexend = folder.attr('id').lastIndexOf('/')+1,
        parentFolder = folder.attr('id').substring(0,indexend),
        name = folder.attr('id').substring(indexend),
        child,addhtml;
        if(folder.attr('id') == 'root'){
            $.pnotify({
                pnotify_title: "Root Verzeichnis",
                pnotify_text: "Root Verzeichnis kann nicht umbenannt werden",
                pnotify_type: 'error',
                pnotify_history: false,
                pnotify_delay: 2000
            });
            return;
        }
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
		
        $("#dialog").dialog('option', 'title', 'Verzeichnis umbenennen');
        $("#dialog").html('Name: <input type="text" id="newname" value="'+name+'" />'+fileOccurrence);
        activateOccurenceSeachButton(folder.attr('id'));
        $("#dialog").dialog('option', 'buttons', {
            'Umbenennen': function() {
                $.post('ajax.php?klasse=dateimanager&act=rename', {
                    'src': folder.attr('id'),
                    'newname': $("#dialog #newname").val()
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        var len = folder.attr('id').length+1;
                        var oldId = folder.attr('id');
                        folder.attr('id',parentFolder+$("#dialog #newname").val());
                        folder.find('li').each(function(){
                            var newid = folder.attr('id')+'/'+$(this).attr('id').substring(len);
                            dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                            delete dateienarchiv[$(this).attr('id')];
                            $(this).attr('id', newid);
                        });
                        child = folder.find('ul').clone(true);
                        folder.html($("#dialog #newname").val());
                        folder.append(child);
                        dateienarchiv[folder.attr('id')] = dateienarchiv[oldId];
                        delete dateienarchiv[oldId];
                    }

                });
                $(this).dialog('close');
            },
            'Sicheres Umbenennen': function() {
                $.post('ajax.php?klasse=dateimanager&act=rename&opt=save', {
                    'src': folder.attr('id'),
                    'newname': $("#dialog #newname").val()
                }, function(d){ 
                    message(d);
                    var len = folder.attr('id').length+1;
                    var oldId = folder.attr('id');
                    folder.attr('id',parentFolder+$("#dialog #newname").val());
                    folder.find('li').each(function(){
                        var newid = folder.attr('id')+'/'+$(this).attr('id').substring(len);
                        dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                        delete dateienarchiv[$(this).attr('id')];
                        $(this).attr('id', newid);
                    });
                    child = folder.find('ul').clone(true);
                    folder.html($("#dialog #newname").val());
                    folder.append(child);
                    dateienarchiv[folder.attr('id')] = dateienarchiv[oldId];
                    delete dateienarchiv[oldId];

                });
                $(this).dialog('close');
            },
            'Abbrechen': function() {
                $(this).dialog('close');
            }
        });
        $("#dialog").dialog('open');
    });
    if(hasFlash()){

        var filter = $.query.get('filter'),filterExt,filterDesc;
        if(filter == 'flv'){
            filterDesc = 'Flash Videos(*.flv)|Alle-Dateien';
            filterExt = '*.flv|*';
        }
        else if(filter == 'img'){
            filterDesc = 'Bild-Dateien (*.jpg,*.gif,*.png)|*';
            filterExt = '*.jpg;*.jepeg;*.gif;*.png|*';
        }
        else{
            filterDesc = 'Alle-Dateien|Bild-Dateien (*.jpg,*.gif,*.png)|Flash Videos(*.flv)';
            filterExt = '*|*.jpg;*.jepeg;*.gif;*.png|*.flv';
        }
        $('#fileInput').uploadify({
            'swf'  : '/admin/upload/uploadify.swf',
            'langFile'  : 'upload/uploadifyLang_de.js',
            'uploader'  : 'ajax.php',
                        
            'postData':{
                'klasse':'dateimanager',
                'act':'uploadPost',
                'sessionid':sessionid
            },
            'method' : 'POST',
            'cancelImg' : 'upload/cancel.png',
            'auto'      : true,
            'multi'      : true,
            'folder'    : absolutepath,
            'simUploadLimit': 2,
            'buttonText': 'Upload',
            'debug': false,
            //			'fileDesc': filterDesc,
            //			'fileExt': filterExt,
            onUploadSuccess: function(ev,qId,fileInfo){
                var name = fileInfo.name,
                path;
                if($(".selected").attr('id') == 'root') path = name;
                else path = $(".selected").attr('id')+name;

                var el = $('#'+path);
                if(el.length > 0){
                    el.attr('src',el.attr('src')+'?t='+Math.random());
                    el.parent().css('backgroundColor', 'green');
                }
            },
            onQueueComplete: function(ev,qId,FileInfo) {
                var root = false;
                if($(".selected").attr('id') == 'root') root = true;
                get_foldercontent($(".selected"),root,true);
            }
        });
    }else{
        updateStaticUpload("../");
    }
	
    $("#dialog").dialog({
        bgiframe: true,
        resizable: false,
        maxHeight: $(window).height(),
        maxWidth: $(window).width(),
        width: 'auto',
        modal: false,
        autoOpen: false,
        overlay: {
            backgroundColor: '#000',
            opacity: 0.5
        }
    });
    $("#dateien").css('maxWidth', ($(window).width()-440)+'px');
        
    $("#filetree").delegate('li','mouseover',function(){
        $(this).find('.hoverAction:first').css('visibility','visible'); 
    }).delegate('li','mouseout', function(){
        $(this).find('.hoverAction:first').css('visibility','hidden'); 
    });
    
    
    viewModel = new dmViewModel();
    if(selectType == 'folder' || selectType == 'none') viewModel.allowFileCommit = false;
    if(selectType == 'file' || selectType == 'none') viewModel.allowFolderCommit = false;
    viewModel.updateContent(openAbsolutePath,function(){
//        console.log("APPLY BINDINGS", viewModel);
        ko.applyBindings(viewModel);        
    });
});

function submitFolderContent(parent, str){
    //  if(parent.attr('id') == 'root')
    var firstList = parent.find('ul');
    if(firstList.length > 0)
        firstList.html(firstList.html()+str)
    else{
        str = '<ul>'+str+'</ul>';
        parent.html(parent.html()+str);
    }
}

function getAbselutePath(path){
    var d=new Date();
    parent = $('#root');
    isroot = true;
    var realPath = true;
    if(path.indexOf("/") == -1){
        $('#root').addClass('selected');
        return false;
    }
    var pathFolders = path.split('/');
    var actFolderAdd = '';
//    console.log(path,',', path.replace(/\//g, '\\/').replace(/ /g, '_'));
    
    lastParent = $('#'+path.replace(/\//g, '\\/').replace(/ /g, '_'));
    $('#root').removeClass('selected');
    $.ajaxSetup({
        async:false
    });
    $.post('ajax.php?klasse=dateimanager&act=get_foldercontentsGlob' + '&' + d.getTime(), {
        pfad: path
    }, function(d){
        if(d == 'Error:NoValidPath'){
            $('#root').addClass('selected');
            realPath = false;
            return;
        }
        parent_str = '';
        var folderLevel = 0;
        var submitted = false;
        var stillFiles = false;
        $(d).find('el').each(function(){
            if($(this).attr('isdir') == 1){
                if($(this).attr('level') > folderLevel){
                    actFolderAdd = $(this).attr('dirpfad').replace(/\//g,'\\/').replace(/ /g,'\\ ').replace(/ /g, '_'); // actFolderAdd.replace(' ', '\\ ')
                    submitFolderContent(parent,parent_str);
                    parent = $('#'+actFolderAdd);
                    parent_str = '';
                }
                parent_str += '<li id="'+$(this).attr('pfad').replace(/ /g, '_')+'"><a href="#">'+($(this).attr('name'))+'</a></li>';
                if($(this).attr('isSubDir') == '0') //hasSubDirs = true;
                    folderLevel = $(this).attr('level');
            }else{
                if(!submitted){
                    submitFolderContent(parent, parent_str);
                    submitted = true;
                    actFolderAdd = $(this).attr('dirpfad').replace(/\//g,'\\/').replace(/ /g,'_'); // actFolderAdd.replace(' ', '\\ ')
                    parent = $('#'+actFolderAdd.replace(/ /g, '_')); //.replace('/', '\\/')
                    dateienarchiv[parent.attr('id')] = new Array();
                }
                var datei = new Array();
                datei['name'] = ($(this).attr('name'));
                datei['art'] = ($(this).attr('art'));
                datei['pfad'] = ($(this).attr('pfad'));
                dateienarchiv[parent.attr('id')].push(datei);
            }
        });
        //		if(!submitted){
        //			submitFolderContent(parent, parent_str);
        //			if(folderLevel > 0) actFolderAdd += '\\/'+pathFolders[folderLevel];
        //			else actFolderAdd = pathFolders[folderLevel];
        //			parent = $('#'+actFolderAdd.replace(' ', '\\ '));
        //		}
        dateibuild(parent.attr('id'));
        parent.addClass('selected');
        parent = $('#root');
        //		parent.unbind('click');
        folders = parent.find('li');		
        folders.click(function(event){
            var parent = $(this);
            $(".selected").removeClass('selected');
            $(this).addClass('selected');
            if(!ignoreUploadify){
                if(hasFlash())
                    $('#fileInput').uploadifySettings('folder', $(this).attr('id'));
                else
                    updateStaticUpload($(this).attr('id'));
            }
            get_foldercontent(parent);
            event.stopPropagation();
        });
        folders.dblclick(function(e){
            e.stopPropagation();
            return madeSelection($(this), false);
        });


        //TODO: Mit Root #+id li,#root
        parent.find('li').andSelf().add('#root,#delfolderdiv').droppable({ //parent.find('li')
            hoverClass: 'drophover',
            greedy: true,
            drop: function(event, ui) {
                event.stopPropagation();
                var len = ui.draggable.attr('id').lastIndexOf("/");
                if(len == -1) len = 0;
                if(($(this).attr('id') != ui.draggable.attr('id').substring(0,len)) && !($(this).attr('id') == 'root' && ui.draggable.attr('id').substring(0,len) == '')){
                    var dropel = $(this);
                    var newel = ui.draggable.clone();
                    if($(this).attr('id') == 'delfolderdiv'){
                        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
						      
                        if(ui.draggable.hasClass('datei')){
                            $("#dialog").dialog('option', 'title', 'Datei L&ouml;schen');
                            $("#dialog").html('M&ouml;chten Sie die Datei "'+ui.draggable.attr('id')+'" wirklich löschen?'+fileOccurrence);
                        }else{
                            $("#dialog").dialog('option', 'title', 'Verzeichnis L&ouml;schen');
                            $("#dialog").html('M&ouml;chten Sie das Verzeichnis "'+ui.draggable.attr('id')+'" wirklich löschen?'+fileOccurrence);
                        }
                        activateOccurenceSeachButton(ui.draggable.attr('id'));
                        $("#dialog").dialog('option', 'buttons', {
                            'Löschen': function() {
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=remDir',{
                                    src: ui.draggable.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            delete dateienarchiv[$(".selected").attr('id')];
                                            ui.draggable.hide();
                                            ui.draggable.removeClass('selected');
                                            $("#root").addClass('selected');
											
                                            if(hasFlash())
                                                $('#fileInput').uploadifySettings('folder', '../');
                                            else
                                                updateStaticUpload('../');
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    delete dateien[i];
                                                }
                                            }
                                            ui.draggable.hide();
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Abbrechen': function() {
                                $(this).dialog('close');
                            }
                        });

                    }else{
                        var fileOccurrence = '<div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
                        if(ui.draggable.hasClass('datei')){
                            $("#dialog").dialog('option', 'title', 'Datei Verschieben/Kopieren');
                            $("#dialog").html('M&ouml;chten Sie die Datei "'+ui.draggable.attr('id')+'" kopieren oder verschieben.<br /><span style="color: #f00">Achtung: Wenn Sie die Dateien verschieben k&ouml;nnen verlinkungen verloren gehen</span><br />'+fileOccurrence);
                        }else{
                            $("#dialog").dialog('option', 'title', 'Verzeichnis Verschieben/Kopieren');
                            $("#dialog").html('M&ouml;chten Sie das Verzeichnis "'+ui.draggable.attr('id')+'" kopieren oder verschieben.<br /><span style="color: #f00">Achtung: Wenn Sie das Verzeichnis verschieben k&ouml;nnen verlinkungen verloren gehen</span><br />'+fileOccurrence);
                        }
                        $('#occurrence').click(function(){
                            var occ = $(this);
                            $.post('ajax.php?klasse=dateimanager&act=checkOccurrence', {
                                src: ui.draggable.attr('id')
                            }, function(d){
                                occ.html(d);
                            });                                                    
                        });
                        $("#dialog").dialog('option', 'buttons', {
                            'Kopieren': function() {
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=dircopy',{
                                    src: ui.draggable.attr('id'),
                                    to: dropel.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable.clone();
                                            //TODO: Check ob neuberechnung noetig ist
                                            var len = newel.attr('id').lastIndexOf("/");
                                            if(len == -1) len = 0;
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.wrap("<ul></ul>");
                                            newel.parent().appendTo(dropel);
                                            newel.wrap("<ul></ul>");
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                var dateien = dateienarchiv[dirid];
                                                var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                                for (var i in dateien){
                                                    if(dateien[i]['name'] == name){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Verschieben': function() {
                                var oldid = ui.draggable.attr('id');
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=dirmove',{
                                    src: oldid,
                                    to:dropel.attr('id')
                                }, function(d){
                                    if(d != 'Ok') debug(d);
                                    else{
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable;
                                            var len = newel.attr('id').lastIndexOf("/")+1;
                                            newel.wrap("<ul></ul>");
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                delete dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.parent().appendTo(dropel);
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var delid = -1;
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                    delete dateien[i];
                                                    ui.draggable.hide();
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'sicher Verschieben (Beta)': function() {
                                var oldid = ui.draggable.attr('id');
                                var dialog = $(this);
                                $.post('ajax.php?klasse=dateimanager&act=savedirmove',{
                                    src: oldid,
                                    to:dropel.attr('id')
                                }, function(d){
                                    if(d.substr(0,6) == 'Error:'){
                                        debug(d)
                                    }
                                    else{
                                        message(d);
                                        if(!ui.draggable.hasClass('datei')){
                                            var newel = ui.draggable;
                                            var len = newel.attr('id').lastIndexOf("/")+1;
                                            newel.wrap("<ul></ul>");
                                            newel.parent().find('li').each(function(index){
                                                var newid = dropel.attr('id')+'/'+$(this).attr('id').substring(len);
                                                dateienarchiv[newid] = dateienarchiv[$(this).attr('id')];
                                                delete dateienarchiv[$(this).attr('id')];
                                                $(this).attr('id', newid);
                                            });
                                            newel.parent().appendTo(dropel);
                                        }else{
                                            var dirid = $(".selected").attr('id');
                                            var delid = -1;
                                            var dateien = dateienarchiv[dirid];
                                            var name = ui.draggable.attr('id').substring(ui.draggable.attr('id').lastIndexOf("/")+1);
                                            for (var i in dateien){
                                                if(dateien[i]['name'] == name){
                                                    if(typeof dateienarchiv[dropel.attr('id')] != "undefined"){
                                                        dateienarchiv[dropel.attr('id')].push(dateien[i]);
                                                    }
                                                    delete dateien[i];
                                                    ui.draggable.hide();
                                                }
                                            }
                                        }
                                        dialog.dialog('close');
                                    }
                                });
                            },
                            'Abbrechen': function() {
                                $(this).dialog('close');
                            }
                        });
                    }
                    $("#dialog").dialog('open');
                }
            }
        });
        parent.find('li').draggable({
            revert: true,
            delay: 250
        });

        //parent.find('li').prepend('[+]');
        parent.click(function(e){
            var parent = $(this);
            $(".selected").removeClass('selected');
            $(this).addClass('selected');
			
            if(!ignoreUploadify){
                if(hasFlash())
                    $('#fileInput').uploadifySettings('folder', '../');
                else
                    updateStaticUpload($(this).attr('id'));
            }
            get_foldercontent(parent,true);
            e.stopPropagation();
        });

        parent.removeClass('load');
    });
    $.ajaxSetup({
        async:true
    });
    return realPath;
//  var parent = $('#root');
//  var isRoot = true;
//  ignoreUploadify = true;
//  var folders = path.split('/');
//  var actpath = '';
//  var folder = '';
//  for(var folderindex in folders) {
//	folder = folders[folderindex];
//	if(folder != ''){
////	  $(".selected").removeClass('selected');
////	  $('#'+actpath+folder).addClass('selected');
//	  $('#'+actpath+folder).trigger('click');
////		get_foldercontent(parent, isRoot);
//		actpath += folder+'\\/';
//	}
//  }
//  ignoreUploadify = false;
}

function activateOccurenceSeachButton(path){
    $('#occurrence').click(function(){
        var occ = $(this);
        $.post('ajax.php?klasse=dateimanager&act=checkOccurrence', {
            src: path
        }, function(d){
            occ.html(d);
        });                                                    
    });
}
