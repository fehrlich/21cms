var debugstart = false,
returnid = '',
realAjaxId = false,
windowResizing = 0,
aktContainer,
mWidth = 0,
cropapi,
aktcropset = new Array(),
cropsub = new Array(),
helpElement = '';

function sendUserMessage(msg){
  $('#UserMessage').html(msg);
  setTimeout("$('#UserMessage').hide(\"slow\")",5000);
  $('#UserMessage').show("slow");
}

function parseMessages(){
  $('#messages').find('.message').each(function(){
	var message = $(this);
	$(this).children('.msg').dialog({
	  title: 'Nachricht von '+message.children('.from_user').html(),
	  buttons: {
		Ok: function() {
		  $.post($('#cmsroot').html()+'admin/ajax.php?el=MessageSystem&act=setRead',{
			id:message.children('.id').html()
		  },function(d){
			});
		  $(this).dialog('close');
		}
	  }
	});
  });
}

function setrandpass(elname){
  var chars = "bcdfghklmnpqrstvwxyz",
  chars2 = "aeiou",
  numbers = "0123456789",
  string_length = 8,
  randomstring = '',
  even = true,
  i,usedchars,rnum;

  for (i=0; i<string_length; i++) {
	if(i > string_length-3)
	  usedchars = numbers;
	else if(even)
	  usedchars = chars;
	else
	  usedchars = chars2;

	rnum = Math.floor(Math.random() * usedchars.length);
	randomstring += usedchars.substring(rnum,rnum+1);
	even = !even;
  }
  $('[name=\''+elname+'\']').val(randomstring);
}
function dump(arr,level) {
  var dumped_text = "",
  level_padding = "", //The padding given at the beginning of the line.
  j,
  item,
  value;
  if(!level) level = 0;


  for(j=0;j<level+1;j++) level_padding += "    ";

  if(typeof(arr) == 'object') { //Array/Hashes/Objects
	for(item in arr) {
	  value = arr[item];

	  if(typeof(value) == 'object') { //If it is an array,
		dumped_text += level_padding + "'" + item + "' ...\n";
		dumped_text += dump(value,level+1);
	  } else {
		dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
	  }
	}
  } else { //Stings/Chars/Numbers etc.
	dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
  }
  return dumped_text;
}

function debug(txt,loadNew){
  var debugDiv = $('#debug');
  loadNew = loadNew || false;
  if(loadNew){
	txt = $('#debug').html();
	$('#debug').html("");
  }

  if(!debugstart){
	debugstart = true;
  }

  debugDiv.html(debugDiv.html()+txt+"<br />");

  var ajaxwindow = $('.ajaxwindow2').html('<p><span style="color:red">Es ist ein Fehler aufgetreten.</span><br /> Bitte schildern sie uns was Sie als letztes getan haben:</p><textarea name="error" id="errorUsermsg"></textarea><br />');
  var error = $('<div id="errormsg" style="display:none">'+debugDiv.html()+'</div>').appendTo(ajaxwindow);
  $('<a href="#">Fehler anzeigen/ausblenden</a>').click(function(){
	error.toggle()
  }).appendTo(ajaxwindow);

  ajaxwindow.dialog('option','title', 'Fehler bericht senden');
  ajaxwindow.dialog('option', 'buttons', {
	'Fehlerbericht senden': function() {
	  if(debugDiv.html().length < 3)
		alert('Bitte tragen Sie ein was Sie als letztes getan haben, damit wir ihren Fehler besser zuordnen können');
	  else{
		$.post($('#cmsroot').html()+'admin/ajax.php?kl=BugReport', {
		  'browser': debugGetBrowser(),
		  'res': debugGetResolution(),
		  'flash': debugHasFlash(),
		  'el': 'BugReport',
		  'ajax': '1',
		  'errormsg': debugDiv.html(),
		  'usermsg':  $(this).find('#errorUsermsg').val()
		}, function(d){
		  $('#debug').html('');
		  ajaxwindow.dialog('close');
		  alert('Danke für Ihren Bugreport.')
		});
	  }
	},
	'Abbrechen': function() {
	  $('#debug').html('');
	  ajaxwindow.dialog('close');
	}
  });
  $(".ajaxwindow2").dialog('open');
}

function debugGetResolution(){
  return screen.availWidth+'x'+screen.availHeight;
}

function debugHasFlash(){
  var hasFlash = false;
  if(typeof(navigator.plugins["Shockwave Flash"]) == "object") hasFlash = true;
  else if(typeof(ActiveXObject) == "function") hasFlash = true
  return hasFlash;
}

function debugGetBrowser(){
//  var browser = "";
//  jQuery.each(jQuery.browser, function(i, val) {
//	browser += ","+i+":"+val;
//  });
//  browser = browser.substr(1);
//  return browser;
  return "not supported anymore";
}

function designEditor(editor){
  if(typeof aktContainer == 'undefined')
	return "";

  var elId = aktContainer.attr('id'),backgrounds,
  colorSet = false,
  imageSet = false,
  textColorSet = false,
  backgroundStr = "",
  textcolorStr = "";

  if(elId == "") return "";
  if($('#'+elId).length == 0) return "";

  backgrounds = $('#'+elId).parent(),

  backgrounds.andSelf().parents('div,body').each(function(){
	if(colorSet == false && $(this).css('backgroundColor') != "" && $(this).css('backgroundColor') != "transparent"){
	  //			inh.css('backgroundColor', $(this).css('backgroundColor'));
	  //			inh.css('backgroundImage', "none");
	  backgroundStr = 'none '+ $(this).css('backgroundColor');
	  colorSet = true;
	}
	if(textColorSet == false && $(this).css('color') != ""){
	  //			inh.css('color', $(this).css('color'));
	  textcolorStr = $(this).css('color');
	  textColorSet = true;
	}

	imageSet = true;
	//		alert((textColorSet && colorSet && imageSet));
	return !(textColorSet && colorSet && imageSet);
  });
  //	inh.attr('id', elId);
  //	inh.addClass(elClass);
  //	inh.css('position', 'static');
  //	inh.css('margin', '0');
  return "margin:0;position:static;background:"+backgroundStr+";textcolorStr:"+textcolorStr;
}

function loadFormBackup(form,backupId){
  //	var evalstr = $.ajax({
  //		async: false,
  //		url: $('#cmsroot').html()+'admin/ajax.php',
  //		data: {
  //			'id': backupId,
  //			'act': 'getFormBackup'
  //		},
  //		dataType: 'json'
  //	}).responseText;
  //	alert(evalstr);
  //	var data = eval(evalstr);
  //	var data = {"Titel":"fdsaasf","KurzText":"dsafsfa","Bild":"","Text":"<p>\n\tdfas asdsadsadsad2asd asd FDSsaf<\/p>\n","public":"1","erstellt":"1285696738","published":"1285696749","title_intern":"fdsaasf","autor":"1"};
  //	alert(dump(data)+"!");
  $.getJSON($('#cmsroot').html()+'admin/ajax.php', {
	'id': backupId,
	'act': 'getFormBackup'
  }, function(data) {
	//			alert(dump(data))
	$.each(data, function(index, value){
	  var field = form.find(':input[name='+index+']');
	  if(field.length == 1)
		field.val(value);
	});
  //		var select = $('#fruitVariety');
  //		var options = select.attr('options');
  //		$('option', select).remove();
  //

  });
}

function gridParse(element){
  element.resizable({
	grid: [10, 10],
	containment: 'parent'
  }).draggable({
	grid: [ 10,10 ],
	containment: 'parent'
  }).css('position','absolute').dblclick(function(){
	var edit = $(this).children('.content');
	edit.addClass('edit');
	//                var dataProcessor = new CKEDITOR.htmlDataProcessor();
	
//		alert($(edit).html());
//		alert(	CKEDITOR.BBCodeParser($(edit).html()));
//		alert(CKEDITOR.htmlParser.fragment.fromBBCode($(edit).html()));
	$(edit).html(($(edit).html()));
	//                alert($(edit).html());
	edit.ckeditor({
	  filebrowserBrowseUrl : 'index.php?mm=Dateimanager',
	  filebrowserWindowWidth : '100%',
	  toolbar: 'Pdf',
	  extraPlugins: 'linkcms,cmsinstantcrop,bbcode',
	  height: edit.parent().height()
	});
	edit.click(function(e){
	  e.stopPropagation();
	});
	$(this).click(function(e){
	  e.stopPropagation();
	});
  })
}

function delegateParse(){
  var elId,style,elClass,tooltips,toolbar,extraPlugins;

  $('#cms_logo').click(function(){
	$('.hover').toggle();
	$('body#inhalt').html('asdf');
  });
  $(document).ajaxStart(function(){
        console.log("test2");
	});
  $(document).ajaxStop(function(){
      console.log("TEST");
	var ajaxwindow = $(this),
	slider;
	ajaxwindow.find('.date').datepicker({
	  'dateFormat' : 'dd.mm.yy'
	});

	ajaxwindow.find('.editor').each(function(){
	  delete CKEDITOR.instances[$(this).attr('name')];
	});
        var autocomplete = ajaxwindow.find('.autocomplete');
        if(autocomplete.length > 0)
        	autocomplete.combobox();


	ajaxwindow.find('.gridBuild').each(function(){
	  var gridArea;
	  gridArea = $(this).children('.a4');
	  $(this).find('.gridElement').each(function(){
//		$(this).html(''+($(this).children('.content').html())+'');
//		alert($(this).children('.content').html());
//		alert(	CKEDITOR.BBCodeParser($(this).children('.content').html()));
//		alert(CKEDITOR.htmlParser.fragment.fromBBCode($(this).children('.content').html()));
		$(this).html('<div class="content">'+($(this).find('.content:last').html())+'</div>');
//		$(this).html('<div class="content">'+bbCode2html($(this).children('.content').html())+'</div>');
		gridParse($(this));
	  })
	  $(this).children('.newGridEl').click(function(){
		var element = $('<div class="gridElement"><div class="content">Doppelklick zum editieren ...</div></div>');
		gridParse(element);
		element.appendTo(gridArea);
	  });
	  //			gridArea = $('<div class="a4"></div>').appendTo($(this));

	  gridArea.click(function(e){
		$(this).find('.edit').each(function(){
		  var editor = $(this).ckeditor();
		  $(this).html(editor.ckeditorGet().getData());
		  editor.ckeditorGet().destroy();
		  $(this).removeClass('edit');
		  var dataProcessor = new CKEDITOR.htmlDataProcessor();
		  $(this).html(dataProcessor.toHtml($(this).html()));
		});
	  });

	});

	//		CKEDITOR.on('instanceCreated', function( e ){
	//			alert("a");
	//			e.editor.addCss( 'body { background-color: grey; }' );
	////			designEditor(e.editor);
	//		});

	if(typeof aktContainer == 'undefined'){
	  elId = 'inhalt';
	  elClass = '';
	  style = "";
	}else{
	  elId = aktContainer.attr('id');
	  elClass = aktContainer.attr('class');
	  style = designEditor();
	}
	if(mWidth == 0)
	  mWidth = $('#maxWidth').html();

	extraPlugins = "cmsvideo,linkcms,cmsinstantcrop,cmsGMap";
	toolbar = 'Inhalt';
	if($.query.get('mm') == 'DataTemplate'){
	  toolbar = 'Template';
	  extraPlugins = "cmsvideo,linkcms,cmsinstantcrop,cmsvars,cmsGMap";
	}
	
	var path = '';
	if($('#cmsroot').length > 0){
	  path = $('#cmsroot').html()+'admin/';
	}
	
	ajaxwindow.find('textarea.editor').ckeditor({
	  filebrowserBrowseUrl : path+'index.php?mm=Dateimanager&maxwidth='+mWidth,
	  filebrowserWindowWidth : '100%',
	  filebrowserWindowHeight : '100%',
	  height: 320,
	  bodyId: elId+'" style="'+style,
	  bodyClass: elClass,
	  toolbar: toolbar,
	  extraPlugins: extraPlugins
	});
	ajaxwindow.find('textarea.editorNewsletter').ckeditor({
	  bodyId: elId+'" style="'+style,
	  bodyClass: elClass,
          contentsCss: ['/tpl/newsletter/style.css'],
          height: 320,
          filebrowserBrowseUrl : path+'index.php?mm=Dateimanager&absoluteLinks=1&maxwidth='+mWidth,
	  filebrowserWindowWidth : '100%',
	  filebrowserWindowHeight : '100%',
          toolbar: 'Newsletter'
	});
	/*
		 * Table Drag&Drop
		 */
	ajaxwindow.find('.sortable').tableDnD();
        ajaxwindow.find('.multiselect').multiselect({
            minWidth: 300,
            height: 400,
            selectedText: "# ausgewählt",
            checkAllText: "Alle auswählen",
            uncheckAllText: "Alle abwählen"
        })
//        .multiselectfilter({
//            placeholder: "Filter Text eingeben"
//        });

	ajaxwindow.find(':input:first').focus();

	ajaxwindow.find('.allowtabs').tabby();
	var tabOptions = {};
	if($('#cmsLanguage').length > 0){
//	  alert('#tab_'+$('#cmsLanguage').html());
	  var langtab = $('#tab_'+$('#cmsLanguage').html());
	  if(langtab.length > 0){
		tabOptions.selected = langtab.parent().index();		
	  }
	}
	ajaxwindow.find("#tabs").tabs(tabOptions);
	if(ajaxwindow.find('#backupIcon').length == 1){
	  ajaxwindow.find('#backupIcon').click(function(){
		$(this).parent().find('.timeline').show();
		$(this).hide();
	  });
	  slider = ajaxwindow.find('.toSlider:not(.init)');
	  if(slider.length > 0){
		slider.addClass('init').selectToUISlider({
		  labelSrc: 'text',
		  sliderOptions:{
			stop: function(){
			  var form = $(this).parents('form:first');
			  loadFormBackup(form, ajaxwindow.find("#formBackup").val());
			}
		  }
		}).change(function(){
		  var form = $(this).parents('form:first');
		  loadFormBackup(form, $(this).val());
		}).removeClass('toSlider');
	  }
	}
	var helpIcon = ajaxwindow.find('#helpIcon');
	if(helpIcon.length > 0){
	  helpIcon.click(function(e){
		e.stopPropagation();
		if($('body').find('#helpDialog').length == 0){
		  $('body').append('<div id="helpDialog">');
		}


		var dia = $('#helpDialog').dialog({
		  autoOpen: true,
		  modal: true,
		  width: 'auto',
		  position: ['center','middle'],
		  beforeclose: function(){
			$('.ajaxwindow .editor').each(function(){
			  delete CKEDITOR.instances[$(this).attr('name')];
			});
			$('.ajaxwindow .editorNewsletter').each(function(){
			  delete CKEDITOR.instances[$(this).attr('name')];
			});
		  }
		});
		dia.dialog('option', 'buttons', {
		  'Schließen': function() {
			dia.dialog('close');
		  }
		});
		$.get($('#cmsroot').html()+'admin/ajax.php',{
		  act : 'help',
		  helpEl : helpElement
		},function (d){
		  dia.html(d);
		  dia.find('#tabs').tabs();
		});
		dia.dialog('open');
	  });
	}
	
	prepareMultiSelect();
	tooltips = ajaxwindow.find('.tooltip');
	if(tooltips.length > 0){
	  tooltips.mouseover(function(e){
		var id = $(this).attr('id').substr(4),
		toolTip = $('#toolcontenttool'+id),
		tabs = $(this).parents('.ui-tabs').offset(),
		off = $(this).offset();
		toolTip.css({
		  'top': (off.top-tabs.top),
		  'left': (off.left-tabs.left+$(this).width()+15)
		  });
		toolTip.show();
	  });
	  tooltips.mouseout(function(){
		var id = $(this).attr('id').substr(4);
		$('#toolcontenttool'+id).hide();
	  });
	}
        $('.dm_autocomplete').autocomplete({
                source: $('#cmsroot').html()+'admin/ajax.php?act=dmSearch',
                minLength: 3
        });
  });
  $('.ajaxwindow').delegate('.savebutton', 'click', function(){
	var form = $(this).parents('form'),
	postdata = form.getFormValues(),
	action = form.attr('action');
	$.post(action, postdata, function(d){
	  if(d != 'Ok'){
		alert(d);
		debug('',true);
	  }
	});
  });
//  var onload = true;
//  $('body').delegate('.cke_dialog_ui_input_text', 'focus', function(){
//      if(onload){
//        alert('Asdf');
//        onload = false;
//	$(this).autocomplete({
//                source: $('#cmsroot').html()+'admin/ajax.php?act=dmSearch',
//                minLength: 3,
//                focus: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');        
//                    return false;
//                },
//                show: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                    
//                },
//                close: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                    
//                },
//                change: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                      
//                },
//                select: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                      
//                },
//                search: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                      
//                },
//                create: function(){
//                    $('.ui-autocomplete').css('zIndex', '99999');                      
//                }
//        });
//      }
//  });
  
  
  
  $('.ajaxwindow').delegate('.template', 'click', function(){
	var newhtml = $(this).children('.tplHtml').html(),
	editor = $(this).parents("form").find("textarea.editor");
	var ckeditor = editor.ckeditorGet();
	//		var ckeditor2 = editor.ckeditor();
	editor.val(editor.ckeditorGet().getData()+newhtml).ckeditor().ckeditorGet().updateElement();
	designEditor(ckeditor)
  });

  $('.ajaxwindow').delegate('.newdia', 'click', function(){
	var klasse = '',
	inputfield = $(this).parent().find(':input'),
	db = '';

	if($(this).hasClass('db')) klasse = 'DatabaseConnector';
	else if($(this).hasClass('tpl')){
	  klasse = 'DataTemplate';
	  db = $(this).attr('id').substr(1);
	}
	else if($(this).hasClass('dataRecord')){
	  klasse = 'DatabaseConnector';
	  db = $(this).attr('id').substr(1);
	}else
	  klasse = $(this).attr('id');

	$.get($('#cmsroot').html()+'admin/ajax.php',{
	  'kl': klasse,
	  'new':'1',
	  'inter':'dia',
	  'db':db
	} ,function(d){
	  if(d.substr(0,6) == 'Error:'){
		alert(d);
		return
	  }
	  $(".ajaxwindow2").html(d);

	  $(".ajaxwindow2 .tabs").tabs();
	  $(".ajaxwindow2").dialog('option', 'buttons', {
		'Speichern': function() {
		  var form = $('".ajaxwindow2 form'),
		  postdata = form.getFormValues(),
		  action = form.attr('action');
		  $.post(action, postdata, function(d){
			if(d.substr(0,4) != 'RET:'){
			  alert(d);
			  return
			}
			else{
			  d = d.substr(4);
			  if(inputfield[0].tagName == 'select'){
				inputfield.append('<option value="'+d+'">Gerade erstellte Datenbank</option>');
			  }else if(inputfield.attr('type') == 'text'){
				$('.tplsel .selected').removeClass('selected');
				inputfield.parent().append('<div class="tplsel selected">Gerade erstelltes Template</div>');
			  }
			  var split = d.split(':');
			  inputfield.val(split[0]);
			  $(".ajaxwindow2").dialog('close');
			}
		  });
		},
		'Abbrechen': function() {
		  $(".ajaxwindow2").dialog('close');
		}
	  });
	});
	$(".ajaxwindow2").dialog('option' , "title" , $(this).attr('title') );
	$(".ajaxwindow2").dialog('option' , 'width', 'auto');
	$(".ajaxwindow2").dialog('open');
  });

  $('.ajaxwindow,.ajaxwindow2').delegate('.onchangevalue :input', 'change', function(){
	var value = $(this).val(),
	  parent =  $(this).parents('.onchangevalue'),
	  event,
	  values,
	  val_spl,
	  changeName,
	  id,
	  match,
	  html,
	  name,
	  count;
	  
	if(parent.find('.showOn').length > 0){
	  parent.find('.showOn').each(function(d){
		var on = $(this).attr('id').substr(2);
		if(value == on) $('.'+$(this).html()).show();
		else $('.'+$(this).html()).hide();
	  });
	}else if(parent.children('[id^=\''+$(this).val()+'-\']').length > 0){
	  event = parent.children('[id^=\''+$(this).val()+'-\']');
	  values = event.attr('id');
	  val_spl = values.split('-');
	  changeName = val_spl[1];
	  id = parent.parents('tr').find(':input:last').attr('name'),
	  match = id.match(/\[(\d+)\]/);

	  html = event.clone();

	  id = match[1];
	  html.find(':input').each(function(){
		name = $(this).attr('name');
                if (typeof name !== 'undefined' && name !== false) {
                    if($(this).attr('name').substr(-1) == ']'){
                    name = name.substring(0,name.length-2);
                    }
                    $(this).attr('name',changeName+'['+id+']['+name+']');
                }
	  });
	  if(parent.children(changeName+'-default').length == 0){
		parent.append('<div class="var dontpost" id="'+changeName+'-default">'+parent.parents('tr').find('.'+changeName).html()+'</div>');
	  }
	  parent.parents('tr').find('.'+changeName).parent().html(html.html());
	}else{
	  count = 0;
	  if(parent.children(changeName+'-default').length == 0){
		event = parent.children('.setValue:first');
		values = event.attr('id');
		val_spl = values.split('-');
		changeName = val_spl[1];
		parent.append('<div class="var dontpost" id="'+changeName+'-default"><input type="text" name="'+parent.parents('tr').find('.'+changeName+' :input:first').attr('name')+'" value="" /></div>');
	  }
	  parent.children('[id$=\'-default\']').each(function(){
		changeName = $(this).attr('id').substring(0, $(this).attr('id').length-8);
		count++;
		parent.parents('tr').find('.'+changeName).parent().html($(this).html());
	  });
	}
  });

  $('.ajaxwindow,.ajaxwindow2').delegate('.delline', 'click', function(){
	var tr = $(this).parents('tr');
	tr.remove();
	$('#tableAnz').val($('#tableAnz').val()-1);
  })
  $('.ajaxwindow,.ajaxwindow2').delegate('.newline','click',function(){//fieldname[1]
	var tab = $(this).parents('table:first'),
	idel = tab.find('.tableorder:last'),
	id = idel.val(),
	content = tab.find('tr:eq(1)').clone(true);
	idel.val(parseInt(idel.val())+1);
	id++;
	content.find(':input').each(function(){
	  if($(this).hasClass('tableorder')){
		$(this).val(id-1);
	  }
	  var attr = $(this).attr('name');
	  
	  if (typeof attr !== 'undefined' && attr !== false) {
		$(this).attr('name', $(this).attr('name').replace('[1]','['+id+']'));
	  }
	});
	tab.find('tr:last').before(content);
	$('#tableAnz').val($('#tableAnz').val()-1+2);
  });

  $('.ajaxwindow,.ajaxwindow2').delegate('table select[name=\'fieldname[]\']','change',function(){
	var parent = $(this).parent().parent(),
	val = $(this).children('[value='+$(this).val()+']'),
	rel = parent.find('select[name=\'relation[]\']'),
	values =parent.find('[name=\'value[]\']'),
	par,html,opts,id;

	rel.children('option').hide();
	if(val.hasClass('datum') || val.hasClass('time')){
	  rel.children('[value=\'<\'],[value=\'>\']').show();
	}else
	  rel.children('[value=\'=\'],[value=\'!=\']').show();

	if(val.hasClass('select')){
	  if(values[0].tagName == 'INPUT'){
		par = values.parent();
		html = '<select name="value[]">';
		opts = $('#var'+val.html()).html();
		id = 0;

		values.remove();
		for(id in opts){
		  html += '<option id="'+id+'">'+opts[id]+'</option>';
		}
		html += '</select>';
		par.append(html);
	  }
	}else{
	  if(values[0].tagName == 'SELECT'){
		par = values.parent(),
		html = '<input type="text" name="value[]" />';

		values.remove();
		par.append(html);
	  }
	}
  });

  $('.ajaxwindow,.ajaxwindow2').delegate('.sortable tr','mouseover',function(){

	$(this.cells[0]).addClass('showDragHandle');
  });
  $('.ajaxwindow,.ajaxwindow2').delegate('.sortable tr','mouseout', function() {
	$(this.cells[0]).removeClass('showDragHandle');
  });
  prepareMultiSelect();
}

function prepareMultiSelect(){	
  $('.multiSelValue').hide();
  $('.multiSelect,.multisel,.addremove').show();
}

function openFileBrowser(url){
  var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes,scrollbars=yes" ;
  sOptions += ",width="+$(window).width();
  sOptions += ",height="+$(window).height();
  sOptions += ",left=0";
  sOptions += ",top=0";

  window.open(url, 'BrowseWindow', sOptions ) ;
}
function showDialog(el,act){
  elementact(el,act);
}

function parseAjaxAnwser(d,parentid,tableact,normalcontent){
  tableact = tableact || false;
  normalcontent = normalcontent || false;
  if(d.substr(0,6) == 'Error:'){
	debug(d.substr(6));
	return false;
  }
  else if(d.substr(0,10) == 'FORMERROR:'){
	alert('In dem Formular ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Angaben.');
	$('.ajaxwindow').html(d.substr(10));
	delegateParse();
	$('.ajaxwindow').show();
	return false;
  }
  else if(normalcontent){
	if(d.substr(0,5) == 'Ok:EL'){
	  $("#"+parentid).replaceWith(d.substr(5));
	}
	else if(d.substr(0,7) == 'Ok:CONT'){
	  if(tableact == 'sub')
		$("#"+parentid).replaceWith(d.substr(7));
	  else{
		if($("#"+parentid).parents('.pseudomain:first').length == 1){
		  $("#"+parentid).parents('.pseudomain:first').replaceWith(d.substr(7));
		}
		else{
		  var el = $("#"+parentid);
		  if(!el.hasClass('mainelement'))
			el = el.parents('.mainelement:first');
		  el.replaceWith(d.substr(7));
		}
	  }
	}
	else if(d.substr(0,9) == 'Ok:RELOAD'){
	  window.location.reload();
	}
	else{
	  debug(d);
	  return false;
	}
	sendUserMessage('Erfolgreich gespeichert');
	initpage();
  }else{
	if(d.substr(0,3) == 'Ok:'){
	  if(tableact == 'Edit'){
		parentid.replaceWith(d.substr(5));
		sendUserMessage('Eintrag Erfolgreich ge&auml;ndert');
	  }else if(tableact == 'New'){
		if(d.substr(0,7) == 'Ok:CONT'){
		  sendUserMessage('Eintr&auml;ge Erfolgreich hinzugef&uuml;gt');
		  parentid.replaceWith(d.substr(7));
		  tableReload();
		}else{
		  sendUserMessage('Eintrag Erfolgreich hinzugef&uuml;gt');
		  $('html').scrollTop(0);
		  parentid.children('tbody').prepend(d.substr(5));
		}
	  }else if(tableact == 'Del'){
		sendUserMessage('Eintrag Erfolgreich gel&ouml;scht');
		parentid.remove();
	  }
	  if(d.substr(0,9) == 'Ok:RELOAD'){
		window.location.reload();
	  }
	}
	else{
	  debug(d);
	  return false;
	}
  }
  return true;
}

function elementaction(el,act,id,cont,art,parentid,zus,mainid,jElementId){
  helpElement = el;
  zus = zus || new Array();
  mainid = mainid || 0;
  jElementId = jElementId || 0;
  act = act || 'edit';
  var seitenid,
  nextMainId = 0,
  spl,
  querystr,
  ids,
  //		start,
  name,
  parentElement;

  if(realAjaxId) seitenid = $('#ajaxid').html();
  else seitenid = $('#seitenid').html();
  if(el == 'container')
	nextMainId = '-1';
  else{
	if($('#'+parentid).length == 1 && $('#'+parentid).hasClass("mainelement")){ //.parents('.mainelement:first').length
	  spl = $('#'+parentid).children('.admineig').children('.edit').attr('id').split('-'); //.parents('.mainelement:first')
	  nextMainId = spl[2];
	}else
	  nextMainId = '-1';
  }
  if(act == 'del'){
	$(".ajaxwindow").html('Sicher das Sie dieses Element l&ouml;schen m&ouml;chten?');
	$(".ajaxwindow").dialog('open');
	$(".ajaxwindow").dialog('option', 'buttons', {
	  'Löschen': function() {
		$.get($('#cmsroot').html()+'admin/ajax.php', {
		  'el': el,
		  'act':'delete',
		  'id':id,
		  'm':seitenid,
		  'c':cont
		}, function(d){
		  if(d != 'Ok'){
			debug(d);
		  }
		  reloadcontainer(cont,act,el,jElementId);
		  $(".ajaxwindow").dialog('close');
		});
	  },
	  'Abbrechen': function() {
		$(".ajaxwindow").dialog('close');
	  }
	});
  }else{
	querystr = {
	  'el': el,
	  'act':'form'+act,
	  'id':id,
	  'get':art,
	  'm':seitenid,
	  'c':cont,
	  'inter':'dia',
	  'parentid':parentid,
	  'mainid':mainid,
	  'mWidth':$("#"+parentid).width()
	};
	mWidth = $("#"+parentid).width();
	for(ids in zus){
	  querystr[ids] = zus[ids];
	}
        console.log($('#cmsroot').html());
	$.get($('#cmsroot').html()+'admin/ajax.php',querystr ,function(d){
	  if(d.substr(0,6) == 'Error:'){
		debug(d);
		return
	  }
	  $(".ajaxwindow").html(d);
	  
	  parentElement = $('#'+parentid);
	  if($('#'+parentid).hasClass('container')){
		aktContainer = parentElement.parent();
	  }else{
		aktContainer = parentElement.parents(".container:last").parent();
		if(aktContainer.length == 0){
		  aktContainer = parentElement.parent();
		}
	  }
	  //			alert($("#"+parentid).width());
	  $(".ajaxwindow .onchangechangedialog").each(function(){
		$(this).find(':input').change(function(){
		  name = 'change'+$(this).attr('name'),
		  zus = new Array();
		  zus[name] = $(this).val();
		  elementaction(el,act,id,cont,art,parentid,zus);
		});
	  });

	  var tree = $(".ajaxwindow .treesort");
	  if(tree.length == 1){
		var treeView = !tree.hasClass('listOnly')
		tree.jstree({
		  plugins : ["html_data","themes",'ui','dnd','json_data','crrm'],
		  "core" : {
			"initially_open" : [ "root" ]
		  },
		  "themes" : {
			"theme" : "apple",
			dots: true,
			icons: true
		  },
		  'crrm':{
			'move':{
			  "check_move" : function (m) {
				if(!treeView){
				  var p = this._get_parent(m.o);
				  p = p == -1 ? this.get_container() : p;
				  if(p === m.np) return true;
				  return false;
				}else
				  return true;
			  }
			}
		  }
		  ,
		  "dnd" : {
			"drop_target" : false,
			"drag_target" : false
		  }
		}).bind("move_node.jstree", function (e, data) {
		  $(".ajaxwindow .treesort").addClass('moved');
		});
	  }

	  $(".ajaxwindow").dialog('option', 'buttons', {
		'Speichern': function() {
		  var postdata = $('.ajaxwindow form').getFormValues();
		  $.post($('#cmsroot').html()+'admin/ajax.php?el='+el+'&act=formPost&art='+act+'&get='+art+'&m='+seitenid+'&id='+id+'&mainid='+mainid+'&nextMainId='+nextMainId+'&c='+cont, postdata, function(d){
			var replaceact = "";
			if(art == "sub" || act == "New") replaceact = "sub";

			if(parseAjaxAnwser(d,parentid,replaceact,true))
			  $(".ajaxwindow").dialog('close');
		  });
		},
		'Abbrechen': function() {
		  $(".ajaxwindow").dialog('close');
		}
	  });
	  if(art != 'sub' && el != 'container'){
		$(".ajaxwindow").dialog({
		  'beforeclose': function(){
			helpElement = '';
			$.get($('#cmsroot').html()+'admin/ajax.php', {
			  act: 'disableBlock',
			  id:id
			}, function(d){
			  if(d != 'Ok') debug(d);
			});
		  }
		});
	  }

	  setTimeout(function(){
		$(".ajaxwindow").dialog('open')
	  }, 100);

	//			var end = new Date().getTime(),
	//				time = end - start;
	//			alert('ElementAction: ' + time);
	});
  }

  $(".ajaxwindow").dialog('option' , "title" , $(this).attr('title'));
}

function reloadcontainer(cont,act,element,parentid,id){
  if(act == 'del'){
	$("#"+parentid).remove();
	initpage();
	return;
  }
  var seitenid;
  if(realAjaxId) seitenid = $('#ajaxid').html();
  else seitenid = $('#seitenid').html();
  if(act == 'edit' || act == 'edit') cont = id;
  $.get($('#cmsroot').html()+'admin/ajax.php', {
	'id': cont,
	'el': element,
	'act':'getContent',
	'm':seitenid
  }, function(d){
	
	if(act == 'del'){
	  $("#"+parentid).remove();
	}else if(act == 'New'){
	  $("#"+parentid).append(d);

	}else
	  $("#"+parentid).replaceWith(d);
  });
}

function initpage(pre,startX,zIndexstart){
  pre = pre || 'body :not(#admintoolbox) ';
  var i = 0;
  $(pre+".element").each(function(){
	$(this).attr('id','el'+i);
	i++;
  });


  $(pre+".element").unbind();
  $(pre+"div .button").unbind();

  $(pre+".element").each(function(){
	var s,levels,
	el = $(this),
	admineig = el.children('.admineig:first'),
	elTop,top,left;

	if(admineig.length != 1) return;
	elTop = el.offset().top;
	if(!el.hasClass('container')){
	  s = '#'+el.attr('id');
	  levels = 0;
	  while($(s + " .element").length > 0){
		s += " .element";
		top = $(s).offset().top;
		if(Math.abs(elTop-top) < 10)
		  levels++;
	  }
	}

	top = el.offset().top;
	left = el.offset().left;
	top = top-68;
//	top = top-112;
	if(!el.hasClass('container')){
	  //                        admineig.position({
	  //                            of: el,
	  //                            my: "left bottom",
	  //                            at: "left top",
	  //                            offset: "0 -"+(levels)*16+"px",
	  //                            collision: 'fit none',
	  //                            using: function(w){
	  //                                w.top -= 94;
	  //                                if(w.top < 0) w.top = 0;
	  //                                $(this).offset(w);
	  //                            }
	  //                        });
	  top = top-(levels)*16;
	}else{
	  /*
			 * Chrome fix
			 */
	  left = left+el.width()-32;

	//			var newoffset = {
	//				top:top-16,
	//				left:left+el.width()-32
	//			};


	//			debug(dump(newoffset));
	//			admineig.offset(newoffset);
	//			admineig.offset({top: 330, left:524});
	//			admineig.offset({top: 330, left:524});
	//			admineig.position({
	//				of: el,
	//				my: "left bottom",
	//				at: "left top",
	//				offset: "0 0",
	//				collision: 'fit'
	//			});
	}
	if(top < 0) top = 0;
	admineig.css({
	  top: top,
	  left:left
	});
  });


  $(pre+".element").mouseover(function(e){
	zIndexstart =  10;
	var elements = $(this).parents('.mainelement').andSelf(),
	i = 0;

	elements.each(function(){

	  $(this).children('.admineig').show();
	  $(this).addClass("activeElement");
	});
  });

  $(pre+".element").mouseout(function(){

	var id = $(this).attr('id').substr(2);
	$(this).children('.admineig').hide();
	$(this).removeClass("activeElement");
  });

  $(pre+".element").dblclick(function(event){
	if($(this).hasClass("mainelement")) $(this).children('.admineig').children('.sub').trigger('click');
	else $(this).children('.admineig').children('.edit').trigger('click');
	event.stopPropagation();
  }).mousedown(function (b) {
	if(b.which == 2){
	  $(this).children('.admineig').children('.edit').trigger('click');
	  return false;
	}
  });

  $(pre+"div .button").click(function(event){
	var act,
	el,
	id,
	spl,
	cont,
	art = '',
	parentid;

	if($(this).hasClass('edit')) act = 'edit';
	else if($(this).hasClass('sub')){
	  act = 'edit';
	  art = 'sub';
	}
	else if($(this).hasClass('del')) act = 'del';
	spl = $(this).attr('id').split('-');
	id = spl[2];
	el = spl[0];
	cont = spl[1];
	if(art == '')
	  parentid = $(this).parents('.element:first').parents('.mainelement:first').attr('id'); //.parents('.element:first')
	else
	  parentid = $(this).parents('.mainelement:first').attr('id'); //.parents('.element:first')
	if(typeof parentid == 'undefined'){
	  parentid = $(this).parents('.element').attr('id');
	  if(typeof parentid == 'undefined') parentid = -1;
	}
	elementaction(el, act,id,cont,art,parentid,'',0,$(this).parent().parent().attr('id'));
	event.stopPropagation();
  });

  return;
}
/*(function( $ ){
  $.fn.val = function() {
	if($(this).hasClass('multiselect'))
	  return $(this).text();
	else return $(this).val();
  };
})( jQuery );*/

jQuery.fn.getFormValues = function(){
  var formvals = {},tree,arr;
  $('textarea.editor').each(function(){
	$(this).ckeditor().ckeditorGet().updateElement();
  });
  $('div.gridBuild').each(function(){
	var gridname = $(this).attr('id');

	formvals[gridname] = new Array();
	formvals[gridname+'Top'] = new Array();
	formvals[gridname+'Left'] = new Array();
	formvals[gridname+'Width'] = new Array();
	formvals[gridname+'Height'] = new Array();
	$(this).find('.gridElement').each(function(){
	  //			formvals[gridname].push(PHPSerializer.serialize({
	  //				'top': $(this).offset().top,
	  //				'left': $(this).offset().left,
	  //				'width': $(this).width(),
	  //				'data': $(this).html()
	  //			}));
	  $(this).resizable('destroy');
	  $(this).draggable('destroy');
	  formvals[gridname].push(($(this).html()));
	  formvals[gridname+'Top'].push($(this).css('top'));
	  formvals[gridname+'Left'].push($(this).css('left'));
	  formvals[gridname+'Width'].push($(this).width());
	  formvals[gridname+'Height'].push($(this).height());
	});
  });
  tree = $(this).find('.treesort.moved');
  if(tree.length > 0){
	arr = tree.jstree('json_data').get_json( -1);
	formvals['treeAnordnung'] = PHPSerializer.serialize(arr);
  }

  formvals[this.attr('name')] = 'submit';
  jQuery.each(jQuery(':input',this).not('.dontpost :input').serializeArray(),function(i,obj){
	if(obj.name.substr(-2) == '[]'){
	  if (typeof formvals[obj.name] == 'undefined'){
		formvals[obj.name] = new Array();
		formvals[obj.name].push(obj.value);
	  }else{
		formvals[obj.name].push(obj.value);
	  }
	}else{
	  if (formvals[obj.name] == undefined)
              formvals[obj.name] = obj.value;
          else if (typeof formvals[obj.name] == Array || typeof formvals[obj.name] == "object")
              formvals[obj.name].push(obj.value);
          else formvals[obj.name] = new Array(formvals[obj.name],obj.value);
	}
  //		}
  });
  formvals[this.attr('id')] = 'submit';
  //	$('.treesort').each(function(i,obj){
  //		$.tree.reference($(this)).destroy();
  ////		formvals[$(this).attr('id')] = PHPSerializer.serialize($.tree.reference($(this)).get("", 'json'));
  //	});
  return formvals;
}

$(document).ready(function(){
  //	var start = new Date().getTime(),
  var	elcount = initpage('body :not(#admintoolbox) ');
  //	wmode="opaque"
  $('embed').each(function(){
	$(this).attr('wmode','transparent');
  });
  $('object').each(function(){
	$(this).append('<param name="wmode" value="transparent"></param>');
  });

  $('body').append('<div class="ajaxwindow"></div><div class="ajaxwindow2"></div><div id="loading"><span>Loading...</span></div><div id="UserMessage"><span></span></div>');

//  if(jQuery.browser['msie'] == true){
//	if(parseFloat(jQuery.browser['version']) < 8){
//	  $(".ajaxwindow").html('<span style="font-size: 18px;color:red">Ihre Browserversion ist zu alt. Bitte w&auml;hlen Sie sich <a href="http://www.browserchoice.eu/BrowserChoice/browserchoice_de.htm">hier</a> einen neueren Browser aus<br />Dieses System ist f&uuml;r Firefox und Google Chrome optimiert.</span>');
//	  $(".ajaxwindow").dialog({
//		autoOpen: true,
//		modal: true,
//		width: 'auto',
//		position: ['center','middle'],
//		beforeclose: function(){
//		  $('.ajaxwindow .editor').each(function(){
//			delete CKEDITOR.instances[$(this).attr('name')];
//		  });
//                  			$('.ajaxwindow .editorNewsletter').each(function(){
//			  delete CKEDITOR.instances[$(this).attr('name')];
//			});
//		}
//	  });
//	  $(".ajaxwindow").show();
//	  $(".ajaxwindow").dialog('open');
//	}
//
//  }


  $(".ajaxlink").click(function(){
	if($("#ajaxLinkWindow").length == '0')
	  $('body').append('<div id="ajaxoverlay"></div><div id="ajaxLinkWindow"><div id="ajaxclose"></div><div id="ajaxcontent"></div></div>');
	var link = $(this).attr('href');
	$('#ajaxoverlay').css('opacity', '0.5');
	$('#ajaxoverlay,#ajaxLinkWindow').show();
	$.get(link, {}, function(d){
	  realAjaxId = true;
	  $('#ajaxoverlay').height($('html').height());
	  $('html').scrollTop(0);
	  $("#ajaxcontent").html(d);
	  $("#ajaxLinkWindow").css('zIndex',20);
	  initpage('#ajaxLinkWindow ',elcount, 25);
	  $('#ajaxclose').click(function(){
		realAjaxId = false;
		$('#ajaxoverlay,#ajaxLinkWindow').hide();
		var x = elcount;
		while($('#hover'+x).length > 0){
		  $('#hover'+x).remove();
		  x++;
		}
	  });
	});
	return false;
  });
  $(".ajaxwindow").dialog({
	autoOpen: false,
	width: 'auto',
	position: ['center','middle'],
	beforeclose: function(){
	  $('.ajaxwindow .editor').each(function(){
		delete CKEDITOR.instances[$(this).attr('name')];
	  });
          			$('.ajaxwindow .editorNewsletter').each(function(){
			  delete CKEDITOR.instances[$(this).attr('name')];
			});
	}
  });
  $(".ajaxwindow2").dialog({
	autoOpen: false,
	width: 'auto',
	position: ['center','middle'],
	beforeclose: function(){
	  $('.ajaxwindow2 .editor').each(function(){
		delete CKEDITOR.instances[$(this).attr('name')];
	  });
          			$('.ajaxwindow .editorNewsletter').each(function(){
			  delete CKEDITOR.instances[$(this).attr('name')];
			});
	}
  });

  $('form,.ajaxwindow,.ajaxwindow2,#dialog').delegate('.multisel','change',function(){
	var name = $(this).attr('name').substring(0,$(this).attr('name').length-3),
	match = $('[name=\''+name+'\']').val().match(','+$(this).val()+',');
	if(match)
	  $('[name=\''+name+'but\']').val('-');
	else
	  $('[name=\''+name+'but\']').val('+');

  });
  $('form,.ajaxwindow,.ajaxwindow2,#dialog').delegate('.addremove','click',function(){
	var name = $(this).attr('name').substring(0,$(this).attr('name').length-3),
	eltxt = $('[name=\''+name+'txt\']'),
	el = $(this).parent().find('.multiSelValue'),
	sel1 =  $(this).parent().find('.multisel').val(),
	sel2 =  $(this).parent().find('.multisel  :selected').text(),
	add;

	if($(this).val() == '-'){
	  el.val(el.val().replace(','+sel1+',', ''));
	  if(el.val().substr(-1) != ',') el.val(el.val()+',');
	  if(el.val().substr(0,1) != ',') el.val(','+el.val());
	  el.val(el.val().replace(',,', ''));

	  eltxt.text(eltxt.text().replace(sel2, ''));
	  eltxt.text(eltxt.text().replace(',,', ''));
	  if(eltxt.text().substr(0,1) == ',') eltxt.text(eltxt.text().substring(1));
	  if(eltxt.text().substr(-1) == ',') eltxt.text(eltxt.text().substring(0,eltxt.text().length-1));
	}else{
	  add = '';
	  if(el.val() == '')el.val(',');
	  if(eltxt.text() != '')add = ',';
	  el.val(el.val()+sel1+',');
	  eltxt.text(eltxt.text()+add+sel2);
	}
	$(this).parent().find('.multisel').trigger('change');
  });
  $("#loading").ajaxStart(function(){
	$(this).show();
	$("#loading span").effect('pulsate',{},1000);
  });
  $("#loading").ajaxStop(function(){
	$(this).hide();
  });
  $('.newRow').click(function(){
	rowNew($(this));
  });
  tableReload();
  delegateParse();
  //	var end = new Date().getTime(),
  //		time = end - start;
  //	alert('Page Init: ' + time);

  parseMessages();

  if($('.editorBBC').length > 0)
	$('.editorBBC').ckeditor({
	  filebrowserBrowseUrl : 'index.php?mm=Dateimanager',
	  filebrowserWindowWidth : '100%',
	  filebrowserWindowHeight : '100%',
	  toolbar: 'Pdf',
	  extraPlugins: 'cmsvideo,linkcms,cmsinstantcrop,bbcode',
	  width:'300'
	});
  $('#setRes').val(debugGetResolution());
  $('#setBrowser').val(debugGetBrowser());
  $('#setFlash').val(debugHasFlash());

  if($('#debug').html().length > 3){
	debug("",true);
  }
  $('body').find('textarea.editor').ckeditor({
	  filebrowserBrowseUrl : 'index.php?mm=Dateimanager&maxwidth='+mWidth,
	  filebrowserWindowWidth : '100%',
	  filebrowserWindowHeight : '100%',
	  height: 320
	});
//    $('.link.newRow').each(function(){
//       var firstCol = $(this).siblings('table:first').find('th:first'); 
//       firstCol.html('[+]'+firstCol.html());
//    });
    $("table").stickyTableHeaders({
        fixedOffset : 110
    });
    $("table").colDelete({
        checkBoxContainer: ".deleteRows",
        dontDeleteLast : true,
        dontDeleteFirst : true
    });
    $("table").delegate('tr','mouseover',function(){
       $(this).find('.hoverAction').show(); 
    }).delegate('tr','mouseout', function(){
       $(this).find('.hoverAction').hide(); 
    });
    $('.multiselect').multiselect({
            minWidth: 300,
            height: 400,
            selectedText: "# ausgewählt",
            checkAllText: "Alle auswählen",
            uncheckAllText: "Alle abwählen",
            noneSelectedText: "Bitte auswählen"
        })
        .multiselectfilter({
            placeholder: "Filter Text eingeben"
        });
});

$(window).resize(function() {
  initpage();
});

function tableReload(){
  $('.sort').tablesorter({headers:{0: { sorter: false }}});
  $(".sort").bind("sortEnd",function() {
	$(this).find('tr').removeClass('even');
	$(this).find('tr:even:not(.headdiv)').addClass('even');
  });
  $('table').delegate('.editRow', 'click',function(){
	rowEdit($(this));
  });

  $('table').delegate('.delRow', 'click',function(){
	var spl = $(this).attr('id').split('-'),
	id = spl[2],
	db = spl[1],
	menu = spl[0],
	thisel = $(this);

	var title = $(this).parents('tr:first').attr('title');
	if(title != '')
	  $(".ajaxwindow").html('Sicher das sie "'+title+'" löschen m&ouml;chten ?');
	else
	  $(".ajaxwindow").html('Sicher das sie dieses Element löschen m&ouml;chten ?');
	$(".ajaxwindow").dialog('option', 'buttons', {
	  'Löschen': function() {
		$.get($('#cmsroot').html()+'admin/ajax.php', {
		  'kl': menu,
		  'del':id,
		  'db':db
		}, function(d){
		  if(parseAjaxAnwser(d, thisel.parents('tr:first'),'Del'))
			$(".ajaxwindow").dialog('close');
		});
	  },
	  'Abbrechen': function() {
		$(".ajaxwindow").dialog('close');
	  }
	});
	$(".ajaxwindow").dialog('option' , 'width', 'auto');
	$(".ajaxwindow").dialog('open');
  });
}
function rowEdit(butel,zus){
  zus = zus || new Array();
  var spl = butel.attr('id').split('-'),
  id = spl[2],
  db = spl[1],
  menu = spl[0],
  querystr =  {
	'kl':menu,
	'edit':id,
	'db':db
  },
  tablerow = butel.parents('tr:first'),
  id2;

  for(id2 in zus){
	querystr[id2] = zus[id2];
  }
  $.get($('#cmsroot').html()+'admin/ajax.php', querystr,function(d){
	//		var start = new Date().getTime();
	//			$('#editnewconent').html(d);
	//			$('#editnewconent').show();
	$(".ajaxwindow").html(d);
	$(".ajaxwindow .onchangechangedialog").each(function(){
	  $(this).find(':input').change(function(){
		var name = 'change'+$(this).attr('name'),
		zus = new Array();
		zus[name] = $(this).val();
		rowEdit(butel,zus);
	  });
	});
	$(".ajaxwindow #tabs").tabs();
	$(".ajaxwindow").dialog('option', 'buttons', {
	  'Speichern': function() {
		var form = $(".ajaxwindow").find('form'),
		postdata = form.getFormValues(),
		action = form.attr('action');
		$.post(action, postdata, function(d){

		  if(parseAjaxAnwser(d, tablerow, 'Edit'))
			$(".ajaxwindow").dialog('close');
		//					if(d != 'Ok'){alert(d);debug(d);}
		//					else{
		//						sendUserMessage('Eintrag wurde erfolgreich gespeichert');
		//						$(".ajaxwindow").dialog('close');
		//
		//					}
		});
	  },
	  'Abbrechen': function() {
		$(".ajaxwindow").dialog('close');
	  }
	});
	$(".ajaxwindow").dialog('option' , 'width', 'auto');
	$(".ajaxwindow").dialog('open');
  //		var end = new Date().getTime();
  //		var time = end - start;
  //		alert('ElementAction: ' + time);
  });
}
function rowNew(butel,zus){
  zus = zus || new Array();
  var spl = butel.attr('id').split('-'),
    db = spl[1],
    kl = spl[0],
    id = spl[2],
    querystr =  {
            'kl': kl,
            'new':'1',
            'db':db,
            'id':id
    },
//  table = butel.siblings('.divTableWithFloatingHeader').children('table'),
    table = butel.parents('table:first'),
    sid;
//  alert(butel.attr('id')+":"+table.length);
  if(table.length == 0)
  table = butel.siblings('.divTableWithFloatingHeader').children('table');
//  alert(butel.attr('id')+":"+table.length);
//  table = table.attr('id');
  for(sid in zus){
	querystr[sid] = zus[sid];
  }
  $.get($('#cmsroot').html()+'admin/ajax.php', querystr,function(d){

	$(".ajaxwindow").html(d);
	$(".ajaxwindow .onchangechangedialog").each(function(){
	  $(this).find(':input').change(function(){
		var name = 'change'+$(this).attr('name'),
		zus = new Array();
		zus[name] = $(this).val();
		rowNew(butel,zus);
	  });
	});
	$(".ajaxwindow #tabs").tabs();
	$(".ajaxwindow").dialog('option', 'buttons', {
	  'Speichern': function() {
		var form = $(".ajaxwindow").find('form'),
		postdata = form.getFormValues(),
		action = form.attr('action');
		$.post(action, postdata, function(d){
		  if(parseAjaxAnwser(d, table, 'New'))
			$(".ajaxwindow").dialog('close');
		//					if(d != 'Ok'){alert(d);debug(d);}
		//					else{
		//						sendUserMessage('Eintrag wurde erfolgreich hinzuge&uuml;gt');
		//						$(".ajaxwindow").dialog('close');
		//					}
		});
	  },
	  'Abbrechen': function() {
		$(".ajaxwindow").dialog('close');
	  }
	});
	$(".ajaxwindow").dialog('option' , 'width', 'auto');
	$(".ajaxwindow").dialog('open');
  });
}


function crop_eig(width, height, strictheight){
  if(width == 0){
	cropapi.setOptions({
	  minSize: [0, 0],
	  aspectRatio: 0,
	  allowSelect: true
	});
	$('#previewdiv').hide();
	$('#prevtext').hide();
	aktcropset = 0;
	return;
  }
  $('#previewdiv').show();
  $('#prevtext').show();
  aktcropset = new Array(width,height,strictheight);
  cropapi.setSelect([1,1,width,height]);
  $('#previewdiv').width(width);
  $('#previewdiv').height(height);
  if(!strictheight){
	cropapi.setOptions({
	  minSize: [width, 0],
	  aspectRatio: 0,
	  allowSelect: false

	});
  }else{
	cropapi.setOptions({
	  minSize: [width, height],
	  aspectRatio: width/height,
	  allowSelect: false
	});
  }

}


function showPreview(coords){
  if(aktcropset == 0){
	cropsub[0] = coords.x;
	cropsub[1] = coords.y;
	cropsub[2] = coords.w;
	cropsub[3] = coords.h;
	cropsub[4] = 0;
	return;
  }
  if (parseInt(coords.w) > 0){
	var rx = 1;
	var ry = 1;
	var mtop;
	var height;
	rx =  aktcropset[0] / coords.w;
	if(aktcropset[2]){
	  ry =  aktcropset[1] / coords.h;
	  height = Math.round(ry * $('#cropit').height()) + 'px';
	  mtop = Math.round(ry * coords.y);
	}else{
	  ry =  aktcropset[1] / coords.h;
	  height = Math.round($('#cropit').height()*rx) + 'px';
	  $('#previewdiv').height(coords.h*rx+ 'px');
	  mtop = Math.round(coords.y*rx);
	}

	$('#preview').css({
	  width: Math.round(rx * $('#cropit').width()) + 'px',
	  height: height,
	  marginLeft: '-' + Math.round(rx * coords.x) + 'px',
	  marginTop: '-' + mtop + 'px'
	});
	cropsub[0] = coords.x;
	cropsub[1] = coords.y;
	cropsub[2] = coords.w;
	cropsub[3] = coords.h;
	cropsub[4] = aktcropset[0];
  }
}

function html2BBCode( html, fixForBody ){
  var pattern = /\[br\]|\[.*?\].*?\[\/.*?\]/,
  i = 0;
  // Convert < and > to their HTML entities.
  //
  // Convert <br> to line breaks.
  html = html.replace(/<p>/gi,"");
  html = html.replace(/<\/p>/gi,"\n");
  html = html.replace(/&nbsp;/gi," ");

  // [url]
  html = html.replace( /<a .*?href=(["'])(.+?)\1.*?>(.+?)<\/a>/gi, '[url=$2]$3[/url]') ;
  //html = html.replace(/<a.*?href=\"(.*?)\".*?>(.*?)<\/a>/gi,"[url=$1]$2[/url]");

  // [b]
  html = html.replace( /<(?:b|strong)>/gi, '[b]') ;
  html = html.replace( /<\/(?:b|strong)>/gi, '[/b]') ;

  // [i]
  html = html.replace( /<(?:i|em)>/gi, '[i]') ;
  html = html.replace( /<\/(?:i|em)>/gi, '[/i]') ;

  // [u]
  html = html.replace( /<u>/gi, '[u]') ;
  html = html.replace( /<\/u>/gi, '[/u]') ;

  // [img]
  //html = html.replace(/<img.*?src=\"(.*?)\".*?\/>/gi,"[img]$1[/img]");
  html = html.replace( /<img .*?src=(["'])(.+?)\1.*?\/>/gi, '[img]$2[/img]') ;
  html = html.replace( /<img .*?src=(["'])(.+?)\1.*?>/gi, '[img]$2[/img]') ;

  // [quote]
  html = html.replace( /<blockquote>/gi, '[quote]') ;
  html = html.replace( /<\/blockquote>/gi, '[/quote]') ;

  // [code]
  html = html.replace( /<code>/gi, '[code]') ;
  html = html.replace( /<\/code>/gi, '[/code]') ;

  while(html.match(/<span style=\"color: ?(.*?);\">(.*)<\/span>/mi) || html.match(/<span style=\"font-size: ?(.*?)px;\">(.*)<\/span>/mi)){
	// [color]
	html = html.replace(/<span style=\"color: ?(.*?);\">(.*)<\/span>/mi,"[color=$1]$2[/color]");
	html = html.replace(/<font.*?color=\"(.*?)\".*?>(.*)<\/font>/mi,"[color=$1]$2[/color]");

	// [size]
	html = html.replace(/<span style=\"font-size: ?(.*?)px;\">(.*)<\/span>/mi,"[size=$1]$2[/size]");

	i++;
  }
  html = html.replace( /<br(?=[ \/>]).*?>/gi, '\r\n') ;
  // Remove remaining tags.
  html = html.replace( /<[^>]+>/g, '') ;

  return html;

}
function bbCode2html( data, fixForBody ){
  var pattern = /\[br\]|\[.*?\].*?\[\/.*?\]/,
  i = 0;
  //                alert(data);
  // Convert < and > to their HTML entities.
  while(data.match(pattern) && i < 5){
	//                    alert(i+":"+data);
	data = data.replace( /</g, '&lt;' ) ;
	data = data.replace( />/g, '&gt;' ) ;

	// Convert line breaks to <br>.
	data = data.replace( /(?:\r\n|\n|\r)/g, '<br>' ) ;

	// [url]
	data = data.replace( /\[url\](.+?)\[\/url]/gi, '<a href="$1">$1</a>' ) ;
	data = data.replace( /\[url\=([^\]]+)](.+?)\[\/url]/gi, '<a href="$1">$2</a>' ) ;

	// [b]
	data = data.replace( /\[b\](.+?)\[\/b]/gi, '<b>$1</b>' ) ;

	// [i]
	data = data.replace( /\[i\](.+?)\[\/i]/gi, '<i>$1</i>' ) ;

	// [u]
	data = data.replace( /\[u\](.+?)\[\/u]/gi, '<u>$1</u>' ) ;

	// [img]
	data = data.replace(/\[img\](.*?)\[\/img\]/gi,'<img src="$1" />');

	// [quote]
	//data = data.replace( /\[quote\](.+?)\[\/quote]/gi, '<quote>$1</quote>' ) ;
	data = data.replace( /\[quote\]/gi, '<blockquote>' ) ;
	data = data.replace( /\[\/quote]/gi, "</blockquote> \n" ) ;

	// [code]
	data = data.replace(/\[code\]/gi,'<code>');
	data = data.replace(/\[\/code\]/gi,'</code>');

	// [color]
	data = data.replace(/\[color=(.*?)\](.*?)\[\/color\]/gi,'<span style="color: $1">$2</span>');

	// [size]
	data = data.replace(/\[size=(.*?)\](.*?)\[\/size]/gi,'<span style="font-size: $1px;">$2</span>');
	i++;
  }

  return data;
}

/* statsfunctions */

$(function() {

    $('input.statsdatepicker').datepicker({
        'dateFormat' : 'dd.mm.yy'
    });

    var changeStatsDate = function () {
        var url = '/admin/index.php?mm=stats';
        var period = $('.statsPeriod').val();
        var date = $('.statsdatepicker').val();
        
        window.location.href = url + '&period=' + period + '&date=' + date.substr(6, 4) + "-" + date.substr(3, 2) + "-" + date.substr(0, 2);
    }
    
    $('.statsdatepicker').change(changeStatsDate);
    $('.statsPeriod').change(changeStatsDate);
    
    
    $('#newsletterSelect').change(function () {
        window.location.href = '/admin/index.php?mm=newsletterTemplates&template=' + $(this).val();
    });
    
    $(".markTable").find("tr").click(function () {
        
        $(".markedTd").html('');
        
        $(this).find("td:last").html("«");
        $(this).find("td:last").addClass("markedTd");
    });
    $('table.sort tr').dblclick(function(){
        $(this).find('.editRow').trigger('click');
    });
//    statistikSetup();
});

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}

function statistikSetup(){
    
        $( ".column" ).sortable({
                connectWith: ".column"
        });

        $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                        .addClass( "ui-widget-header ui-corner-all" )
                        .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
                        .end()
                .find( ".portlet-content" );

        $( ".portlet-header .ui-icon" ).click(function() {
                $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
                $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
        });
        var height = "";
        $(".piwikIframe" ).load(function(){
//            alert($(this).contents().find("html").height().offsetHeight);
            $(this).css('height', 'auto');
//            alert($(this).children('body').height()+"!");
        });
//        alert(height);
        
}


function createCookie(name,value,days) {
    var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function cropImage(img,imgeig,forceeig,msg,dialog){
    dialog = dialog || $("#dialog");
    forceeig = forceeig || false;
    msg = msg || '';
    var previews = '',
    dateiid,inDM = true;
    var datei = img.parents('.datei');
    if(datei.length == 0) datei = img.parents('.dateiBig');
    if(datei.length == 0){
        var cut = $('#cmsroot').attr('id').length+8;
        dateiid = img.attr('src').substr(cut,img.attr('src').length-cut);
        inDM = false;

    }else dateiid = datei.attr('id');
    dialog.dialog('option', 'title', 'Bild editieren');
    dialog.html(msg+'<div id="previewbuttons"></div><div id="imageedit"><img src="'+img.attr('src')+'" alt="" id="cropit" /><span id="prevtext">Vorschau:<br /></span><div id="previewdiv"><img src="'+img.attr('src')+'" id="preview" /></div></div>');

    $("#cropit").load(function(){

        cropapi = $.Jcrop('#cropit', {
            onChange: showPreview,
            onSelect: showPreview
        });
        if(!forceeig) previews += '<input type="button" value="Frei" onclick="crop_eig(0,0,0)" />';
        for (var name in imgeig){
            if($('#cropit')[0].naturalWidth > imgeig[name][0]){
                previews += '<input type="button" value="'+name+'" onclick="crop_eig('+imgeig[name][0]+','+imgeig[name][1]+','+imgeig[name][2]+')" />';
            }
            if(forceeig){
                crop_eig(imgeig[name][0],imgeig[name][1],imgeig[name][2]);
            }
        }
        $('#previewbuttons').html(previews);
    });
    dialog.dialog('option', 'buttons', {
        'Orginalbild überschreiben': function() {
            var dialog = $(this);
            $.post('ajax.php?klasse=dateimanager&act=crop',{
                src: dateiid,
                cropx: cropsub[0],
                cropy: cropsub[1],
                cropw: cropsub[2],
                croph: cropsub[3],
                solw: cropsub[4]
            }, function(d){
                if(d != 'Ok') debug(d);
                img.attr('src', img.attr('src')+"?t=" + Math.random());
                dialog.dialog('close');
                if(!inDM){
                    img.css('width', '');
                    img.css('height', '');
                }
                if($('.selectedFile').length){
                    //					$(".selectedFile").trigger('dblcick');
                    returnToForm($(".selectedFile").attr('id')+"?t=" + Math.random());
                }
            });
        },
        'Thumbnail erstellen': function() {
            var index = dateiid.lastIndexOf('.');
            var dialog = $(this);
            var endWidth = cropsub[2];
            var endHeight = cropsub[3];
            //			alert('DEBUG'+dump(cropsub));
            if(cropsub[2] > cropsub[4]){
                var fak = cropsub[4]/cropsub[2];
                endWidth = cropsub[4];
                endHeight = Math.round(cropsub[3]*fak);
            }
            var newname = dateiid.substring(0,index)+'_thumb('+endWidth+'x'+endHeight+')'+dateiid.substring(index);

            index = dateiid.lastIndexOf('/');
            var realnewname = newname.substring(index+1);
            $.post('ajax.php?klasse=dateimanager&act=crop',{
                src: dateiid,
                cropx: cropsub[0],
                cropy: cropsub[1],
                cropw: cropsub[2],
                croph: cropsub[3],
                solw: cropsub[4],
                newname: newname
            }, function(d){
                if(d != 'Ok') debug(d);
                dialog.dialog('close');
                if(!inDM){
                    img.css('width', '');
                    img.css('height', '');
                    img.attr('src', $('#cmsroot').html()+'dateien/'+newname);
                    img.attr('_cke_saved_src', $('#cmsroot').html()+'dateien/'+newname);
                }
                var newdatei = new Array();
                newdatei['name'] = realnewname;
                newdatei['art'] = 'img';
                newdatei['pfad'] = $(".selected").attr('id');
                newdatei['pfad'] = newname;

                if(inDM){
                    if($('.selectedFile').length){
                        returnToForm(newname);
                    }else{
                        dateienarchiv[$(".selected").attr('id')].push(newdatei);
                        dateibuild($(".selected").attr('id'));
                    }
                }
            });

        },
        'Bild trotzdem in Orginalgröße einfügen': function() {
            var dialog = $(this);
            img.attr('src', img.attr('src')+"?t=" + Math.random());
            dialog.dialog('close');
            if($('.selectedFile').length){
                returnToForm($(".selectedFile").attr('id')+"?t=" + Math.random());
            }
        },
        'Abbrechen': function() {
            $(this).dialog('close');
        }
    });
    dialog.dialog('open');

    dialog.dialog('option',
        'maxWidth', $(window).width()
        );
}