(function ($) {
	$.colDelete = function (el, options) {
		// To avoid scope issues, use 'base' instead of 'this'
		// to reference this class from internal events and functions.
		var base = this;
                var offsetTop = 94;

		// Access to jQuery and DOM versions of element
		base.$el = $(el);
		base.el = el;

		// Cache DOM refs for performance reasons
		base.$window = $(window);
		base.$clonedHeader = null;
		base.$originalHeader = null;

		// Add a reverse reference to the DOM object
		base.$el.data('colDelete', base);

		base.init = function () {
			base.options = $.extend({}, $.colDelete.defaultOptions, options);

		base.$el.each(function () {
                            var $this = $(this);
                            var checkBoxContainer  = $(base.options.checkBoxContainer);
                            var checkBoxHtml = '<div class="arrowText">Ansicht anpassen:</div>';
                            var addSel = "";
                            if(base.options.dontDeleteLast) addSel += ":not(:last)";
                            if(base.options.dontDeleteFirst) addSel += ":not(:first)";
                            $this.find('tr:first').children('th'+addSel).each(function(){ 
                                var name = $(this).html();
//                                var name = $(this).attr('id').substr(6);
                               checkBoxHtml += '<div class="checkbox"><input type="checkbox" name="deleteCol" class="deleteCol" id="delete'+name+'" checked="checked" value="1" />'+name+'</div>'; 
                            });
                            checkBoxContainer.html(checkBoxHtml);
                            checkBoxContainer.find('.deleteCol').change(function(){
                                var index = $(this).parent().index();
                                if(!base.options.dontDeleteFirst) index--;
                                if($(this).is(':checked')){
                                    $this.find('tr').find('td:eq('+index+')').show();
                                    $this.find('tr').find('th:eq('+index+')').show();                                    
                                }else{
                                    $this.find('tr').find('td:eq('+index+')').hide();
                                    $this.find('tr').find('th:eq('+index+')').hide();
                                }
                                var elVal = "";
                                checkBoxContainer.find('input:not(:checked)').each(function(){
                                    elVal += ","+$(this).attr('id');
                                });
                                elVal = elVal.substr(1)
                                createCookie($this.attr('id')+'Cols',elVal,365*10);
                            });
                            
                            var cookie = readCookie($this.attr('id')+'Cols');
                            if(cookie !== null){
                                var delCols = cookie.split(',');
                                for(var index in delCols){
                                    if(delCols[index] != ""){
                                        console.log(delCols);
                                        console.log(delCols[index]);
                                        checkBoxContainer.find('#'+delCols[index]).attr('checked',false).trigger('change');  
                                    }
                                }
                            }
                            
			});
                        
		};

		// Run initializer
		base.init();
	};

	$.colDelete.defaultOptions = {
		checkBoxContainer: "",
                dontDeleteLast : false,
                dontDeleteFirst : false
	};

	$.fn.colDelete = function (options) {
		return this.each(function () {
			(new $.colDelete(this, options));
		});
	};

})(jQuery);
