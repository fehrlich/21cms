var imgTypes = new Array('jpg', 'gif', 'png');
var supportedFiles = new Array('jpg', 'gif', 'png');

var dmViewModel = function(){
    var self = this;
    self.rootFolder = new dmFolder("Root","",true);


    self.selectedFile = ko.observable();
    self.fileSelected = ko.observable(false);
    self.allowFileCommit = true;
    self.allowFolderCommit = true;
    self.updating = false;
    self.selectedFolder = ko.observable();

    //LOAD UI DESIGN
    var headerHeight = 25;
    var useHeight = $(document).height()-110-40;
    var headers = $('#folderHeader,#fileHeader,#previewHeader');
    var content = $('#folderContent,#fileContent,#dateien2')

    $('#dateimanager,#folderDiv,#fileDiv,#previewDiv').height(useHeight);
    content.height(useHeight-headerHeight).css("overflow","auto").css("width", "100%");
//    console.log("useHeight",useHeight);
    //    self.selectedView = new dmSelectedView(self.rootFolder.files());
    var loadingFiles = new Array();
    var renameFiles = new Array();
    $('#newFolderBut').button();
    $('#uploadButton').button().click(function(){
        $('#newFileDiv  .qq-upload-button > input')
        .trigger("click");
    });
    self.uploader = $('#newFileDiv').fineUploader({
        request: {
            endpoint: 'ajax.php'
        }
    }).on('error', function(event, id, filename, reason) {
        alert("uploaderror:"+reason);
    })
    .on('complete', function(event, id, fileName, responseJSON){
        var file = viewModel.selectedFolder().getFile(fileName);
        var indexend = fileName.lastIndexOf('.')+1;
        var endung = fileName.substring(indexend);
        for(var i = 0; i < imgTypes.length; i++) {
            if(imgTypes[i] == endung.toLowerCase()) endung = "img";
        }
        if(!file)
            viewModel.selectedFolder().files.push(new dmFile(fileName,viewModel.selectedFolder().path()+"/"+fileName,self.selectedFolder(),endung));
        else{
            var d = new Date();
            file.addTime("?t="+d.getTime());

        }
    }).on('submit', function(event, id, fileName) {
            var i = 2;
            if(viewModel.selectedFolder().getFile(fileName)){
                //                while(viewModel.selectedFolder().getFile(fileName+i)){
                //                    i++;
                //                }
                //                var newName = prompt("Die Datei existiert bereits! Möchten Sie die Datei umbenennen ?", fileName);
                ////                $('#dialog').html('Dateiname existiert bereits').dialog('option', 'buttons', {
                ////                   'Überschreiben': function(){
                ////                       return true;
                ////                   },
                ////                   'Abbrechen': function(){
                ////                       return false;
                ////                   },
                ////                   'Umbenennen': function(){
                ////                       return fileName+i;
                ////                   }
                ////                });
                ////                $('#dialog').html('Dateiname existiert bereits').dialog('option', 'modal', true);
                ////                $('#dialog').dialog('open');
                //                if(test == null || test == false)
                //                    return false;
                //                else if(fileName != newName)
                //                    renameFiles[fileName] = newName
                return confirm("Die Datei '"+fileName+"' exestiert bereits. Möchten Sie die Datei überschreiben?");
            }
    });
    ;

//    self.uploader = new qq.FileUploader({
//        element: document.getElementById('newFileDiv'),
//        action: 'ajax.php',
//        onSubmit: function(id, fileName){
//            var i = 2;
//            if(viewModel.selectedFolder().getFile(fileName)){
//                //                while(viewModel.selectedFolder().getFile(fileName+i)){
//                //                    i++;
//                //                }
//                //                var newName = prompt("Die Datei existiert bereits! Möchten Sie die Datei umbenennen ?", fileName);
//                ////                $('#dialog').html('Dateiname existiert bereits').dialog('option', 'buttons', {
//                ////                   'Überschreiben': function(){
//                ////                       return true;
//                ////                   },
//                ////                   'Abbrechen': function(){
//                ////                       return false;
//                ////                   },
//                ////                   'Umbenennen': function(){
//                ////                       return fileName+i;
//                ////                   }
//                ////                });
//                ////                $('#dialog').html('Dateiname existiert bereits').dialog('option', 'modal', true);
//                ////                $('#dialog').dialog('open');
//                //                if(test == null || test == false)
//                //                    return false;
//                //                else if(fileName != newName)
//                //                    renameFiles[fileName] = newName
//                return confirm("Die Datei '"+fileName+"' exestiert bereits. Möchten Sie die Datei überschreiben?");
//            }
//        },
//        onProgress: function(id, fileName, loaded, total){
//        //            var tmpFile = loadingFiles[fileName];
//        //            if(!tmpFile){
//        //                console.log('create');
//        //                tmpFile = new dmFile(fileName,viewModel.selectedFolder().path()+""+fileName,viewModel.selectedFolder());
//        //                viewModel.selectedFolder().files.push(tmpFile);
//        //                loadingFiles[fileName] = tmpFile
//        //            }
//        //            var loadedPro = Math.round((loaded/total)*100);
//        ////            console.log(loaded+":"+total);
//        //            tmpFile.isLoading(loadedPro);
//        },
//        params:{
//            'klasse':'dateimanager',
//            'act':'uploadPost',
//            'sessionid':sessionid,
//            'folder': '/'
//        },
//        onComplete: function(id, fileName, responseJSON){
//            var file = viewModel.selectedFolder().getFile(fileName);
//            var indexend = fileName.lastIndexOf('.')+1;
//            var endung = fileName.substring(indexend);
//            for(var i = 0; i < imgTypes.length; i++) {
//                if(imgTypes[i] == endung.toLowerCase()) endung = "img";
//            }
//            if(!file)
//                viewModel.selectedFolder().files.push(new dmFile(fileName,viewModel.selectedFolder().path()+"/"+fileName,self.selectedFolder(),endung));
//            else{
//                var d = new Date();
//                file.addTime("?t="+d.getTime());
//
//            }
//        }
//    });

    self.updateContent = function(path,onReady){
        if(path != ""){
            self.openGlobalPath(path,function(){
                callCallback(onReady)
            });
        }else{
            self.rootFolder.updateContent(true,function(){
                callCallback(onReady)
            });
        }
    }
    self.folderView = function(){
        self.fileSelected(false);
    }

    self.openGlobalPath = function(path,onReady){
        var d = new Date();
        $.post('ajax.php?klasse=dateimanager&act=get_foldercontentsGlob'+'&pfad='+path + '&' + d.getTime(), {
            pfad: path
        }, function(d){
            if(d == 'Error:NoValidPath'){
                self.selectedFolder(self.rootFolder);
                return;
            }
            var currentFolder = self.rootFolder;
            var tmpFolder = currentFolder;
            var selFolder = currentFolder;
            var selected = false;
            var lvl = 0;

            tmpFolder.isUpdated = 2;
            $(d).find('el').each(function(){
                if($(this).attr('level') > lvl){
                    tmpFolder = self.getFolder($(this).attr('pfad'));
                    currentFolder = tmpFolder;
                }
                if($(this).attr('isdir') == 1){
                    tmpFolder = new dmFolder($(this).attr('name'),$(this).attr('pfad'),currentFolder);
                    tmpFolder.isUpdated = 0; // no idea, aber mit 0 gehts immerhin, ansonst lädt der unterordner nicht sauber nach im DM!
                    currentFolder.subFolders.push(tmpFolder);
                    if($(this).attr('pfad') == path){
                        selected = true;
                        self.selectedFolder(tmpFolder);
                        tmpFolder.isUpdated = 1;
                        selFolder = tmpFolder;
                    }
                }else{
                    if(!selected){
                        selected = true;
                        var f = self.getFolder(path);
                        self.selectedFolder(f);
                        f.isUpdated = 1;
                        selFolder = f;
                    }
                    selFolder.files.push(new dmFile($(this).attr('name'),$(this).attr('pfad'),selFolder,$(this).attr('art')));
                }
                if(!selected){
                    self.selectedFolder(tmpFolder);
                }
            });
            callCallback(onReady);
        });
    }

    self.getFolder = function(path){
        var spl = path.split("/");
        var tmpF = self.rootFolder;
        for(var index in spl){
            var tmp = tmpF.getSubFolder(spl[index]);
            if(tmp)
                tmpF = tmp;
            else
                return tmpF;
        }
        return tmpF;
    }

    self.selectedFolder.subscribe(function(newValue){
        self.uploader.fineUploader('setParams', {
            'klasse':'dateimanager',
            'act':'uploadPost',
            'sessionid':sessionid,
            'folder': newValue.path()
        });
    });
}

var dmFile = function(name,path,parentFolder,type){
    var self = this;

    self.name = ko.observable(name);
    self.path = ko.observable(path);
    self.type = ko.observable(type);

    self.parentFolder = parentFolder;

    if(type != 'img'){
        self.icon = ko.observable(type.toLowerCase());
        $('<img src="/admin/images/icons/fileTypes/file_extension_'+type.toLowerCase()+'.png" alt="" />').error(function() {
            self.icon = ko.observable("empty");
        });
    }else
        self.icon = ko.observable(false);

    self.size = 0;
    self.info = "0";
    self.addTime = ko.observable("");

    self.isLoading = ko.observable(100);


    self.selectFile = function(){
        viewModel.selectedFile(self);
        viewModel.fileSelected(true);
    }
    self.renameDialog = function(){
        var indexend = self.name().lastIndexOf('.')+1;
        var name = self.name().substring(0,indexend-1);
        var endung = self.name().substring(indexend);
        var folderPath = self.path().substr(0,self.path().length-self.name().length)
        $("#dialog").dialog('option', 'title', 'Datei umbenennen');
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';

        $("#dialog").html('Name: <input type="text" id="newname" value="'+name+'" />.'+endung+fileOccurrence);
        activateOccurenceSeachButton(self.path());
        $("#dialog").dialog('option', 'buttons', {
            'Umbenennen': function() {
                var newName = $("#dialog #newname").val();
                $.post('ajax.php?klasse=dateimanager&act=rename', {
                    'src': self.path(),
                    'newname': newName,
                    'endung':endung
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        self.name(newName+"."+endung);
                        self.path(folderPath+newName+"."+endung);
                    }

                });
                $(this).dialog('close');
            },
            'Sicheres Umbenennen (Beta)': function() {
                var newName = $("#dialog #newname").val();
                $.post('ajax.php?klasse=dateimanager&act=rename&opt=save', {
                    'src': self.path(),
                    'newname': newName,
                    'endung':endung
                }, function(d){
                    self.name(newName);
                    self.path(folderPath+newName+"."+endung);
                });
                $(this).dialog('close');
            },
            'Abbrechen': function() {
                $(this).dialog('close');
            }
        });
        $("#dialog").dialog('open');
    }

    self.imageEditDialog = function(){
        var img = $('<img src="'+$('#cmsroot').html()+'dateien/'+self.path()+'" alt="" />'),
        //        var img = $(this).parent().parent().find('.dateithumb'),
        imgeig = new Array();

        imgeig = eval("("+$.ajax({
            async: false,
            url: $('#cmsroot').html()+'admin/ajax.php',
            data: {
                act:'ajaxCropEig'
            },
            dataType: 'json'
        }).responseText+")");
        cropImage(img, imgeig);
    }
    self.fileEditDialog = function(){
        var path = self.path().replace('+','%2B').replace('&','%26');
        var datei = $(this).parent().parent();
        var dialog = $("#dialog").dialog('option', 'title', 'Dateieigenschaften &auml;ndern');
        $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+path, function(d){
            dialog.html(d);
            dialog.dialog('option', 'buttons', {
                'Speichern': function() {
                    var postdata = dialog.find('form').getFormValues();
                    $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+path, postdata,function(d2){
                        dialog.dialog('close');
                        if(d2 != 'Ok')
                            debug(d2);
                    });

                }
            });
        });
        dialog.dialog('open');
    }
    self.fileDeleteDialog = function(){
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';

        $("#dialog").dialog('option', 'title', 'Datei l&ouml;schen');
        $("#dialog").html('Sind Sie sicher das sie die Datei \''+self.path()+'\' l&ouml;schen m&ouml;chten?'+fileOccurrence);
        activateOccurenceSeachButton(self.path());

        $("#dialog").dialog('option', 'buttons', {
            'Ja': function() {
                $.post('ajax.php?klasse=dateimanager&act=delete', {
                    src: self.path()
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        viewModel.fileSelected(false);
                        viewModel.selectedFile(null);
                        self.parentFolder.files.remove(self);
                    }
                });
                $(this).dialog('close');
            },
            'Nein': function() {
                $(this).dialog('close');

            },
            'Abbrechen': function() {
                $(this).dialog('close');
            }
        });
        $("#dialog").dialog('open');
    }
    self.commitFile = function(){

        if(!viewModel.allowFileCommit) return;

        var check = true,mw,mh;

        mw = $.query.get('maxwidth');
        mh = $.query.get('maxheight');
        if(mh == '' || mh === true) mh = 0;
        if(mw == '' || mh === true) mw = 0;
        if(mw != 0 || mh != 0){
            check = checkImage($('<img src="'+self.path()+'" alt="" />'),mw,mh);
        }
        createCookie('cmsLastDMSelection',self.path(),30);
        if(check){
            returnToForm(self.path(),getURLParameter("absoluteLinks"));
        }
        return;
    }
}

var dmFolder = function(name,path,isroot,parentFolder){
    var self = this;
    var filteradd = "";
    name = name || "";
    path = path || "";

    self.name = ko.observable(name);
    self.path = ko.observable(path);
    self.files = ko.observableArray();
    self.subFolders = ko.observableArray();

    self.parentFolder = parentFolder;

    self.isRoot = isroot;
    self.isUpdated = 0;
    self.isUpdating = false;
    //    self.showContent = ko.observableArray(true);
    self.showSubFolders = ko.observable(true);


    self.updateContent = function(force,onReady){
        if(self.isUpdating) return;
        force = force || false;
        viewModel.selectedFolder(self);
        viewModel.fileSelected(false);
        if(self.isUpdated == 1){
            self.showSubFolders(!self.showSubFolders());
            return;
        }
        self.showSubFolders(true);
        self.isUpdating = true;
        var date=new Date();
        var updatePath = self.path();

        $.post('ajax.php?klasse=dateimanager&act=get_foldercontents' + '&' + date.getTime() +filteradd, {
            pfad: updatePath
        }, function(d){
            $(d).find('el').each(function(){
                if($(this).attr('isdir') == 1){
                    console.log(self.isUpdated);
                    if(self.isUpdated != 2)
                        self.subFolders.push(new dmFolder($(this).attr('name'),$(this).attr('pfad'),false,self));
                }else{
                    self.files.push(new dmFile($(this).attr('name'),$(this).attr('pfad'),self,$(this).attr('art')));
                }
            });
            self.isUpdated = 1;
            self.isUpdating = false;
            self.subFolders.sort(function(left, right) {
                var leftName = left.name().toLowerCase();
                var rightName = right.name().toLowerCase();
                return leftName == rightName ? 0 : (leftName < rightName ? -1 : 1)
            });
            callCallback(onReady);
        });
    }

    self.getSubFolder = function(name){
        var f;
        console.log("GET Folder: "+name);
        for(var index in self.subFolders()){
            f = self.subFolders()[index];
            if(f.name() == name)
                return f;
        }
        return false;
    }
    self.getFile = function(name){
        var f;
        console.log("GET File: "+name);
        for(var index in self.files()){
            f = self.files()[index];
            if(f.name() == name)
                return f;
        }
        return false;
    }


    self.rename = function(newname,callback){
        $.post('ajax.php?klasse=dateimanager&act=rename', {
            'src': self.path(),
            'newname': newname
        }, function(d){
            if(d != 'Ok') debug(d);
            else{
                self.name(newname);
                if (callback && typeof(callback) === "function") {
                    callback();
                }
            }
        });

    }

    self.saveRename = function(newname){
        $.post('ajax.php?klasse=dateimanager&act=rename&opt=save', {
            'src': self.path(),
            'newname': newname
        }, function(d){
            message(d);
            self.name(newname);
        });
    }

    self.renameDialog = function(){

        var name = self.name(),
        dialog = $("#dialog");

        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';

        dialog.dialog('option', 'title', 'Verzeichnis umbenennen');
        dialog.html('Name: <input type="text" id="newname" value="'+name+'" />'+fileOccurrence);
        activateOccurenceSeachButton(self.path());
        dialog.dialog('option', 'buttons', {
            'Umbenennen': function() {
                self.rename($("#dialog #newname").val(),function(){
                    dialog.dialog('close');
                });
            },
            'Sicheres Umbenennen': function() {
                self.rename($("#dialog #newname").val(),function(){
                    $(this).dialog('close');
                });
            },
            'Abbrechen': function() {
                dialog.dialog('close');
            }
        });
        $("#dialog").dialog('open');

    }
    self.deleteDialog = function(){
        var fileOccurrence = '<br /><div id="occurrence"><input type="Button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Nach Vorkommen suchen" /></div>';
        var dialog = $("#dialog");
        //
        //        if(ui.draggable.hasClass('datei')){
        //            $("#dialog").dialog('option', 'title', 'Datei L&ouml;schen');
        //            $("#dialog").html('M&ouml;chten Sie die Datei "'+ui.draggable.attr('id')+'" wirklich löschen?'+fileOccurrence);
        //        }else{
        dialog.dialog('option', 'title', 'Verzeichnis L&ouml;schen');
        dialog.html('M&ouml;chten Sie das Verzeichnis "'+self.path()+'" wirklich löschen?'+fileOccurrence);
        //        }
        activateOccurenceSeachButton(self.path());
        dialog.dialog('option', 'buttons', {
            'Löschen': function() {
                $.post('ajax.php?klasse=dateimanager&act=remDir',{
                    src: self.path()
                }, function(d){
                    if(d != 'Ok') debug(d);
                    else{
                        self.parentFolder.subFolders.remove(self);
                        dialog.dialog('close');
                    }
                });
            },
            'Abbrechen': function() {
                dialog.dialog('close');
            }
        });
        dialog.dialog("open");
    }

    self.editDialog = function(){
        var dialog = $("#dialog").dialog('option', 'title', 'Verzeichniseigenschaften von "'+self.name()+'" &auml;ndern');
        var path = self.path().replace('+','%2B').replace('&','%26');
        $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+path, function(d){
            dialog.html(d);
            dialog.dialog('option', 'buttons', {
                'Speichern': function() {
                    var postdata = dialog.find('form').getFormValues();
                    $.post('ajax.php?klasse=dateimanager&act=fileedit&id='+path, postdata,function(d2){
                        dialog.dialog('close');
                        if(d2 != 'Ok')
                            debug(d2);
                    });

                }
            });
        });
        dialog.dialog('open');
    }

    self.newFolderDialog = function(){
        var foldername = prompt('Bitte Verzeichnisnamen eingeben');
        if(foldername != null && foldername != ""){
            $.post('ajax.php?klasse=dateimanager&act=mkdir', {
                'pfad': self.path(),
                'name':foldername
            }, function(d){
                if(d != 'Ok') debug(d);
                var newFolder = new dmFolder(foldername,self.path()+"/"+foldername,false,self);
                self.subFolders.push(newFolder);
                newFolder.updateContent();
            });
        }
    }

    self.commitFolder = function(){
        if(!viewModel.allowFolderCommit) return;

        createCookie('cmsLastDMSelection',self.path(),30);
        returnToForm(self.path()+'/');

    }
}



function returnToForm(name,absolute){
    if (absolute == 1) {
        var pfad = 'http://' + window.location.hostname + $('#cmsroot').html()+'dateien/'+name;
    } else {
        var pfad = $('#cmsroot').html()+'dateien/'+name;
    }

    if($.query.get('retid') != '')
        window.opener.$('#'+$.query.get('retid')).val(pfad);
    else
        window.opener.CKEDITOR.tools.callFunction($.query.get('CKEditorFuncNum'), pfad);
    window.close();
}

function callCallback(callback){
    if(callback != undefined && typeof callback == 'function') callback();
}