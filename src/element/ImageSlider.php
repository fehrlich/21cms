<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if($GLOBALS['interface'] == 'ajax')
	include_once('../klassen/container.php');

class ImageSlider extends element {  

        protected $deleteable = true;
//        protected $strucktur = false; 
        static public $order = 1;

	public function formBuild() {

//            $this->form->addElement('Reihenfolge', 'order', 'select', '0', array(0 => 'Neuste Oben', 1 => 'Neuste Unten', 2 => 'Eigene'));
            $this->form->addElement('Pfeile Anzeigen', 'arrows', 'select',0,array(0 => "ja","1" => "nein"));
            $this->form->addElement('zus. Navigation', 'zusNav', 'select',0,array('Keine','Punkte','Seiten'));
            $this->form->addElement('Bilderverzeichnis', 'imagefolder', FormType::FOLDER);
            $this->form->addElement('Als Hintergrundbild verwenden', 'useBg', FormType::SIMPLECHECKBOS);
            
	}

	public function getInline($data = array()){
            
            $html = '<div class="slider">';
            $images = cms\file::getFolderContent($this->data['imagefolder']);
//            new dBug($x);
            foreach($images as $image){
                $html .= '<div class="image">';
                $imgPath = $image['path'].'/'.$image['name'];
//                new dBug($image);
                if($this->data['useBg']) $html .= '<div class="bgImg" style="background-image: url(\''.$imgPath.'\');"></div>';
                else $html .= '<img src="'.$imgPath.'" />';
                $html .= '</div>';
            }
            $html .= '</div';

            $admin = (isAdmin()) ? " isAdmin" : '';
//            $html .= '<div class="contentNav '.$admin.'"><span class="contentLeft">';
////            $html .= (isset($this->arrows) && $this->arrows === 0)? '&laquo;' : '';
//            $html .= '</span>';
//            $html .= (isset($this->arrows) && $this->arrows === 0)? ' | ' : '';
//            $html .= '<span class="contentRight">';
//            $html .= (isset($this->arrows) && $this->arrows === 0)? '&raquo' : '';
//            $html .= '</span>';
//            $html .= (isset($this->arrows) && $this->arrows === 0)? '<p>Leistungen</p>' : '';
            $html .= '</div>';


			if(isset ($this->data['zusNav']) && $this->data['zusNav'] > 0){
				$html .= '<div class="zusNav"><div class="zusNav2">';
				$html .= $zusNavHtml;
				$html .= '</div></div>';
			}

            return $html;
	}
}
?>
