<?php

/**
 * Description of DateiTabelle
 * 
 * @author Franz
 */
class DateiTabelle extends element {

	private $table;
	private $userFilter;
	private $userTableData;

	public function __construct($id = 0, $data = array(), $container = '', $mainid = 0, $deletable = true, $buildform = true) {
//	include_once
		include_once $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'klassen/userFilter.php';
		$this->userFilter = new \userFilter();
		$this->userFilter->addFilterKrit('Name', 'text', array(), 'name');
		$this->userFilter->addFilterKrit('Author', 'text', array(), 'loginname', 't2');
		$this->userFilter->addFilterKrit('Datum', 'date');
		parent::__construct($id, $data, $container, $mainid, $deletable, $buildform);
	}

	public function formBuild() {
		$this->form->addElement('Verzeichnis', 'folder', FormType::FOLDER);
		$this->form->addElement('Rekursiv', 'rekursiv', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Limit', 'limit', FormType::TEXTLINE);
		$this->form->addElement('Sortierung', 'sort', FormType::SELECT, '', array(
			'' => 'Standard',
			'uploadedTime' => 'Upload-Datum',
			'name' => 'Dateiname',
			'size' => 'Größe',
			'filetype' => 'Dateityp',
			'author' => 'Admin-Author',
			'author2' => 'FrontEnd-Author',
			'ext' => 'Dateiendung',
			'path' => 'Pfad'
		));
		$this->form->addElement('Sortierung', 'sort2', FormType::SELECT, '', array(
			'ASC' => 'Aufsteigend',
			'DESC' => 'Absteigend'
		));

		$this->form->addElement('Autor anzeigen', 'showAuthor', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Datum anzeigen', 'showDate', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Größe anzeigen', 'showSize', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Aktion "Löschen" anzeigen', 'showActionDelete', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Aktion "Download" anzeigen', 'showActionDownload', FormType::SIMPLECHECKBOS);
		$this->form->addElement('Weitere (selbsterstellte) Eigenschaften anzeigen:', 'showUserData', FormType::TEXTAREA);



		$this->form->useTab('UserFilter');
		$this->userFilter->addToForm($this->form);
	}

	public function getInline() {
		$html = '';
		$html .= $this->userFilter->getFilterHtml($this->data);
		if (isSet($_GET['para']) && $_GET['para'] != '') {
			addWhere('loginname', '=', $_GET['para']);
			select('frontEndUser', 'id');
			$row = getRow();
			$user = $row['id'];
		}else
			$user = null;
		$parser = new Parser(null, null, $user);
		$this->data['folder'] = $parser->parseTxt($this->data['folder']);
//		$this->data['folder'] = Vars::parseUserVars($this->data['folder']);
		if (!isAdmin()) {
			include_once('klassen/html/Table.php');
		}
		$this->userTableData = array();
		$head = array('Name');
                $headClasses = array();
		if (isSet($this->data['showAuthor']) && $this->data['showAuthor'] == '1')
			$head[] = 'Autor';
		if (isSet($this->data['showDate']) && $this->data['showDate'] == '1'){
                    $headClasses[count($head)] = '{sorter: \'germandate\'}';
                    $head[] = 'Datum';
                }
		if (isSet($this->data['showSize']) && $this->data['showSize'] == '1'){
                    $headClasses[count($head)] = '{sorter: \'filesize\'}';
                    $head[] = 'Gr&ouml;ße';
                }
		if (isSet($this->data['showUserData']) && trim($this->data['showUserData']) != '') {
			$lineSpl = explode("\n", $this->data['showUserData']);
			foreach ($lineSpl as $line) {
				$head[] = trim($line);
				$this->userTableData[] = trim($line);
			}
		}
		
		if ((isSet($this->data['showActionDownload']) && $this->data['showActionDownload'] == '1')
				|| (isSet($this->data['showActionDelete']) && $this->data['showActionDelete'] == '1'))
			$head[] = 'Aktionen';

		$this->table = new Table($head, true, false);
                $this->table->tdheadClass = $headClasses;
		if(substr($this->data['folder'], 0, 8) == '/dateien') $this->data['folder'] = substr($this->data['folder'],8);
		$dir = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'dateien' . $this->data['folder'];
		if (isset($this->data['limit']) && $this->data['limit'] > 0)
			setLimit($this->data['limit']);
		if (isset($this->data['sort']) && $this->data['sort'] != '')
			$sort = $this->data['sort'] . ' ' . $this->data['sort2'];
		else
			$sort = 'id';

//	$t_1 = microtime();
		$mainDir = '/dateien' . slashFolderFix($this->data['folder']);
//	addWhere('filetype', '!=', 'directory');
//	if($this->data['rekursiv'])
//	    addWhere('SUBSTRING(t.path,1,'.strlen ($mainDir).')', '=', $mainDir,'s','AND','');
//	else
//	    addWhere('path', '=', $mainDir);
//	select('files','*',$sort);
                
		$this->userFilter->setupAddWhere();
		$rows = \cms\file::getFolderContent($mainDir, true, (isSet($this->data['rekursiv']) && $this->data['rekursiv']), $sort);
		
		$rows = $this->userFilter->filterFileResult($rows);
//
//	$rows = getRows();

		foreach ($rows as $file) {
			$this->addFile($file);
		}
//	$t_2 = microtime();
//	$this->showFiles($dir,$this->data['rekursiv']);
		$html .= $this->table;
		return $html;
	}

	public function showFiles($dir, $rek = false) {
		$files = glob($dir . '*');
		if (is_array($files)) {
			foreach ($files as $file) {
				$isDir = is_dir($file);
				if ($isDir && $rek)
					$this->showFiles($file . '/', true);
				elseif (!$isDir) {
					$fileData = \cms\file::load($file, true);
					$author = $fileData->getFrontEndAuthor();
					if($author == '') $author = $fileData->getAuthor ();
//					if($author == ''){
//						$author = $fileData->getFrontEndAuthor();
//					}
					$time = date('d.m.Y', $fileData->getTime());
					$data = array(
						'name' => basename($file),
						'author' => $author,
						'uploadedTime' => $fileData->getTime(),
						'size' => filesize($file)
					);


					$this->addFile($data);
//		    $this->table->addContent(array(basename($file),$author,$time, formatBytes(filesize($file)), '<span class="delete" id="i' . $this->id . '-' . basename($file) . '">Delete</span> <span class="download" id="d' . $this->id . '-' . basename($file) . '">Download</span>'));
					//		    $table->addContent(array(basename($file), formatBytes(filesize($file)), '<span class="delete" id="i' . $this->id . '-' . basename($file) . '">Delete</span> <a href="'.$_SERVER['QUERY_STRING'].'">Download</span>'));
				}
			}
		}
	}

	public function addFile($file) {
		if(strlen($file['name']) > 20 && strpos($file['name'], ' ') === false)
			$shortName = substr($file['name'],0,32).'...';
		else $shortName = $file['name'];
                $rekPath = $this->data['folder'];
                if(substr($rekPath, 0, 8) != '/dateien') $rekPath = '/dateien'.$rekPath;
                if(substr($rekPath, -1, 1) == '/') $rekPath = substr($rekPath,0,-1);
                $filePathInTable  = '';
                if($file['path'] != $rekPath){
                    $filePathInTable = substr($file['path'],  strlen($rekPath)+1).'/';
	    
//                    echo $filePathInTable.'!';
                }

		$data = array('<a href="'.str_replace('//', '/', $GLOBALS['cms_rootdir'].$file['path']).'/'.$file['name'].'" target="_blank">'.$shortName.'</a>');

		if (isSet($this->data['showAuthor']) && $this->data['showAuthor'] == '1'){
			$data[] = ($file['frontEndAuthor'] != '')?$file['frontEndAuthor']:$file['adminAuthor'];
		}
                if (isSet($this->data['showDate']) && $this->data['showDate'] == '1'){
                    $data[] = date('d.m.y', $file['uploadedTime']);
                }
                if (isSet($this->data['showSize']) && $this->data['showSize'] == '1')
			$data[] = formatBytes($file['size']);
		if (count($this->userTableData) > 0) {
			foreach ($this->userTableData as $userData) {
				if (isSet($file['data'][$userData])){
					if(isSet($file['data'][$userData][0]) && $file['data'][$userData][0] == ',')
						$file['data'][$userData] = substr ($file['data'][$userData], 1, -1);
					$data[] = str_replace(array('_',','), array(' ', ', '), $file['data'][$userData]);
				}else
					$data[] = '';
			}
		}
		$actionData = '';
		if (isSet($this->data['showActionDelete']) && $this->data['showActionDelete'] == '1'){
			$actionData .= '<span class="delete link" id="i' . $this->id . '-' . $filePathInTable.$file['name'] . '">L&ouml;schen</span>';
		}
		if (isSet($this->data['showActionDownload']) && $this->data['showActionDownload'] == '1'){
			$actionData .= ' <span class="download link" id="d' . $this->id . '-' . $filePathInTable.$file['name'] . '">Download</span>';
		}
		if($actionData != '')
			$data[] = $actionData;
//                if(isAdmin()) new dBug($data);
		$this->table->addContent($data);
	}

	public static function deleteFile($id, $name) {
		addWhere('id', '=', $id);
		addWhere('klasse', '=', 'DateiTabelle');
		select('elements', 'eldata');
		$row = getRow();
		$data = unserialize($row['eldata']);
		$data['folder'] = Vars::parseUserVars($data['folder']);
                if(substr($data['folder'], 0, 8) != '/dateien') $data['folder'] = '/dateien'.$data['folder'];
		$link = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $data['folder'] . $name);
		\cms\file::load($link,true)->delete();
		echo 'Ok';
	}

	public static function downloadFile($id, $name) {
		addWhere('id', '=', $id);
		addWhere('klasse', '=', 'DateiTabelle');
		select('elements', 'eldata');
		$row = getRow();
		$data = unserialize($row['eldata']);

		$data['folder'] = Vars::parseUserVars($data['folder']);
		$file = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . '/dateien' . $data['folder'] . $name;
		if (!is_file($file))
			die("<b>404 Datei nicht gefunden!</b>");
		$filesize = filesize($file);
		$file_extension = strtolower(substr(strrchr($file, "."), 1));
		switch ($file_extension) {
			case "pdf": $ctype = "application/pdf";
				break;
			case "exe": $ctype = "application/octet-stream";
				break;
			case "zip": $ctype = "application/zip";
				break;
			case "doc": $ctype = "application/msword";
				break;
			case "xls": $ctype = "application/vnd.ms-excel";
				break;
			case "ppt": $ctype = "application/vnd.ms-powerpoint";
				break;
			case "gif": $ctype = "image/gif";
				break;
			case "png": $ctype = "image/png";
				break;
			case "jpeg":
			case "jpg": $ctype = "image/jpg";
				break;
			case "mp3": $ctype = "audio/mpeg";
				break;
			case "wav": $ctype = "audio/x-wav";
				break;
			case "mpeg":
			case "mpg":
			case "mpe": $ctype = "video/mpeg";
				break;
			case "mov": $ctype = "video/quicktime";
				break;
			case "avi": $ctype = "video/x-msvideo";
				break;

			default: $ctype = "application/force-download";
		}

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: $ctype");
		header("Content-Disposition: attachment; filename=\"" . $name . "\";");
		header("Content-Transfer-Encoding:­ binary");
		header("Content-Length: " . $filesize);
		readfile($file);
	}

}

function formatBytes($bytes, $precision = 2) {
	$units = array('B', 'KB', 'MB', 'GB', 'TB');

	$bytes = max($bytes, 0);
	$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
	$pow = min($pow, count($units) - 1);

	$bytes /= pow(1024, $pow);

	return round($bytes, $precision) . ' ' . $units[$pow]; 
}

?>
