<?php
    class gallery2 extends element {

        private $pfad = null;
        private $html = "<div id=\"gallery\">Galerie leer!</div>\n";
        private $request = null;
        private $files = array();

        public function getInline() {
            if (!isset($this->data["bild"])) return false;
            if (substr($this->data["bild"],-1) == "/") $this->data["bild"] = substr($this->data["bild"],0,-1);



            return (!isset($this->data["single"]) || $this->data["single"] == 0) ? $this->getMulti() : $this->getSingle();

        }

        public function getSingle() {

            mys::getObj()->cleanup()->clearWhere();
            addWhere("path","LIKE","{$this->data["bild"]}");
            addWhere("filetype","<>","directory");
            select("files","name,path,comment","sort desc,uploadedTime desc");

            $html = "<div id=\"gallery\">\n";

            $rows = getRows();

            foreach($rows as $row){

                if (file_exists(CMS_ROOT ."/" . $row["path"] . "/" . $row["name"])) {
                    $this->ImageResize($row["name"],CMS_ROOT ."/" . $row["path"]);
                }

                $html .= "<div class=\"gallery-dir\"><div class='helper'>\n";
                $html .= "   <div class=\"gallery-preview\">\n";
                $html .= "     <a href=\"".urlEncodeWithoutSlash($row["path"]) ."/".urlEncodeWithoutSlash($row["name"])."\" title=\"{$row["comment"]}\">\n";
                $html .= "       <div class=\"img\"><img src=\"". urlEncodeWithoutSlash($row["path"]) ."/__thumb2/".urlEncodeWithoutSlash($row["name"])."\" alt=\"". htmlentities($row["comment"],ENT_QUOTES) . "\" /></div>\n";
                $html .= "     </a>\n";
                $html .= "   </div>\n";
                $html .= "</div></div>\n";
            }

            $html .= "<div style=\"clear: both;\"></div>\n";
            $html .= "</div>";

            $html .= '<script type="text/javascript" src="/js/jquery.lightbox-0.5.min.js"></script>';
            $html .= '<script type="text/javascript">$(document).ready(function() {';
            $html .= '$(\'#gallery a\').lightBox({txtImage: \'Bild\',txtOf: \'von\'});';
            $html .= '}); </script>';

            return $html;

        }

        public function getMulti() {

            mys::getObj()->cleanup()->clearWhere();
            addWhere("path","LIKE","{$this->data["bild"]}");
            addWhere("filetype","=","directory");
            select("files","name,path,comment,id","sort desc,uploadedTime desc");;

            //fetching folders
            $folders = getRows();

            $html = "<div id=\"gallery\">\n";


            $i = 0;
            $galleryIds = array();

            foreach($folders as $folderName => $data) {



#                mys::getObj()->cleanup()->clearWhere();
                addWhere("path","LIKE","{$data["path"]}/{$data["name"]}");
                addWhere("filetype","<>","directory");
                $sort = 'sort asc';
                if(isSet($this->data['sortField'])){
                    $sort = $this->data['sortField'];
                    if(isset($this->data['sortDirection']) && $this->data['sortDirection'] == '1')
                        $sort .= ' desc';
                }

                select("files","name,comment,path",$sort);
                $rows = getRows();

                if ( !isset( $rows[0] ) ) {
                    continue;
                }

                $row = $rows[0];

                if ( file_exists(CMS_ROOT ."/" . $row["path"] . "/" . $row["name"])) {
                        $this->ImageResize($row["name"],CMS_ROOT ."/" . $row["path"]);
                } else  {
                    continue;
                }

                $links = '';
                $imagecount = 1;

                $foldername =  basename($row["path"]);

                for ( $i = 1; $i < count($rows); $i++) {

                    $row2 = $rows[$i];

                    if (file_exists(CMS_ROOT ."/" . $row2["path"] . "/" . $row2["name"])) {
                        $this->ImageResize($row2["name"],CMS_ROOT ."/" . $row2["path"]);
                        $links .= "<a href=\"". urlEncodeWithoutSlash( $row2["path"]) ."/".urlEncodeWithoutSlash( $row2["name"])."\" title=\"{$row2["comment"]}\" data-lightbox=\"{$foldername}\"></a>\n";
                        $imagecount++;
                    }
                }

                $html .= "<div class=\"gallery-dir\"><div class='helper'>\n";


                if ( $this->data["showFolder"] == 1 ) {
                    $html .= "  <h4 class=\"gallery-name\">" . $data["comment"] . "</h4>\n";
                }

                $html .= "   <div class=\"gallery-preview dir" . ++$i.$data['id'] ."\">\n"; //$data['id']
                $galleryIds[] = $i.$data['id'];
                $html .= "     <a href=\"".urlEncodeWithoutSlash($row["path"]) ."/".urlEncodeWithoutSlash($row["name"])."\" title=\"{$row["comment"]}\" data-lightbox=\"{$foldername}\">\n";
                $html .= "       <div class=\"img\"><img src=\"". urlEncodeWithoutSlash($row["path"]) ."/__thumb2/".urlEncodeWithoutSlash($row["name"])."\" alt=\"". htmlentities($row["comment"],ENT_QUOTES) . "\" /></div>\n";
                if(!isSet($this->data['showImgNumber']) || $this->data['showImgNumber'] == '1')
                    $html .= "       <span class=\"gallery-imagecount\">enthält {$imagecount} Bilder</span>\n";
                $html .= "     </a>\n";

                $html .= $links;

                $html .= "   </div>\n";


                if ( $this->data["showFolder"] == 2 ) {
                    $html .= "  <h4 class=\"gallery-name\">" . $data["comment"] . "</h4>\n";
                }

                $html .= "</div></div>\n";


            }
//            $html .= "</div>\n";
            $html .= "<div style=\"clear: both;\"></div>\n";
            $html .= "</div>\n";

            return $html;
        }

        public function ImageResize($name,$path) {
            if (!file_exists("{$path}/__thumb1/$name") || !file_exists("{$path}/__thumb2/$name")) {
                 if (isset($this->data["maxHeight"]) && $this->data["maxHeight"] && isset($this->data["maxWidth"]) && $this->data["maxWidth"] != "") {
                     $thumb = image::create("{$path}/__thumb1/$name",1,false);

                     if($thumb->getWidth() == $this->data['maxWidth'] || $thumb->getHeight() == $this->data['maxHeight']) {
                         return true;
                     }

                 }


            } else {
                return true;
            }

            unset($thumb);

            $image = @getimagesize($path ."/". $name);

            if (is_writeable($path)) {
                if (!file_exists($path . "/__thumb1")) {
                    mkdir($path . "/__thumb1");
                    mkdir($path . "/__thumb2");
                }
            } else {
                return false;
            }
            //filetype ansehen!

            $sizes = array();
            $sizes[] = array(900,675);

            if (isset($this->data["maxHeight"]) && $this->data["maxHeight"] && isset($this->data["maxWidth"]) && $this->data["maxWidth"] != "") {
                $sizes[] = array((int) $this->data["maxWidth"],(int) $this->data["maxHeight"]);
            } else {
                $sizes[] = array(125,94);
            }

            for ($i = 0; $i < count($sizes);$i++) {
                  $thumbpath = "{$path}/__thumb".($i +1)."/$name";
                  try {
                      image::create($path ."/".$name,1)
                              ->resize($sizes[$i][0], $sizes[$i][1],1)
                              ->save($thumbpath)
                              ->cleanup();
                  } catch (Exception $e) {

                  }
            }

            return true;
        }

        public function formbuild() {
            $this->form->addelement('Ordner', 'bild','folder');
            $this->form->addelement('Bilderanzahl anzeigen',"showCount", 'select','0',array(0 => "nein",1 => "ja"));
            $this->form->addelement('Ordnernamen anzeigen',"showFolder", 'select','0',array("nein", "Über Bild","Unter Bild"));
            $this->form->addelement('Anzahl der Bilder anzeigen',"showImgNumber", 'select','0',array("Nein", "Ja"));
            $this->form->addElement('Thumbnailhöhe', 'maxHeight','text');
            $this->form->addElement('Thumbnailbreite', 'maxWidth','text');
            $this->form->addElement('Einzelgalerie', 'single','select','0',array("0" => "Nein","1" => "Ja"));
            $this->form->useTab('Sortierung');
            $this->form->addElement('Bildersortierung', 'sortField','select','0',array(
                "sort" => "Interne Sortierung",
                "name" => "Namen (A-Z)",
                "uploadedTime" => "Uploaddatum"
            ));
            $this->form->addElement('Bildersortierung', 'sortDirection','select','0',array(
                "0" => "aufwärts",
                "1" => "abwärts"
            ));
        }
    }
?>
