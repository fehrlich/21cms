<?php
    class galleryng extends element {

        private $pfad = null;
        private $html = "<div id=\"gallery\">Galerie leer!</div>\n";
        private $request = null;
        private $files = array();

        public function getInline() {
            
            /* preperation */
            if (!isset($this->data["previewCount"]) || $this->data["previewCount"] < 1) $this->data["previewCount"] == 0;
            
            if (!isset($this->data["bild"])) return false;
            if (substr($this->data["bild"],-1) == "/") $this->data["bild"] = substr($this->data["bild"],0,-1);

            return $this->getGal();
        }

        public function getGal() {

            mys::getObj()->cleanup()->clearWhere();
            addWhere("path","LIKE","{$this->data["bild"]}");
            addWhere("filetype","<>","directory");
            select("files","name,path,comment");

            $html = "<div id=\"gallery-ng\">\n";
            
            $preview  = "<div id=\"gallery-ng-preview\">";
            $mainView = "<div id=\"gallery-ng-big\">";
            
            $rows = getRows();
            $counter = 0;
            
            foreach($rows as $row){
               
                if (file_exists($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'] . $row["path"] . "/" . $row["name"])) {
                    $this->ImageResize($row["name"],$_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'] . $row["path"]);
                }
                
                if ($counter < $this->data["previewCount"]) {
                    $preview .= "<img src=\"". urlEncodeWithoutSlash($row["path"]) ."/__thumb1/". $row["name"]."\" alt=\"". htmlentities($row["comment"],ENT_QUOTES) . "\" />";
                }
                
                $mainView .= "<img src=\"". urlEncodeWithoutSlash($row["path"]) ."/". $row["name"]."\" alt=\"". htmlentities($row["comment"],ENT_QUOTES) . "\" />";
                
                ++$counter;
            }

            $preview .= "</div>";
            $mainView .= "</div>";
            
            $html .= $preview . $mainView;
            
            $html .= "</div>";
            return $html;

        }

        public function ImageResize($name,$path) {
            if (!file_exists("{$path}/__thumb1/$name") || !file_exists("{$path}/__thumb2/$name")) {
                 if (isset($this->data["maxHeight"]) && $this->data["maxHeight"] && isset($this->data["maxWidth"]) && $this->data["maxWidth"] != "") {
                     $thumb = image::create("{$path}/__thumb1/$name",1,false);
                     
                     if($thumb->getWidth() == $this->data['maxWidth'] || $thumb->getHeight() == $this->data['maxHeight']) {
                         return true;
                     }
						
                 }
                 
                 
            } else {
                return true;
            }
            
            unset($thumb);

            $image = @getimagesize($path ."/". $name);

            if (is_writeable($path)) {
                if (!file_exists($path . "/__thumb1")) {
                    mkdir($path . "/__thumb1");
                    mkdir($path . "/__thumb2");
                }
            } else {
                return false;
            }
            //filetype ansehen!

            $sizes = array();
            $sizes[] = array(900,675);

            if (isset($this->data["maxHeight"]) && $this->data["maxHeight"] && isset($this->data["maxWidth"]) && $this->data["maxWidth"] != "") {
                $sizes[] = array((int) $this->data["maxWidth"],(int) $this->data["maxHeight"]);
            } else {
                $sizes[] = array(125,94);
            }

            for ($i = 0; $i < count($sizes);$i++) {
                  $thumbpath = "{$path}/__thumb".($i +1)."/$name";
                  try {
                      image::create($path ."/".$name,1)
                              ->resize($sizes[$i][0], $sizes[$i][1],1)
                              ->save($thumbpath)
                              ->cleanup();
                  } catch (Exception $e) {
                      
                  }
            }

            return true;
        }

        public function formbuild() {
            $this->form->addelement('Ordner', 'bild','folder');
            $this->form->addElement('Anzahl der Vorschaubilder', 'previewCount','text');
        }
    }
?>
