<?php
    class referenzSlider extends element {

        private $items = array();
        private $previews = array();
        private $itemHtml = null;
        private $type = array(0 => "Marketing", 1 => "Events",2 => "Foto",3 =>"Web",4 =>"Print");
        private $startItem = '';
        private $startIndex = 0;
        private $count = 0;
        private $startTab = 0;

        public function  getInline() {
            if (isset($_GET["para"])) $this->startItem = $_GET["para"];

            $this->fetchData();
            $this->generateItems();

            $html = '';
            
            $html .= '<div id="referenzSlider">';
            $html .= '<a class="prev browse left" title="Vorheriger Kunde"</a>';
            $html .= '<a class="next browse right" title="Nächster Kunde"></a>';
            $html .= '<div class="scrollable">   ';


            $html .= '<div class="items">';

            $html .= $this->itemHtml;

            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';

            addJs("js/jquery.lightbox-0.5.min.js");
			addJs("tpl/js/jquery.tools.min.js");                                


            $html .= '<script type="text/javascript">
                            var gal_count = '. $this->count .';
                            var slider_start = '. $this->startIndex .';';
            $html .= '</script>';

            return $html;
        }

        public function ImageResize($name,$path) {

            if (file_exists("{$path}/__thumb1/$name") && file_exists("{$path}/__thumb2/$name") && file_exists("{$path}/__thumb3/$name") && file_exists("{$path}/__thumb4/$name")) {
                return true;
            }

            $image = @getimagesize($path ."/". $name);

            if (is_writeable($path)) {
                if (!file_exists($path . "/__thumb1")) {
                    @mkdir($path . "/__thumb1");
                    @mkdir($path . "/__thumb2");
                    @mkdir($path . "/__thumb3");
                    @mkdir($path . "/__thumb4");
                }
            } else {
                return false;
            }

            $sizes = array();
            $sizes[] = array(400,300);
            $sizes[] = array(150,102);
            $sizes[] = array(900,675);
            $sizes[] = array(125,94);

            for ($i = 0; $i < count($sizes);$i++) {
                  $thumbpath = "{$path}/__thumb".($i +1)."/$name";
                  image::create($path ."/".$name,1)
                          ->resize($sizes[$i][0], $sizes[$i][1],true)
                          ->save($thumbpath,array("jpeg_quality" => 90))
                          ->cleanup();
            }

            return true;
        }

        private function fetchData() {
            mys::getobj()->cleanup()->clearWhere();
            
            if (isset($this->data["filter"]) && $this->data["filter"] != 5) addWhere("kategorie","=",(int) $this->data["filter"]);

            addWhere("public","=","1");
            select("data_Referenzen","Kunde,nameIntern,Pfad,kurztext,mitteltext,langtext,kategorie","datum desc");
            
            while($row = getRow()) {
                $this->items[$row["nameIntern"]][] = $row;
            }

            mys::getObj()->cleanup();
        }

        private function generateItems() {

            $realpath = dirname(realpath(__FILE__)) . "/../";

            $startindex = 0;
            foreach($this->items as $name => $data) {

                if ($this->startItem == $name) {
                    $this->startIndex = $startindex;
                    $active = "active";
                } else {
                    $active = '';
                    $startindex++;
                }
                
                $html = '<div class="scrollable-item '.$active.'">';
                
                $content = "  <div class=\"panes\">\n";
                $tabsArray = array();

                foreach($data as $row) {

                    $this->count++;
                    $tabsArray[] = "<li>". $this->type[$row["kategorie"]] . "</li>";
                    $content .= "<div class=\"pane\">";
                    $content .= "<div class=\"beschreibung\">{$row["mitteltext"]}</div>";
                    
                    //gallery
                    $content .= "<div class=\"gal\">";

                    $img = array();
                    $pfad = $realpath . $row["Pfad"];

                    if (is_readable($pfad)) {
                        $handle = opendir($pfad);
                        while($file = readdir($handle)) {
                            if (preg_match("~^({$row["nameIntern"]}[0-9]+)~",$file,$hits)) {
                                $this->ImageResize($file,$pfad);
                                $img[] = $file;
                            }
                        }
                        closedir($handle);
                    }
                    $i = array_pop($img);

                    $content .= "<div class=\"image-big zoom-{$this->count}\">";
                    $content .= "<a href=\"{$row["Pfad"]}//__thumb3/{$i}\" title=\"{$row["Kunde"]}\">";
                    $content .= "<img class=\"image-big\" src=\"{$row["Pfad"]}/__thumb1/{$i}\" alt=\"{$row["Kunde"]}\" /></a>";
                    
                    $buf = "<img class=\"image-small\" src=\"{$row["Pfad"]}/__thumb4/{$i}\" alt=\"{$row["Kunde"]}\" />";
                    $links = '';
                    
                    foreach($img as $i) {
                        $links .= "<a href=\"{$row["Pfad"]}/__thumb3/{$i}\" title=\"{$row["Kunde"]}\"></a>";
                        $buf .= "<img class=\"image-small\" src=\"{$row["Pfad"]}/__thumb4/{$i}\" alt=\"{$row["Kunde"]}\" />";
                    }

                    $content .= $links ."</div>" .$buf;
                    $content .= "</div>";


                    
                    $content .= "</div>";
                }

                $content .= "</div>";

                $tabs = "  <ul class=\"tabs\">\n";
                foreach($tabsArray as $row) $tabs .= $row;
                
                $tabs .= "  </ul>";

                $html .= (isset($this->data["showcat"]) && $this->data["showcat"] == 0)? $tabs : '';
                $html .= $content . "</div>";

		$this->itemHtml .= $html;
            }
        }


        public function formbuild() {
            $this->form->addElement('Datenbank', 'database', 'database','');

            $filter = array(5 => "alle", 0 => "marketing", 1 => "event",2 => "foto",3 =>"web",4 =>"print",);
            $this->form->addElement('Datenbank', 'filter', 'select',5,$filter);
            $this->form->addElement('Kategorieanzeige', 'showcat', 'select',5,array("ja","nein"));
	}
    }
?>
