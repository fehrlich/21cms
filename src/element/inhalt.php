<?php

    class inhalt extends element {

        public function getInline() {
            $html = '';

            if ( $this->listStyle ) {
                $html .= '<a href="#">';
            }

            if( !isset($this->data['html']) ) {
                return '';
            }

            $html .= $this->data['html'];
            $html = str_replace(array('%0a','%0A'), '%250a', $html);

            if ( $this->listStyle ) {
                $html .= '</a>';
            }

            return $html;
        }

        public function formbuild() {

            $html = UserTemplates::getTemplatesOverview();

            $this->form->addelement('Template', 'select','html','',$html,false);
            $this->form->addelement('', 'html','editor');

            $data = $this->getData();

            if ( isset($data['group']) ) {
                $this->form->addElement('Gruppe', 'group','text');
            }

            $this->form->setMultiLanguage( array('html','searchtxt') );
        }

        public function formpost() {

            $_POST['searchtxt'] = trim( strip_tags($_POST['html']) );
            return parent::formpost();
        }
    }