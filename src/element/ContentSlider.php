<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if($GLOBALS['interface'] == 'ajax')
	include_once('../klassen/container.php');

class ContentSlider extends mainelement {

        protected $deleteable = true;
//        protected $strucktur = false; 
        static public $order = 1;

	public function formBuild() {

            $this->form->addElement('Reihenfolge', 'order', 'select', '0', array(0 => 'Neuste Oben', 1 => 'Neuste Unten', 2 => 'Eigene'));
            $this->form->addElement('Pfeile Anzeigen', 'arrows', 'select',0,array(0 => "ja","1" => "nein"));
            $this->form->addElement('zus. Navigation', 'zusNav', 'select',0,array('Keine','Punkte','Seiten'));
            
	}

	public function getInline($data = array()){
            if($this->listStyle) return '<a href="#">SliderContent('. $this->data['id'].')</a>';
            $html = '';
            $datas = array();
            if (isset($this->data["order"])) self::$order = $this->data["order"];
			if(self::$order == '0') $order = 'erstellt';
			elseif(self::$order == '2') $order = 'ordernr';
			else $order = 'erstellt DESC';
            $data = $this->getArrayList('*',false,$order);
//			new dBug($data);
//			echo 'Asdf';
//            usort($data,array("ContentSlider","sorting"));
//			new dBug($data);

            foreach($data  as $dat){
                $el_dat = unserialize($dat['eldata']);
                $datas[] = (string)new $dat['klasse']($dat['id'], $el_dat, $dat['container'],$this->id,true,false);
            }

            $add = ' active"';
			$zusNavHtml = '';
			$x = 1;
            foreach($datas as $content){
                $html .= '<div class="contents'.$add.' id="content'.$x.'">'.$content.'</div>';
				if(isset ($this->data['zusNav']) && $this->data['zusNav'] > 0){
					$zusNavHtml .= '<div class="button'.(($x == 1)?$add:'').'" id="goto'.$x.'">';
					if($this->data['zusNav'] == 2) $zusNavHtml .= $x;
					$zusNavHtml .= '</div>';
				}
				$x++;
                $add = '" style="display: none"';
            }

            $admin = (isAdmin()) ? " isAdmin" : '';
            $html .= '<div class="contentNav '.$admin.'"><span id="contentLeft">';
            $html .= (isset($this->arrows) && $this->arrows === 0)? '&laquo;' : '';
            $html .= '</span>';
            $html .= (isset($this->arrows) && $this->arrows === 0)? ' | ' : '';
            $html .= '<span id="contentRight">';
            $html .= (isset($this->arrows) && $this->arrows === 0)? '&raquo' : '';
            $html .= '</span>';
            $html .= (isset($this->arrows) && $this->arrows === 0)? '<p>Leistungen</p>' : '';
            $html .= '</div>';


			if(isset ($this->data['zusNav']) && $this->data['zusNav'] > 0){
				$html .= '<div class="zusNav"><div class="zusNav2">';
				$html .= $zusNavHtml;
				$html .= '</div></div>';
			}

            return $html;
	}
	public static function getSubName(){
//		if(!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'container')
			$sub = 'inhalt';
//		else $sub = $_GET['el'];
		return $sub;
	}
	public static function getSub($id){
//		if(!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'Accordion')
			$sub = 'inhalt';
//		else $sub = $_GET['el'];
		$c = new $sub(0,true,'',$id);
		
		return $c;
	}

        public static function sorting($a1,$a2) {

            if (!isset($a1["erstellt"],$a2["erstellt"])) {
                return -1;
            }

            if (self::$order == 0) {
                return ($a1["erstellt"] > $a2["erstellt"]) ? +1 : -1;
            } else {
                return ($a1["erstellt"] < $a2["erstellt"]) ? +1 : -1;
            }

            
        }
}
?>
