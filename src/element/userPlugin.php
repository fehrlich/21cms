<?php

class userPlugin extends element{

	public function formBuild(){

		$this->form->addElement('Name', 'name', 'text');
	}

	public function getInline(){
	    include_once ($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].'tpl/'.$this->data['name'].'.php');
	    $fkt = "user".$this->data['name'];
	    ob_start();
	    if(!function_exists($fkt))
		    echo 'Funktion existiert nicht';
	    else{
		    $str = $fkt();
	    }
	    $str .= ob_get_contents();
	    ob_end_clean();
	    return $str;
	}
}
?>