<?php
class menu extends mainelement{
	protected $treeview = true;
	protected $usemainid = false;

	public function __construct($id = 0,$data = array(),$container = '', $mainid = 0,$deletable = true,$buildform = true){
            if (isAdmin()) $this->allowEdit = (rights::getObj()->isAdmin() || PageRights::getObj()->canCreatePages());
			if(isSet($data['dynamic']) && $data['dynamic'] == '1') $this->addClass ('dynamic');
            parent::__construct($id,$data,$container, $mainid,$deletable,$buildform);
	}
	
	private function checkSecurity(){
		if(!isAdmin()){
//			$GLOBALS['mysql_debug']=true;
			addWhereBracket('(');
			addWhere('secure', '=', '0','i');
			if(frontendSession::getObj()->isLoggedIn()){
				addWhere('allow_groups', 'LIKE', '%,'.frontendSession::getObj()->getGroupId().',%','s','OR');
				addWhere('allow_user', 'LIKE', '%,'.frontendSession::getObj()->getUserId().',%','s','OR');
			}
			addWhereBracket(')');
		}
	}

    public function getInline() {
		$what = '*';
		if($this->id != 1){
			$this->id = 1;
			$fake = true;
		}else $fake = false;
		if(frontendSession::getObj()->isLoggedIn()){
			frontendSession::getObj()->getGroupId();
			frontendSession::getObj()->getUserId();
		}
		if(isset($this->data['dynamic']) && $this->data['dynamic'] == 0){ //$this->data['opento'] > 0 &&
			if(!isset($lvl)){
				if($this->data['startlevel'] > 0){
					if($GLOBALS['akt_menuepunkt']['level'] < $this->data['startlevel'])
						$lvl = (int) $GLOBALS['akt_menuepunkt']['level'];
					else
						$lvl = $this->data['startlevel']-1;
					if(isset($GLOBALS['aktmenu'][$lvl])){
						addWhere('level', '>=', $this->data['startlevel'],'i');
						addWhere('parent', '=', $GLOBALS['aktmenu'][$lvl],'s');
						setLimit(1);
						$this->checkSecurity();
						select('menuepunkte','ordernr','ordernr');
						$row = getRow();
						$lowordernr = $row['ordernr'];
						addWhere('ordernr', '>=', $lowordernr);
						if(!isAdmin() && (!isSet($this->data['showAlsoInvisible']) || !$this->data['showAlsoInvisible']))
							addWhere('visible', '=', '1');
						$stoplvl = -1;
						if($this->data['opento'] > 0){
							addWhereBracket('(');
							$stoplvl = $this->data['opento'];
							if($this->data['stoplevel'] > 0 && $this->data['stoplevel'] < $stoplvl){
								$stoplvl = $this->data['stoplevel'];
							}
							addWhere('level', '<=', $stoplvl,'i');
							addWhere('parent', 'IN', $GLOBALS['aktmenu'],'i','OR');
							addWhereBracket(')');
						}
						if($this->data['stoplevel'] > 0){
							addWhere('level', '<=', $this->data['stoplevel'],'i');
						}
						$this->checkSecurity();
						select('menuepunkte', '*', 'ordernr');
						$what = array();

						while(($row = getRow()) && $row['level'] > $lvl){
							$what[] = $row;
						}
					}
				}else{
					if($this->data['opento'] > 0){
//						addWhere('level', '<', $this->data['opento']);
						addWhereBracket('(');
						addWhere('level', '<', $this->data['opento'],'i');
						addWhere('parent', 'IN', $GLOBALS['aktmenu'],'i','OR');
						addWhereBracket(')');
					}
					if($this->data['stoplevel'] > 0) addWhere('level', '<', $this->data['stoplevel']);
					if(!isAdmin() && (!isSet($this->data['showAlsoInvisible']) || !$this->data['showAlsoInvisible']))
							addWhere('visible', '=', '1');
				}
			}
//			addWhere('parent', '=', $GLOBALS['akt_menuepunkt']['id'],'s','OR');
//			addWherePrepare('(level < ? AND level >= ?) OR parent = ?', array($this->data['opento'],$this->data['startlevel'],$GLOBALS['akt_menuepunkt']['id']), 'iii');
		}
		$this->checkSecurity();
		$menutree = $this->getTree($what,$GLOBALS['aktmenu']);
		if(isSet($this->data['seperator']) && !empty($this->data['seperator'])){
			$menutree->setSeperator($this->data['seperator']);
		}
		
		return (string)$menutree;
    }

	public function formBuild() {
		$this->form->addelement('Ausklappen durch', 'dynamic','select', '0',array('Mausklick', 'Mausover'));
		$this->form->addelement('Anzeigen ab Ebene', 'startlevel','select', '0',array('Ebene 0', 'Ebene 1', 'Ebene 2', 'Ebene 3', 'Ebene 4'));
		$this->form->addelement('Anzeigen bis Ebene', 'stoplevel','select', '0',array('Letzte', 'Ebene 1', 'Ebene 2', 'Ebene 3', 'Ebene 4'));
		$this->form->addelement('Ausgeklappt bis', 'opento','select', '0',array('Alle Ebenen', 'Ebene 1', 'Ebene 2', 'Ebene 3', 'Ebene 4'));
		$this->form->addelement('Auch unsichtbare Menuepunkte anzeigen', 'showAlsoInvisible','simpleCheck', '');
		$this->form->addelement('Seperator', 'seperator','text', '');
		$this->form->addelement('BilderOrdner', 'imgfolder','datei');
		$this->form->useTab('Quickadd');
		$this->form->addElement('Quickadd', 'quickadd','textarea','',array(
			'allowtabs' => true
		),false);
	}
	
	public function formPost(){
		if(checkVar('quickadd')){
			$text = $_POST['quickadd'];
			$lines = explode("\n", $text);
			setLimit(1);
			select('menuepunkte','ordernr', 'ordernr DESC');
			$row = getRow();
			$anordnung = $row['ordernr']+1;
			$insert = array();
			$mainids = array(0);
			$insert['level'] = 0;
			foreach($lines as $line){
				$insert['title'] = trim($line);
				$insert['title_intern'] = menuepunkte::setTitelIntern($insert['title'],'menuepunkte');
				$insert['ordernr'] = $anordnung;
				$level = 0;
				$next = substr($line, 0, 1);
				while($next == "\t"){
					$level++;
					$next = $line[$level];
				}
				if($insert['level'] < $level) $mainids[] = $lastinsert;
				elseif($insert['level'] > $level){
					$anz = $insert['level']-$level;
					for($i=0;$i<$anz;$i++)
						array_pop($mainids);
				}
				$insert['mainid'] = $this->id;
				$insert['parent'] = $mainids[count($mainids)-1];
				$insert['level'] = $level;
				$insert['visible'] = '1';
				$insert['erstellt'] = time();
				$insert['lastmod'] = time();
				$insert['verlinkung'] = '';
				$parastr = 'issiiiiiii';
				$anordnung++;
				$lastinsert = insertArray('menuepunkte', $insert, $parastr);
			}
		}
		return parent::formPost();
	}

	public static function getSubName() {
		return 'menuepunkte';
	}
    
    public static function getName($id) {
        addWhere("id","=",$id);
        select("menuepunkte","title");
        
        if ($row = getRow()) {
            return $row["title"];
        } else {
            return false;
        }
        
        
    }
}
?>