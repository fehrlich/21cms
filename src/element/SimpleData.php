<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SimpleData
 *
 * @author F.Ehrlich
 */
class SimpleData extends element {
	protected $strucktur = false;

	public function formBuild() {
		$this->form->addElement('Datenbank', 'database', 'database');
		$this->form->addElement('Listen Template', 'tpl', 'dataTemplate','0',array('dbformname' => 'database'));
		if(isset($_GET['changedatabase']) && $_GET['changedatabase'] > 0)
			$dbid = $_GET['changedatabase'];
		else{
//			var_dump($this->form->mysqlrow);
			/*
			 * if(isset($_GET['change'.$dbformname]) && (int)$_GET['change'.$dbformname] > 0) $dbid = (int)$_GET['change'.$dbformname];
			else $dbid = $this->elements[$dbformname]['value'];
			 */
			if(isSet($_GET['act']) && $_GET['act'] == 'formedit')
				$this->form->connectData($this->id);
			$el = $this->form->getElement('database');
			if($el['value'] > 0)$dbid = $el['value'];
			else $dbid = key(array_pop($el['data']));
		}
		$dbs = new DatabaseConnector();
		addWhere('id', '=', $dbid, 'i');		
		$db = array_pop($dbs->getDatabases('name,fields,fieldid'));
		$what = $dbs->getFieldId();
		$arr = $dbs->getData($db['name'],'id,'.$what);
		$s = array();
		foreach($arr as $a){
			$s[$a['id']] = $dbs->getFieldId($a);
		}
		if(count($s) == 0) $s[0] = '[Es ist noch kein Datensatz vorhanden]';
		$this->form->addElement('Datensatz', 'dataid', 'select','',$s);
	}

	public function getInlineList(){
		$dbs = new DatabaseConnector($this->data['database'],$this->data['tpl'],$this->data['dataid']);
		return 'SmpleData';
	}
	
	public function getInline() {
		$dbs = new DatabaseConnector($this->data['database'],$this->data['tpl']);
		$GLOBALS['cmsdb']->clearWhere();
		addWhere('id', '=', $this->data['database'], 'i');
		setLimit(1);
		$db = array_pop($dbs->getDatabases('name,fields,fieldid'));

//		addWhere('public', '=','1','i');
//		echo $this->data['dataid'].'!!!';
		addWhere('id', '=', $this->data['dataid']);
		$data = $dbs->getData($db['name']);
		return $dbs->htmlShowData();
	}
	public static function getSubName() {
		return 'DatabaseConnector';
	}
}
?>