<?php

    class LatestGalleryPicutures extends element {

        private $pictures = array();

        public function formBuild() {
            $this->form->addelement('Ordner', 'bild', 'folder');
            $this->form->addElement('Thumbnailbreite', 'maxWidth', 'text');
            $this->form->addElement('Thumbnailhöhe', 'maxHeight', 'text');
            $this->form->addElement('Anzahl der Letzten Bilder', 'count', 'text');

            $links = buildSelectArray('menuepunkte', array('title_intern','title'), true);

            $this->form->addElement('Verlinkte Seite', 'verlinkung', 'select','',$links);
        }

        public function formPost() {
            System::cleanTemp();
            return parent::formPost();
        }

        private function _loadPictures() {

            if (!empty($this->data["bild"])) {
                mys::getObj()->cleanup()->clearWhere();
                addWhere("path", "LIKE", "{$this->data["bild"]}%");
                addWhere("path", "NOT LIKE", "%__thumb%");
                addWhere("filetype", "<>", "directory");
                setLimit($this->data["count"]);
                select("files", "name,path,comment", "uploadedTime desc");

                $this->pictures = getRows();
            }
        }

        public function _checkThumbfiles() {
            if (empty($this->pictures))
                return false;

            foreach ($this->pictures as &$file) {

                $file["tempPath"] = System::generateTempPath("tmpLatestGallery_" . $file["name"]);

                if (!file_exists(CMS_ROOT . $file["tempPath"])) {
                    $this->_generateThumb($file, $file["tempPath"]);
                }
            }
        }

        private function _generateThumb($file, $path) {
            $image = new image("{$file["path"]}/{$file["name"]}");
            $image->resize($this->data["maxWidth"], $this->data["maxHeight"]);
            $image->save($path, array("dontSaveToDb" => true));
            $image->cleanup();
        }

        public function getInline() {

            $this->_loadPictures();
            $this->_checkThumbfiles();

            $html = "<div class='latestGalleryPictures'>";
            $html .= "<div class='content'>";

            foreach ($this->pictures as $pic) {
                $html .= "<div class='img'>";
                $html .= "<a href='{$this->data["verlinkung"]}.html' />";
                $html .= "<img src='{$pic["tempPath"]}' alt='' />";
                $html .=" </a>";
                $html .=" </div>";
            }

            $html .=" </div>";
            $html .="</div>";

            return $html;
        }

    }