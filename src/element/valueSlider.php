<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of value
 *
 * @author XX
 */
class valueSlider extends element{
    public function formBuild() {
        $this->form->addElement('Wert', 'value', 'text','0',array(),true,false,'',false,'','valueSlider');
        $this->form->addHtml('<div id="updatevalue" class="valueSliderSlider" />');
        $this->form->useTab('inhalt');
        $this->form->addElement('Inhalt', 'template', FormType::HTMLEDITOR);
    }

    public function getInline() {
        $html = str_replace('[VALUE]', $this->data['value'], $this->data['template']);
        return $html;
    }    
}

?>
