<?php

class WarenKorb extends element{

	public function formBuild(){
            $this->form->addElement('Anzeige wenn Leer', 'empty', FormType::HTMLEDITOR,'Ihr Einkaufswagen ist leer.');
	}

	public function getInline(){
            return cart::getObj()->getHtml($this->data['empty']);
	}
}
?>