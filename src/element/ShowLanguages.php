<?php

class ShowLanguages extends element {

	public function getInline() {
		$html = '';

        if (!isset($GLOBALS['languages'])) {
            return "No_Lang";
        }

        $aktLang = Languages::getLang();
        $uri_add = substr($_SERVER['REQUEST_URI'],strlen($GLOBALS['cms_roothtml']));
        $uri_start = $GLOBALS['cms_roothtml'];

        if (isAdmin()) {
            $uri_add = substr($uri_add,6);
            $uri_start .= 'admin/';
        }


        $showTxt = (!isset($this->data["showTxt"]) || $this->data["showTxt"] == 0);

        //german ... dirty shit!
        $germanClass = (empty($aktLang)) ? "de active" : "de";
        $html .= $this->buildLink(false, $germanClass, $showTxt, true);

        foreach($GLOBALS['languages'] as $short => $lang) {

            $class = ($short == $aktLang)? "active" : "";
            $html .= $this->buildLink($short, $class, $showTxt);
        }

        //store language in session
        if(isAdmin() || isset($_GET["updateLang"])) {
            \cms\session::getObj()->set("currentLanguage",\Languages::getLang(false));
        }

		return $html;
	}

    /**
     * returns cmsified link
     * @param type $short k�rzel
     * @param type $class klassenname
     */
    private function buildLink($short,$class,$showTxt = true,$deu = false) {

        $short = strtolower($short);

        $tmp = '';

        $tmp .= (!$deu && $showTxt) ? " | " : "";
        $tmp .= "<a href=\"";
        $tmp .= buildLink('', '', ($deu) ? false : $short);

        if (isAdmin()) {
            $tmp .= "?updateLang";
        }

        $tmp .= "\" class=\"{$class} {$short}\">";

        /* LTB dirty Hack clean:Create working "Rename language" function */
        if ($short == "Englisch") { $short = "Eng"; }

        /* dirty für deutsch */
        $tmp .= ($deu && $showTxt) ? 'Deu' : "";

        $tmp .= ($showTxt) ? ucfirst($short) : '';

        $tmp .= '</a>';
        return $tmp;
    }

	public function formbuild() {
//		$this->form->addelement('Text', 'html','editor');
		$this->form->addelement('Textanzeigen', 'showTxt','select',0,array( "0" => "ja","1" => "nein"));
	}
}
