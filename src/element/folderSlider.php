<?php
  class folderSlider extends element {

    private $files = array();
    private $cat = array();
    private $html = '';

    public function formbuild() {
      $this->form->addelement('Pfad', 'folder','folder');
      $this->form->addelement('Subject', 'subject',  FormType::TEXTLINE);
      $this->form->addelement('Fuss', 'footer',FormType::TEXTAREA);
      $this->form->addelement('Body', 'footer',FormType::HTMLEDITOR);
    }


    public function getInline() {
        
        $this->getFiles();
        $this->generateHtml();
        
        return $this->html;
    }
    
    private function getFiles() {
        $folder = \cms\file::getFolderContent($this->data["folder"],false,true);
        
        if (substr($this->data["folder"],-1) != "/") {
            $this->data["folder"] .= "/";
        }
        
        $nameLenght = strlen($this->data["folder"]);
        
        foreach($folder as $row) {
            if ($row["filetype"] == "directory") {
                $this->cat[$row["name"]] = false;
            } else {
                $path = explode("/",$row["path"]);
                $cat = array_pop($path);
                
                
                
                if ($row["name"] != "_button.png") {
                    $this->cat[$cat] = true;
                    $this->files[$cat][] = $row["path"] . "/" . $row["name"];
                }
            }
        }
    }
    
    private function generateHtml() {
        $this->html .= '<div id="folderSlider">';
        
        $i = 0;
        
        $buttons = '<div class="buttons">';
        
        foreach($this->cat as $cat => $bool) {
            //if (!$cat || !isset($this->files[$cat])) continue;
            if (!$bool) continue;
            
            ++$i;
            
            $active = ($i == 1) ? " active" : "";
            
            $this->html .= "<div class=\"slides slide{$i}{$active}\">";
            $this->html .= "  <div class=\"images\">";

            $buttons .= "<div class=\"button{$i}{$active}\"><img src=\"{$this->data["folder"]}/{$cat}/_button.png\" /></div>";
            
            $imageNum = 0;
            
            foreach($this->files[$cat] as $img) {
                ++$imageNum;
                
                $active = ($imageNum == 1) ? " active" : "";
                $this->html .= "<div class=\"image{$imageNum}{$active}\"><img src=\"{$img}\" alt=\"\" /></div>";
            }
            
            
            
            $this->html .= "  </div>";
            $this->html .= "</div>";
            
            
        }
        
        $buttons .= "</div>";
        
        $this->html .= $buttons;
        
        $this->html .= '</div>';
    }
  }
?>