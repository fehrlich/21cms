<?php

class Action {
    protected $name = "";
    protected $retObj = null;
    protected static $debugActions = false;
	
	
	public function getName() {
		return $this->name;
	}
	
//	abstract public function execute();
	
	
	public static function getActions(){
		$sendMail = array(
			'to',
			'from',
			'subject',
			'message',
			'attachmets'
		);
		
		$actions = array(
			'sendMail' => $sendMail
		);
		$actions = array('Keine Aktion','SendMail','getUrl');
		
		$ret = array();
		foreach($actions as $a){
			$ret[$a] = $a;
		}
		return $ret;
	}
//	abstract function getForm(){
//		
//	}

	/**
	 *
	 * @param type $events
	 * @return formular Formular
	 */
	public static function getEventActionForm($events = array()){
		$form = new formular('','','','evAct');
		$events = Event::getDefaultEvents()+$events;
		$actions = Action::getActions();
//		new dBug($actions);
		$form->addElement('Event', 'event',  FormType::SELECT,'',$events);
		$form->addElement('Aktion','actionName', FormType::SELECT,'',$actions);
//		$sendMail = new action\SendMail($to, $from, $subject, $message, $attachmets);
//		$form->addElement('Aktion','actionData[]', FormType::FORMULA,'', \action\SendMail::getForm());
		
		foreach(Action::getActions() as $act){
			if($act != 'Keine Aktion'){
//			$form->addOnChangeSetForm('actionName[]', $act, 'actionData[]', call_user_func_array(array("action\\".$act, "getForm"),array()));
				$form->addElement('form','actionData'.$act, FormType::FORMULA,'', call_user_func_array(array("action\\".$act, "getForm"),array()));
				$form->addOnlyShowOn('actionData'.$act, 'actionName', $act);
			}
		}
//		
//		foreach($actions as $action){
//			$f
//		}
		
		return $form;
	}
	public static function triggerEvent($name,$dataString,$retObj = null){
		if($name == "[Keine]") return;
		if(strlen($dataString) > 0) $data = unserialize ($dataString);
		else return;
		
		if(in_array($name,$data['event'])){
			foreach($data['event'] as $index => $eventName){
				if($eventName == $name){
					$actionName = $data['actionName'][$index];
					$actionData = unserialize($data['actionData'.$actionName]);
					$parsedActionData = array();
					$className = "action\\".$actionName;
					foreach($actionData as $id => $dataArray){
                                            if(is_array($dataArray)){
						foreach($dataArray as $i => $d){
							$parsedActionData[$i][$id] = $d;
                                                }
                                            }
					}
					foreach($parsedActionData as $actionArray){
                                            
                                                if(Action::$debugActions){
                                                    echo 'Action: '.$className;
                                                    new dBug($actionArray);
                                                    echo 'ReturnObj:';
                                                    new dBug($retObj);
                                                }
						$action = new $className($actionArray,$retObj);
						$action->setRetObj($retObj);
						$action->execute();
					}
				}
			}
		}
	}
	
	public function getRetObj() {
		return $this->retObj;
	}

	public function setRetObj($retObj) {
		$this->retObj = $retObj;
	}

		
	public function parseStr($str){
		$parser = new Parser();
		$parser->useAdminAuthor();
		$str = $parser->parseTxt($str);
		if(method_exists($this->retObj, 'parseStr'))
			return $this->retObj->parseStr($str);
		else return $str;
	}
}
?>