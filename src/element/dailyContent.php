<?php

    class dailyContent extends element {

        public function formBuild() {

            $this->form->addElement("Standardinahtl anzeigen, wenn leer?", "defaultOnEmpty", \FormType::SELECT, "", array(1 => "Nein", 2 => "Ja"));
            $this->form->addElement("Standardinhalt", "default", \FormType::HTMLEDITOR);

            $this->form->useTab("Mo");
            $this->form->addElement("Inhalt Monatg", "mo", \FormType::HTMLEDITOR);
            $this->form->useTab("Di");
            $this->form->addElement("Inhalt Dienstag", "di", \FormType::HTMLEDITOR);
            $this->form->useTab("Mi");
            $this->form->addElement("Inhalt Mittwoch", "mi", \FormType::HTMLEDITOR);
            $this->form->useTab("Do");
            $this->form->addElement("Inhalt Donnerstag", "do", \FormType::HTMLEDITOR);
            $this->form->useTab("Fr");
            $this->form->addElement("Inhalt Freitag", "fr", \FormType::HTMLEDITOR);
            $this->form->useTab("Sa");
            $this->form->addElement("Inhalt Samstag", "sa", \FormType::HTMLEDITOR);
            $this->form->useTab("So");
            $this->form->addElement("Inhalt Sonntag", "so", \FormType::HTMLEDITOR);

            $this->form->setMultiLanguage(array('default', 'mo', "di", "mi", "do", "fr", "so", "sa"));
        }

        public function getInline() {
            $date = new DateTime("now");
            $dayOfTeWeek = $date->format("w");
            unset($date);

            switch ($dayOfTeWeek) {
                case 1: return $this->_getData("mo");
                case 2: return $this->_getData("di");
                case 3: return $this->_getData("mi");
                case 4: return $this->_getData("do");
                case 5: return $this->_getData("fr");
                case 6: return $this->_getData("sa");
                case 0: return $this->_getData("so");
            }
        }

        private function _getData($day) {
            if (empty($this->data[$day]) && $this->data["defaultOnEmpty"] == 2) {
                return $this->data["default"];
            } else {
                return $this->data[$day];
            }
        }
    }