<?php

    class DatenListe extends mainelement {

        protected $strucktur = false;
        private $dataFields = array(
            'id' => 'id',
            'erstellt' => 'erstellt',
            'published' => 'Veröffentlicht',
            'group' => 'Gruppe'
        );

        /**
         *
         * @return userFilter userFilter
         */
        private function buildUserFilter($fields, $dbname = '') {
            $userFilter = new userFilter();
            $userFilter->setDatabase($dbname);

            foreach ( $this->dataFields as $id => $field ) {

                $type = 'text';
                $data = array();

                if ( $id == 'group' ) {
                    $type = 'select';
                    $data = buildSelectArray('frontEndGroups', array( 'id', 'name' ), false, 'name');
                }

                $userFilter->addFilterKrit($id, $type, $data, $id, '', $field);
            }
            foreach ( $fields as $field ) {

                if ( !isSet($field['fieldname']) ) {
                    $field['fieldname'] = $field['name'];
                }

                if ( isSet($this->dataFields[$field['fieldname']]) )
                    $desc = $this->dataFields[$field['fieldname']];
                else
                    $desc = '';

                if ( $field['type'] == 'dbCon' )
                    $field['eig']['allowNew'] = false;

                if ( $field['fieldname'] == 'group' ) {
                    $field['eig']['add'] = array( '' => '[Alle]' );
                }

                if ( isSet($this->data['userFilterTxt' . $field['fieldname']]) && $this->data['userFilterTxt' . $field['fieldname']] != '' )
                    $desc = $this->data['userFilterTxt' . $field['fieldname']];

                $userFilter->addFilterKrit($field['fieldname'], $field['type'], $field['eig'], '', 't', $desc);
            }

            return $userFilter;
        }

        public function formBuild() {

            $seiten = array( '' => '[nicht überschreiben]' ) + buildSelectArray('menuepunkte', array( 'title_intern', 'title' ), true);
            $this->form->addElement('Datenbank', 'database', 'database', ''); //,array('addAllDB' => true)

            $dbid = 0;
            if ( isset($_GET['changedatabase']) && (int) $_GET['changedatabase'] > 0 )
                $dbid = (int) $_GET['changedatabase'];
            else {
                $dbel = $this->form->getElement('database');
                $data = $this->getData();
                if ( isSet($data['database']) )
                    $dbid = $data['database'];
            }

            $dbs = new DatabaseConnector($dbid);
            $fields = $dbs->getFields();

            $this->form->addElement('Limit', 'limit', 'text');
            $this->form->addElement('Verhalten nach Limit', 'doAfterLimit', 'select', '', array( 'nichts machen', 'Seitenzahlen (unten)', 'Seitenzahlen (oben/unten)' ));
            $this->form->addElement("Detailansicht nicht anzeigen", "dontShowDetail", "simpleCheck");
            $this->form->addElement("Links auf folgendes Ziel &uuml;berschreiben", "overWriteLinks", 'select', '', $seiten);

            $this->form->addElement('TemplateArt', 'isTable', 'select', '0', array( 'Template', 'Tabelle' ));
            $this->form->addElement('Drucklink', "tableShowPrint", FormType::SIMPLECHECKBOS);
            foreach ( $this->dataFields as $id => $field ) {
                $this->form->addElement($field, "tableField" . $id, FormType::SIMPLECHECKBOS);
                $this->form->addOnlyShowOn("tableField" . $id, 'isTable', '1');
            }
            foreach ( $fields as $field ) {
                $this->form->addElement($field['name'], "tableField" . $field['name'], FormType::SIMPLECHECKBOS);
                $this->form->addOnlyShowOn("tableField" . $field['name'], 'isTable', '1');
            }


            $this->form->addElement('Listen Template', 'tpl', 'dataTemplate', '0', array( 'dbformname' => 'database' ));
            $this->form->addElement('Detail Template', 'maintpl', 'dataTemplate', '0', array( 'dbformname' => 'database' ));

            $this->form->addOnlyShowOn('tpl', 'isTable', '0');
            $this->form->useTab('Inhalte');
            $this->form->addElement('Vorher', 'contentPrev', 'editor');
            $this->form->addElement('Nachher', 'contentNext', 'editor');
            $this->form->addElement("Text wenn keine Daten vorhanden", "noDataText", FormType::HTMLEDITOR, 'Im Moment sind keine aktuellen Eintr&auml;ge vorhanden');
            $this->form->addElement('Kein Datensatz nach Filter', 'noDataOnFilterTxt', 'editor');
            $this->form->useTab('Filter');
            $this->form->addElement('Filter', 'filter', 'filter', '0', array( 'dbformname' => 'database' ));

            $this->form->useTab("Sortierung");
            $this->form->addElement('Sortieren', 'sort', 'dbField', '0', array( 'dbformname' => 'database' ));
            $this->form->addElement("Art", "order", "select", 0, array( "Aufsteigend", "Absteigend" ));

            $this->form->useTab("UserFilter");

            $userFilter = $this->buildUserFilter($fields);

            $userFilter->addToForm($this->form);
        }

        public function getInline() {
            if ( isset($_GET['page']) )
                $page = (int) $_GET['page'];
            else
                $page = 1;
            if ( $page == 0 )
                $page = 1;
            $html = '';
            if ( isset($_GET['db']) && isset($_GET['title']) && $_GET['title'] != '' && \cms\session::getObj()->isLoggedIn() )
                $skipPublic = true;
            else
                $skipPublic = false;

            if ( !isset($this->data['database']) )
                return "Es wurde keine Datenbank ausgewählt";
            $dbs = new DatabaseConnector($this->data['database'], $this->data['tpl']);
            mys::getObj()->clearWhere();
            addWhere('id', '=', $this->data['database'], 'i');
            setLimit(1);

            $dbArray = $dbs->getDatabases('name,fields,fieldid,dbType');
            $db = array_pop($dbArray);
            if ( !is_array($db) )
                return "Diese Datenbank existiert nicht mehr";
            addWherePrepare('title_intern != title');
            select('tags');
//		addWhere('', $op, $value)
            $tag_rows = getRows();
            $tags = array();
            foreach ( $tag_rows as $tag ) {
                $tags[$tag['title_intern']] = $tag['title'];
            }
            if ( (isset($_GET['db']) && $_GET['db'] == $db['name']) && (!isSet($this->data['dontShowDetail']) || !$this->data['dontShowDetail']) ) {
                $detailView = true;
                $dbs->setTpl($this->data['maintpl']);
            } else
                $detailView = false;
            if ( isset($_GET['tag']) && $_GET['tag'] != '' )
                $tagView = true;
            else
                $tagView = false;

            $addW = array();
            $addW[] = array('public', '=','1','i');

            if ( $detailView && isset($_GET['db']) && $_GET['db'] != '' && $this->data['database'] && isSet($_GET['title']) ) {
//			addWhere('title_intern', '=', $_GET['title']);
                setLimit(1);
                $addW[] = array( 'title_intern', '=', $_GET['title'], 's' );
            } elseif ( $this->data['filter'] != '0' ) {

                $this->data['filter'] = unserialize($this->data['filter']);
                $filters = flipArray($this->data['filter']);

//			addWhere('public', '=','1','i');
//			$addW[] = array('public', '=','1','i');
                foreach ( $filters as $filter ) {

                    if ( $filter['fieldname'] != '' ) {
                        $ignore = false;
                        if ( isSet($filter['value']['tagname']) && $filter['value']['tagname'] == '[TAG]' ) {
                            if ( !isset($_GET['tag']) )
                                $ignore = true;
                            else {
                                if ( isSet($tags[$_GET['tag']]) )
                                    $filter['value']['tagname'] = $tags[$_GET['tag']];
                                else
                                    $filter['value']['tagname'] = $_GET['tag'];
                            }
                        }
                        if ( !$ignore ) {

                            if ( is_array($filter['value']) ) {
                                $filter['value'] = $filter['value']['tagname'];
                            }

                            if ( $filter['relation'] == '=' || $filter['relation'] == '!=' || $filter['relation'] == '<' || $filter['relation'] == '>' ) {
                                if ( $filter['relation'] == '<' || $filter['relation'] == '>' )
                                    $filter['value'] = matheval(str_replace("NOW", time(), $filter['value']));
                                $addW[] = array( $filter['fieldname'], $filter['relation'], $filter['value'], 's' );
                            } else if ($filter['relation'] == "in") {
                                $addW[] = array( $filter['fieldname'], 'IN', explode(",",$filter['value']), 's' );

                            } elseif( $filter['relation'] == "1" ) {
                                $filter['value'] = menuepunkte::setTitelIntern($filter['value']);
                                $addW[] = array( $filter['fieldname'], 'NOT LIKE', '%,' . $filter['value'] . ',%', 's' );
			    } else {
                                $filter['value'] = menuepunkte::setTitelIntern($filter['value']);
                                $addW[] = array( $filter['fieldname'], 'LIKE', '%,' . $filter['value'] . ',%', 's' );
			    }
                        }
                    }
                }
            };

            $order = null;
            if ( isset($this->data["sort"]) && $this->data["sort"] != "" ) {
                $order = $this->data["sort"];
                if ( isset($this->data["order"]) && $this->data["order"] != 0 ) {
                    $order .= ' ';
                    $order .= ( $this->data["order"] == 1) ? "desc" : "asc";
                }
            }
//		if($detailView && session::getObj()->isLoggedIn()) $skipPub = true;
//		else $skipPub = false;

            $fields = $dbs->getFields();
            /*
             * UserFilter
             */
            if ( !$detailView && (isSet($this->data['shoUserFilter']) && $this->data['shoUserFilter'] == '1') ) {
                $userFilter = $this->buildUserFilter($fields, $db['name']);

                $html .= $userFilter->getFilterHtml($this->data);
                $userFilter->setupAddWhere();
                if ( $userFilter->isFilterPostet() ) {
                    $_SESSION['userFilterPosted'] = true;

                    if ( $userFilter->getFilterForm()->posted ) {
                        header("Location: " . $_SERVER["REQUEST_URI"]);
                    }
                }
            }


            if ( isSet($this->data['isTable']) && $this->data['isTable'] == '1' && !$detailView ) {
                $html .= '<table class="sort">';
                $html .= '<tr>';
                $tplHtml = '<tr>';
                $colNr = 1;
                foreach ( $this->dataFields as $id => $field ) {
                    if ( isSet($this->data['tableField' . $id]) && $this->data['tableField' . $id] == '1' ) {
                        $html .= '<th class="col' . $colNr . '">' . $field . '</th>';
                        $tplHtml .= '<td class="col' . $colNr . '"><a href="[LINK(this,' . $db['name'] . ',this)]">[DB_' . $field . ']</a></td>';
                        $colNr++;
                    }
                }
                foreach ( $fields as $field ) {
                    if ( isSet($this->data['tableField' . $field['name']]) && $this->data['tableField' . $field['name']] == '1' ) {
                        $html .= '<th class="col' . $colNr . '">' . $field['name'] . '</th>';
                        $tplHtml .= '<td class="col' . $colNr . '"><a href="[LINK(this,' . $db['name'] . ',this)]">[DB_' . $field['name'] . ']</a></td>';
                        $colNr++;
                    }
                }
                if ( isset($this->data['tableShowPrint']) && $this->data['tableShowPrint'] == '1' ) {
                    $html .= '<th>Drucken</th>';
                    $tplHtml .= '<td class="col' . $colNr . '"><a class="print" href="[LINK(this,' . $db['name'] . ',this,print)]">Drucken</a></td>';
                }
                $html .= '</tr>';
                $tplHtml .= '</tr>';
                $dbs->setTpl($tplHtml);
            }

            if ( isset($this->data['limit']) && $this->data['limit'] > 0 ) {
                if ( strpos($this->data['limit'], ',') )
                    setLimit($this->data['limit']);
                else
                    setLimit(($page - 1) * $this->data['limit'] . ',' . $this->data['limit']);
            }

            foreach ( $addW as $w ) {
                addWhere('`' . $w[0] . '`', $w[1], $w[2], $w[3]);
            }

//		if(isAdmin()) $GLOBALS['mysql_debug'] = true;
            $data = $dbs->getData($db['name'], "*", $order, $skipPublic);
            $GLOBALS['mysql_debug'] = false;
            $descId = '';
            $tagId = '';
            foreach ( $fields as $field ) {
                if ( $field['identifiers'] == 1 )
                    $descId = $field['name'];
                elseif ( $field['identifiers'] == 1 )
                    $tagId = $field['name'];
            }
            if ( $detailView ) {
                if ( !is_array($data) || !isSet($data[0]) ) {
                    $html .= 'Dieser Datensatz existiert nicht mehr.';
                } else {
                    $desc_ids = array( 'Kurztext', 'KurzText', 'KurzBeschreibung', 'KurzBeschreibung' );
                    $i = 0;
                    foreach ( $data[0] as $id => $d ) {
                        if ( in_array($id, $desc_ids) || $id == $descId )
                            $GLOBALS['SEO_description'] = html_entity_decode(strip_tags(str_replace(array( "\n", "\r", "<br />", "<br>" ), '', $d)), ENT_NOQUOTES, "UTF-8");
                        if ( $id == 'Tags' || $id == $tagId )
                            $GLOBALS['SEO_tags'] = substr($d, 1, -1);
                        $i++;
                    }
                    $GLOBALS['SITE_TITLE'] = $dbs->getTitle($data[0]);
                    $tpl = $dbs->getTemplate();
                    if ( $tpl['preview'] != '' ) {
                        if ( $tpl['preview'] != $GLOBALS['akt_menuepunkt']['title_intern'] ) {
                            $GLOBALS['SEO_canonical'] = $db['name'] . '/' . $tpl['preview'] . '-' . $dbs->getFieldId($data[0]);
                        }
                    }
                }
            }
            if ( isSet($this->data['overWriteLinks']) )
                $dbs->setOverWriteLinks($this->data['overWriteLinks']);
            if ( count($data) == 0 ) {
                if ( !$detailView && (isSet($this->data['shoUserFilter']) && $this->data['shoUserFilter'] == '1') && $userFilter->isFilterPostet() && isSet($this->data['noDataOnFilterTxt']) && $this->data['noDataOnFilterTxt'] != '' ) {
                    $html .= '<span class="noResult">' . $this->data['noDataOnFilterTxt'] . '</span>';
                } elseif ( isSet($this->data['noDataText']) && $this->data['noDataText'] != '' )
                    $html .= '<span class="noResult">' . $this->data['noDataText'] . '</span>';
                else {
                    $html .= '<span class="noResult">Im Moment sind keine aktuellen Eintr&auml;ge vorhanden</span>';
                }
            }
            $html .= $dbs->htmlShowData();

            if ( isSet($this->data['isTable']) && $this->data['isTable'] == '1' && !$detailView ) {
                $html .= '</table>';
            }
            $seitenHtml = '';
            if ( isset($this->data['limit']) && $this->data['limit'] > 0 && (!isSet($this->data['doAfterLimit']) || $this->data['doAfterLimit'] == 1 || $this->data['doAfterLimit'] == 2) ) {
                if ( isSet($this->data['addUserFilter']) && $this->data['addUserFilter'] == '1' && !$detailView ) {
                    $userFilter->setupAddWhere();
                }
                addWhere('public', '=', '1');
                foreach ( $addW as $w ) {
                    addWhere($w[0], $w[1], $w[2], $w[3]);
                }
                if ( !$detailView && (isSet($this->data['shoUserFilter']) && $this->data['shoUserFilter'] == '1') ) {
                    $userFilter->setupAddWhere();
                }
                $contentcount = dbCheck('data_' . $db['name']);
                $seitenzahl = ceil($contentcount / $this->data['limit']);

                if ( $seitenzahl > 1 ) {

                    $seitenHtml .= '<div class="pages">';
                    $seitenHtml .= '<div class="pageHelper">';
                    
                    if ( $page > 1 ) {
                        $add = '-Seite' . ($page - 1);
                        if ( $tagView )
                            $seitenHtml .= " <a class=\"seite prev\" href=\"" . $GLOBALS['cms_root'] . 'Tag/' . $_GET['tag'] . $add . ".html\">«</a>";
                        else
                            $seitenHtml .= " <a class=\"seite prev\" href=\"" . $GLOBALS['cms_root'] . $GLOBALS['akt_menuepunkt']['title_intern'] . $add . ".html\">«</a>";
                    }

                    for ( $i = 1; $i < $seitenzahl + 1; $i++ ) {
                        ////                    if($realsektor["afterlimit"] == '1') «»
                        $z = $i;
                        ////			else
                        //              	$z = '';
                        //			if($i == 0) $html .= " <a class=\"seite\" href=\"".$GLOBALS['cms_roothtml'].$GLOBALS['akt_menuepunkt']['title_intern'].".html\">".$z."</a>";

                        if ( $i < 2 || $i > $seitenzahl - 3 || ($i > $page - 3 && $i < $page + 3) || $seitenzahl < 10 ) {

                            $add = ($i == 1) ? '' : '-Seite' . $i;
                            if ( $i == $page ) {
                                $seitenHtml .= " <span class=\"seiteact\">" . $z . "</span>";
                            } else {
                                if ( $tagView )
                                    $seitenHtml .= " <a class=\"seite\" href=\"" . $GLOBALS['cms_root'] . 'Tag/' . $_GET['tag'] . $add . ".html\">" . $z . "</a>";
                                else
                                    $seitenHtml .= " <a class=\"seite\" href=\"" . $GLOBALS['cms_root'] . $GLOBALS['akt_menuepunkt']['title_intern'] . $add . ".html\">" . $z . "</a>";
                            }
                        } elseif ( $i == 2 || $i == $seitenzahl - 3 ) {
                            $seitenHtml .= ' <span class="dots">...</span>';
                        }
                    } //seitenzähler

                    if ( $page < $seitenzahl - 1 ) {
                        $add = '-Seite' . ($page + 1);
                        if ( $tagView )
                            $seitenHtml .= " <a class=\"seite next\" href=\"" . $GLOBALS['cms_root'] . 'Tag/' . $_GET['tag'] . $add . ".html\">»</a>";
//						$html .= " <a class=\"seite\" href=\"".buildLink($GLOBALS['akt_menuepunkt']['title_intern'],'', '', $page+1)."\">»</a>";
                        else
                            $seitenHtml .= " <a class=\"seite next\" href=\"" . $GLOBALS['cms_root'] . $GLOBALS['akt_menuepunkt']['title_intern'] . $add . ".html\">»</a>";
                    }

                    $seitenHtml .= '</div>';
                    $seitenHtml .= '</div>';
                }
                if ( $this->data['doAfterLimit'] == 2 )
                    $html = $seitenHtml . $html;
            }

            if ( !$detailView && isSet($this->data['contentPrev']) ) {
                $html = $this->data['contentPrev'] . $html;
            }
            $html .= $seitenHtml;
            if ( !$detailView && isSet($this->data['contentNext']) ) {
                $html .= $this->data['contentNext'];
            }
            return $html;
        }

        public static function getSubName() {
            return 'DatabaseConnector';
        }

    }

?>
