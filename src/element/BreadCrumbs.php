<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BreadCrumbs
 *
 * @author F.Ehrlich
 */
class BreadCrumbs extends element {

	public function getInline() {
		return BreadCrumbs::createBreadCrumbs(false, $this->data['seperator']);
	}

	static public function createBreadCrumbs($menupunkt = false, $seperator = ' - ') {
		if ($menupunkt == false)
			$menuepunkte = $GLOBALS['aktmenu'];
		else {
			$menuepunkte = getMenuHierarchy($menupunkt);
		}
//		print_r($menuepunkte);
		$html = '';
//$GLOBALS['mysql_debug'] = true;
		addWhere('id', 'IN', $menuepunkte);
		addWhere('visible', '=', '1');
		select('menuepunkte', 'id,title_intern,title,verlinkung', 'ordernr');
//$GLOBALS['mysql_debug'] = false;
		$mps = getRows();
		$h = array();
		$anz = count($mps);
//		print_r($mps);
		for ($i = 0, $anz; $i < $anz; $i++) {
			#$html .= '<a href="'.$GLOBALS['cms_roothtml'].$mps[$i]['title_intern'].'.html">'.$mps[$i]['title'].'</a>';
			if($mps[$i]['id'] == $GLOBALS['akt_menuepunkt']['id'])
				$html .= $mps[$i]['title'];
			else{
				if ($mps[$i]['verlinkung'] != '' && $mps[$i]['verlinkung'] != '0') {
					if (substr($mps[$i]['verlinkung'], 0, 7) == 'http://') {
						$link = $mps[$i]['verlinkung'];
					} else {
						$link = $GLOBALS['cms_roothtml'] . $mps[$i]['verlinkung'] . '.html';
					}
				} else {
					$link = $GLOBALS['cms_roothtml'] . $mps[$i]['title_intern'] . '.html';
				}

				$html .= '<a href="' . $link . '">' . $mps[$i]['title'] . '</a>';


				if($i != $anz-1) $html .= $seperator;
			}
		}
//		if ($anz >= 0)
//			$html .= $mps[$anz]['title'];

		return $html;
	}

	public function formBuild() {
		$this->form->addElement('Html Seperator', 'seperator');
	}

}
?>
