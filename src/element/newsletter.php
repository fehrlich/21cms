<?php
    class newsletter extends element {

        private $token = null;
        private $lastMessage = null;


        public function formBuild() {

            $groups = buildSelectArray("addressGroups", array('id', 'name'));

            $this->form->addelement('Buttontext', 'buttontext', 'text');
            $this->form->addelement('addToGroup', 'addToGroup', 'select', 0, $groups);
            $this->form->addelement('fromMail', 'fromMail', 'text');

            $this->form->useTab('Content');
            $this->form->addelement('Bestätigungsmail', 'addMailTxt', FormType::HTMLEDITOR, 'Vielen dank für Ihr Interesse an dem Newsletter.');
            $this->form->addelement('AbmeldeMail', 'removeMailTxt', FormType::HTMLEDITOR, 'Vielen dank für Ihr Interesse an dem Newsletter.');
        }


        private function _replaceToken($body) {

            return str_replace('{token}', $this->token, $body);

        }

        private function _checkForActivation() {

            if (isset($_GET["para"]) && !isset($_GET["remove"])) {

                if (strlen($_GET["para"]) == 40 && addresses::getObj()->authUser($_GET["para"])) {
                    $this->lastMessage = "Ihre Emailadresse wurde erfolgreich aktiviert!";
                }
            }
        }

        private function _checkForDeletion() {
            if (isset($_GET["para"]) && isset($_GET["remove"])) {

                if (strlen($_GET["para"]) == 40 && addresses::getObj()->deleteUser($_GET["para"])) {
                    $this->lastMessage = "Ihre Emailadresse wurde erfolgreich gelöscht!";
                }
            }
        }

        private function _checkForUpdate() {
            if (isset($_POST["email"]) && preg_match("~^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$~i", $_POST["email"])) {

                if (!addresses::getObj()->getUser($_POST["email"])) {
                    $this->token = addresses::getObj()->addUser($_POST,$this->data["addToGroup"]);

                    $mailBody = $this->_replaceToken($this->data["addMailTxt"]);

                    if (isSet($this->data['addMailTxt']) && $this->data['addMailTxt'] != '') {
                        $mail = new mimeMail();
                        $mail->addRecipient($_POST["email"]);
                        $mail->setBody($mailBody);

                        if ( isset($this->data["fromMail"]) ) {
                            $mail->setFrom($this->data["fromMail"], $this->data["fromMail"]);
                        }

                        $mail->send();
                    }

                    $this->lastMessage =  'Vielen Dank für Ihr Interesse';
                } else {
                    $this->token = addresses::getObj()->getUser($_POST["email"]);
                    $this->token = $this->token["token"];

                    $mailBody = $this->_replaceToken($this->data["removeMailTxt"]);

                    if (isSet($this->data['removeMailTxt']) && $this->data['removeMailTxt'] != '') {
                        $mail = new mimeMail();
                        $mail->addRecipient($_POST["email"]);
                        $mail->setBody($mailBody);

                        if ( isset($this->data["fromMail"]) ) {
                            $mail->setFrom($this->data["fromMail"], $this->data["fromMail"]);
                        }

                        $mail->send();
                    }

                    $this->lastMessage =  'Vielen Dank für Ihr Interesse';
                }
            }
        }


        public function getInline() {

            $btnTxt = (isset($this->data['buttontext']) && $this->data['buttontext'] != '') ? $this->data['buttontext'] : 'Anmelden/Abmelden';

            $html = '';

            $this->_checkForActivation();
            $this->_checkForDeletion();
            $this->_checkForUpdate();

            $html .= '<form action="'.$_SERVER["REQUEST_URI"].'" method="post">';
            $html .= '  <div class="newsletter">';

            if ($this->lastMessage !== null) {
                $html .= '  <p>' . $this->lastMessage . '</p>';
            }

            $html .= '<input type="text" name="email" value="Email" />';
            $html .= '<input type="submit" class="signin" value="'.$btnTxt.'" />';
            $html .= '</div>';
            $html .= "</form>";

            return $html;
        }
    }
?>