<?php
    class gallery extends element {

        private $pfad = null;
        private $webpfad = null;
        private $html = "<div id=\"gallery\">Galerie leer!</div>\n";
        private $request = null;
        private $files = array();


        public function getInline() {

            if (!isset($this->data["bild"])) {
                return "Keine Bilder vorhanden!";
            }

            $this->pfad = realpath(dirname(__FILE__) . "/..") . $this->data["bild"];
            $this->docrootlenght = strlen(realpath(dirname(__FILE__) . "/.."));
            $this->webpfad = $this->data["bild"];

            if (isset($_GET["para"]) && $_GET["para"] != "") {

                $this->request = explode("\\",$_GET["para"]);
                $ordner = '/';
                foreach($this->request as $ord) {
                    $ordner .= $ord . "/";
                }

                if (file_exists($this->pfad . $ordner)) {
                    $this->pfad = $this->pfad . $ordner;
                    $this->webpfad = $this->data["bild"] . $ordner;
                } else {
                    unset($_GET["para"]);
                }
            }

            $this->readdir($this->pfad);


            if (!empty($this->files)) $this->genHTML();


            return $this->html;
        }

        private function genHTML() {

            $this->html = "<div id=\"gallery\">\n";
            $this->html .= "<div class=\"backlink\">";

            if (isset($_GET["para"]) && $_GET["para"] != "") {

                $dir = explode("\\",$_GET["para"]);

                unset($dir[0]);
                unset($dir[count($dir)]);

                if (count($dir) > 1) {

                    $dir = implode("\\",$dir);
                } else {
                    $dir = "\\";
                }

                $this->html .= "<a href=\"".$GLOBALS['cms_root'].$GLOBALS['akt_menuepunkt']['title_intern'];

                if ($dir != "\\") $this->html .= "-P_". $dir ;
                $this->html .= ".html\">eine Ebene höher</a>\n";
            }

            $this->html .= "</div>\n";



            $i = 0;
            ksort($this->files);
            $this->files = array_reverse($this->files);

            foreach($this->files as $pfad => $files) {


                if (!is_array($files)) {
                    $para = "";
                    if (isset($_GET["para"])) {
                        $para = $_GET["para"] . "\\" . $files;
                    }

                    $this->html .= "<div class=\"gallery-dir\">\n";
                    $add = '';

                    if (isset($this->data["showFolder"]) && $this->data["showFolder"] > 0) {
                        if($this->data["showFolder"] == 1) {
                            $this->html .= "  <span class=\"gallery-name\">" . htmlentities($files,ENT_QUOTES) . "</span>\n";
                        } else {
                            $add = "  <span class=\"gallery-name\">" . htmlentities($files,ENT_QUOTES) . "</span>\n";
                        }
                    }

                    $this->html .= "   <div class=\"gallery-preview\">\n";
                    $this->html .= "     <a href=\"".$GLOBALS['cms_root'].$GLOBALS['akt_menuepunkt']['title_intern'] . "-P_". $para .".html\">\n";
                    $this->html .= "       <div class=\"img\"><img src=\"/images/folder.png\" alt=\"". htmlentities($files,ENT_QUOTES) . "\" /></div>\n";
                    $this->html .= "     </a>\n";
                    $this->html .= "   </div>\n";
                    $this->html .= $add;
                    $this->html .= "</div>\n";

                } else {

                    sort($files);
                    $galname = explode("/",$pfad);
                    $galname = array_pop($galname);

                    $filecount = count($files);

                    $path = explode("::",$pfad,2);
                    $path = substr($path[1],$this->docrootlenght);

                    $this->html .= "<div class=\"gallery-dir\">\n";
                    $add = '';

                    if (isset($this->data["showFolder"]) && $this->data["showFolder"] > 0) {
                        if($this->data["showFolder"] == 1) {
                            $this->html .= "  <span class=\"gallery-name\">" . htmlentities($galname,ENT_QUOTES) . "</span>\n";
                        } else {
                            $add = "  <span class=\"gallery-name\">" . htmlentities($galname,ENT_QUOTES) . "</span>\n";
                        }
                    }

                    $handle = new \cms\file($path ."/". $files[0]);
                    $comment = $handle->getcomment();
                    unset($handle);


                    $this->html .= "   <div class=\"gallery-preview dir" . ++$i ."\">\n";
                    $this->html .= "     <a href=\"".urlEncodeWithoutSlash($path) ."/__thumb1/".urlEncodeWithoutSlash($files[0])."\" title=\"{$comment}\">\n";
                    $this->html .= "       <div class=\"img\"><img src=\"". urlEncodeWithoutSlash($path) ."/__thumb2/".urlEncodeWithoutSlash($files[0])."\" alt=\"". htmlentities($files[0],ENT_QUOTES) . "\" /></div>\n";

                    if (isset($this->data["showCount"]) && $this->data["showCount"] == 1)
                        $this->html .= "       <span class=\"gallery-imagecount\">enthält {$filecount} Bilder</span>\n";

                    $this->html .= "     </a>\n";

                    unset($files[0]);
                    if (count($files) > 0) {
                        foreach($files as $file) {
                            $handle = new \cms\file($path ."/". $file);
                            $comment = $handle->getcomment();
                            unset($handle);

                            $this->html .= "<a href=\"". urlEncodeWithoutSlash($path) ."/__thumb1/".urlEncodeWithoutSlash($file)."\" title=\"{$comment}\"></a>\n";
                        }
                    }

                    $this->html .= "   </div>\n";
                    $this->html .= $add;
                    $this->html .= "</div>\n";

                }

            }

            $this->html .= "<div style=\"clear: both;\"></div>\n";
            $this->html .= "</div>\n";

//            $this->html .= '<script src="/js/jquery.lightbox-0.5.min.js"></script>';
//            $this->html .= '<script type="text/javascript"> $(document).ready(function() {';
//
//            for($j=1;$j <= $i;$j++)
//                    $this->html .= '$(\'.dir'.$j.' a\').lightBox();';
//
//            $this->html .= '}); </script>';
        }

        private function checksubfolders($pfad) {
            $dir = opendir($pfad);
            $return = false;

            while ($file = readdir($dir)) {
                if ($file == "." || $file == ".." || $file == "__thumb1" || $file == "__thumb2") {
                    continue;
                }
                $arr = explode(".",$file);
                $ext = strtolower(array_pop($arr));
                if ($ext == "jpg" || $ext == "gif" || $ext == "png") {
                    if ($this->ImageResize($file,$pfad)) {
                        $this->files[(filemtime($pfad)) . "::" .$pfad][] = $file;
                        $return = true;
                    }
                }
            }

            closedir($dir);
            return $return;

        }

        private function readdir($pfad,$lvl = 0) {
            if ($lvl > 1) return false;

			if(!is_dir($pfad)) return false;
            $dir = opendir($pfad);

            while ($file = readdir($dir)) {
                if ($file == "." || $file == ".." || $file == "__thumb1" || $file == "__thumb2") {
                    continue;
                }

                if (is_dir($pfad . "/" . $file)) {
                    if (!$this->checksubfolders($pfad . "/" . $file)) {
                        $this->files[(filemtime($pfad)) . "::" .$pfad . "/" . $file] = $file;
                    } elseif ($lvl < 1) {
                        $this->readdir($pfad . "/" . $file,$lvl + 1);
                        continue;
                    }
                }
            }

            closedir($dir);
        }

        public function ImageResize($name,$path) {

            if (file_exists("{$path}/__thumb1/$name") && file_exists("{$path}/__thumb2/$name")) {
                return true;
            }

            $image = @getimagesize($path ."/". $name);

            if (is_writeable($path)) {
                if (!file_exists($path . "/__thumb1")) {
                    mkdir($path . "/__thumb1");
                    mkdir($path . "/__thumb2");
                }
            } else {
                return false;
            }


            //filetype ansehen!

            $sizes = array();
            $sizes[] = array(900,675);

            if (isset($this->data["maxHeight"]) && $this->data["maxHeight"] && isset($this->data["maxWidth"]) && $this->data["maxWidth"] != "") {
                $sizes[] = array((int) $this->data["maxWidth"],(int) $this->data["maxHeight"]);
            } else {
                $sizes[] = array(125,94);
            }

            for ($i = 0; $i < count($sizes);$i++) {
                  $thumbpath = "{$path}/__thumb".($i +1)."/$name";
                  image::create($path ."/".$name,1)
                          ->resize($sizes[$i][0], $sizes[$i][1],1)
                          ->save($thumbpath)
                          ->cleanup();
            }

            return true;
        }

        public function formbuild() {
            $this->form->addelement('Ordner', 'bild','folder');
            $this->form->addelement('Bilderanzahl anzeigen',"showCount", 'select','0',array(0 => "nein",1 => "ja"));
            $this->form->addelement('Ordnernamen anzeigen',"showFolder", 'select','0',array("nein", "Über Bild","Unter Bild"));
            $this->form->addElement('Thumbnailhöhe', 'maxHeight','text');
            $this->form->addElement('Thumbnailbreite', 'maxWidth','text');

//            $this->form->addelement('Text ausrichtung',"showText", 'select','1',array("Nicht anzeigen","Über Bild","Unter Bild"));
	}
    }
?>