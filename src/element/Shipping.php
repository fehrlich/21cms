<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shipping
 *
 * @author XX
 */
class Shipping extends element {

    protected $versandArten = array(
        'abhohlung' => 'Abhohlung',
        'versand' => 'Versand'
    );

    public function formBuild() {
//        new dBug($this->versandArten);
        $this->form->addElement('Versandarten', 'shippingOptions', FormType::MULTISELECT, '', $this->versandArten);
        $this->form->useTab('Paketgrößen');
        $f = new formular();
        $f->addElement('Größe in kg', 'size', 'text');
        $f->addElement('Preis', 'price', 'text');
        $this->form->addElement('Paketgrößen', 'packageOptions', FormType::FORMULA, '', $f);
    }

    public function getInline() {
        $this->data['shippingOptions'] = explode(',', substr($this->data['shippingOptions'],1,-1));
        $html = 'Bitte w&auml;hlen Sie ihre Versandart aus: ';
        $form = new formular();
        $html .= '<select name="versandoptionen" id="versandoptionen">';
        if (isSet($this->data['shippingOptions'])) {
            foreach ($this->data['shippingOptions'] as $key => $art) {
                if ($this->data['shippingOptions'])
                    $html .= '<option value="' . $art . '">' . $art . '</option>';
            }
        }
        $html .= '</select>';

        $packages = unserialize($this->data['packageOptions']);
        $packages = flipArray($packages);
        foreach ($packages as $temp_list) {
            $sort_aux[] = $temp_list['size'];
        }
        array_multisort($sort_aux, SORT_NUMERIC, $packages);

//        new dBug($packages);
        $weight = cart::getObj()->getWeight();
        $weightOver = $weight;
        $price = 0;
        while($weightOver > 0){
            $package = $this->getBestPackage($packages, $weightOver);
            $price += $package['price'];
            $weightOver -= $package['size'];
            $weightOver -= 5;
//            new dBug($package);
        }
        $html .= '<br />Gewicht: '.number_format($weight, 2,',','.').'kg<br>';
        $html .= 'Versandkosten: '.number_format($price, 2,',','.').'€';
        
        cart::getObj()->setShippingPrice($price);

        return $html;
    }
    
    private function getBestPackage($packages, $weight){
        foreach($packages as $i => $p){
            if($weight < $p['size']) return $p;
        }
        return $packages[count($packages)-1];
    }
    
}

?>
