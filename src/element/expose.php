<?php
/**
 * Description of profilSeite
 *
 * @author Franz Ehrlich
 */
class expose extends element {

        public function formBuild() {
            $this->form->addelement('Bildquelle', 'bild','bild');
        }

        public function getInline() {

            $html = '';
            
            if (cms\session::getObj()->get("adShown") !== "yes") {
                #setcookie("adShown", true, time()+60*60*24*7, "/");
                cms\session::getObj()->set("adShown","yes");
                $html = "<img style=\"position: absolute; top: 100px; \" class=\"expose\" src=\"{$this->data["bild"]}\" alt=\"\" />";
                $html .= "<style>#exposeMask { background-color: #000; }</style>";
                $html .= '<script src="/js/toolbox.expose.min.js" type="text/javascript"></script>';
                $html .= '<script type="text/javascript">

                                    $("img.expose").expose({
                                        color: "#000000",
                                        onClose: function() {
                                            $("img.expose").hide();
                                            }
                                    });
                                    $("img.expose").click(function () {
                                        $.mask.close();
                                        $("img.expose").hide();
                                    });

                          </script>';
            } else {
                $html = "";
            }

            return $html;
        }
}
?>