<?php
    class newsletterArchiv extends element {

        private $newsletter = array();

        public function getInline() {
        
            $this->getNewsletterList();
            return $this->getList();
        }



        public function getNewsletterList() {
            addWhere("public","=",1);
            select("newsletterTemplates","id,datum,title,sent","datum desc");

            while($row = getRow()) {
                $date = new datetime("@" . $row["sent"]);
                //$this->newsletter[$date->format("Y")][$date->format("m")][$row["id"]] = array("id" => $row["id"],"title" => $row["title"]);
                $this->newsletter[$date->format("Y")][$row["id"]] = array("id" => $row["id"],"title" => $row["title"]);
            }
        }

        public function getList() {
            

            if (empty($this->newsletter)) return "Keine Newsletter vorhanden";
            $html = "<div class=\"newsletterArchiv\">";

            $request = explode("-",$_GET["para"],3);

            $years = array_keys($this->newsletter);
            

            if (isset($request[0]) && in_array($request[0], $years)) {
                $yearActive = $request[0];
            } else {
                $yearActive = max($years);
            }
/*
            $month = array_keys($this->newsletter[$yearActive]);

            if (isset($request[1]) && in_array($request[1], $month)) {
                $monthActive = $request[1];
            } else {
                $monthActive = max($month);
            }

            $ids = $this->newsletter[$yearActive][$monthActive];
*/          $ids = $this->newsletter[$yearActive];
            
            if (isset($request[1]) && in_array($request[1], array_keys($ids))) {
               $idActive = $request[1];
            } else {
                $idActive = array_keys($ids);
                reset($ids);
                $idActive = current($ids);
                $idActive = $idActive["id"];
            }

            //build year and month
            $yearSelect = " Jahr:<select id=\"newsletterYear\">";
            foreach($years as $year) {
                $selected = (isset($yearActive) && $year == $yearActive) ? "selected=\"selected\"" : '';
                $yearSelect .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
            }
            $yearSelect .= "</select>";
/*
            $monthSelect = " Monat:<select id=\"newsletterMonth\">";
            foreach($month as $m) {
                $selected = (isset($monthActive) && $m == $monthActive) ? "selected=\"selected\"" : '';
                $monthSelect .= "<option value=\"{$m}\" {$selected}>{$m}</option>";
            }
            $monthSelect .= "</select>";
*/
            $idSelect = " Newsletter:<select id=\"newsletterId\">";
            foreach($this->newsletter[$yearActive] as $id) {
                
                $selected = (isset($idActive) && $id["id"] == $idActive) ? "selected=\"selected\"" : '';
                $id["title"] = htmlentities(utf8_decode($id["title"]));
                $idSelect .= "<option value=\"{$id["id"]}\" {$selected}>{$id["title"]}</option>";
            }
            $idSelect .= "</select>";

            $html .= $yearSelect . $idSelect;
            $html .= "</div>";

            $html .= $this->getNewsletter($idActive);

            return $html;

        }
        public function getNewsletter($id) {
            addWhere("id","=",$id);
            select("newsletterTemplates","html");
            $data = getRow();

            return (isset($data["html"])? $data["html"] : '');
        }

      public function formBuild() {

      }


    }
?>