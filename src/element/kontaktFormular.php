<?php

class kontaktFormular extends mainelement {

	private $sendedMailHtml = '';
	private $kontaktForm = null;

	public function parseMailTpl($tpl) {
		$tpl = str_replace('[THIS_SITE_HREF]', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], $tpl);
		if(strpos($tpl, '[HTTP_REFERER_NAME]') || strpos($tpl, '[HTTP_REFERER_NAME]')){
			if (isSet($_SERVER['HTTP_REFERER']) && 'http://' . $_SERVER['HTTP_HOST'] == substr($_SERVER['HTTP_REFERER'], 0, strlen('http://' . $_SERVER['HTTP_HOST']))) {
				$ref_link = $_SERVER['HTTP_REFERER'];

				$ref_name = substr($_SERVER['HTTP_REFERER'], strlen('http://' . $_SERVER['HTTP_HOST'] . $GLOBALS['cms_roothtml']), -5);
				$spl = explode('/', $ref_name);
				if (isSet($spl[2]) || (isSet($spl[1]) && !isAdmin())) {
					if (isAdmin ()) {
						$spl[0] = $spl[1];
						$spl[1] = $spl[2];
					}
					$spl2 = explode('-', $ref_name);
					if (isSet($spl2[1])) {
						$ref_name = $spl2[1];
					}

					$dbname = 'data_' . $spl[0];
					addWhere('name', '=', $spl[0]);
					select('databases', 'id,fieldid');
					$db = getRow();
					$dbid = $db['id'];
					if ($db['id'] > 0) {
						addWhere('title_intern', '=', $ref_name);
						select($dbname, $db['fieldid']);
						$row = getRow();
						$ref_name = $row[$db['fieldid']];
					}
				} else {
					addWhere('title_intern', '=', $ref_name);
					select('menuepunkte', 'title');
					$row = getRow();
					if (isSet($row['title']) && $row['title'] != '')
						$ref_name = $row['title'];
				}
			}else {
				$ref_link = 'http://' . $_SERVER['HTTP_HOST'] . '/';
				$ref_name = $_SERVER['HTTP_HOST'];
			}
	//		$tpl = str_replace('[TITEL]', substr($_SERVER['HTTP_REFERER'],strlen('http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml']), -5), $tpl);
			$tpl = str_replace('[HTTP_REFERER_LINK]', '<a href="' . $ref_link . '">' . $ref_name . '</a>', $tpl);
			$tpl = str_replace('[HTTP_REFERER_NAME]', $ref_name, $tpl);
		}
		$tpl = str_replace('[SENDED_MAIL]', $this->sendedMailHtml, $tpl);

		return $tpl;
	}

	public function getInline() {

		if (!isAdmin()) {
			include_once('klassen/formular.php');
			include_once('klassen/html/Table.php');
			include_once('klassen/mimeMail.php');
		}
		if (strip_tags($this->data['mailTpl']) == '')
			$this->data['mailTpl'] = '';
		if (strip_tags($this->data['ReturnMailTpl']) == '')
			$this->data['ReturnMailTpl'] = '';
		if (isset($this->data['mailTpl']) && $this->data['mailTpl'] != '')
			$useCustomTpl = true;
		else
			$useCustomTpl = false;

		$this->kontaktForm = new formular('formSubmits', '', '', 'kontakt' . $this->id);
		$this->kontaktForm->setDoBackup(false);
//        $form->setI
//		$this->kontaktForm->addElement("", "time", "hidden", time());
		$this->kontaktForm->addElement("", "seitenTitel", "hidden", $GLOBALS['akt_menuepunkt']['title']);
		$this->kontaktForm->addElement("", "formId", "hidden", $this->id);
		$this->kontaktForm->startSerialize('data');
		$data = $this->getArrayList('*',false,'ordernr',true);
		foreach ($data as $formLineData) {
			$line = unserialize($formLineData['eldata']);
			if (!isSet($line['pflicht']))
				$line['pflicht'] = false;
			switch ($line['type']) {
				case 'select':
//					$line['type'] = 'selectTxt';
					$lang = Languages::getLang (false);
					if(isSet($line['fields_'.$lang]) && $line['fields_'.$lang] != '')
						$line['fields'] = $line['fields_'.$lang];
					$line['fields'] = Vars::parseTagVars($line['fields']);
					$eldata_spl = explode(',', $line['fields']);
					$eldata = array();
					foreach ($eldata_spl as $d) {
						$eldata[$d] = $d;
					}
					break;
				case 'simpleCheck':
					$eldata = $line['text'];
					break;
				default:
					$eldata = array();
			}
			$line['data'] = $eldata;
			$line['formbuild'] = true;
			$el = new formularzeile($formLineData['id'], $line);
			if ($useCustomTpl)
				$this->data['mailTpl'] = str_replace($line['title'], menuepunkte::setTitelIntern($line['title']), $this->data['mailTpl']);
//			$form->addElement($line['title'], menuepunkte::setTitelIntern($line['title']),$line['type'],'',$line);
			$default = (isSet($line['default'])) ? $this->parseMailTpl($line['default']) : '';
			if (isSet($line['uploaddir']))
				$default = $line['uploaddir'];
			if ($line['type'] == 'bild') {
				$line['type'] = 'upload';
				$htmlData = $el->getData();
				$htmlData['html'] = (string) $el;
				$default = $line['bilduploaddir'];
			}else {
				$htmlData = (string) $el->getContent();
			}
			if($line['type'] == 'upload' && isSet($line['uploadFileData'])){
				$htmlData = $el->getData();
				$htmlData['html'] = (string) $el;
				$htmlData['fileData'] = $line['uploadFileData'];
			}
			$line['secType'] = $line['type'];
			$line['name'] = menuepunkte::setTitelIntern($line['title']);
			$line['value'] = $default;
			$line['tab'] = '';
			$line['art'] = $line['type'];
			$lang = Languages::getLang(false);
			if($lang != '' && isSet($line['title_'.$lang]) && $line['title_'.$lang] != '')
				$line['desc'] = $line['title_'.$lang];
			else
				$line['desc'] = $line['title'];

			if(!isAdmin() && isSet($line['onlyShowOnMainElement']) && $line['onlyShowOnMainElement'] != ''){
				$this->kontaktForm->addOnlyShowOn($formLineData['id'], $line['onlyShowOnMainElement'], ($line['onlyShowOnMainValue']));
			}
			$cssClass = (isSet($line['cssClass']))?$line['cssClass']:'';
			$this->kontaktForm->addElement($line['desc'], $formLineData['id'], 'html', $default, $htmlData, true, $line['pflicht'], $line['type'],false,'',$cssClass); //#917 richtige anzeige: $line['desc'] = ''
//			$form->addElementArrays($line);
		}
		$this->kontaktForm->stopSerialize();
		$this->kontaktForm->setFormAction($_SERVER['REQUEST_URI']);

		/* Maildaten auswerten */
		if (substr($this->data['mailTo'], 0, 1) == '[' && $this->kontaktForm->posted) {
			$mailAdress = trim($_POST[menuepunkte::setTitelIntern(substr($this->data['mailTo'], 1, -1))]);
		}
		else
			$mailAdress = $this->data['mailTo'];
		if (substr($this->data['fromName'], 0, 1) == '[' && $this->kontaktForm->posted)
			$fromName = $_POST[menuepunkte::setTitelIntern(substr($this->data['fromName'], 1, -1))];
		else
			$fromName = $this->data['fromName'];
		if (substr($this->data['fromEmail'], 0, 1) == '[' && $this->kontaktForm->posted)
			$fromEmail = $_POST[menuepunkte::setTitelIntern(substr($this->data['fromEmail'], 1, -1))];
		else
			$fromEmail = $this->data['fromEmail'];

		if (!isSet($this->data['subject']))
			$this->data['subject'] = '';
		$this->kontaktForm->setMailAdress($mailAdress, $fromName, $fromEmail, $this->data['subject']);
		if ($useCustomTpl) {
			$this->data['mailTpl'] = $this->parseMailTpl($this->data['mailTpl']);
			$this->kontaktForm->setMailTemplate($this->data['mailTpl']);
		}
		if (isSet($this->data['buttonTxt']))
			$buttonTxt = $this->data['buttonTxt'];
		else
			$buttonTxt = 'Senden';
		$this->kontaktForm->setSaveButton($buttonTxt, true);
		if (isSet($this->data['spamProtection']) && $this->data['spamProtection'] == '1')
			$this->kontaktForm->setSpamProtection(true);
		$this->sendedMailHtml = $this->kontaktForm->getMailSource();

		if (isSet($this->data['pdfTemplate']) && $this->data['pdfTemplate'] != '' && isSet($this->data['pdfPreview']) && $this->data['pdfPreview']){
			$this->kontaktForm->addElement('Preview', 'showPreview',  FormType::HIDDENFIELD,'',array(),false);
			$this->kontaktForm->addElement('Preview', 'ajaxform'.$this->kontaktForm->getFormId(),  FormType::HIDDENFIELD,'1',array(),false);
			$this->kontaktForm->addElement('Preview', 'preview',  FormType::BUTTON,'Vorschau',array('onclick' => "$(this).parents('form').find('input[name=showPreview]').val(1);$(this).parents('form').attr('target','__blank').submit();$(this).parents('form').attr('target','');$(this).parents('form').find('input[name=showPreview]').val(0);"),false);
		}
		$ret = (string) $this->kontaktForm->__toString();
		if ($ret == 'Ok') {
			if(!isSet($_POST['showPreview']) || $_POST['showPreview']!='1')
                            $this->triggerEvent('FormSended');
			if (isSet($this->data['ReturnMailTpl']) && strip_tags($this->data['ReturnMailTpl']) != '') {
				$rettpl = $this->parseMailTpl($this->data['ReturnMailTpl']);
				$msg = $this->kontaktForm->parseMailTpl($rettpl);
				$mail = new mimeMail();
				$mail->setFrom($this->data["mailTo"],$this->data["fromName"]);
				$mail->setSubject($this->data['subject']);
				$mail->addRecipient($fromEmail);
				$mail->setBody($msg);
				$mail->send();
			}
			if (isSet($this->data['pdfTemplate']) && $this->data['pdfTemplate'] != '') {
				if (!isAdmin()){
					include 'klassen/PdfTemplate.php';
				}else{
					include '../klassen/PdfTemplate.php';
				}
				$dest = 'F';
				if(isSet($_POST['showPreview']) && $_POST['showPreview']=='1')
					$dest = 'I';
				$parser = new Parser($this->kontaktForm);
				$file = PdfTemplate::generatePdf($this->data['pdfTemplate'], $parser, str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $this->data['pdfTemplateFolder']) . $this->data['uploadFileName'].'.pdf', $dest);

				if(is_array($file)){
					$elements = $this->kontaktForm->getElements();
					foreach($elements as $formEl){
						if(in_array(menuepunkte::setTitelIntern($formEl['desc']), $file) || in_array(menuepunkte::setTitelIntern($formEl['name']), $file)){
							$this->kontaktForm->setElement($formEl['name'], 'errormsg', 'Dieses Element ist zu groß f&uuml;r den vorgegebenen Platz in der Pdf-Datei');
						}
					}
					$this->kontaktForm->posted = false;
					return $this->kontaktForm->show(true);
				}
				if($dest == 'I') exit();
				if(isSet($this->data['uploadFileData'])){
					$txt = $parser->parseTxt($this->data['uploadFileData']);
					$spl = explode("\n",$txt);
					foreach($spl as $line){
						$lineSpl = explode("=",$line,2);
						if(isSet($lineSpl[1])){
							$name = trim($lineSpl[0]);
							$value = trim($lineSpl[1]);
							if($name != ''){
								$file->set($name, $value);
							}
						}
					}
				}
				$file->close();

			}
			if (isSet($this->data['acceptTxt']) && $this->data['acceptTxt'] != '') {
				$ret = $this->kontaktForm->parseMailTpl($this->data['acceptTxt']);
			}else
				$ret = 'Ihre Anfrage wurde erfolgreich versendet';
			if(isSet($this->data['postRedirect']) && $this->data['postRedirect']){
				if(!strpos($_SERVER['REQUEST_URI'], '-P_'))
					$url = substr($_SERVER['REQUEST_URI'], 0,-5).'-P_success.html';
				else {
					$url = $_SERVER['REQUEST_URI'];
				}
				header ('Location: '.$url);
			}
			return '<span class="accepted">' . htmlspecialchars($ret, ENT_QUOTES, 'UTF-8') . '</span>';
		}
		else{
			if(isSet($this->data['postRedirect']) && $this->data['postRedirect'] && isSet($_GET['para']) && $_GET['para'] == 'success'){
				if (isSet($this->data['acceptTxt']) && $this->data['acceptTxt'] != '') {
					$retAcc = $this->kontaktForm->parseMailTpl($this->data['acceptTxt']);
				}else
					$retAcc = 'Ihre Anfrage wurde erfolgreich versendet';
				$ret = '<span class="accepted">' . htmlspecialchars($retAcc, ENT_QUOTES, 'UTF-8') . '</span><br />'.$ret;
			}
//			if(frontendSession::getObj()->get);
			$sessionId = '<div id="sessionId" style="display: none">'.  frontendSession::getObj()->getSessionId().'</div>';
			return $sessionId.((isset($this->data["textbefore"]) && !empty($this->data["textbefore"]))? $this->data["textbefore"]:'') . $ret.((isset($this->data["textAfter"]) && !empty($this->data["textAfter"]))? $this->data["textAfter"]:'');
		}
	}

	public function formParse($form, $txt) {
		$form = new formular($tab, $formAction, $mailadress, $formid);
		$elements = $form->getElements();
		foreach ($elements as $el) {
			$txt = str_replace('[FORM_' . $el['name'] . ']', $el['value'], $txt);
			$txt = str_replace('[FORM_' . menuepunkte::setTitelIntern($el['desc']) . ']', $el['value'], $txt);
		}
		return $txt;
	}

	public function formBuild() {


		$this->form->addelement('Email an', 'mailTo', 'text', 'info@' . $_SERVER['HTTP_HOST']);
		$this->form->addelement('von Name', 'fromName', 'text', 'info@' . $_SERVER['HTTP_HOST']);
		$this->form->addelement('von Email', 'fromEmail', 'text', 'info@' . $_SERVER['HTTP_HOST']);
		$this->form->addelement('Betreff', 'subject', 'text', "KontaktFormular von " . $_SERVER['HTTP_HOST']);
		$this->form->addelement('Spamschutz aktivieren', 'spamProtection', FormType::SIMPLECHECKBOS, '0', 'Spamschutz aktivieren');
		$this->form->addelement('Butten Label', 'buttonTxt', 'text', 'Senden');
		$this->form->addelement('Seite Redirekten', 'postRedirect', FormType::SIMPLECHECKBOS, '0', 'Seite Redirekten');
//		$this->form->addelement('Formular wieder anzeigen (nach verschicken)', 'showFormAgain', FormType::SIMPLECHECKBOS, '0', 'Formular wieder anzeigen');
		$this->form->addelement('Best&auml;tigungstext', 'acceptTxt', 'textarea', 'Ihre Anfrage wurde erfolgreich versendet');
		$this->form->useTab('Inhalte');
			$this->form->addelement('Text davor', 'textbefore', 'editor', '');
			$this->form->addelement('Text danach', 'textAfter', 'editor', '');
		$this->form->useTab('MailTemplate');
		$this->form->addelement('MailTemplate', 'mailTpl', 'editor', '');
		$this->form->useTab('ReturnMailTemplate');
		$this->form->addelement('Best&auml;tigungsMail', 'ReturnMailTpl', 'editor', '');
		$this->form->useTab('Quickadd');
		$this->form->addElement('Quickadd', 'quickadd', 'textarea', '', array(
				), false);

		$this->form->useTab('Admin');
		addWhere('type', '=', 'pdf');
		$this->form->addelement('PdfTemplate', 'pdfTemplate', FormType::MYSQLCONNECTION, '', array(
			'table' => 'dataTemplates',
			'class' => 'DataTemplate',
			'what' => array('id', 'name'),
			'add' => array('' => '[Kein]')
		));

		$this->form->addElement('Dateiname (ohne Erweiterung)', 'uploadFileName', FormType::TEXTLINE, '');
		$this->form->addElement('zstl. Datei-Daten', 'uploadFileData', FormType::TEXTAREA, '');

		$this->form->addelement('pdfTemplate Verzeichnis', 'pdfTemplateFolder', FormType::FOLDER);
		$this->form->addelement('pdfVorschau Button', 'pdfPreview', FormType::SIMPLECHECKBOS);

        $this->form->setMultiLanguage(array('buttonTxt', 'textbefore', 'textAfter'));
	}

	public static function getSubName() {
		return 'formularzeile';
	}

	public function formPost() {
		if (checkVar('quickadd')) {
			$text = $_POST['quickadd'];
			$lines = explode("\n", $text);
			setLimit(1);
			addWhere('klasse', '=', 'formularzeile');
			select('elements', 'ordernr', 'ordernr DESC');
			$row = getRow();
			$anordnung = $row['ordernr'] + 1;
			$insert = array();
			$mainids = array(0);
			foreach ($lines as $line) {
				$data = array();
				$data['title'] = trim($line);
				$data['type'] = 'text';
				$data['pflicht'] = 0;

				$insert['eldata'] = serialize($data);
				$insert['ordernr'] = $anordnung;
				$insert['mainid'] = $this->id;
				$insert['visible'] = '1';
				$insert['erstellt'] = time();
				$insert['lastmod'] = time();
				$insert['parent'] = -1;

				$parastr = 'siiiiii';
				$anordnung++;
				$lastinsert = insertArray('elements', $insert, $parastr);
			}
		}
		return parent::formPost();
	}
	public function getEvents() {
		return array('FormSended' => 'FormSended');
	}

	public function parseStr($str){
		$parser = new Parser($this->kontaktForm);
		$str = $parser->parseTxt($this->parseMailTpl($str));
		return $str;
	}
}

?>