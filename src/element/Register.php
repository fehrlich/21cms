<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Register
 *
 * @author XX
 */
class Register extends kontaktFormular{

    public function formBuild() {
        $data = buildSelectArray('frontEndGroups', array('id','name'));
        $this->form->addElement('Standard Gruppe', 'defGroup', 'select', '',$data);
//        $this->fields = explode(',',  substr($this->data['fieldNames'], 1, -1));
        parent::formBuild();
//        
//        $fu = new FrontEndUser();
//        $dbc = $fu->getDbc();
//        $fields = $dbc->getFields();
////        new dBug($fields);
//        foreach($fields as $field){
//            $this->form->addElement('Standard Gruppe', 'fieldNames', FormType::MULTISELECT, '',$fields);            
//        }
    }
//
    public function getInline() {
        $html = parent::getInline();
        
        if($this->getKontaktForm()->posted){
//            $frontendUser = new FrontEndUser();
//            $dbc = $frontendUser->getDbc();
//            echo $dbc->form;
            $insert = array();
            
            $fields  = $this->getArrayList();
            $paraStr = '';
            $useMailasUsername = true;
            
            $mailId = 0;
            foreach($fields as $field){
                $fieldData = unserialize($field['eldata']);
                if($fieldData['title'] == 'password') $_POST[$field['id']] = md5($_POST[$field['id']]);
                $insert[$fieldData['title']] = $_POST[$field['id']];
                $paraStr .= 's';
                if($fieldData['title'] == 'loginname') $useMailasUsername = false;
                if($fieldData['title'] == 'email') $mailId = $field['id'];
            }
            if($useMailasUsername){
                if($mailId == 0) die('Kein Loginname/Email ausgew&auml;hlt');
                $insert['loginname'] = $_POST[$mailId];
                $paraStr .= 's';                
            }
            if(!isSet($insert['group'])){
                $insert['group'] = $this->data['defGroup'];
                $paraStr .= 'i';
            }
            
            insertArray('frontEndUser', $insert, $paraStr);
        }
        return $html;
    }
//    
    public static function getSubName() {
        return 'RegisterFormularZeile';
    }
}

?>
