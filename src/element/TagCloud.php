<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tagcloud
 *
 * @author F.Ehrlich
 */
class TagCloud extends mainelement {
	protected  $usemainid = false;
	
	public function formBuild() {
		$tags = $this->getArrayList('id,title');
//		$tags = array();
//		foreach($tgs as $t){
//			$tags[$t] = $t;
//		}
		$this->form->addElement('MainTag','maintag','select','',array("" => "[keine Unterordnung]")+$tags);
		$this->form->addElement('Maximal Gr&ouml;&szlig;e (px)','maxsize','text');
		$this->form->addElement('Minimale Gr&ouml;&szlig;e (px)','minsize','text');
		$this->form->addElement('Limit','limit','text','0');
	}

	public function getInline() {
		$html = '';
		if(isset($this->data['maintag']) && $this->data['maintag'] != "") addWhere('parent', '=', $this->data['maintag']);
                addWhere("visible","=",1);
		$shuffle = false;
		$orderBy = 'ordernr';
		if(isset($this->data['limit']) && $this->data['limit'] > 0){
			setLimit($this->data['limit']);
			$orderBy = 'prio Desc';
			$shuffle = true;
		}
		$tags = $this->getArrayList('id,title,title_intern,prio',false,$orderBy);
		$max = '16';
		$min = '8';

		if(isset($this->data['maxsize'])) $max = $this->data['maxsize'];
		if(isset($this->data['minsize'])) $min = $this->data['minsize'];
		$minOc = 0;
		$maxOc = 0;
		foreach($tags as $tag){
			if($tag['prio'] > $maxOc) $maxOc = (int)$tag['prio'];
			if($tag['prio'] < $minOc || $minOc == 0) $minOc = (int)$tag['prio'];
		}
		if($minOc == 0) $minOc = 1;
		if($maxOc == 0) $maxOc = 2;
		if($shuffle){
			srand((float)microtime() * 1000000);
			shuffle($tags);
		}
		foreach($tags as $i => $tag){
			//(Math.log(occurencesOfCurrentTag)-Math.log(minOccurs))/(Math.log(maxOccurs)-Math.log(minOccurs))
//			echo '('.log($tag['prio']+1).'-'.log($minOc).')/('.log($maxOc).'-'.log($minOc).')<br>';
			if($maxOc == $minOc)
				$weight = 1;
			else
				$weight = (log($tag['prio']+1)-log($minOc))/(log($maxOc)-log($minOc));
//			echo $weight.'!';
			$fontSize = $min + round(($max-$min)*$weight);
			$fontColor = 180-round(120*$weight);
//			echo $fontSize.'!!';
//			$size = round(log((int)$tag['prio']+1, $min)*$max);
//			echo $size.'!';
//			if($size < $min) $size = $min;
//			$html .= ' <a href="'.$GLOBALS['cms_roothtml'].'Tags/'.$tag['title'].'.html">'.$tag['title'].'</a>';
			$color = 'rgb('.$fontColor.','.$fontColor.','.$fontColor.')';
			$html .= ' '.Tag::buildLink($tag['title'],$tag['title_intern'],$fontSize,$color);
//			if($GLOBALS['Inteface'] == 'admin') $html .= '<a href="'.$GLOBALS['linkprefix'].'index.php?tag='.$row['name'].'"><span style="font-size: '.$size.'px;">'.$row['name'].'</span></a> ';
//			else $html .= '<a href="'.$GLOBALS['cmsroot'].'tags/'.str_replace(' ', '%20',$row['name']).'.html" style="font-size: '.$size.'px;">'.$row['name'].'</a> ';
		}
		return $html;

	}
	public static function getSubName() {
		return 'Tag';
	}
}
?>
