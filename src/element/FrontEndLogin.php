<?php

/**
 * Description of frontendLogin
 *
 * @author F.Ehrlich
 */
class FrontEndLogin extends element {

    public function formBuild() {
        $seiten = array("" => "[Diese Seite]") + buildSelectArray('menuepunkte', array('title_intern', 'title'), true);
        $this->form->addElement("LandingPage", "landingPage", "select", "", $seiten);
        $this->form->addElement("'Passwort vergessen' hinzuf&uuml;gen", "forgotPW", FormType::SIMPLECHECKBOS);
		$this->form->addElement('Willkommenstext', 'welcomeTxt','text', 'Herzlich Willkommen [FS_loginname]');
		$this->form->addElement('Passwort merken', 'setCookie',  FormType::SIMPLECHECKBOS);
		$this->form->addElement('Placeholder anzeigen', 'enablePlaceholder',  FormType::SIMPLECHECKBOS);
//		$this->form->addElement("Optionen", "options", "check", "", array('User d&uuml;rfen eigenes Profil &auml;ndern'));
//		$this->form->addElement('User d&uuml;rfen eigenes Profil &auml;ndern', "profil", "simpleCheck", "", 'User d&uuml;rfen eigenes Profil &auml;ndern');
    }

    public static function logincheck($hash = '') {
        
        mys::getObj()->clearWhere();

        if (frontendSession::getObj()->isLoggedIn())
            return true;
        if (!isset($_POST["username"], $_POST["password"]) && $hash == '')
            return false;
		if(isSet($_POST['username']) && isSet($_POST["password"])){
			addWhere("loginname", "=", $_POST["username"]);
			addWhere("password", "=", md5($_POST["password"]));
		}else{
			addWhere('cookieHash', '=', $hash);
		}
        addWhere("public", "=", '1');
        setLimit(1);
        select("frontEndUser", "id,loginname,password");
        $data = getRow();

		$username = $data['loginname'];
		$password = $data['password'];

        if (!empty($data)) {
            frontendSession::getObj()->setUserId($data["id"]);
            if (isSet($GLOBALS['forum_integration']) && $GLOBALS['forum_integration'] != '') {
                $api = new phpbb($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['forum_integration'], 'php');
				$userData = array(
					'username' => $username,
					'password' => $password
				);
				if($api->user_change_password ($userData) != 'SUCCESS') echo 'Error: Das Passwort für den Forum User konnte nicht geändert werden';
				$res = $api->user_login(array(
                            'username' => $username,
                            'password' => $password,
                            'admin' => '1',
                            'autologin' => true,
                        ));
                if ($res != 'SUCCESS')
                    echo 'Der Login im Forum ist fehlgeschlagen. Bitte loggen sie sich manuell ein'; //Der Login im Forum ist fehlgeschlagen. Bitte loggen sie sich manuell ein
            }
            return 2;
        } else {
            return false;
        }
    }

    public function getInline() {
//		$GLOBALS['mysql_debug'] = true;
        if(!$this->data){
            addWhere('id', '=', $this->id);
            select('elements','eldata');
            $row = getRow();
            $this->data = unserialize($row['eldata']);
        }
        if (isSet($this->data['landingPage']) && $this->data['landingPage'] != "")
            $action = $this->data['landingPage'] . ".html";
        else
			$action = "index.html";

        if (isSet($_GET['para']) && $_GET['para'] == 'logout') {
            frontendSession::getObj()->destroySession();
			setcookie ("frontEndLogin", "", time() - 3600);
            $html = 'Sie haben sich erfolgreich ausgeloggt';
            $currentLocation = explode("-P_logout.html",$_SERVER["REQUEST_URI"],2);

            header("Location: " . $currentLocation[0].".html");

            return $html;
        }
		if(isSet($_COOKIE["frontEndLogin"])){
			$hash = $_COOKIE["frontEndLogin"];
		}else $hash = '';

        $check = frontendLogin::logincheck($hash);
        if ($check === 2 && $hash == '')
            header("Location: " . $action);
        if ($check) {
			if(isSet($_POST['rememberPw'])){
				$hash = md5(microtime()+$_POST['username']);
				$expTime = time()+60*60*24*365;
				addWhere('loginname', '=', $_POST['username']);
				updateArray('frontEndUser', array('cookieHash' => $hash), 's');
				setcookie("frontEndLogin", $hash, $expTime);
			}
			if(isSet($this->data['welcomeTxt']))
				 $html = $this->data['welcomeTxt'];
			else
	            $html = 'Herzlich Willkommen ' . frontendSession::getObj()->getUserName();

            if ($check === 2)
                $html .= '<br /><a href="' . substr($action, 0, -5) . '.html">Weiter zu ihrem Loginbereich</a>';
            $html .= '<br /><a href="'.substr($action, 0, -5).'-P_logout.html">Logout</a>'; //' . substr($action, 0, -5) . '
        } else {
			$sended = false;
            if (isSet($_POST['login']))
                $html = '<span class="error">Login fehlgeschlagen</span>';
            elseif(isSet($_POST['sendPwButton'])){
				$html = 'Das Passwort wurde ihnen zugeschickt.';
				addWhere('email', '=', $_POST['email']);
				select('frontEndUser', 'id,loginname');
				$userRow = getRow();
				if($userRow['id'] > 0){
					$newpw = zufallspw();
					addWhere('id', '=', $userRow['id']);
					updateArray('frontEndUser', array(
						'password' => md5($newpw)
					), 's');
					$emailTxt = 'Ihr Passwort wurde auf '.$_SERVER['HTTP_HOST'].' zur&uuml;ckgesetzt. Ihre neuen Zugangsdaten lauten:<br />Username:  '.$userRow['loginname'].'<br />Neues Passwort: '.$newpw.'';
					$mail = new mimeMail();
					$mail->subject = 'Ihr neues Passwort auf '.$_SERVER['HTTP_HOST'].'';
					$mail->body = $emailTxt;

					$mail->addRecipient($_POST['email']);
					$mail->from = UserConfig::getObj()->getContactMail();
					$mail->send();
				}
				$sended = true;
			}else
                $html = '';

            $lang = Languages::getLang();

            $lang_username['Englisch'] = "Username";
            $lang_username['eng'] = "Username";

            $lang_password['Englisch'] = "Password";
            $lang_password['eng'] = "Password";

            $lang_username[$lang] = (isSet($lang_username[$lang])) ? $lang_username[$lang] : "Benutzername";
            $lang_password[$lang] = (isSet($lang_password[$lang])) ? $lang_password[$lang] : "Passwort";

            $html .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
			
            if(!$sended && isSet($_GET['para']) && $_GET['para'] == 'forgotPw'){
				$html .= '<label>Email: :</label> <input type="text" name="email" /><br />';

				$html .= '<input type="submit" name="sendPwButton" class="login" value="Passwort zuschicken" /><br />';
			}else{
				$html .= '<label>' . $lang_username[$lang] . ':</label> <input class="loginInput" type="text" name="username" '. (( isset($this->data["enablePlaceholder"]) && $this->data["enablePlaceholder"] != 0 ) ?  'placeholder="Benutzername"  ' : '') .'/><br />';
				$html .= '<label>' . $lang_password[$lang] . ':</label> <input class="loginInput" type="password" name="password" '. (( isset($this->data["enablePlaceholder"]) && $this->data["enablePlaceholder"] != 0 ) ?  'placeholder="Passwort"  ' : '') .'/><br />';
				$html .= '<input type="submit" name="login" class="loginButton" value="Login" /><br />';
				if(isSet($this->data['setCookie']) && $this->data['setCookie'] == '1'){
					$html .= '<br /><br /><input class="checkBox" type="checkbox" name="rememberPw"> Passwort merken';
				}
			}
            $html .= '</form>';
			if(isSet($this->data['forgotPW']) && $this->data['forgotPW'] == '1'){
				if($_SERVER['REQUEST_URI'] == '/') $link = 'index-P_forgotPw.html';
				else $link = substr($_SERVER['REQUEST_URI'],0,-4) . '-P_forgotPw.html';
				$link = $GLOBALS['akt_menuepunkt']['title_intern'].'-P_forgotPw.html';
				$html .= '<a href="' .$link.'" rel="nofollow">Passwort vergessen</a>';
			}
        }
        return $html;
    }

}

?>
