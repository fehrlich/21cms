<?php

/**
 * Description of profilSeite
 *
 * @author Franz Ehrlich
 */
class profilSeite extends element {

	public function formBuild() {
            $this->form->addElement('Speichern Button', 'saveButton', 'text','Speichern');
		$dbc = new DatabaseConnector();
		addWhere('dbType', '=', '2');
		setLimit(1);
		$db = $dbc->getDatabases('fields,id');
//		var_dump($db);
		if (count($db) == 1) {
			$row = array_pop($db);
			$this->form->addElement("", "userDbId", FormType::TEXTLINE, $row['id'], array(), true);
			$this->form->addElement("Template", "template", FormType::DATATEMPLATE, "", array('dbid' => $row['id']));

			$fields = $dbc->getFields();

			$dontAdd = array('id', 'public', 'erstellt', 'published', 'autor', 'title_intern');
			foreach ($fields as $el){
                            if (!in_array($el['name'], $dontAdd)){
                                    $this->form->addElement($el['name'] . " ist editierbar", 'edit' . $el['name'], FormType::SIMPLECHECKBOS);
                                    $this->form->addElement($el['name'] . " ist pflichtfeld", 'pflicht' . $el['name'], FormType::SIMPLECHECKBOS);
    //					echo $el['name'].'-'.$_POST['edit'.$el['name']].'<br>';

                            }
                        }
		}
	}

	public function getInline() {
		if (isSet($_GET['para']) && $_GET['para'] != '' && isSet($this->data['template']) && $this->data['template'] > 0) {
//			echo $this->data['userDbId'].'!';
			$dbc = new DatabaseConnector($this->data['userDbId'], $this->data['template'], frontendSession::getObj()->getUserId());
			addWhere ('id', '=', $this->data['userDbId']);
			$dbc->getDatabases();
			$dbname = $dbc->getDatabaseName();
			addWhere('loginname', '=', $_GET['para']);
			$dbc->getData($dbname);
			return $dbc->htmlShowData();
		} elseif (frontendSession::getObj()->isLoggedIn()) {
			if (!isAdmin())
                            include_once 'klassen/formular.php';

			$form = new formular("frontEndUser", $_SERVER['REQUEST_URI']);
//		$form->addElement("Loginname", "loginname", "text");
			$form->addElement("Passwort", "password", "pwchange", "", array("type" => "password"));
			$form->addElement("Email", "email", "email",'',array(),true,true);
                        if(!isSet($this->data['saveButton']) || isSet($this->data['saveButton']) != ''){
                            $form->setSaveButton((isSet($this->data['saveButton']))?$this->data['saveButton']:"Speichern", true);
                        }

			$addDbc = new \DatabaseConnector(0, 0, 0, frontendSession::getObj()->getUserId());
			addWhere('dbType', '=', '2');
			$addForm = $addDbc->getForm();
			if ($addForm) {
				$addElements = $addForm->form->getElements();
				$dontAdd = array('id', 'public', 'erstellt', 'published', 'autor', 'title_intern');
				foreach ($addElements as $el) {
					if (!in_array($el['name'], $dontAdd)) {
						if ($el['art'] == FormType::DATEI || $el['art'] == FormType::BILD) {
							$el['art'] = FormType::UPLOAD;
							$dir = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'dateien/__userFiles';

							if (!is_dir($dir))
								mkdir($dir);
							$el['value'] = '/__userFiles/' . frontendSession::getObj()->getUserName() . "/";
							$el['path'] = '/__userFiles/' . frontendSession::getObj()->getUserName() . "/";
						}
                                                if(isSet($this->data['pflicht' . $el['name']]) && $this->data['pflicht' . $el['name']]) $el['pflicht'] = true;
						if (isSet($this->data['edit' . $el['name']]) && $this->data['edit' . $el['name']] == '1')
							$form->addElementArrays($el);
					}
				}
			}
			$form->setDoBackup(false);
			$form->connectData(frontendSession::getObj()->getUserId());
			if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
				if(isSet($_POST['password']) && $_POST['password'] != '' && $_POST['password'][0] == $_POST['password'][1]){
					$userData = array(
						'username' => frontendSession::getObj()->getUserName(),
						'password' => $_POST['password'][0]
					);
					if($api->user_change_password($userData) != 'SUCCESS') echo 'Error: Das Passwort für den Forum User konnte nicht geändert werden';
				}
			}

			$html = $form->show(true);
		} else {
			$html = 'Bitte loggen Sie sich ein';
		}
		return $html;
	}

}

?>
