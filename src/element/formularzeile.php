<?php
/**
 * Description of menuepunkt
 *
 * @author F.Ehrlich
 */
class formularzeile extends subelement{
//	protected $treeview = true;

	public function parseMailTpl($tpl){
		$tpl = str_replace('[THIS_SITE_HREF]', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], $tpl);

		if(strpos($tpl, '[HTTP_REFERER_NAME]') || strpos($tpl, '[HTTP_REFERER_NAME]')){
			if(isSet($_SERVER['HTTP_REFERER']) && 'http://'.$_SERVER['HTTP_HOST'] == substr($_SERVER['HTTP_REFERER'],0,strlen('http://'.$_SERVER['HTTP_HOST']))){
				$ref_link = $_SERVER['HTTP_REFERER'];

				$ref_name = substr($_SERVER['HTTP_REFERER'],strlen('http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml']), -5);
				$spl = explode('/', $ref_name);
				if(isSet($spl[2]) || (isSet($spl[1]) && !isAdmin())){
					if(isAdmin()){
						$spl[0] = $spl[1];
						$spl[1] = $spl[2];
					}
					$spl2 = explode('-', $ref_name);
					if(isSet($spl2[1])){
						$ref_name = $spl2[1];
					}

					$dbname = 'data_'.$spl[0];
					addWhere('name', '=', $spl[0]);
					select('databases','id,fieldid');
					$db = getRow();
					$dbid = $db['id'];
					if($db['id'] > 0){
						addWhere('title_intern', '=', $ref_name);
						select($dbname, $db['fieldid']);
						$row = getRow();
						$ref_name = $row[$db['fieldid']];
					}
				}else{
					addWhere('title_intern', '=', $ref_name);
					select('menuepunkte', 'title');
					$row = getRow();
					if(isSet($row['title']) && $row['title'] != '')
						$ref_name = $row['title'];
				}
			}else{
				$ref_link = 'http://'.$_SERVER['HTTP_HOST'].'/';
				$ref_name = $_SERVER['HTTP_HOST'];
			}
	//		$tpl = str_replace('[TITEL]', substr($_SERVER['HTTP_REFERER'],strlen('http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml']), -5), $tpl);
			$tpl = str_replace('[HTTP_REFERER_LINK]', '<a href="'.$ref_link.'">'.$ref_name.'</a>', $tpl);
			$tpl = str_replace('[HTTP_REFERER_NAME]', $ref_name, $tpl);
		}

		return $tpl;
	}

	public function getInline(){
//		var_dump($this->data);
//		echo '!!!';
		if($this->data['type'] == 'bbCode')
		    addJs('admin/editor/ckeditor.js');
//                new dBug($this->data);
		if(isSet($this->data['formbuild']) && $this->data['formbuild']){
			if(!isSet($this->data['pflicht'])) $this->data['pflicht'] = false;
			$default = (isSet($this->data['default']))?$this->parseMailTpl($this->data['default']):'';
			if($this->data['type'] == 'select'){
				$newdata = array();
				foreach($this->data['data'] as $i => $d){
					$newdata[menuepunkte::setTitelIntern($i)] = $d;
				}
				$this->data['data'] = $newdata;
			}
			if($this->data['type'] == 'select' && $this->data['pflicht']){
				$key = key($this->data['data']);
				$val = current($this->data['data']);
				if($key != '0'){
					unset($this->data['data'][$key]);
					$this->data['data'] = array(0 => $val)+$this->data['data'];
				}
			}
			$form = new formular('','','','kontakt');
			if(isSet($this->data['uploaddir']) && $this->data['uploaddir'] !='')
                            $default = $this->data['uploaddir'];
//			var_dump($this->data);
			$type = $this->data['type'];
			if($type == 'bild'){
//			    var_dump($this->data);
			    $type = 'upload';
			    $this->data['type']= 'upload';
			}

			if($type != 'upload' && isSet($_GET['para']) && $_GET['para'] != ''){
				$paraSplit = explode('_',$_GET['para']);
				for($i=0;$i<count($paraSplit)-1;$i+=2){
					if(isSet($paraSplit[$i+1]))
						$getValues[$paraSplit[$i]] = $paraSplit[$i+1];
				}
				if(isSet($getValues[$this->data['title']]))
					$default = $getValues[$this->data['title']];
			}
//			if(isAdmin()) new dBug ($form);
			$parser = new Parser($form);

			if($type == 'upload' && isSet($this->data['uploadFileData']))
				$this->data['data']['fileData'] = $this->data['uploadFileData'];

//			new dBug($this->data);
			foreach($this->data as $i => $d){
//				new dBug($d);
				$id = $i;
				if(!is_array($d))
					$this->data[$i] = $parser->parseTxt($this->data[$i]);
				else{
					foreach($d as $i2 => $d2){
						$this->data[$i][$i2] = $parser->parseTxt($this->data[$i][$i2]);
					}
				}
			}
			if($type == 'multiselect'){
				$d = array();
				$spl = explode(',',$this->data['fields']);
				foreach($spl as $v){
					if($v != '')
						$d[$v] = $v;
				}
				$this->data['data'] = $d;
			}
			if($type == 'radio'){
				$d = array();
				if(!isSet($this->data['fields2'])) $this->data['fields2'] = $this->data['fields'];
				$spl = explode(',',$this->data['fields2']);
				foreach($spl as $v){
					if($v != '')
						$d[$v] = $v;
				}
				$this->data['data'] = $d;
			}
			if($type == 'check'){
				$d = array();
				if(!isSet($this->data['fields3'])) $this->data['fields3'] = $this->data['fields'];
				$spl = explode(',',$this->data['fields3']);
				foreach($spl as $v){
					if($v != '')
						$d[$v] = $v;
				}
				$this->data['data'] = $d;
			}
			if($type == FormType::DATARECORD){
				$this->data['data'] = array(
					'dbId' => $this->data['database'],
					'allowNew' => false,
					'valueAsText' => true
				);
			}
			$cssClass = (isSet($this->data['cssClass']))?$this->data['cssClass']:'';

                        //FIX: for undefined placeholder
                        $placeHolder = (isset($this->data["placeHolder"]) && $this->data["placeHolder"]);
			$html = $form->buildElementHtml($this->data['title'],  menuepunkte::setTitelIntern($this->id), $type, $default, $this->data['data'],true,(boolean)$this->data['pflicht'],false,$cssClass,$placeHolder); //#917 richtige anzeige: ,true
		}
		else{
			$title = menuepunkte::setTitelIntern($this->data['title']);
			if($title == ''){
				$title = $this->data['text'];
			}
			$html = '<a href="#">'.$title.'</a>';
		}
		return $html; //<ins> </ins>
	}
	public function formBuild(){
		$this->form->addElement('Titel', 'title', 'text');
		$this->form->addElement('Art', 'type', 'select', '0', array(
			'text' => 'Text-Zeile',
			'textarea' => 'Text-Feld',
			'email' => 'E-mail',
			'simpleCheck' => 'CheckBox',
			'check' => 'Multi-CheckBox',
			'radio' => "Radiobuttons",
			'date' => 'Datum-Feld',
			'textarea' => 'Text-Feld',
			'select' => 'Drop-Down',
			'multiselect' => 'Multi Select',
			'upload' => 'Datei-Upload',
			'bild' => 'Bild-Upload',
			'bbCode' => 'BB-Code Text',
			FormType::DATARECORD => 'DatenbankFeld',
			FormType::TAGS => 'Tag',
                        FormType::HIDDENFIELD => 'Versteckt'
		));

        $this->form->addElement('Name als Platzhalter',"placeHolder","simpleCheck","0");
		$this->form->addElement('Pflichtfeld', 'pflicht', 'simpleCheck', '0');
		$this->form->addElement('Felder', 'fields', 'text', 'Auswahl1,Auswahl2,Auswahl3');
		$this->form->addElement('Felder', 'fields2', 'text', 'Auswahl1,Auswahl2,Auswahl3');
		$this->form->addElement('Felder', 'fields3', 'text', 'Checkbox1,Checkbox2,Checkbox3');
		$this->form->addElement('Text', 'text', 'text', '');
		$this->form->addElement('StandardWert', 'default', 'text', '');
		$this->form->addElement('UploadVerzeichnis', 'uploaddir', FormType::FOLDER, '');
		$this->form->addElement('Dateiname (ohne Erweiterung)', 'uploadFileName', FormType::TEXTLINE, '');
		$this->form->addElement('zstl. Datei-Daten', 'uploadFileData', FormType::TEXTAREA, '');
		$this->form->addElement('Dateiname (ohne Erweiterung)', 'uploadBildName', FormType::TEXTLINE, '');
		$this->form->addElement('zstl. Datei-Daten', 'uploadBildData', FormType::TEXTAREA, '');

		$this->form->addElement('UploadVerzeichnis', 'bilduploaddir', FormType::FOLDER, '');
		$this->form->addElement('Maximale Breite', 'maxwidth', FormType::TEXTLINE, '0');
		$this->form->addElement('Maximale Höhe', 'maxheight', FormType::TEXTLINE, '0');
		$this->form->addElement('Genaue Größe', 'forceDim', FormType::SIMPLECHECKBOS, '0');

//		$this->form->addElement('zus. Html (nach Element)', 'zusHtml', FormType::TEXTAREA);


		$this->form->addElement('Datenbank', 'database', FormType::DATABASE,'',array(
			'noreload' => true
		));
		$this->form->addOnlyShowOn('database', 'type', FormType::DATARECORD);

		$this->form->addOnlyShowOn('fields', 'type', 'select');
		$this->form->addOnlyShowOn('fields2', 'type', 'radio');
		$this->form->addOnlyShowOn('fields3', 'type', 'check');
		$this->form->addOnlyShowOn('text', 'type', 'simpleCheck');
		$this->form->addOnlyShowOn('uploaddir', 'type', 'upload');
		$this->form->addOnlyShowOn('uploadFileName', 'type', 'upload');
		$this->form->addOnlyShowOn('uploadFileData', 'type', 'upload');

		$this->form->addOnlyShowOn('uploadBildName', 'type', 'bild');
		$this->form->addOnlyShowOn('uploadBildData', 'type', 'bild');

		$this->form->addOnlyShowOn('bilduploaddir', 'type', 'bild');
		$this->form->addOnlyShowOn('maxwidth', 'type', 'bild');
		$this->form->addOnlyShowOn('maxheight', 'type', 'bild');
		$this->form->addOnlyShowOn('forceDim', 'type', 'bild');
		$this->form->setMultiLanguage(array('title','text','default','fields','fields2'));
		$this->form->useTab('Sichbarkeit');
		addWhere('klasse', '=', 'formularzeile');
		$mainidEl = $this->form->getElement('mainid');
//		new dBug($mainidEl);
//		if($mainidEl['value'] != ''){
//			addWhere('mainid', '=', $mainidEl['value'],'i');
//			echo $mainidEl['value'].'!';
//		}
//		new dBug($this->getMainElement());
//		addWhere('mainid', '=', $this->getMainElement());
		select('elements', 'id,eldata', 'ordernr');
		$rows = getRows();
		$data = array();
		$data2 = array();
		foreach($rows as $d){
			$dd = unserialize($d['eldata']);
			if($dd['type'] == 'select');
			$data[$d['id']] = $dd['title'];
//			$fields = explode(',',$dd['fields']);
//			$data2 = array();
//			foreach($fields as $field){
//				$data2[$field] = $field;
//			}
		}
		$this->form->addElement('Nur Anzeigen wenn Element', 'onlyShowOnMainElement', FormType::SELECT,'',array(''=>'[Immer]')+$data);
//		$this->form->addElement('folgenden Wert hat', 'onlyShowOnMainValue', FormType::SELECT,'',$data2);
		$this->form->addElement('folgenden Wert hat', 'onlyShowOnMainValue', FormType::TEXTLINE);
		$this->form->useTab('Inhalte');

		$this->form->addElement('Inhalt davor','beforeTxt',  FormType::HTMLEDITOR);
		$this->form->addElement('Inhalt dahinter','zusHtml',  FormType::HTMLEDITOR);
		$this->form->useTab('Admin');
		$this->form->addElement('interner Titel', 'title_intern');
		$this->form->addElement('CSS-Klasse', 'cssClass');
	}

	public function getMainName() {
		return 'kontaktFormular';
	}

	public function formPost(){
//		if($_POST['title_intern'] == ''){
//			$title = $_POST['title'];
//			addWhere('klass', '=', 'formularzeile');
//			select('elements','eldata');
//			$rows = getRows();
//			$count = 2;
//			$anz = count($rows);
//			for($i=0;$anz;$i++){
//				$data = unserialize($row['eldata']);
//				if(isSet($data['title_intern']) && $data['title_intern'] == $title){
//					$title = $title.$count;
//					$count++;
//					$i=0;
//				}
//			}
//			$_POST['title_intern'] = $title;
//		}
		return parent::formPost();
	}
}
?>
