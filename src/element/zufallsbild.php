<?php
    class zufallsbild extends element {

        private $files = array();

        public function getInline() {

            if (!isset($this->data["bild"])) {
                return "leer";
            }



            //$dir = dirname(realpath("./" . $this->data["bild"]));
            $dir = realpath(dirname(__FILE__) ."/..") . dirname($this->data["bild"]);
            
            if (is_readable($dir)) {
                
                $handle = opendir($dir);
                

                while($file = readdir($handle)) {
                    if ($file == "." || $file == "..") continue;
                    $name = explode(".",$file,2);
                    
                    if (!isset($name[1]) || ($name[1] != "jpg" && $name[1] != "jpeg" && $name[1] != "png" && $name[1] != "gif")) continue;
                    
                    $this->files[] = $file;
                }
            } else {
                return "not readable";
            }

            $html = '';
            $html .= "<div id=\"random_images\">";

            for ($i = 0; $i < 3; $i++) {
                $html .= '<img src="' . dirname($this->data["bild"]) . '/' . $this->getRandom()  .'" />';
            }

            $html .= '</div>';

            return $html;
        }
        
        private function getRandom() {
            if (empty($this->files)) return false;
            
            $i = array_rand($this->files,1);
            $tmp = $this->files[$i];
            unset($this->files[$i]);
            
            return $tmp;
        }



        public function formbuild() {

            $this->form->addelement('Ordner', 'bild','bild');

	}

    }
?>