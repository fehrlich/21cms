<?php
class suche extends element {



	public function text_cut($text, $wort){
		$text = preg_replace('/\[EL_(.*?)\((.*?)\)\]/','',$text);
		$laenge = 255;

		$len = strlen($text);
		$pos = strpos(strtolower($text), strtolower($wort));

		if($pos > $laenge){
			$start = $pos-$laenge;
			while($text[$start] != " " && $start < $pos){
				$start++;
			}
		}
		else $start = 0;
		if($pos < $len-$laenge){
			$ende = $pos+$laenge;
			while(isset($text[$ende]) && $text[$ende] != " " && $ende > $pos){
				$ende--;
			}
		}
		else $ende = $len;

		$length = $ende-$start;
		$newtext = substr($text,$start, $length);

//		$newtext = eregi_replace('('.$wort.')', '<span style="background-color: #ff9;">\1</span>',$newtext);
		if($start != 0) $newtext = "...".$newtext;
		if($ende != $len) $newtext .= "...";
	return trim($newtext);
}

	public function getInline() {
            
                $html = '';
		if(!isAdmin()) include_once('klassen/search.php');
		if(!isSet($_POST["search"]) || $this->data['searchOnly']) {

                        $showIn = (isset($this->data['showIn'])) ? $this->data['showIn'] : '';
                        $buttonTxt = (isset($this->data['buttonTxt'])) ? $this->data['buttonTxt'] : '';


                        $html .= '<form action="'.$GLOBALS['cms_roothtml']. $showIn .'.html" method="post">';
			$val = (isSet($_POST['search']))?$_POST['search']:'';
			$html .= '<p>';
                        $html .= '<input type="text" class="suchfeld" name="search" value="'.htmlspecialchars($val,ENT_QUOTES,'UTF-8').'" />';
                        $html .= '<input type="submit" class="suchbutton" value="'.htmlspecialchars($buttonTxt ,ENT_QUOTES,'UTF-8').'" name="searchsub" />';
                        $html .= '</p>';
			$html .= '</form>';
		} else {
			$search = new search($_POST["search"]);
			$GLOBALS['SITE_TITLE'] .= htmlspecialchars($_POST["search"],ENT_QUOTES,'UTF-8');
			$dbIds = array();

			foreach($this->data as $id => $dat) {
				if(substr($id,0,2) == 'db' && $dat != 0) {
                                        $dbIds[] = substr($id,2);
                                }
			}
			$search->searchDbs($dbIds);
			$search->searchElements((boolean)$this->data['showElements']);
			$search->exec();
			$results = $search->getResult();
			$count = 0;
			$allResults =  $search->getAllResult();

//			$dbTpl = array();
//			foreach($allResults as $result){
//				foreach($results['db'] as $dbid => $dataIds) {
//					$count++;
//					if(!isSet($dbTpl[$dbid])){
//						$dbc = new DatabaseConnector($dbid);
//						$dbc->setTpl($this->data['db'.$dbid]);
//						$tpl = $dbc->getTemplate();
//						$tpl_html = $tpl['html'];
//						if(isSet( $this->data['showOn_'.$dbid]) &&  $this->data['showOn_'.$dbid] != '')
//							$tpl_html = str_replace('this', $this->data['showOn_'.$dbid], $tpl_html);
//						$dbTpl[$dbid] = $tpl_html;
//					}
//                   	$database = $dbc->getDB();
//					addWhere('id', '=', $result['id']);
//					$datas = $dbc->getData($database['name'], '*');
//					foreach($datas as $data) {
//						$html .= $dbc->parseData($tpl_html, $data,$_POST['search']);
//					}
//				}
////				$html = str_replace($_POST['search'], '<span class="highlight">'.$_POST['search'].'</span>', $html);
//				$html = preg_replace('/(?<=>)(.[^\<>]*)?('.$_POST['search'].')(.[^<>]*)?(?=<)/is', '$1<span class="highlight">$2</span>$3', $html);
//			}
//			if(isSet($results['elements'])) {
////				foreach($results['elements'] as $id) $ids[] =
////				var_dump($results['elements']);
//				addWhere('id', 'IN', $results['elements']);
//				addJoin('menuepunkte', 'seitenid', 'id', 'title_intern');
//				select('elements','searchtxt');
//				while($row = getRow()){
//					$count++;
//					$row['searchtxt'] = str_ireplace($_POST['search'], '<span class="highlight">'.$_POST['search'].'</span>', $row['searchtxt']);
//					$html .= '<h2><a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html">Inhalt</a></h2>';
//					$html .= '<p class="searchresult">'; //<a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html">
//					$html .= $this->text_cut($row['searchtxt'],$_POST['search']);
//					$html .= '</p>'; //</a>
//					$html .= '<p style="text-align:right"><a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html" class="weiter">[weiter lesen]</a></p>';
//
//				}
//			}
//			if($count == 0) $html .= 'Leider erzielte Ihre Suche keine Ergebnisse.';
			$erg = array();
			if(isSet($results['db'])) {
				foreach($results['db'] as $dbid => $dataResults) {
					$dataIds = array();
					$score = array();
					foreach($dataResults as $d){
						$dataIds[] = $d['id'];
						$score[$d['id']] = $d['score'];
					}
					$count++;
					$dbc = new DatabaseConnector($dbid);
					$dbc->setTpl($this->data['db'.$dbid]);
					$tpl = $dbc->getTemplate();
					$tpl_html = $tpl['html'];
					if(isSet( $this->data['showOn_'.$dbid]) &&  $this->data['showOn_'.$dbid] != ''){
						$dbc->setOverWriteLinks($this->data['showOn_'.$dbid]);
					}else
						$dbc->setOverWriteLinks('');
                   	$database = $dbc->getDB();
					addWhere('id', 'IN', $dataIds);
					$datas = $dbc->getData($database['name'], '*');
					foreach($datas as $data){
//						echo '<textarea>'.$tpl_html.'!</textarea>';
						$erg[] = array(
							'html' => $dbc->parseData($tpl_html, $data,$_POST['search']),
							'score' => $score[$data['id']]
						);
//						$html .= $dbc->parseData($tpl_html, $data,$_POST['search']);
					}
				}
//				$html = str_replace($_POST['search'], '<span class="highlight">'.$_POST['search'].'</span>', $html);
			}
			if(isSet($results['elements'])) {
//				foreach($results['elements'] as $id) $ids[] =
//				var_dump($results['elements']);
				foreach($results['elements'] as $d){
					$dataIds[] = $d['id'];
					$score[$d['id']] = $d['score'];
				}
//				$GLOBALS['mysql_debug'] = true;
				addWhere('id', 'IN', $dataIds);
				addWhere('secure', '=', "0",'s','AND','t2');
				addJoin('menuepunkte', 'seitenid', 'id', 'title_intern,title,parent');
//				addJoin('menuepunkte', 'seitenid', 'id', 'title_intern','inner','title_intern');
				select('elements','searchtxt,id,seitenid');
				$rows = getRows();
				foreach ($rows as $row){
					$count++;
					$id = $row['id'];
					$row['id'] = $row['seitenid'];
					$row['searchtxt'] = str_ireplace($_POST['search'], '<span class="highlight">'.$_POST['search'].'</span>', $row['searchtxt']);
					$subHtml = '<h2>'.BreadCrumbs::createBreadCrumbs($row).'</h2>';
//					$subHtml = '<h2><a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html">'.$row['title'].'</a></h2>';
					$subHtml .= '<p class="searchresult">'; //<a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html">
					$subHtml .= $this->text_cut($row['searchtxt'],$_POST['search']);
					$subHtml .= '</p>'; //</a>
					$subHtml .= '<p style="text-align:right"><a href="'.$GLOBALS['cms_roothtml'].$row['title_intern'].'.html" class="weiter">[weiter lesen]</a></p>';
					$erg[] = array(
						'html' => $subHtml,
						'score' => $score[$id]
					);

				}
			}
			if($count == 0) $html .= 'Leider erzielte Ihre Suche keine Ergebnisse.';
			else {
				uasort($erg, 'sortSearchResult');
				foreach($erg as $id => $d){
					$html .= $d['html']; //$d['score'].'!!'.
				}
				$html = preg_replace('/(?<=>)(.[^\<>]*)?('.preg_quote ($_POST['search'], '/').')(.[^<>]*)?(?=<)/is', '$1<span class="highlight">$2</span>$3', $html);
			}
		}

		return $html;
	}
	public function formBuild() {
		$dbc = new DatabaseConnector();
                addWhere('dbType', '=', '0');
		$dbs = $dbc->getDatabases('id,name');
		select('dataTemplates','id,dbid,name');
		$tpls = getRows();
		$groupedTpls = array();
		foreach($tpls as $tpl) {
			$groupedTpls[$tpl['dbid']][$tpl['id']] = $tpl['name'];
		}
//		$GLOBALS['mysql_debug'] = true;
		$data = buildSelectArray('menuepunkte', array('title_intern','title'), true);
//		$sites = buildSelectArray('menuepunkte', 'idtitle_intern', true);
       	$this->form->addElement('Ergebnisse anzeigen auf ', 'showIn' ,'select', '0',$data);
		$this->form->addElement('Button Text ', 'buttonTxt' ,'text');
		$this->form->addElement('Inhalte durchsuchen ', 'showElements' ,'simpleCheck');
		$this->form->addElement('Nur Suchfeld ', 'searchOnly' ,'simpleCheck');
		foreach($dbs as $id => $db) {
			if(isSet($groupedTpls[$id])) {
				$this->form->addElement($db.' anzeigen in ', 'db'.$id ,'select', '0', array(0 => 'Nicht anzeigen')+$groupedTpls[$id]);
				$data = array('', 'Default')+$data;
				$this->form->addElement($db.' anzeigen auf ', 'showOn_'.$id ,'select', '0', $data);
			}
		}
	}
}
?>