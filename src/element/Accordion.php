<?php

if($GLOBALS['interface'] == 'ajax')
	include_once('../klassen/container.php');
class Accordion extends mainelement {
	protected $deleteable = true;
	



	public function formBuild() {
		$dbData = array();
		$dbData['add'] = array(0 => 'Statisch');
		$this->form->addElement('Inhalt', 'content', 'database','0',$dbData);
		if((isset($_GET['changecontent']) && $_GET['changecontent'] > 0) || isSet($_POST['groupby'])){
			$gropby = array();
			if(isset($_GET['changecontent'])){
				$dbc = new DatabaseConnector($_GET['changecontent']);
				$fields = $dbc->getFields();
				foreach($fields as $field){
					$gropby[$field['name']] = $field['name'];
				}
			}
                        
			$this->form->addElement('Gruppierung', 'groupby', 'select', '0',$gropby);
			$this->form->addElement('Template', 'template', 'dataTemplate','',array('dbformname' => 'content'));
			
		}
		$this->form->addElement('Reihenfolge', 'order', 'select', '0', array('Neuste Oben','Neuste Unten'));
		$this->form->addElement('Standardm&auml;&szlig;ig offen', 'open', 'select', '0', array('Nein','Ja'));
	}

	public function getInline($data = array()){
		if($this->listStyle) 
                        return '<a href="#">Accordion('. ((isset($this->data['open']) && $this->data["open"] == 1) ?
                                                                $this->data['open'] :
                                                                "true" ).')</a>';
		$html = '';
		$groups = array();
		
                if (isset($this->data['content']) && $this->data['content'] > 0) {

                    $dbc = new DatabaseConnector($this->data['content'],$this->data['template']);
                    $datas = $dbc->getData();
                    $tpl = $dbc->getTemplate();

                    foreach($datas as $data){
						
                        if (isset($this->data['groupby']) && strpos($data[$this->data['groupby']], ',') !== false) {
                            $tags = explode(',',substr($data[$this->data['groupby']],1,-1));
                            foreach($tags as $g){
                                $groups[$g][] = $dbc->parseData($tpl['html'], $data);
                            }
                        } else
                            $groups[$data[$this->data['groupby']]][] = $dbc->parseData($tpl['html'], $data);
			}
		}

		$data = $this->getArrayList();
		foreach ($data  as $dat) {
			$el_dat = unserialize($dat['eldata']);
                        if (isAdmin() || (!isAdmin() && $dat["visible"] != 0))
            			$groups[$el_dat['group']][] = (string)new $dat['klasse']($dat['id'], $el_dat, $dat['container'],$this->id,true,false);
		}

		foreach($groups as $name => $groupcontent){
			if($name != '' || isAdmin()){
				if ($name == '') $name = '[nicht zugeordnet]';
				$classadd = ($this->data['open'] == $name || isAdmin())?' open':'';
				$html .= '<div class="group'.$classadd.'">';
				$html .= '<div class="head">'.$name;
				$html .= '<div class="openclose"></div>';
				$html .= '</div>';
				foreach($groupcontent as $content)
					$html .= '<div class="content">'.$content.'</div>';
				$html .= '</div>';
			}
		}
		return $html;
	}
	public static function getSubName(){
		if(!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'container') $sub = 'inhalt';
		else $sub = $_GET['el'];
		return $sub;
	}
	public static function getSub($id){
		if (!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'Accordion') {
                    $sub = 'inhalt';
                } else {
                    $sub = $_GET['el'];
                }

                $c = new $sub(0,true,'',$id);
                $c->form->addElement('Gruppe', 'group','text');

		return $c;
	}
}
?>
