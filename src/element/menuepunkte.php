<?php

    /**
     * Description of menuepunkt
     *
     * @author F.Ehrlich
     */
    class menuepunkte extends subelement {

        protected $treeview = true;

        public function __construct($id = 0, $data = array(), $mainid = 0, $skipbuild = false, $deleteable = false, $buildform = true) {
            if (isAdmin())
                $this->allowEdit = (rights::getObj()->isAdmin() || PageRights::getObj()->canCreatePages());
            parent::__construct($id, $data, $mainid, $skipbuild, $deleteable, $buildform);
        }

        public static function setTitelIntern($txt, $uniqueTable = '', $id = 0, $altTitle = '', $titleIntern = 'title_intern') {
            $allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $replace = array(
                '&' => 'und',
                'ä' => 'ae',
                'ü' => 'ue',
                'ö' => 'oe',
                'ß' => 'ss',
                'Ä' => 'Ae',
                'Ü' => 'Ue',
                'Ö' => 'Oe'
            );
            $txt = str_replace(array_keys($replace), $replace, $txt);
            $anz = strlen($txt);
            for ($i = 0; $i < $anz; $i++) {
                if (strpos($allowed, $txt[$i]) === false) {
                    $txt = str_replace($txt[$i], '_', $txt);
                }
            }
            while (is_int(strpos($txt, '__')))
                $txt = str_replace('__', '_', $txt);
            $txt = trim($txt, "_");
            if ($uniqueTable != '') {
                $check = true;
                $add = 0;
                while ($check && $add < 15) {
                    $add++;
                    $newTxt = ($add > 1) ? $txt . $add : $txt;
                    if ($id > 0)
                        addWhere('id', '!=', $id, 'i');
                    addWhere($titleIntern, '=', $newTxt);
//				echo $newTxt.'!';
                    $check = dbCheck($uniqueTable);
                    if ($check && $altTitle != '') {
//					echo 'JAAAA';
                        return menuepunkte::setTitelIntern($altTitle, $uniqueTable, $id);
                    }
                }
                if ($add > 1)
                    $txt .= $add;
            }
            return $txt;
        }

        public function getInline() {

            $html = '';
            $style = '';
            $class = '';
            $target = '';

            if ( isset($_GET['inter']) && $_GET['inter'] == 'dia') {
                $title = $this->data['title_intern'];
            } else {
                $title = (($this->data['title']));
            }

            if (isSet($this->getMainElement()->data['imgfolder']) && $this->getMainElement()->data['imgfolder'] != '') {
                $file = $_SERVER['DOCUMENT_ROOT'] . $this->getMainElement()->data['imgfolder'] . $this->data['title_intern'] . '.png';
                if (file_exists($file) && !isAdmin()) {
                    $title = '<img src="' . $this->getMainElement()->data['imgfolder'] . $this->data['title_intern'] . '.png" alt="' . $title . '" />';
                    $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . $this->getMainElement()->data['imgfolder'] . $this->data['title_intern'] . '.png');
                    $this->width = $info[0];
                    $this->height = $info[1];
                    $this->typ = $info[2];
                    $this->html = $info[3];
                    $style = ' style="width:' . $info[0] . 'px;background-image:url(' . $this->getMainElement()->data['imgfolder'] . $this->data['title_intern'] . '.png);"';
                    $title = '';
                }
            }

            $active_menupunkt = (isset($GLOBALS['aktmenu']) && in_array($this->id, $GLOBALS['aktmenu']));

            if ( isAdmin() ) {
                $add = 'admin/';
            } else {
                $add = '';
            }

            if ($active_menupunkt) {
                $class = ' class="active"';
            }

            if ( $this->data['verlinkung'] != '' && $this->data['verlinkung'] != '0') {

                if (substr($this->data['verlinkung'], 0, 7) == 'http://') {
                    $link = $this->data['verlinkung'];

                    if ($this->data["extLinkNewWindow"] == 0) {
                        $target = ' target="_blank"';
                    }

                } else {
                    $link = buildLink($this->data['verlinkung'], '', '', false, false, false, false);
                }

            } else {
                $link = buildLink($this->data['title_intern'], '', '', false, false, false, false);
            }

            if ( isset($this->data["noContent"]) && $this->data["noContent"] ) {
                $link = "#";
            }

            $html .= '<a href="' . $link . '"' . $class . $target . $style . '>' . $title . '</a>';

            if (isset($this->data['add_database']) && $this->data['add_database'] > 0) {
                if ($this->data['erw_needclick'] == '0' || $active_menupunkt) {
                    $dbs = new DatabaseConnector($this->data['add_database']);
                    $data = $dbs->getData('', 'fieldid');
                    if (count($data) > 0) {
                        $html .= '<ul>';
                        foreach ($data as $d) {
                            if (isSet($_GET['db']) && $_GET['db'] == $dbs->getDatabaseName() && $_GET['title'] == $d['fieldid'])
                                $class = ' class="active"';
                            else
                                $class = '';
                            $html .= '<li><a href="' . $GLOBALS['cms_roothtml'] . $add . $dbs->getDatabaseName() . '/' . $this->data['title_intern'] . '-' . menuepunkte::setTitelIntern($d['fieldid']) . '.html"' . $class . '>' . $d['fieldid'] . '</a></li>';
                        }
                        $html .= '</ul>';
                    }
                }
            }
            return $html;
        }

        public function formEdit() {

            if ($this->id > 0) {
                $this->form->connectData($this->id, false); //,$this->data
            }

            $verlinkungEl = $this->form->getElement('verlinkung');

            if (strlen($verlinkungEl['value']) > 4 && substr($verlinkungEl['value'], 0, 4) == 'http') {
                $this->form->setElement('verlinkung', 'value', 'externerLink');
                $this->form->setElement('link_ext', 'value', $verlinkungEl['value']);
            }

            echo $this->form->__toString();
        }

        public function formBuild() {

            $this->form->stopSerialize();
            $defaultSubLevel = -1;
            if (isSet($_GET['get']) && $_GET['get'] == 'sub') {
                $GLOBALS['seitenid'] = getMenuid('', $_GET['m']);
                preparation();
                addWhere('id', '=', $_GET['id']);
                select('elements', 'eldata');
                $row = getRow();
                $menuData = unserialize($row['eldata']);
                if ($menuData['startlevel'] > 0) {
                    $defaultSubLevel = $GLOBALS['aktmenu'][$menuData['startlevel'] - 1];
                    $this->form->setElement('parent', 'value', $defaultSubLevel);
                }
            }
            $this->form->addElement('Titel', 'title', 'text');
            addWhere('id', '!=', $this->id, 'i');
            $seiten = buildSelectArray('menuepunkte', array('id', 'title'), true);
            $arr = buildSelectArray('menuepunkte', array('title_intern', 'title'), true);
            $this->form->addElement('Verlinkung', 'verlinkung', 'select', '', array('' => '[Keine]', 'index' => '[Startseite]', 'externerLink' => '[Extern]') + $arr);
            $this->form->addElement('Externer Link', 'link_ext', 'text', 'http://', array(), false);
            $this->form->addElement('In neuem Fenster öffnen', 'extLinkNewWindow', 'select', '', array("ja", "nein"));
            $this->form->addElement('Startseite', 'startpage', 'simpleCheck');
            $tpls = array();
            foreach (UserConfig::getObj()->getUserTemplate() as $tpl) {
                $tpls[$tpl['name']] = $tpl['name'];
            }
            $this->form->addElement('Aussehen', 'userTpl', 'select', '', $tpls);
            $this->form->addElement('Erweitern', 'erweitern', 'select', '0', array('Nein', 'Datens&auml;tze'), false);
            $this->form->addElement('Seite abschalten', 'disabled', 'select', '0', array('Nein', 'Ja'));
            $this->form->addElement('Ohne Inhalt', 'noContent', 'select', '0', array('Nein', 'Ja'));
            $this->form->addOnChangeReload('erweitern');
            $this->form->addOnlyShowOn('link_ext', 'verlinkung', 'externerLink');
            $this->form->addOnlyShowOn('extLinkNewWindow', 'verlinkung', 'externerLink');

            if ((isset($_GET['changeerweitern']) && $_GET['changeerweitern'] == '1') || isset($_POST['add_database'])) {
                $erw_db = true;
            } else
                $erw_db = false;
            if ($erw_db) {
                $data = array('noreload' => true);
                $this->form->addElement('Datenbank', 'add_database', 'database', '', $data);
                $this->form->addElement('Erweitung nur ausklappen wenn angeklickt', 'erw_needclick', 'simpleCheck');
            }

            $this->form->startSerialize('cvars');
            foreach ($GLOBALS['CVARS'] as $name => $var) {
                if (isset($var['lokal']) && $var["lokal"]) {
                    $this->form->addElement($name, $name, $var['typ'], $var['value'], $var['data']);
                }
            }

            $this->form->stopSerialize();

            $this->form->useTab('SEO');
            $this->form->addElement('Neuer Seitentitel', 'seo_newTitle', FormType::TEXTLINE);
            $this->form->addElement('Tags', 'seo_tags', 'text');
            $this->form->addElement('Beschreibung', 'seo_desc', 'textarea');
            $seiten2 = buildSelectArray('menuepunkte', array('title_intern', 'title'), true);
            $this->form->addElement('kanonische URL', 'canonical', 'select', '', array('' => '[Keine]') + $seiten2);

            $this->form->useTab('Sperren');
            $this->form->addElement('Diese Seite sichern', 'secure', 'simpleCheck');
            $groups = buildSelectArray('frontEndGroups', array('id', 'name'));
            $user = buildSelectArray('frontEndUser', array('id', 'loginname'));
            $this->form->addElement('Gruppen zugriff zulassen', 'allow_groups', 'multiselect', '', $groups);
            $this->form->addElement('User zugriff zulassen', 'allow_user', 'multiselect', '', $user);
            $this->form->setMultiLanguage(array('title'));

            if (rights::getObj()->isAdmin()) {
                $this->form->useTab('Admin');
                $this->form->addElement('interner Titel', 'title_intern', 'text');
                $this->form->addElement('Template', 'useTpl', 'text', 'index');
            } else {
                $this->form->addElement('', 'title_intern', 'hidden');
                $this->form->addElement('', 'canonical', 'hidden', '');
                $this->form->addElement('', 'useTpl', 'hidden', '');
            }
        }

        public function formPost() {
            if ($_POST['title_intern'] == '')
                $_POST['title_intern'] = strtolower(menuepunkte::setTitelIntern($_POST['title'], 'menuepunkte'));
            if (isset($_POST['startpage']))
                updateArray('menuepunkte', array('startpage' => '0'), 's');
            if (isset($_POST['link_ext']) && isset($_POST['verlinkung']) && $_POST['verlinkung'] == 'externerLink')
                $_POST['verlinkung'] = $_POST['link_ext'];
            return parent::formPost();
        }

        public function getMainName() {
            return 'menu';
        }

        static public function getTabName() {
            return 'menuepunkte';
        }

    }

?>