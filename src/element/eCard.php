<?php
  class eCard extends element {

    private $images = array();
    private $footer = '';
    private $subject = '';

    public function formbuild() {
      $this->form->addelement('Bilder', 'bild','folder');
      $this->form->addelement('Subject', 'subject',  FormType::TEXTLINE);
      $this->form->addelement('Fuss', 'footer',FormType::TEXTAREA);
      $this->form->addelement('Body', 'footer',FormType::HTMLEDITOR);
    }

    private function prepareData() {
      $this->data["bild"] = (substr($this->data["bild"],-1) === "/")? substr($this->data["bild"],0,-1) : $this->data["bild"];

      mys::getObj()->clearWhere()->cleanup();
      addWhere("path","LIKE","%" . $this->data["bild"] . "%");
      addWhere("filetype","LIKE","%image%");
      select("files");

      $i = 0;
      $this->images = array();
      while($row = getRow()) {
        $this->images[++$i] = $row["path"] . "/" . $row["name"];
      }

      $this->footer = $this->data["footer"];
      $this->subject = $this->data["subject"];
    }

    public function getInline() {

      $this->prepareData();
      if (issetArray($_POST,array("to","from","fromname","toname","msg","image"))) {
        $this->sendMail();
      }

      return $this->getForm();
    }

    private function sendMail() {
        
      $this->footer = str_replace("[msg]", htmlspecialchars($_POST["msg"]), $this->footer);
      $this->footer = str_replace("[image]", '<img src="http://'. $_SERVER["SERVER_NAME"] . '/' .$this->images[(int) $_POST["image"] + 1].'">', $this->footer);
      $this->footer = str_replace("[fromname]", htmlspecialchars($_POST["fromname"]), $this->footer);
      $this->footer = str_replace("[toname]", htmlspecialchars($_POST["toname"]), $this->footer);

      mys::getObj()->query("DELETE FROM eCardsBlacklist WHERE time < " . (time() - 60*60*24));
      
      addWhere("ip","=",$_SERVER["REMOTE_ADDR"]);
      select("eCardsBlacklist","id");

      if (count(getRows()) >= 3) {
        return false;
      }

      $mail = new mimeMail();
      $mail->setFrom($_POST["from"], "Grußkarte");
      $mail->setSubject("Grußkarte");
      $mail->addRecipient($_POST["to"]);
      $mail->setBody($this->footer);
      $mail->send();
      unset($mail);

      insertArray("eCardsBlacklist",array("time" => time(), "ip" => $_SERVER["REMOTE_ADDR"]) , "is");
        
    }

    private function getForm() {

      $html = "";
      $html .= "<div id=\"eCard\">";
      $html .= "<form action=\"". $GLOBALS['cms_root'].$GLOBALS['akt_menuepunkt']['title_intern']  .".html\" method=\"post\">";

      $i = 0;
      $html .= "<div class=\"eCardBox\">";
      foreach ($this->images as $path) {
        $checked = ($i == 0)? "checked=\"checked\"" :"";
        $html .= "<div class=\"imagebox\">";
        $html .= "<a rel=\"lightbox\" href=\"{$path}\"><img src=\"{$path}\" alt=\"Bild {++$i}\" style=\"width: 150px;\"></a>";
        $html .= "<input type=\"radio\" name=\"image\" value=\"$i\" {$checked}>";
        $html .= "</div>";
      }
      $html .= "<div style=\"clear: left;\"></div>";
      $html .= "</div>";

      $html .= "<div class=\"data\">";
      $html .= "<label>Empfänger Email:</label> <input type=\"text\" name=\"to\" value=\"E-Mail\"><br />";
      $html .= "<label>Empfänger Name:</label> <input type=\"text\" name=\"toname\" value=\"Name\"><br />";
      $html .= "<label>Absender Email:</label> <input type=\"text\" name=\"from\" value=\"E-Mail\"><br />";
      $html .= "<label>Absender Name:</label> <input type=\"text\" name=\"fromname\" value=\"Name\"><br />";
      $html .= "<label>Grußkartentext:</label> <textarea name=\"msg\" value=\"mail\"></textarea>";

      $html .= "<input type=\"submit\" class=\"eCardButton\" value=\"Absenden\">";
      $html .= "</div>";
      $html .= "<form>";
      $html .= "</div>";

      $html .= '<script type="text/javascript"> $(document).ready(function() {';
      $html .= '$(\'.imagebox a\').lightBox({txtImage: \'Bild\',txtOf: \'von\'});';
      $html .= '}); </script>';


      return $html;

    }
  }
?>