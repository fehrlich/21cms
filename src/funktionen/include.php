<?php
    $GLOBALS['CVARS'] = array();
    $GLOBALS['jsInclude'] = array();
    if(!isSEt($GLOBALS['languages'])) $GLOBALS['languages'] = array();
    date_default_timezone_set('Europe/Berlin');
    ini_set('date.timezone', 'Europe/Berlin');
    ini_set('default_charset', 'utf-8');
    ini_set('memory_limit', '256M');
    mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX );
?>