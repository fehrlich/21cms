<?php

function __autoload($name) {
	$classForFrontEnd = array('formular', 'phpbb', 'phpbb', 'file', 'addresses', 'mimeMail', 'image','userFilter','subelement','UserConfig','DatabaseConnector','Languages');

        $shopClasses = array('shop','cart','product');

	if (substr($name, 0, 7) == 'action\\'){
//		echo substr($name, 7).'!!!';
		$file = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'actions/' . substr($name, 7) . '.php';
		if(file_exists($file))
			include($file);
		else {
			throw new \ElementMissingException("Aktion {$name} nicht gefunden");
		}
		return;
	}


	if (substr($name, 0, 4) == 'cms\\')
		$name = substr($name, 4);

	if (in_array($name, $classForFrontEnd)) {
		include($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'klassen/' . $name . '.php');
	} elseif (in_array($name, $shopClasses)) {
		include($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'klassen/shop/' . $name . '.php');
	} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'element/' . $name . '.php')) {
		include($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'element/' . $name . '.php');
	} else {
		throw new \ElementMissingException("Element {$name} nicht gefunden");
	}
}

function checkVar($vars, $attr = '', $isget = false) {
	if (!is_array($vars))
		$vars = array($vars);
	$glob = ($isget) ? $_GET : $_POST;
//	var_dump($glob);
//	var_dump($glob);
	foreach ($vars as $i => $var) {
		if (!isset($glob[$var]) || ($glob[$var] == '' && (!isSet($attr[$i]) || $attr[$i] != 'd')))
			return false;
		elseif ($attr != '') {
			if ($attr[$i] == 'i')
				$glob[$var] = (int) $glob[$var];
			elseif ($attr[$i] == 'd') {
				$dVar = str_replace('..', '', $glob[$var]);
				while ($dVar != str_replace('..', '', $dVar)) {
					$dVar = str_replace('..', '', $dVar);
				}
				if ($isget) {
					$_GET[$var] = ($dVar);
				}else
					$_POST[$var] = ($dVar);
				if ($glob[$var] == 'root') {
					if ($isget)
						$_GET[$var] = '';
					else
						$_POST[$var] = '';
				}
			}
		}
	}
	return true;
}

function buildLink($name = '', $admin = '', $lang = '', $page = '', $rss = '', $db = '', $dataTitle = '') {
	$name = ($name === '' && isSet($_GET['str'])) ? $_GET['str'] : $name;
	$admin = ($admin === '') ? isAdmin() : $admin;
	$lang = ($lang === '') ? Languages::getLang() : $lang;
	$page = ($page === '' && isSet($_GET['page'])) ? $_GET['page'] : $page;
	$rss = ($rss === '') ? (isSet($_GET['act']) && $_GET['act'] == 'getrssfeed') : $rss;
	$db = ($db === '') ? (isSet($_GET['db']) && $_GET['db'] != '') : $db;
	$dataTitle = ($dataTitle === '') ? (isSet($_GET['title']) && $_GET['title'] != '') : $dataTitle;

	$addAdmin = ($admin) ? 'admin/' : '';
    $addLang = ($lang !== false && $lang !== '') ? strtolower($lang) . '/' : '';

	$link = $GLOBALS['cms_roothtml'] . $addAdmin . $addLang;
	if ($name == '' || $name == 'index') {
		$link .= 'index.html';
	} elseif ($rss) {
		$link .= $db . '/' . $name . '.xml';
	} elseif ($dataTitle != '') {
		//Produkte/Versorger_Produkte-KuRma.html
		$link .= $db . '/' . $name . '-' . $dataTitle . '.html';
	} else {
		$addPage = ($page > 0) ? '-Seite' . $page : '';
		$link .= $name . $addPage . '.html';
	}
	$link = str_replace(' ', '%20',$link);
	return $link;
}

function buildGet($namen, $isfirst = false, $forcefirst = false) {
	$str = '';
	$spl = explode(',', $namen);
	if ($isfirst)
		$add = '';
	else
		$add = '&';
	foreach ($spl as $get) {
		if (isset($_GET[$get]) && $_GET[$get] != '') {
			$str .= $add . $get . '=' . $_GET[$get];
			if ($add == '')
				$add = '&';
		}
	}
	if (($isfirst && $str != '') || $forcefirst)
		$str = '?' . $str;
	return $str;
}

function getMenuid($str = "", $useint = false) {
	$id = 0;
	$startpage = false;
	if ($useint) {
		addWhere('id', '=', $useint, 'i');
	} elseif ($str == 'index' || $str == '') {
//		addWhere('verlinkung', '=', 0);
		addWhere('startpage', '=', '1', 'i');
		$startpage = true;
	}
	else {
            addWhere('title_intern', '=', $str);
        }

	setLimit(1);
	select('menuepunkte', '*');
	$row = getrow();

    if (isset($row["disabled"]) && $row["disabled"] == 1) {
		header("Status: 302 Moved Temporarily");
		header("Location: " . $GLOBALS['cms_roothtml']);
		exit();
    }

	if (!$startpage && (!isSet($row['id']) || $row['id'] <= 0) && $GLOBALS['interface'] != 'ajax') {
		header("Status: 302 Moved Temporarily");
		header("Location: " . $GLOBALS['cms_roothtml']);
		exit();
	}

	if (!empty($row["verlinkung"]) && $row["verlinkung"] != 'index') {
	  header("Location: /{$row["verlinkung"]}.html");
	  exit();
	}

	$GLOBALS['SEO_tags'] = $row['seo_tags'];
	$GLOBALS['SEO_description'] = $row['seo_desc'];
	$GLOBALS['SEO_canonical'] = $row['canonical'];

	if ( isSet($row['seo_newTitle']) && $row['seo_newTitle'] != '')
        $GLOBALS['SITE_TITLE'] = $row['seo_newTitle'];
    else {

        $lang = Languages::getLang();
        if ($lang == "" || !isset($GLOBALS["languages"][strtolower($lang)])) {
            $GLOBALS['SITE_TITLE'] = $row['title'];
        } else {
            $GLOBALS['SITE_TITLE'] = $row['title_' . $GLOBALS["languages"][strtolower($lang)]];
        }
    }

        $id = $row['id'];
	$GLOBALS['akt_menuepunkt'] = $row;
	if ($row['cvars'] != '') {
		$data = unserialize($row['cvars']);
		foreach ($data as $name => $val) {
			$GLOBALS['CVARS'][$name] = array();
			$GLOBALS['CVARS'][$name]['value'] = $val;
		}
	}
	/*
	 * Check User Rights
	 */
	if (!isAdmin() && isSet($row['secure']) && $row['secure'] == '1') {
		if (frontendSession::getObj()->isLoggedIn()) {
//			$allow = unserialize($row['secure_data']);
			$allow_user = explode(',', substr($row['allow_user'], 1, -1));
			$allow_groups = explode(',', substr($row['allow_groups'], 1, -1));
			if (in_array(frontendSession::getObj()->getUserId(), $allow_user) || in_array(frontendSession::getObj()->getGroupId(), $allow_groups)) {
				;
			} else{
                            if(UserConfig::getObj()->getLoginSite()){
                                header('location: '.UserConfig::getObj()->getLoginSite());
                            }
                            die('Permission denied');
                        }
		} else{
                    if(UserConfig::getObj()->getLoginSite()){
                        header('location: '.UserConfig::getObj()->getLoginSite());
                    }
                    die('Permission denied');
                }
	}
	return (int) $id;
}

function isAdmin() {
	return ($GLOBALS['interface'] == 'admin' || $GLOBALS['interface'] == 'ajax');
}

function buildSelectArray($tab, $what = array('id', 'title'), $tree = false, $order = '') {

	$rows = array();
	$add = ($tree) ? ',level' : '';
	if ($tree)
		$order = 'ordernr';
	else{
		if($order == '') $order = $what[0];
        }
	select($tab, $what[0] . ',' . $what[1] . $add, $order);
	while ($row = getrow()) {
		if ($tree) {
			$rows[$row[$what[0]]] = array('val' => $row[$what[1]], 'style' => 'padding-left:' . (20 * $row['level']) . 'px');
		}else
			$rows[$row[$what[0]]] = $row[$what[1]];
	}
	return $rows;
}

function stdHeader() {
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>[TITEL]</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>

<body>';
}

function stdFooter() {
	echo '
	<script src="js/global.js" type="text/javascript"></script>
</body>
</html>';
}

if (!function_exists('get_called_class')) {

	function get_called_class() {
		$bt = debug_backtrace();
		//debug($bt);
		$l = 0;
		do {
			$l++;
			$lines = file($bt[$l]['file']);
			$callerLine = $lines[$bt[$l]['line'] - 1];
			//debug($callerLine);
			preg_match('/([a-zA-Z0-9\_]+)::' . $bt[$l]['function'] . '/', $callerLine, $matches);
		} while (in_array($matches[1], array('parent', 'self')) && $matches[1]);
		return $matches[1];
	}

}

function preparation() {
//	$tmp = $GLOBALS['akt_menuepunkt'];
//	if($tmp['id'] > 0)
//		$GLOBALS['aktmenu'] = array($tmp['id']);
//	else
//		$GLOBALS['aktmenu'] = array(0);
//	$last = 0;
//	while($tmp['parent'] > 0 && $tmp['parent'] != $last){
//		$last = $tmp['parent'];
//		$GLOBALS['aktmenu'][] = $tmp['parent'];
//		addWhere('id', '=', $tmp['parent']);
//		select('menuepunkte','parent');
//		$tmp = getRow();
//	}
//	$GLOBALS['aktmenu'] = array_reverse($GLOBALS['aktmenu']);
	$GLOBALS['aktmenu'] = getMenuHierarchy();


	if (isSet($GLOBALS['akt_tag']))
		$GLOBALS['SITE_TITLE'] = $GLOBALS['akt_tag']['title'];
}

function getMenuHierarchy($menuepunkt = 0) {
	if ($menuepunkt == 0)
		$menuepunkt = $GLOBALS['akt_menuepunkt'];
	$hir_array = array();
	$tmp = $menuepunkt;
	if ($tmp['id'] > 0)
		$hir_array = array($tmp['id']);
	else
		$hir_array = array(0);
	$last = 0;
//	$GLOBALS['mysql_debug'] = true;
	while ($tmp['parent'] > 0 && $tmp['parent'] != $last) {
		$last = $tmp['parent'];
		$hir_array[] = $tmp['parent'];
		addWhere('id', '=', $tmp['parent']);
		select('menuepunkte', 'parent');
		$tmp = getRow();
	}
//	$GLOBALS['mysql_debug'] = false;
	$hir_array = array_reverse($hir_array);
	return $hir_array;
}

function flipArray($arr) {
	$ret = array();
	if (!is_array($arr))
		return array();
	foreach ($arr as $name => $ar) {
		if(is_array($ar)){
			foreach ($ar as $id => $val) {
				if (!isset($ret[$id]))
					$ret[$id] = array();
				$ret[$id][$name] = $val;
			}
		}
	}
	return $ret;
}

function mysql2selectArray($arr, $ret = array()) {
	foreach ($arr as $k => $ar) {
		if (is_array($ar)) {
			$ar = array_values($ar);
			if (isset($ar[1]))
				$ret[$ar[0]] = $ar[1];
			else
				$ret[$ar[0]] = $ar[0];
		}else {
			$ret[$k] = $ar;
		}
	}
	return $ret;
}

/**
 *
 * @return Tree Admin Menu
 */
function getAdminMenu() {

	$menu = new Tree();

	$menu->addNode('WebSeite', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php">WebSeite</a>');

	$menu->addNode('DateiManager', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Dateimanager&art=none">DateiManager</a>');
	$rights = \cms\session::getObj()->getAdminMenuRights();
	if (rights::getObj()->isSuperAdmin() || (isSet($rights['allow6']) && $rights['allow6'])) {
		addWhere('dbType', '=', '0');
		select('databases', 'id,name');
		$dbs = getRows();
		$menu->addNode(1, '<a href="#">Daten</a>');
		if (rights::getObj()->isAdmin()) {
			$menu->addNode(6, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=DatabaseConnector">Datenbanken</a>', 1);
			$menu->addNode(7, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Comments">Kommentare</a>', 1);
		} elseif (count($dbs) > 0) {
			$menu->addNode(6, '<a href="#">Datenbanken</a>');
			$menu->addNode(7, '<a href="#">Kommentare</a>', 1);
		}
		foreach ($dbs as $row) {
			$menu->addNode('DB' . $row['id'], '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=DatabaseConnector&amp;db=' . $row['id'] . '">' . $row['name'] . '</a>', 6);
			$menu->addNode('DBKOM' . $row['id'], '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Comments&amp;db=' . $row['id'] . '">' . $row['name'] . '</a>', 7);
		}
	}


//	if (rights::getObj()->isAdmin())
	$menu->addNode('DataTemplate', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=DataTemplate">DataTemplate</a>', 1);
//	if (rights::getObj()->isAdmin())
	$menu->addNode('PdfTemplate', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=PdfTemplate">Pdf-DataTemplate</a>', 1);

//	if (rights::getObj()->isAdmin()) {

	$menu->addNode(4, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=User">User</a>');
	$menu->addNode('usergroups', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Gruppen">Usergroups</a>', 4);
	$menu->addNode('Gruppen', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=GruppenDbRechte">Datenbanken Rechte</a>', 4);
	$menu->addNode('seiten', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=PageRights">Seiten Rechte</a>', 4);
	$menu->addNode('Tags', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=TagSystem">Tags</a>', 1);

	$menu->addNode(8, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=stats">Statistik</a>');
	$menu->addNode('SendedForms', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=SendedForms">Gesendete Formulare</a>', 8);
	$menu->addNode('SendedForms', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=StatsSearch">Suchanfragen</a>', 8);
	$menu->addNode('Rss', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=RssFeeds">Rss Feeds</a>', 1);

	$menu->addNode(5, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=FrontEndUser">FrontEndUser</a>');
	$menu->addNode('status', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=FrontEndGroups">Gruppen</a>', 5);
	$menu->addNode('sprachen', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Languages">Sprachen</a>');
	$menu->addNode('pms', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=MessageSystem">Nachrichten</a>', 4);

	$menu->addNode(10, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=newsletterTemplates">Newsletter</a>');
	$menu->addNode('Adressen', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=addresses">Adressen</a>', 10);
	$menu->addNode('Newsletter', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=addressGroups">Gruppen</a>', 10);
	$menu->addNode('NewsletterQueue', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=NewsletterQueue">NewsletterQueue</a>', 10);
	$menu->addNode('Verschicken', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=sendNewsletter">Verschicken</a>', 10);

	if (rights::getObj()->isSuperAdmin()) {
		$menu->addNode(3, '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=server">Server</a>');
		$menu->addNode('status', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=servstatus">Status</a>', 3);
		$menu->addNode('info', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=servinfo" target="_blank">Info</a>', 3);
		$menu->addNode('stat', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=servereinst">Einstellungen</a>', 3);
		$menu->addNode('fix', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=fixes">Fixes</a>', 3);
	}
//	}
	if (isSet($GLOBALS['trac_milestone']) && $GLOBALS['trac_milestone'] != '')
		$menu->addNode('bugreport', '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=BugReport">Fehler Melden</a>');
	if (isSet($GLOBALS['forum_integration']) && $GLOBALS['forum_integration'] != '')
		$menu->addNode('forum', '<a href="' . $GLOBALS['forum_integration'] . '" target="_blank">Forum</a>');



	$menu->addNode("logout", '<a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=logout">Logout (' . \cms\session::getObj()->getUserName() . ')</a>');

	$menuValues = $menu->getArray();
	$newMenu = new Tree();
	foreach ($menuValues as $node) {
		if (rights::getObj()->isSuperAdmin() || (isSet($rights['allow' . $node->getId()]) && $rights['allow' . $node->getId()] == '1')) {
			$newMenu->addNode($node->getId(), $node->getContent(), $node->getParent());
		}
	}

	return $newMenu;
}

function getAdminBreadCrumb(){
    if(!isSet($_GET['mm'])) return "";
    $menu = getAdminMenu();
//    new dBug($menu);

    $node = $menu->getNodeByContent('mm='.$_GET['mm'].'');
    if(!isset($node) || $node == NULL) return "";
    $html = $node->content;
//    new dBug($node);
    while($node->getParent() > 0){
        $node = $menu->getNode($node->getParent());
//    new dBug($node);
        $html = $node->content.' | '.$html;
    }
    return $html;
}

function getAdminMenuId($mm){
	$menu = getAdminMenu();
	$menuValues = $menu->getArray();
	foreach ($menuValues as $node) {
		if(strpos($node->getContent(), 'mm='.$mm))
			return $node->getId();
	}
}

function buildToolbar() {

	$html = '';
	$html .= '<img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/new/logo.gif" id="cms_logo" alt="21 CMS" />';
	$menu = getAdminMenu();
	$html .= $menu->__toString();
	return '' . $html . '';
}

function urlEncodeWithoutSlash($txt) {
	$txt = str_replace('+', 'PLUSPROTECTION', $txt);
	$txt = str_replace(array('%2F', '+'), array('/', ' '), urlencode($txt));
	$txt = str_replace('PLUSPROTECTION', '+', $txt);
	return $txt;
}

function addJs($js) {
	if (!isSet($GLOBALS['includeJs']))
		$GLOBALS['includeJs'] = array();
	if (!in_array($js, $GLOBALS['includeJs']))
		$GLOBALS['includeJs'][] = $js;
//    var_dump($GLOBALS['includeJs']);
}

function filterArray(&$arr, $allow = false) {
	if (!is_array($arr))
		return false;

	foreach ($arr as $key => $value) {
		if (!in_array($key, $allow))
			unset($arr[$key]);
	}
}

function slashFolderFix($folder) {
	$slashes = str_replace('//', '/', $folder);
	while ($slashes != $folder) {
		$folder = $slashes;
		$slashes = str_replace('//', '/', $folder);
	}

	while (substr($folder, -1) == '/')
		$folder = substr($folder, 0, -1);

	return $folder;
}

function zufallspw($len = 8) {
	$newpass = "";
	$laenge = $len - 2;
	$voc = "aeiou";
	$kons = "bcdfghjklmnpqrstvwxyz";
	$zahlen = "0123456789";

	mt_srand((double) microtime() * 1000000);
	$even = true;
	for ($i = 1; $i <= $laenge; $i++) {
		if ($even)
			$newpass .= substr($kons, mt_rand(0, strlen($kons) - 1), 1);
		else
			$newpass .= substr($voc, mt_rand(0, strlen($voc) - 1), 1);
		$even = !$even;
	}
	for ($i = 1; $i <= 2; $i++) {
		$newpass .= substr($zahlen, mt_rand(0, strlen($zahlen) - 1), 1);
	}

	return $newpass;
}

function matheval($equation) {
	$equation = preg_replace("/[^0-9+\-.*\/()%]/", "", $equation);
	// fix percentage calcul when percentage value < 10
	$equation = preg_replace("/([+-])([0-9]{1})(%)/", "*(1\$1.0\$2)", $equation);
	// calc percentage
	$equation = preg_replace("/([+-])([0-9]+)(%)/", "*(1\$1.\$2)", $equation);
	// you could use str_replace on this next line
	// if you really, really want to fine-tune this equation
	$equation = preg_replace("/([0-9]+)(%)/", ".\$1", $equation);
	if ($equation == "") {
		$return = 0;
	} else {
		eval("\$return=" . $equation . ";");
	}
	return $return;
}

function issetArray($haystack,$needle) {
  if (!is_array($haystack) || !is_array($needle)) {
    return false;
  }

  foreach($needle as $key) {
    if (!isset($haystack[$key])) {
      return false;
    }
  }

  return true;
}
function in_string($needle,$haystack){
    if(!is_array($needle)) return (strpos ($haystack, $needle)!==false);
    else{
        foreach($needle as $n){
            if(strpos ($haystack, $n)!==false)
                return true;
        }
    }
}
function strtodate($dateString){

    $timeSplit = explode('.', $dateString);
    if(isSet($timeSplit[2])){
        if(strlen($timeSplit[2]) == 2) $dateString = $timeSplit[0].'.'.$timeSplit[1].'.'.substr (date('Y'), 0,2).$timeSplit[2];
    }
    return strtotime($dateString);
}
?>
