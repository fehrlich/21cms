<?php
    $ajax = array(
                   'klassen/formular.php',
                  'klassen/DataTemplate.php',
                  'klassen/html/Table.php',
                  'klassen/dateimanager.php',
                  'klassen/MessageSystem.php',
                  'klassen/config.php',
                  'klassen/UserTemplates.php',
                  'klassen/TagSystem.php',
                  'klassen/login.php',
                  'klassen/sendmail.php',
                  'klassen/mimeMail.php',
                  'klassen/addresses.php',
                  'klassen/addressGroups.php',
                  'klassen/newsletterTemplates.php'
                );
    $extern = array(
                  'funktionen/include.php',
                  'klassen/tpl.php',
                  'klassen/container.php',
                  'klassen/stats.php',
                  'element/menu.php',
                  'klassen/filter.php',
            );
    $intern = array(
                  'klassen/login.php',
                  'klassen/MessageSystem.php',
                  'klassen/formular.php',
                  'klassen/tpl.php',
                  'klassen/container.php',
                  'klassen/html/Table.php',
                  'klassen/DataTemplate.php',
                  'klassen/filter.php',
                  'klassen/User.php',
                  'klassen/Gruppen.php',
                  'klassen/GruppenDbRechte.php',
                  'klassen/TagSystem.php',
                  'element/menu.php',
                  'klassen/search.php',
                  'klassen/sendmail.php',
                  'klassen/mimeMail.php',
                  'klassen/config.php',
                  'klassen/upgrade.php',
                  'klassen/stats.php',
                  'klassen/addresses.php',
                  'klassen/addressGroups.php',
                  'klassen/newsletterTemplates.php'
    );
    $global = array(

                  'klassen/mysql.php',
                  'klassen/session.php',
                  'funktionen/global.php',
                  'klassen/Vars.php',
                  'klassen/Tree.php',
                  'klassen/DataEditor.php',
                  'klassen/DatabaseConnector.php',
                  'klassen/PageRights.php',
                  'klassen/Custom_Enum.php',
                  'klassen/element.php',
                  'klassen/mainelement.php',
                  'klassen/subelement.php',
                  'klassen/Tag.php',
                  'klassen/Languages.php',
                  'klassen/frontendSession.php',
                  'klassen/Parser.php',
                  'klassen/rights.php',
                  'klassen/sendNewsletter.php',
                  'klassen/System.class.php',
                  'klassen/Exceptions/ElementMissingException.class.php',
                  'klassen/Logger.class.php',
                  'klassen/NewsletterQueue.php',
                  'klassen/MailQueueRunner.class.php',
                  'klassen/mimeMail.php',
    );

    foreach($global as $path){
        if(!include_once( ((intern) ? '../' : '') . $path)){
            echo 'Error: Include of "'.$path.'" failed: '.((intern) ? '../' : '') . $path;

        }

    }

    if (defined("ajax")) {
        foreach($ajax as $path) include_once("../" . $path);
    } elseif(intern) {
        foreach($intern as $path) include_once("../" . $path);
    } else {
        foreach($extern as $path) include_once($path);
    }

?>
