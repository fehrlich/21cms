<?php
include_once 'ElementTestCase.php';
/**
 * Description of addKontaktFormular
 *
 * @author Franz Ehrlich
 */
class addKontaktFormular  extends ElementTestCase {

	public function getElementId(){
		return "";
	}

    public function addContentForm(){
		$this->open("/sanderm/admin/index.php?");
		$this->click("addElementkontaktFormular");
		$this->type("mailTo", "info2@localhost");
		$this->type("acceptTxt", "testNachricht");
		$this->click("//button[@type='button']");
		// $this->Senden();
    }

    public function addWaitFor(){
		$this->waitForElementPresent('css=.kontaktFormular');
		return $this->isElementPresent('css=.kontaktFormular');
    }

    public function editContentForm(){
		$this->open("/sanderm/admin/index.php?#");
		$this->click("link=Quickadd");
		$this->type("quickadd", "test1\nname\nvorname\nfirma");
		$this->click("//button[@type='button']");
    }

    public function editWaitFor(){
		return "name";
    }
}
?>
