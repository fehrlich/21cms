<?php
/**
 * Description of config
 *
 * @author Franz Ehrlich
 */
class Testconfig {
	static public $domain = 'http://localhost';
	static public $path = '/sanderm/admin/';

	public static function getDomain() {
	 return Testconfig::$domain;
	}

	public static function getPath() {
	 return Testconfig::$path;
	}
}
?>
