<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';
include_once 'Testconfig.php';

/**
 * Description of TableTestCase
 *
 * @author Franz Ehrlich <Franz.Ehrlich@21advertise.de>
 */
abstract class TableTestCase extends PHPUnit_Extensions_SeleniumTestCase{

    function setUp(){
        $this->setBrowser('*firefox C:\Program Files (x86)\Mozilla Firefox\firefox.exe');
        $this->setBrowserUrl(Testconfig::getDomain().Testconfig::getPath());
    }

	function Login(){
		$this->open(Testconfig::getPath());
		$this->type("username", "admin");
		$this->type("passwort", "admin");
		$this->click("loginsub");
		$this->waitForPageToLoad("30000");
//		$this->cookieString = $this->getCookie();
	}

    function testAddContent(){
		$this->Login();
       	$this->open(Testconfig::getPath().$this->getLocation());
		$this->click("css=.newRow");
		$this->waitForElementPresent('css=form');
		$this->fillNewForm();
		$this->click("//button[@type='button']");
		$this->waitForElementPresent('css=tr.edit');
       	$this->assertTrue($this->isTextPresent($this->addWaitFor()));

    }
	
    function testEditContent(){
		$this->Login();
		$this->open(Testconfig::getPath().$this->getLocation());
		$elid = $this->getEval("window.$('.editRow:last').attr('id');");
		$this->click("id=".$elid);
		$this->waitForElementPresent('css=form');
		$this->fillEditForm();
		$this->click("//button[@type='button']");
		$this->waitForElementPresent('css=tr.edit');
		$this->assertTrue($this->isTextPresent($this->editWaitFor()));
    }

    function testDelContent(){
		$this->Login();

    }
	abstract function getLocation();
	abstract function fillNewForm();
	abstract function fillEditForm();
	
	abstract function addWaitFor();
	abstract function editWaitFor();
}
?>