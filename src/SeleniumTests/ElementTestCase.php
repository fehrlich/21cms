<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


require_once 'PHPUnit/Extensions/SeleniumTestCase.php';
include_once 'Testconfig.php';

/**
 * Description of ElementTestCase
 *
 * @author Franz Ehrlich <franz.ehrlich@21advertise.de>
 */
abstract class ElementTestCase extends PHPUnit_Extensions_SeleniumTestCase {
    protected $captureScreenshotOnFailure = TRUE;
    protected $screenshotPath = 'D:\xampp\xampp\htdocs\failScreens';
    protected $screenshotUrl = 'http://localhost/failScreens';
	
    protected $elementid;
    protected $cookieString;

    function setUp() {
        $this->setBrowser('*firefox C:\Program Files (x86)\Mozilla Firefox\firefox.exe');
        $this->setBrowserUrl(Testconfig::getDomain().Testconfig::getPath());
		$this->elementid = $this->getElementId();
		$this->cookieString = "";
    }
	
//	function setCookie(){
//		$this->createCookieAndWait($this->cookieString);
//	}

	function testLogin(){
		$this->open(Testconfig::getPath());
		$this->type("username", "admin");
		$this->type("passwort", "admin");
		$this->click("loginsub");
		$this->waitForPageToLoad("30000");
		$this->cookieString = $this->getCookie();
	}

    function testAddContentCase() {
		
//		$this->setCookie();
		$this->testLogin();
       	$this->click("css=.hover.container .admineig2 .sub");
		$this->waitForElementPresent('id=addElements');
		if($this->getElementId() != ''){
			$this->click("id=addElement".$this->elementid);
			$this->waitForElementPresent('css=form');
		}
		$this->addContentForm();
		$this->click("//button[@type='button']");
		if($this->addWaitForBodyText !== false){
			$this->waitForBodyText($this->addWaitForBodyText());
			$this->assertTrue($this->isTextPresent($this->addWaitForBodyText()));
		}else{
			$this->waitForBodyText($this->addWaitForBodyText());
			$this->assertTrue($this->addWaitFor());
		}

    }

	function testEditContentCase(){
//		$this->setCookie();
		$this->testLogin();
		$elid = $this->getEval("window.$('#testcase').parents('.inhalt:first').attr('id').substr(2);");

		$this->click("css=#hover".$elid." .admineig2 .edit");
		$this->editContentForm();
		$this->click("//button[@type='button']");
		$this->waitForBodyText($this->editWaitFor());
		$this->assertTrue($this->isTextPresent($this->editWaitFor()));
		$this->clickAndWait("css=#nodeWebSeite a");
		$this->assertTrue($this->isTextPresent($this->editWaitFor()));
	}

	function testDelContentCase(){
//		$this->setCookie();
		$this->testLogin();
		$elid = $this->getEval("window.$('#testcase').parents('.inhalt:first').attr('id').substr(2);");

		$this->click("css=#hover".$elid." .admineig2 .del");
		$this->waitForBodyText('Sicher das Sie');
		$this->click("//button[@type='button']");
		$this->waitForNotBodyText($this->editWaitFor(),1000);
		$this->assertFalse($this->isTextPresent($this->editWaitFor()));
		$this->open(Testconfig::getPath());
		$this->waitForPageToLoad("30000");
		$this->assertFalse($this->isTextPresent($this->editWaitFor()));
	}

	function setEditorContent($txt){
		$this->waitForElementPresent('id=cke_html');
		$this->runScript("CKEDITOR.instances['html'].setData('".$txt."');");
	}

	abstract function getElementId();
	abstract function addContentForm();
	function addWaitFor(){
		return true;
	}
	function addWaitForBodyText(){
		return false;
	}
	abstract function editContentForm();
	abstract function editWaitFor();
}
?>