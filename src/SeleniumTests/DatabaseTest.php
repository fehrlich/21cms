<?php

include "TableTestCase.php";

/**
 * Description of DatabaseTest
 *
 * @author Franz Ehrlich <Franz.Ehrlich@21advertise.de>
 */
class DatabaseTest extends TableTestCase {
    //put your code here
    public function addWaitFor(){
		return "testDB";
    }

    public function editWaitFor(){
		return "testDB";
    }

    public function fillEditForm(){
    }

    public function fillNewForm(){
		$this->type("name", "testDB");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[1]", "test1");
		$this->type("fieldname[2]", "test2");
		$this->type("fieldname[3]", "test3");
		$this->type("fieldname[4]", "test4");
		$this->select("type[2]", "label=Textbereich");
		$this->select("type[3]", "label=Formatierbarer Text");
		$this->select("type[4]", "label=Emailfeld");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[5]", "test5");
		$this->type("fieldname[6]", "test6");
		$this->select("type[5]", "label=Datum");
		$this->select("type[6]", "label=Auswahl DropDown");
		$this->type("eig[6]", "a,b,c");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[7]", "test7");
		$this->select("type[7]", "label=Datei");        
    }

    public function getLocation(){
		return "index.php?mm=DatabaseConnector";
    }

}
?>
