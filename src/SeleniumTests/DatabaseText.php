<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


require_once 'PHPUnit/Extensions/SeleniumTestCase.php';
include_once 'Testconfig.php';

/**
 * Description of DatabaseText
 *
 * @author XzenTorXz
 */
class DatabaseText extends PHPUnit_Extensions_SeleniumTestCase {
   
    function setUp() {
        $this->setBrowser('*firefox C:\Program Files (x86)\Mozilla Firefox\firefox.exe');
        $this->setBrowserUrl(Testconfig::getDomain().Testconfig::getPath());
    }

	function testLogin(){
		$this->open(Testconfig::getPath());
		$this->type("username", "admin");
		$this->type("passwort", "admin");
		$this->click("loginsub");
		$this->waitForPageToLoad("30000");
	}

    function testAddDatabase() {
         $this->open(Testconfig::getPath()."index.php?mm=Datenbanken");
		$this->waitForPageToLoad("30000");
		$this->click("css=.newRow");
		$this->type("name", "testDB");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[1]", "test1");
		$this->type("fieldname[2]", "test2");
		$this->type("fieldname[3]", "test3");
		$this->type("fieldname[4]", "test4");
		$this->select("type[2]", "label=Textbereich");
		$this->select("type[3]", "label=Formatierbarer Text");
		$this->select("type[4]", "label=Emailfeld");
		$this->click("link=Neue Zeile");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[5]", "test5");
		$this->type("fieldname[6]", "test6");
		$this->select("type[5]", "label=Datum");
		$this->select("type[6]", "label=Auswahl DropDown");
		$this->type("eig[6]", "a,b,c");
		$this->click("link=Neue Zeile");
		$this->type("fieldname[7]", "test7");
		$this->select("type[7]", "label=Datei");
		$this->click("//button[@type='button']");
		$this->waitForBodyText('testDB');
//		$this->waitForCondition('function(){var wait = function() { return jQuery.active == 0; }return wait.call(selenium.browserbot.getCurrentWindow());}();');
		$this->assertTrue($this->isTextPresent("testDB"));
    }
	function testDelDatabase() {
		$this->open(Testconfig::getPath()."index.php?mm=DatabaseConnector");
		$this->click("//div[@id='admincontent']/table/tbody/tr[16]/td[5]/span[2]");
		$this->click("//button[@type='button']");
		$this->waitForNotBodyText('testDB',1000);
		$this->assertFalse($this->isTextPresent("testInhalt"));
		$this->open(Testconfig::getPath());
		$this->waitForPageToLoad("30000");
		$this->assertFalse($this->isTextPresent("testDB"));
	}
}
?>