<?php

    /**
     * Klasse zum erstellen von Formularen
     *
     * @author F.Ehrlich
     */
    function formSort( $krit1, $krit2 ) {
        $m = $krit1['orderNr'];
        $n = $krit2['orderNr'];
        if ( $m == $n )
            return 0;
        return ($m > $n) ? 1 : -1;
    }

    class formular {

        private $elements = array();
        private $aelements = array();
        private $my_tab = '';
        private $tabs = array( 'std' );
        private $akttab = 'std';
        private $serializeto = '';
        private $serarrays = array();
        private $mysqladd = array();
        private $connectid = 0;
        private $connected = false;
        private $formAction;
        private $saveButton;
        private $events = array();
        private $pseudoevents = array();
        private $submit = false;
        private $formid = '';
        private $doBackup = true;
        private $sucessMessage = 'Ihre Daten wurden erfolgreich gespeichert';
        private $showHelp = false;
        private $mailadress = '';
        private $mailFromName;
        private $mailFromEmail;
        private $mailTemplate = '';
        private $mailSubject = '';
        private $reOrder = false;
        private $orderCount = 0;
        private $spamProtection = false;
        private $doublePostProtection = false;
        private $multiLangElements = array();
        private $multiplyTable = false;
        private $multiplyTableRowAnz = 0;
        private $useCaptcha = false;
        private $captchaError = '';
        public $noDataInput = false;
        public $posted;
        public $mysqlrow;
        public $useform = true;
        public $dataid;
        public $isOnSameLine = false;
        public $clearLine = false;
        public $overWriteNameWithDescriptonForSerialization = false;

        public function setShowHelp( $showHelp ) {
            $this->showHelp = $showHelp;
            $this->showHelp = false;
        }

        public function useCaptcha( $useOne = true ) {
            $this->useCaptcha = $useOne;
        }

        private function securityCheck( $forceEditable = false ) {
            $ret = true;
            if ( $this->spamProtection ) {
                $spamProt = array( 's_name', 's_telefon', 's_fax', 's_company' ); //,'s_city','s_state'
                foreach ( $spamProt as $s ) {
                    if ( !isSet( $_POST[$s] ) || $_POST[$s] != "" ) {
                        $ret = false;
                    }
                }

                if ( isset( $_POST["s_time"] ) && (time() - ((int) $_POST["s_time"])) < 5 )
                    $ret = false;
            }
            if ( $this->useCaptcha ) {
                require_once('recaptchalib.php');
                $privatekey = UserConfig::getObj()->getCaptchaKey();
                $resp = recaptcha_check_answer( $privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"] );

                if ( !$resp->is_valid ) {
                    $this->captchaError = "Das Captcha war nicht korrekt.<br />";
                    $ret = false;
                }
            }

            foreach ( $this->elements as $id => $el ) {
                $msg = '';
                $visible = $this->checkIfElementVisible( $el['name'] );
                if ( isSet( $el['errormsg'] ) && $el['errormsg'] != '' )
                    $msg = $el['errormsg'];
                if ( ($el['pflicht'] || $el['value'] != '') && $el['secType'] == 'email' && !preg_match( "/[\.a-z0-9_-]+@[\.a-z0-9-]{2,}\.[a-z]+$/i", $el['value'] ) ) {
                    $msg = 'Dies ist keine g&uuml;ltige Emailadresse<br />';
                }
                if ( $el['pflicht'] && $el['value'] == '' && $visible ) {
                    $msg = 'Bitte f&uuml;llen Sie dieses Pflichtfeld aus.<br />';
                }

                if ( $el['pflicht'] && ($el['secType'] == 'select' || $el['secType'] == 'selectTxt') && $el['value'] == '0' && $visible ) {
                    $msg = 'Bitte w&auml;hlen Sie einen Wert<br />';
                }

                if ( $this->spamProtection && substr_count( $el["value"], 'http://' ) > 1 ) {
                    $msg = 'Maximal ein Link pro Kommentar erlaubt<br />';
                }
                if ( isset( $el['unique'] ) && $el['unique'] && (!$forceEditable && $this->connectid < 1) ) {
                    addWhere( $el['name'], '=', $el['value'] );
                    if ( dbCheck( $this->my_tab ) )
                        $msg = 'Dieser Eintrag ist schon vorhanden.<br />';
                }

                if ( $msg != '' ) {
                    $ret = false;
                    $this->elements[$id]['errormsg'] = $msg;
                }
            }
            return $ret;
        }

        public function checkValidation( $forceEditable = false ) {
            return $this->securityCheck( $forceEditable );
        }

        public function startLine() {
            $this->isOnSameLine = true;
        }

        public function endLine() {
            $this->isOnSameLine = false;
            $this->clearLine = true;
        }

        private function parse_values() {
            foreach ( $this->elements as $id => $el ) {
                //$elname = str_replace(' ', '_', $el['name']);
                $elname = menuepunkte::setTitelIntern( $el['name'] );
                if ( isset( $_POST[$elname] ) ) {
                    if ( $el['art'] == 'pwchange' ) {
                        if ( is_array( $_POST[$elname] ) )
                            $pw = $_POST[$elname][0];
                        else
                            $pw = $_POST[$elname];
                        if ( $pw == '' )
                            $this->elements[$id]['mysql'] = false;
                        $this->elements[$id]['value'] = md5( $pw );
                    } else
                        $this->elements[$id]['value'] = $_POST[$elname];
                }
                /*
                 * FIX: tableAnz not clean, can't handle two tables or Singe select and table
                 */

                if ( is_object( $el['data'] ) && get_class( $el['data'] ) == 'formular' && !isSet( $_POST[$elname] ) && isSet( $_POST['tableAnz' . $el['data']->getFormId()] ) ) {
                    $tableCheck = true;
                    $tableMaxAnz = $_POST['tableAnz' . $el['data']->getFormId()];
                } else
                    $tableCheck = false;

                if ( ($el['art'] == 'select' || $el['art'] == 'selectTxt') && $tableCheck ) {
                    if ( is_array( $tableMaxAnz ) )
                        $maxAnz = 0;
                    else
                        $maxAnz = $tableMaxAnz;
                    $this->elements[$id]['value'] = array();
                    for ( $i = 0; $i < $maxAnz; $i++ ) {
                        $this->elements[$id]['value'][$i] = '0';
                    }
                } elseif ( $el['art'] == 'multiselect' || $el['art'] == 'tags' ) {
                    if ( isSet( $_POST[$elname] ) ) {
                        if ( is_array( $_POST[$elname] ) ) {
                            $newArray = array();
                            foreach ( $_POST[$elname] as $multiName => $multiValue ) {
                                if ( is_array( $multiValue ) ) {
                                    $newArray[$multiName] = implode( ',', $multiValue );
                                }
                            }
                            if ( count( $newArray ) > 0 )
                                $this->elements[$id]['value'] = $newArray;
                            else
                                $this->elements[$id]['value'] = ',' . implode( ',', $_POST[$elname] ) . ',';
                        } else
                            $this->elements[$id]['value'] = ',' . $_POST[$elname] . ',';
                    } else
                        $this->elements[$id]['value'] = ',,';
                }
                elseif ( $el['art'] == 'simpleCheck' ) {
                    if ( !isSet( $_POST[$elname] ) || !is_array( $_POST[$elname] ) ) {
                        if ( $elname == 'allowWebseite' ) {
                            echo $elname . ':' . isSet( $_POST[$elname] );
                        }
                        if ( isSet( $_POST[$elname] ) ) {
                            $this->elements[$id]['value'] = '1';
                        } else {
                            $this->elements[$id]['value'] = '0';
                        }
                    } elseif ( (isSet( $_POST[$elname] ) && is_array( $_POST[$elname] )) || ($tableCheck) ) {
//					$maxAnz = $tableMaxAnz;
//					for($i=0;$i<$maxAnz;$i++){
//						$this->elements[$id]['value'][$i] = '0';
//					}
//					$startI = 1;
                        $this->elements[$id]['value'] = array();
                        if ( isSet( $_POST[$elname] ) ) {
                            foreach ( $_POST[$elname] as $setI => $v ) {
                                //						for($i=$startI;$i<=$setI;$i++)
                                //							$this->elements[$id]['value'][$i] = '0';
                                //						$startI = $setI+1;
                                $this->elements[$id]['value'][$setI] = '1';
                            }
                        }
                    }
                }
//			elseif($el['art'] == 'CHECK'){
//				if(!isSet($_POST[$elname])) $elname = substr ($elname, 0, -2);
//				$this->elements[$id]['value'] = $_POST[$elname];
//				new dBug($_POST);
//				new dBug($this->elements[$id]);
//			}
                elseif ( $el['art'] == 'date' ) {
                    if ( strstr( $this->elements[$id]['value'], '.' ) ) {
                        $spl = explode( '.', $this->elements[$id]['value'] );
                        $day = $spl[0];
                        $month = $spl[1];
                        $year = $spl[2];
                        $time = mktime( 1, 0, 0, $month, $day, $year );
                        $this->elements[$id]['value'] = $time;
                    }
                } elseif ( $el['art'] == 'grid' ) {
                    $val = array();
                    foreach ( $_POST[$elname] as $i => $html ) {
                        $val[] = array(
                            'top' => $_POST[$elname . 'Top'][$i],
                            'left' => $_POST[$elname . 'Left'][$i],
                            'width' => $_POST[$elname . 'Width'][$i],
                            'height' => $_POST[$elname . 'Height'][$i],
                            'data' => $html
                        );
                    }
                    $this->elements[$id]['value'] = serialize( $val );
                } elseif ( $el['art'] == 'formular' || $el['art'] == 'filter' ) { // || (isSet($this->events[$elname]) && isSet($this->events[$elname]))
                    if ( $el['art'] == 'filter' ) {
                        $this->filterBuild( $elname, $el['value'], $el['data'] );
//					$_POST[$this->elements[$id]['value']] = serialize($this->elements[$id]['data']['form']->post());
                        $this->elements[$id]['value'] = serialize( $this->elements[$id]['data']['form']->post() );
                    } elseif ( $el['art'] == 'formular' )
                        $this->elements[$id]['value'] = serialize( $el['data']->post() );
                }
            }
        }

        public function parseMailTpl( $tpl ) {
            foreach ( $this->elements as $el ) {
                $val = (is_array( $el['value'] )) ? implode( ',', $el['value'] ) : $el['value'];
                $tpl = str_replace( '[' . $el['name'] . ']', htmlspecialchars( $val, ENT_QUOTES, 'UTF-8' ), $tpl );
                $tpl = str_replace( '[' . $el['desc'] . ']', htmlspecialchars( $val, ENT_QUOTES, 'UTF-8' ), $tpl );
            }
            return $tpl;
        }

        private function post( $retstr = false ) {
            $this->parse_values();
            $insert = array();
            $parastr = '';
            if ( !$this->securityCheck() )
                return false;

            foreach ( $this->elements as $elId => $el ) {
                if ( $el['art'] == 'upload' || $el['secType'] == 'upload' ) {
                    if ( !empty( $_FILES ) && ($_FILES[$el['name']]['tmp_name'] != '' || isSet( $_FILES['Filedata']['tmp_name'] )) ) {
                        if ( $_FILES[$el['name']]['tmp_name'] == '' )
                            $tmpElName = 'Filedata';
                        else
                            $tmpElName = $el['name'];
                        $tempFile = $_FILES[$tmpElName]['tmp_name'];
                        if ( isSet( $el['path'] ) )
                            $pfad = $el['path']; //['uploadDir'];
                        else
                            $pfad = $el['value']; //['uploadDir'];

                        $parser = new Parser( $this );
                        $pfad = Vars::parseUserVars( $pfad );
                        $pfad = $parser->parseTxt( $pfad );

                        if ( substr( $pfad, 0, 8 ) != '/dateien' )
                            $pfad = '/dateien/' . $pfad;
                        $pfad = str_replace( '//', '/', $pfad );
//                                        if(substr($pfad, -1) != '/') $pfad .= '/';


                        $targetPath = str_replace( '//', '/', $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $pfad );
                        if ( !file_exists( $targetPath ) )
                            mkdir( $targetPath );

                        $el['value'] = str_replace( '//', '/', $pfad . $_FILES[$tmpElName]['name'] );
                        $targetFile = $targetPath . $_FILES[$tmpElName]['name'];
                        move_uploaded_file( $tempFile, $targetFile );
                        $file = cms\file::load( $targetFile, true );
                        if ( isSet( $el['data']['fileData'] ) ) {
//                                                if(isAdmin()) echo $el['data']['fileData'].'!';
                            $txt = $parser->parseTxt( $el['data']['fileData'] );
                            $spl = explode( "\n", $txt );
                            foreach ( $spl as $line ) {
                                $lineSpl = explode( "=", $line, 2 );
                                if ( isSet( $lineSpl[1] ) ) {
                                    $name = trim( $lineSpl[0] );
                                    $value = trim( $lineSpl[1] );
                                    if ( $name != '' ) {
                                        $file->set( $name, $value );
                                    }
                                }
                            }
                        }
                        $file->close();
                        $this->elements[$elId]['value'] = $pfad . '/' . str_replace( '..', '', $_FILES[$tmpElName]['name'] );
                        if ( $el['data']['maxwidth'] || $el['data']['maxheight'] ) {
                            $el['data']['forceDim'] = (isSet( $el['data']['forceDim'] )) ? $el['data']['forceDim'] : false;
                            $img = image::create( $targetFile, true );
                            if ( $img ) {
                                if ( $el['data']['maxwidth'] > 0 )
                                    $img->resize( $el['data']['maxwidth'], $el['data']['maxheight'], $el['data']['forceDim'] )->save();
                            }else {
                                return "Das ist kein bild";
                            }
                        }
                    } else
                        $el['mysql'] = false;
                }
                if ( $el['mysql'] ) {
                    //$el['value'] = $_POST[$el['name']];
                    if ( $el['serto'] != '' ) {
                        if ( !isset( $this->serarrays[$el['serto']] ) )
                            $this->serarrays[$el['serto']] = array();
                        $useName = ($this->overWriteNameWithDescriptonForSerialization) ? 'desc' : 'name';
                        $this->serarrays[$el['serto']][$el[$useName]] = $el['value'];
                    }else {
                        if ( !isset( $insert[$el['mysqlField']] ) ) {
                            $parastr .= 's';
                        }
                        $insert[$el['mysqlField']] = $el['value'];
                        //echo 'Add('.$el['name'].'):'.$parastr."\n";
                    }
                }
            }
            foreach ( $this->serarrays as $name => $ser ) {
                $insert[$name] = serialize( $ser );
                $insert[$name] = preg_replace( "/(\n)|(\r)/", " ", $insert[$name] );
                $parastr .= 's';
            }
            $ins_id = '0';
            if ( $this->my_tab != '' && !$this->noDataInput ) {
                if ( count( $this->mysqladd ) > 0 ) {
                    $parastr .= 's';
                    $insert = array_merge( $insert, $this->mysqladd );
                    //echo 'AddMULTI:'.$parastr."\n";
                }
                $forceinsert = false;
                if ( $this->my_tab == 'elements' && (!isset( $_POST['container'] ) || $_POST['container'] == '') ) {
                    addWhere( 'id', '=', $this->connectid, 'i' );
                    if ( !dbcheck( $this->my_tab ) ) {
                        $forceinsert = true;
                        $insert['id'] = $this->connectid;
                        $parastr .= 'i';
                        //echo 'Add(id):'.$parastr."\n";
                    }
                }
                if ( $this->connectid > 0 && !$forceinsert ) {
                    addWhere( 'id', '=', $this->connectid, 'i' );
                    $this->dataid = $this->connectid;
                    updatearray( $this->my_tab, $insert, $parastr );
                } else {
                    $this->dataid = insertarray( $this->my_tab, $insert, $parastr );
                }
                if ( $this->doBackup ) {
                    $parser = new Parser();
                    $author = $parser->parseTxt( '[AUTHOR_loginname]' );
                    $backupData = array(
                        'table' => $this->my_tab,
                        'erstellt' => time(),
                        'querystr' => $_SERVER['QUERY_STRING'],
                        'data' => serialize( $insert ),
                        'dataId' => $this->connectid,
                        'autor' => $author
                    );
                    insertarray( 'formBackup', $backupData, 'sissis' );
                }
            }
            if ( $this->mailadress != '' ) {
                $msg = $this->getMailSource();

                $mail = new mimeMail();
                $mail->setFrom( $this->mailFromEmail, $this->mailFromName );

                $mail->setSubject( $this->parseMailTpl( $this->mailSubject ) );
                $mail->addRecipient( $this->mailadress );
                $mail->setBody( $msg );
                $mail->send();
            }
            if ( $this->my_tab == '' && !$retstr )
                return $insert;

            if ( isset( $_GET['inter'] ) && $_GET['inter'] == 'dia' )
                return 'RET:' . $this->dataid;
            if ( $this->doublePostProtection && $this->submit ) {
                header( "Location: " . $this->formAction );
            }
            return 'Ok';
        }

        private function checkIfElementVisible( $elname ) {
            if ( isset( $this->pseudoevents[$elname] ) ) {
                if ( is_array( $this->pseudoevents[$elname] ) ) {
                    foreach ( $this->pseudoevents[$elname] as $ev ) {
                        if ( $ev['event'] == 'OnlyShowOn' ) {

                            if ( isSet( $this->elements[$ev['mainElement']] ) )
                                $elValue = $this->elements[$ev['mainElement']]['value'];
                            else
                                $elValue = '';
//						if($this->elements[$ev['mainElement']]['secType'] == 'select')
//							$elValue = $this->elements[$ev['mainElement']]['data'][$elValue];
                            if ( isSet( $this->elements[$ev['mainElement']]['value'] ) && menuepunkte::setTitelIntern( $ev['onValue'] ) != $elValue )
                                return false;
                        }
                    }
                }
            }
            return true;
        }

        public function getMailSource() {

            if ( strlen( strip_tags( $this->mailTemplate ) ) > 3 ) {
                $msg = $this->parseMailTpl( $this->mailTemplate );
            } else {
//				$table = new Table(array('Feld','Wert'));
                $table = new Table();
                foreach ( $this->elements as $el ) {

                    if ( $this->checkIfElementVisible( $el['name'] ) && $el['art'] != 'hidden' && ($el['art'] != 'mysql' || $el['name'] == 'seitenTitel') ) {
                        $val = $el['value'];
                        $add = true;
                        if ( $el['secType'] == 'simpleCheck' ) {
                            if ( $val == 'on' )
                                $val = 'Ja';
                            else
                                $val = 'Nein';
                            $name = strip_tags( $el['data'] );
                        } else
                            $name = $el['desc'];
                        if ( is_array( $el['value'] ) )
                            $val = implode( ',', $el['value'] );

//                                                    new dBug($el);
                        if ( $el['secType'] == 'upload' ) {
                            if ( $val == '' || $val == $el['data']['uploaddir'] )
                                $val = '-';
                            else {
                                $table->addContent( array( '<b>' . $name . '</b>', '<a href="http://' . $_SERVER['HTTP_HOST'] . '/' . $val . '">Anzeigen</a>' ) );
                                $add = false;
                            }
                        }
                        if ( $add )
                            $table->addContent( array( '<b>' . $name . '</b>', htmlspecialchars( $val, ENT_QUOTES, 'UTF-8' ) ) );
                    }
                }
                $msg = (string) $table;
                $msg = str_replace( '<table', '<table border=1 width="500"', $msg );
            }
            return $msg;
        }

//	public function setFormId($id){
//		$this->formid = $id;
//	}
        /**
         * Change a specific property of an Element
         * @param string $name name of the element
         * @param string $eig name of the property
         * @param string $val name of the value that should be set
         */
        public function setElement( $name, $eig, $val ) {
            if ( $eig == 'name' ) {
                $this->elements[$val] = $this->elements[$name];
                $this->elements[$val]['name'] = $val;
                unset( $this->elements[$name] );
            } elseif ( $eig == 'addvalue' ) {
                if ( !isset( $this->elements[$name]['value'] ) || !is_array( $this->elements[$name]['value'] ) )
                    $this->elements[$name]['value'] = array();
                $this->elements[$name]['value'][] = $val;
            } else
                $this->elements[$name][$eig] = $val;
        }

        public function setMultiLanguage( $elementsArray, $all = true ) {
            if ( $all )
                $this->multiLangElements = $elementsArray;
            $lang = config::getObj()->get( 'languages' );
            if ( $lang ) {
                foreach ( $lang as $l ) {
                    $this->useTab( $l );
                    foreach ( $elementsArray as $name ) {
                        $el = $this->elements[$name];
                        if ( $el['tab'] == 'Admin' ) {
                            $this->useTab( 'Admin' );
                            $desc = $el['desc'] . ' (' . $l . ')';
                        } else
                            $desc = $el['desc'];
                        $this->addElement( $desc, $name . '_' . $l, $el['art'], $el['value'], $el['data'], $el['mysql'], $el['pflicht'], $el['secType'] );
                        $this->setElement( $name, 'serto', $el['serto'] );
                    }
                }
            }
        }

        public function setSpamProtection( $spamProtection ) {
            $this->spamProtection = $spamProtection;
        }

        public function setDoublePostProtection( $doublePostProtection ) {
            $this->doublePostProtection = $doublePostProtection;
        }

        public function getMultiLanguage() {
            return $this->multiLangElements;
        }

        public function getElements() {
            $els = array();
            foreach ( $this->elements as $el ) {
                $els[$el['name']] = $el;
            }
            return $els;
        }

        /**
         * Adds Elements to the Formular
         * @param array $elementArray can be one element or an array of elements
         */
        public function addElementArrays( $elementArray ) {
            if ( isSet( $elementArray['name'] ) )
                $this->elements[] = $elementArray;
            elseif ( is_array( $elementArray ) ) {
                foreach ( $elementArray as $el ) {
                    $this->elements[] = $el;
                }
            }
        }

        public function getElement( $name ) {
            if ( !isSet( $this->elements[$name] ) )
                return false;
            return $this->elements[$name];
        }

        public function getDummyData() {
            $data = array();
            foreach ( $this->elements as $el ) {
                $data[$el['name']] = "";
            }
            return $data;
        }

        /**
         * connects a mysql data to the form
         * @param int $id
         * @param boolean $connectOnly
         * @return void
         */
        public function connectData( $id, $connectOnly = false, $data = array() ) {
            $this->connectid = $id;
            $this->connected = true;
            if ( $connectOnly )
                return;
            $what = '';
            $serarr = array();
            foreach ( $this->elements as $el ) {
                if ( $el['mysql'] ) {
                    if ( $el['serto'] == '' )
                        $what .= ($what == '') ? $el['mysqlField'] : ',' . $el['mysqlField'];
                    else {
                        $what .= (in_array( $el['serto'], $serarr )) ? '' : ',' . $el['serto'];
                        $serarr[] = $el['serto'];
                    }
                }
            }
            if ( count( $data ) == 0 ) {
                addWhere( 'id', '=', $id );
                select( $this->my_tab, $what );
                $row = getrow();
            } else {
                $row = $data;
            }
            $this->mysqlrow = $row;
            $debug = false;
//                $debug = isSet($row['to']);
//                if($debug) new dBug($row);
            foreach ( $this->elements as $id => $el ) {
                if ( $el['art'] != 'html' && $el['mysql'] ) {
//                                if($debug) new dBug($el);
                    if ( $el['serto'] != '' ) {
                        $dat = unserialize( $row[$el['serto']] );

                        $this->elements[$id]['value'] = (isSet( $dat[$el['name']] )) ? $dat[$el['name']] : '';
                    } elseif ( $el['art'] == 'formular' || $el['art'] == 'grid' ) {
//                                    new dBug($this->elements[$id]);
                        $this->elements[$id]['value'] = unserialize( $row[$el['name']] );
                    } elseif ( $debug && $el['art'] == 'multiselect' ) {
                        if ( !isSet( $row[$el['mysqlField']] ) )
                            $row[$el['mysqlField']] = '';
                        if ( !is_array( $row[$el['mysqlField']] ) )
                            $this->elements[$id]['value'] = array( 1 => $row[$el['mysqlField']] );
                        else
                            $this->elements[$id]['value'] = $row[$el['mysqlField']];
                    }else {
                        if ( !isSet( $row[$el['mysqlField']] ) )
                            $row[$el['mysqlField']] = '';
                        $this->elements[$id]['value'] = $row[$el['mysqlField']];
                    }
//                                if($debug) new dBug($row[$el['mysqlField']]);
//                                if($debug) new dBug($this->elements[$id]);
//                                if($debug) new dBug($this->elements[$id]['value']);
                }
            }
//                if($debug) new dBug($this->elements);
        }

        public function setMailAdress( $mailadress, $fromName = '21cms', $fromEmail = 'info@21cms.de', $subject = '' ) {
            if ( $subject == '' )
                $subject = "KontaktFormular von " . $_SERVER['HTTP_HOST'];
            $this->mailadress = $mailadress;
            $this->mailFromName = $fromName;
            $this->mailFromEmail = $fromEmail;
            $this->mailSubject = $subject;
        }

        public function setMailTemplate( $tpl ) {
            $this->mailTemplate = $tpl;
        }

        /**
         * creates a form
         * @param string $tab mysql table name
         */
        public function __construct( $tab = '', $formAction = '', $mailadress = '', $formid = '', $forcePosted = false ) { //, $mailadress = ''
            $this->formid = $formid;
            if ( $formAction == '' ) {
                if ( isAdmin() )
                    $formAction = $_SERVER['PHP_SELF'] . buildGet( 'c,nextMainId,mm', true );
                else
                    $formAction = $_SERVER['REQUEST_URI'];
            }
//		echo 'ajaxform'.$this->formid.':'.$_POST['ajaxform'.$this->formid].'!!!!!!';
            if ( $forcePosted )
                $this->posted = true;
            elseif ( isset( $_POST['submit' . $this->formid] ) || isset( $_POST['ajaxform' . $this->formid] ) )
                $this->posted = true;
            else
                $this->posted = false;
            $this->my_tab = $tab;
            $this->formAction = $formAction;
            $this->saveButton = '';
            $this->mailadress = $mailadress;
        }

        /**
         * sets the action attribute of the form element
         * @param string $formAction action url
         */
        public function setFormAction( $formAction ) {
            $this->formAction = $formAction;
        }

        /**
         * creates or use a tab for following elements
         * @param string $name tabname
         * @param string $html uses html instead of elements
         */
        public function useTab( $name, $html = '' ) {
            if ( !in_array( $name, $this->tabs ) ) {
                $this->tabs[] = $name;
            }
            $this->akttab = $name;
        }

        /**
         * returns the currently used Tab
         */
        public function getUsedTab() {
            return $this->akttab;
        }

        /**
         * serializes the following elements
         * @param string $name name of mysql table col
         */
        public function startSerialize( $name ) {
            $this->serializeto = $name;
        }

        public function overWriteNameWithDescriptonForSerialization( $boolean ) {
            $this->overWriteNameWithDescriptonForSerialization = $boolean;
        }

        /**
         * stops the serializeation
         */
        public function stopSerialize() {
            $this->serializeto = '';
        }

        /**
         * adds an mysql data which is not visible
         * @param string $name
         * @param string $value
         */
        public function addMysqlData( $name, $value ) {
            $this->mysqladd[$name] = $value;
        }

        /**
         * Removes an Element from Form
         * @param string $name name of the element
         */
        public function removeElement( $name ) {
            if ( isset( $this->elements[$name] ) )
                unset( $this->elements[$name] );
        }

        /**
         * adds an element to the form
         * @param string $description description of the element
         * @param string $name name of the mysql col and post string
         * @param string $art the type of the element
         * @param string $value the default value
         * @param array $data specify the element propperties
         * @param boolean $mysql if this data should be stored
         * @param boolean $pflicht if it has to have a value
         * @param string $securityType if you want to change how the value been checked
         */
        public function addElement( $description, $name, $art = 'text', $value = '', $data = array(), $mysql = true, $pflicht = false, $securityType = '', $unique = false, $mysqlField = '', $cssClass = '', $orderNr = '' ) {
            if ( $this->posted && isset( $_POST[$name] ) && $_POST[$name] != '' && $art != 'mysql' ) {
                $value = $_POST[$name];
            }
            if ( $art == 'database' ) {
                if ( !isset( $data['dbArray'] ) ) {
                    $dbs = new DatabaseConnector();
//				addWhere('dbType', '=', '0');
                    $data['dbArray'] = $dbs->getDatabases( 'id,name' );
                    if ( $value == '' && is_array( $data['dbArray'] ) ) {
                        $value = key( $data['dbArray'] );
                    }
                    if ( isset( $data['add'] ) ) {
                        foreach ( $data['add'] as $i => $add ) {
                            $data['dbArray'][$i] = $add;
                        }
                    }
                }
                if ( !isset( $data['noreload'] ) || !$data['noreload'] )
                    $this->addOnChangeReload( $name );
            }elseif ( $art == 'dbCon' ) {
                if ( !isSet( $data['selectData'] ) ) {
                    if ( isSet( $data['add'] ) )
                        $data['selectData'] = $data['add'];
                    else
                        $data['selectData'] = array();
                    $data['selectData'] += buildSelectArray( $data['table'], $data['what'] );
                }
            }

            $el = array();
            $el['desc'] = $description;
            $el['name'] = $name;
            $el['art'] = $art;
            $el['value'] = $value;
            $el['data'] = $data;
            $el['mysql'] = $mysql;
            $el['tab'] = $this->akttab;
            $el['serto'] = $this->serializeto;
            $el['pflicht'] = $pflicht;
            $el['unique'] = $unique;
//		echo $securityType.'!';
            $el['secType'] = ($securityType != '') ? $securityType : $art;
            $el['startLine'] = $this->isOnSameLine;
            $el['endLine'] = $this->clearLine;
            $el['mysqlField'] = ($mysqlField != '') ? $mysqlField : $name;
            $el['cssClass'] = $cssClass;
            if ( $orderNr == '' || $orderNr == 0 || $orderNr < 0 ) {
                $orderNr = $this->orderCount;
                $this->orderCount++;
            } else {
                $this->reOrder = true;
            }
            $el['orderNr'] = $orderNr;

            $this->clearLine = false;
            $this->elements[$name] = $el;
            if ( in_array( $name, $this->multiLangElements ) ) {
                $this->setMultiLanguage( array( $name ), false );
            }
        }

        /**
         * set the page to be reloaded if this element is changed
         * @param string $name name of the element
         */
        public function addOnChangeReload( $name ) { //, $do
            $this->events[$name] = true;
        }

        /**
         * If the name of an element is changed to a specific value the form changed to a specific form at a specific element
         * @param string $elname name of the element which should be watched
         * @param string $value the value when the form should change
         * @param string $changeName the element that should change
         * @param formular $form the form to change
         */
        public function addOnChangeSetForm( $elname, $value, $changeName, $form ) {
            $form->useform = false;
            $this->events[$elname][] = array(
                'event' => 'OnChangeSetForm',
                'onValue' => $value,
                'change' => $changeName,
                'setValue' => $form
            );
        }

        public function addOnlyShowOn( $elname, $mainElementName, $showOnValue ) {
            $ev = array(
                'event' => 'OnlyShowOn',
                'mainElement' => $mainElementName,
                'onValue' => $showOnValue,
                'show' => $elname
            );
            $this->events[$mainElementName][] = $ev;
            $this->pseudoevents[$elname][] = $ev;
        }

        /**
         * adds a html string at this position
         * @param string $html the html to insert
         */
        public function addHtml( $html, $orderNr = "", $name = "" ) {
            $el = array();
            $el['desc'] = '';
            $el['name'] = '';
            $el['art'] = 'html';
            $el['value'] = '';
            $el['data'] = $html;
            $el['mysql'] = false;
            $el['tab'] = $this->akttab;
            $el['serto'] = '';
            $el['pflicht'] = false;
            $el['secType'] = 'html';
            $el['cssClass'] = 'addedHtml';

            if ( $orderNr == '' || $orderNr == 0 || $orderNr < 0 ) {
                $orderNr = $this->orderCount;
                $this->orderCount++;
            } else {
                $this->reOrder = true;
            }
            $el['orderNr'] = $orderNr;
            if ( $name == '' ) {
                $name = "f" . uniqid();
            }
            $this->elements[$name] = $el;
        }

        public function show( $showFormAgain = false ) {
            $html = '';
            $submit = false;
            $ret = true;
            if ( $this->getPosted() ) {
                $ret = $this->post( true );
                $submit = true;
                if ( $ret !== false && !$showFormAgain )
                    return $ret;
            } else
                $ret = false;
            $first = true;
            $showtabs = '<div id="tabs"><ul>';
            if ( $ret !== false && $showFormAgain && $this->sucessMessage != '' )
                $html = '<span class="accepted">' . $this->sucessMessage . '</span>';
            if ( $this->useform )
                $html .= '<form action="' . $this->formAction . '" method="post" enctype="multipart/form-data" id="' . $this->formid . '">'; //  name="ajaxform'.$this->formid.'"
            if ( $this->spamProtection ) {
                $html .= '<input type="text" class="s" name="s_name" value="" />'; // style="visible:none"
                $html .= '<input type="text" class="p" name="s_telefon" value="" />';
                $html .= '<input type="text" class="a" name="s_time" value="' . time() . '" />';
                $html .= '<input type="text" class="a" name="s_fax" value="" />';
//			$html .= '<input type="text" name="s_city" value="" size="0" />';
//			$html .= '<input type="text" class="b" name="s_state" value="" />';
                $html .= '<input type="hidden" name="s_company" value="" />';
            }
            $tabhtml = array();
            $lineid = 1;
            if ( $this->reOrder ) {
                usort( $this->elements, "formSort" );
                foreach ( $this->elements as $i => $el ) {
                    unset( $this->elements[$i] );
                    $this->elements[$el['name']] = $el;
                }
            }
            foreach ( $this->elements as $el ) {
                if ( !isset( $tabhtml[$el['tab']] ) ) {
                    $showtabs .= '<li><a href="#tab-' . $el['tab'] . '" id="tab_' . $el['tab'] . '">' . $el['tab'] . '</a></li>';
                    if ( $el['tab'] != 'std' || count( $this->tabs ) > 1 )
                        $tabhtml[$el['tab']] = '<div id="tab-' . $el['tab'] . '">';
                    else
                        $tabhtml[$el['tab']] = '<div class="formdiv">';
                }
                if ( $el['art'] == 'simpleCheck' )
                    $el['data'] = $el['desc'];
//			elseif($el['desc'] != '' && $el['art'] != 'hidden')
//				$tabhtml[$el['tab']] .= '<label>'.$el['desc'].(($el['pflicht'])?('*'):'').':</label>';
                $tabhtml[$el['tab']] .= $this->elementBuild( $el, $lineid, true );
                $lineid++;
            }
            $showtabs .= '</ul>';
            if ( count( $tabhtml ) > 1 )
                $html .= $showtabs;

            if ( $this->showHelp )
                $html .= '<div class="link" id="helpIcon">Hilfe</div>';
            if ( isAdmin() && $this->doBackup ) {
                addWhere( 'table', '=', $this->my_tab );
                addWhere( 'dataId', '=', $this->connectid );
                select( 'formBackup', 'id,erstellt,autor' );
                $backups = getRows();
                $anz = count( $backups );
                if ( $anz > 0 ) {
                    $html .= '<div id="formBackups"><div class="link" id="backupIcon">Backups</div><div class="timeline">';

                    $html .= '<select name="backup" id="formBackup" class="toSlider">';
                    foreach ( $backups as $i => $backup ) {
                        $add = ($i == ($anz - 1)) ? ' selected="selected"' : '';
                        $html .= '<option value="' . $backup['id'] . '"' . $add . '>Author:' . $backup['autor'] . ' ' . date( 'd.m.Y H:i:s', $backup['erstellt'] ) . '</option>';
                    }
                    $html .= '</select>';
                    $html .= '</div></div>';
                }
            }
            foreach ( $this->tabs as $tab ) {
                if ( isset( $tabhtml[$tab] ) ) {
                    $html .= $tabhtml[$tab] . '</div>';
                }
            }
            if ( count( $tabhtml ) > 1 )
                $html .= '</div>';
            if ( !$this->submit || $this->saveButton == '' ) {
                $html .= '<div class="submit"><input type="hidden" name="submit' . $this->formid . '" class="savebutton" value="1" /></div>';
            }
            if ( $this->useCaptcha ) {
                require_once('recaptchalib.php');
                $html .= '<div id="recaptcha" class="captcha">';
//                    $html .= '<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>';
                $publickey = UserConfig::getObj()->getCaptchaKeyPublic();

                $html .= '<span style="color: red">' . $this->captchaError . '</span>';
//                    $html .= '<script type="text/javascript"
//     src="http://www.google.com/recaptcha/api/challenge?k='.$publickey.'">
//  </script>
//  <noscript>
//     <iframe src="http://www.google.com/recaptcha/api/noscript?k='.$publickey.'"
//         height="300" width="500" frameborder="0"></iframe><br>
//     <textarea name="recaptcha_challenge_field" rows="3" cols="40">
//     </textarea>
//     <input type="hidden" name="recaptcha_response_field"
//         value="manual_challenge">
//  </noscript>';
//                    $html .= recaptcha_get_html($publickey);
                $html .= '</div>';
            }
            if ( $this->saveButton != '' ) {
                if ( $this->submit )
                    $html .= '<div class="submit"><input type="submit" name="submit' . $this->formid . '" class="savebutton" value="' . $this->saveButton . '" /></div>';
                else
                    $html .= '<div class="submit"><input type="button" name="savebutton" class="savebutton" value="' . $this->saveButton . '" /></div>';
            }
            if ( $this->useform )
                $html .= '</form>';
            return $html;
        }

        /**
         * converst the formular to html string
         * @return string html string
         */
        public function __toString() {
            return $this->show();
        }

        public function buildElementHtml( $description, $name, $art = 'text', $value = '', $data = array(), $mysql = true, $pflicht = false, $showlabel = false, $cssClass = '', $placeHolder = false ) {
            $el = array();
            $el['desc'] = $description;
            $el['name'] = $name;
            $el['art'] = $art;
            $el['value'] = $value;
            $el['data'] = $data;
            $el['mysql'] = $mysql;
            $el['tab'] = $this->akttab;
            $el['serto'] = $this->serializeto;
            $el['pflicht'] = $pflicht;
            $el['cssClass'] = $cssClass;
            $el["placeHolder"] = $placeHolder;
            $this->elements[] = $el;
            $this->parse_values();
            return $this->elementBuild( reset( $this->elements ), 1, $showlabel );
        }

        private function elementBuild( $el, $lineid = 1, $showlabel = false ) {
            if ( $this->multiplyTable )
                $addname = '[' . $lineid . ']';
            else
                $addname = '';
            $addstyle = '';
            $html = '';
            /**
             * @var $setValue formular
             */
            $setValue;
            $closediv = false;
            $function = $el['art'] . 'Build';
            if ( !method_exists( $this, $function ) ) {
                echo 'Funktion "' . $function . '" existiert nicht<br />' . print_r( $el, true );
                return;
            }
            //TODO: Put in PseudoElements
            if ( ($this->connected > 0 || $this->posted == true ) ) { // || $this->posted

                /*
                 * alle Events durchlaufen
                 */
                foreach ( $this->events as $name => $evarr ) {
                    if ( is_array( $evarr ) ) {
                        foreach ( $evarr as $i => $ev ) {
                            if ( $ev['event'] == 'OnChangeSetForm' && $ev['change'] == $el['name'] ) {
                                /*
                                 * Wenn aktuelles Element das Onchange Value besitzt
                                 */
//							echo $el['name'].'!'.$lineid;
//							echo $this->elements[$name]['value'][$lineid].'=='.$ev['onValue'].'<br>';
                                if ( isSet( $this->elements[$name]['value'][$lineid] ) && $this->elements[$name]['value'][$lineid] == $ev['onValue'] ) {
                                    $setValue = $ev['setValue'];
                                    $elements = $setValue->getElements();
                                    foreach ( $elements as $name => $ele ) {
                                        if ( substr( $ele['name'], -2 ) == '[]' ) {
                                            $n = substr( $ele['name'], 0, -2 );
                                        } else
                                            $n = $ele['name'];
                                        if ( strpos( $name, '][' ) ) {
                                            $spl = explode( '[', $n );
                                            $newname = substr( $spl[count( $spl ) - 1], 0, -1 );
                                            if ( isset( $el['value'][$newname] ) )
                                                $setValue->setElement( $name, 'value', $el['value'][$newname] );
                                            $setValue->setElement( $name, 'name', str_replace( '[' . ($lineid - 1) . ']', '[' . $lineid . ']', $name ) );
                                        }else {
                                            if ( isset( $el['value'][$n] ) )
                                                $setValue->setElement( $name, 'value', $el['value'][$n] );
                                            $setValue->setElement( $name, 'name', $el['name'] . '[' . $lineid . '][' . $n . ']' );
                                        }
                                    }
                                    $setValue .= '<input type="hidden" name="' . $el['name'] . '[' . $lineid . '][' . $name . ']" value="ser" />';
                                    return $setValue;
                                }
                            }
                        }
                    }
                }
            }
            $addstyle = '';
            if ( !$this->checkIfElementVisible( $el['name'] ) )
                $addstyle .= 'display:none;';
//		if(isset($this->pseudoevents[$el['name']])){
//			if(is_array($this->pseudoevents[$el['name']])){
//				foreach($this->pseudoevents[$el['name']] as $ev){
//					if($ev['event'] == 'OnlyShowOn'){
//					    if(isSet($this->elements[$ev['mainElement']]['value']) && $ev['onValue'] != $this->elements[$ev['mainElement']]['value'])
//							$addstyle .= 'display:none;';
//					}
//				}
//			}
//		}

            if ( isSet( $el['startLine'] ) && $el['startLine'] )
                $addstyle .= 'float:left;';
            if ( isSet( $el['endLine'] ) && $el['endLine'] )
                $addstyle .= 'clear:both';

            $labelStyle = str_replace( 'display:none;', '', $addstyle );
            if ( $addstyle != '' )
                $addstyle = ' style="' . $addstyle . '"';
            if ( $labelStyle != '' )
                $labelStyle = ' style="' . $labelStyle . '"';

            $cssClass = (isSet( $el['cssClass'] ) && $el['cssClass'] != '') ? ' ' . $el['cssClass'] : '';
            if ( isset( $this->events[$el['name']] ) ) {
                if ( $this->events[$el['name']] === true )
                    $event = '<div class="onchangechangedialog">'; //<div class="url">'.$event.'</div
                elseif ( is_array( $this->events[$el['name']] ) ) {
                    $event = '<div class="onchangevalue">'; //<div class="url">'.$event.'</div
                    foreach ( $this->events[$el['name']] as $ev ) {
                        if ( $ev['event'] == 'OnChangeSetForm' ) {
                            $event .= '<div class="var setValue dontpost" id="' . menuepunkte::setTitelIntern( $ev['onValue'] ) . '-' . menuepunkte::setTitelIntern( $ev['change'] ) . '">' . $ev['setValue'];
                            $event .= '<input type="hidden" name="' . menuepunkte::setTitelIntern( $el['name'] ) . '[]" value="ser" />';
                            $event .= '</div>';
                        } elseif ( $ev['event'] == 'OnlyShowOn' ) {

                            $event .= '<div class="var showOn dontpost"' . $addstyle . ' id="on' . menuepunkte::setTitelIntern( $ev['onValue'] ) . '">' . menuepunkte::setTitelIntern( $ev['show'] ) . '</div>';
                        }
                    }
                    $event .= '<div class="formcontent ' . menuepunkte::setTitelIntern( $el['name'] ) . $cssClass . '"' . $addstyle . ' id="nr' . $this->formid . $lineid . '">'; // id="nr'.$lineid.'"
                    $closediv = true;
                } else
                    $event = '<div class="formcontent ' . menuepunkte::setTitelIntern( $el['name'] ) . $cssClass . '"' . $addstyle . ' id="nr' . $this->formid . $lineid . '">'; // id="nr'.$lineid.'"
            }
            else {
                $strpos = strpos( $el['name'], '[' );
                $splitName = menuepunkte::setTitelIntern( $el['name'] );
                if ( $strpos !== false ) {
                    $splitName = substr( $splitName, 0, $strpos );
                }
                $event = '<div class="formcontent formcontent-' . $splitName . $cssClass . '"' . $addstyle . ' id="nr' . $this->formid . $lineid . '">'; // id="nr'.$lineid.'"
            }



            if ( $showlabel && $el['desc'] != '' && $el['art'] != 'hidden' && $el['art'] != 'simpleCheck' ) {
                $label = '<span class="fieldname">' . $el['desc'] . (($el['pflicht']) ? ('*') : '') . ':</span>';
            } else {
                $label = '';
            }


            if ( isset( $_GET['change' . $el['name']] ) )
                $el['value'] = $_GET['change' . $el['name']];


            $html .= $event . $label . $this->$function( $el['name'] . $addname, $el['value'], $el['data'], (isset( $el["placeHolder"] ) && $el["placeHolder"] == 1) ? $el["desc"] : null );
            if ( isset( $el['errormsg'] ) && $el['errormsg'] != '' )
                $html .= '<span class="formerror" style="color:red">' . $el['errormsg'] . '</span>';


            if ( $showlabel && $el['desc'] != '' && $el['art'] != 'hidden' && $el['art'] != 'simpleCheck' )
                $html .= '';

            $html .= '</div>';
            if ( $closediv )
                $html .= '</div>';
            return $html;
        }

        /**
         * saveButton setter
         * @param string $savebutton value of the savebutton
         */
        public function setSaveButton( $savebutton, $submit = false ) {
            $this->saveButton = $savebutton;
            $this->submit = $submit;
        }

        /**
         *
         * converst the formular to html string
         * @return string html string
         */
        public function toTable() {
            $this->multiplyTable = true;
            $header = array();
            $content = array();
            $names = array();
            $edit = false;
            $this->multiplyTableRowAnz = 1;
            foreach ( $this->elements as $name => $el ) {
                $header[] = $el['desc'];
//                        $names[] = $name;
//                        if(!is_array($el['value']) && strpos($el['value'], ',')){
//                            $el['value'] = explode(',', substr($el['value'],1,-1));
//                        }
                if ( is_array( $el['value'] ) || $edit ) { // || $el['art'] == 'simpleCheck'
                    $edit = true;
                    $names[] = $name;
                } else {
                    $content[] = $this->elementBuild( $el );
                }
            }
            $header[] = 'Aktionen';
            $tab = new Table( $header, false, true, $this->formid );
            $lineid = 0;

            if ( count( $names ) > 0 ) {
                $this->multiplyTableRowAnz = 0;
                $dirtyCount = 0;
                if ( !is_array( $this->elements[$names[$dirtyCount]]['value'] ) )
                    $dirtyCount++;
                foreach ( $this->elements[$names[$dirtyCount]]['value'] as $i => $val ) {
                    $content = array();
                    $lineid++;
                    foreach ( $names as $name ) {
                        $tmp = $this->elements[$name];
                        if ( isSet( $this->elements[$name]['value'][$i] ) )
                            $tmp['value'] = $this->elements[$name]['value'][$i];
                        $content[] = $this->elementBuild( $tmp, $lineid );
                    };
                    $content[] = '<a href="#" class="delline">Zeile L&ouml;schen</a>';
                    $tab->addContent( $content );
                    $this->multiplyTableRowAnz++;
                }
            } else
                $tab->addContent( $content );

            $tab->addContent( array( '<a href="#" class="newline">Neue Zeile</a>' ) );
            return (string) $tab . '<input type="hidden" name="tableAnz' . $this->formid . '" id="tableAnz" value="' . $this->multiplyTableRowAnz . '" />';
        }

        private function textBuild( $name, $value, $eig, $placeHolder = null ) {
            $style = (isSet( $eig['style'] ) && $eig['style'] != '') ? ' style="' . $eig['style'] . '"' : '';
            $id = (isSet( $eig['id'] ) && $eig['id'] != '') ? ' id="' . $eig['id'] . '"' : '';
            if ( is_array( $value ) )
                $value = current( $value );

            $placeHolder = (!empty( $placeHolder )) ? "placeholder=\"{$placeHolder}\"" : "";
            return '<input type="text" ' . $placeHolder . ' name="' . $name . '" value="' . $value . '" ' . $style . $id . ' />';
        }

        private function numberBuild( $name, $value, $eig, $placeHolder = null ) {
            $style = (isSet( $eig['style'] ) && $eig['style'] != '') ? ' style="' . $eig['style'] . '"' : '';
            $id = (isSet( $eig['id'] ) && $eig['id'] != '') ? ' id="' . $eig['id'] . '"' : '';
            if ( is_array( $value ) )
                $value = current( $value );

            $placeHolder = (!empty( $placeHolder )) ? "placeholder=\"{$placeHolder}\"" : "";
            return '<input type="number" ' . $placeHolder . ' name="' . $name . '" value="' . $value . '" ' . $style . $id . ' />';
        }

        private function mysqlBuild( $name, $value, $eig ) {
            return '';
        }

        private function timeBuild( $name, $value, $eig ) {
            return '<input type="text" name="' . $name . '" value="' . $value . '" class="date_not" />';
            return '<input type="text" name="' . $name . '" value="zeit: ' . $value . '" class="time" />';
        }

        private function dateBuild( $name, $value, $eig ) {
            if ( $value == '' )
                $value = time();
            if ( is_numeric( $value ) )
                $value = date( 'd.m.Y', $value );
            return '<input type="text" name="' . $name . '" value="' . $value . '" class="date" />';
        }

        private function hiddenBuild( $name, $value, $eig ) {
            return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
        }

        private function emailBuild( $name, $value, $eig, $placeHolder = null ) {
            $placeHolder = (!empty( $placeHolder )) ? "placeholder=\"{$placeHolder}\"" : "";
            return '<input type="text"  ' . $placeHolder . ' name="' . $name . '" value="' . $value . '" />';
        }

        private function multiselectBuild( $name, $value, $data ) {
            if ( is_array( $data ) && isSet( $data['data'] ) ) {
                $options = $data;
                $data = $data['data'];
                unset( $options['data'] );
            } else
                $options = array();

            $data = $this->parseSelectString( $data );
//            if(!is_array($data)){
//                    $data = explode(',', $data);
//                    $ndata = array();
//                    foreach($data as $i => $d){
//                        if(strpos($d,'|') !== false){
//                            $d = explode('|', $d,2);
//                            $ndata[$d[0]] = $d[1];
//                        }elseif($d != '') $ndata[$i] = $d;
//                    }
//                    $data = $ndata;
//            }
            if ( !is_array( $value ) ) {
                if ( empty( $value ) )
                    $value = array();
                elseif ( $value[0] == ',' && $value[count( $value ) - 1] == ',' )
                    $value = explode( ',', substr( $value, 1, -1 ) );
                else
                    $value = explode( ',', $value );
            }
            if ( !isAdmin() )
                $name .= '[]';
            $html = '<select name="' . $name . '" size="5" class="multiselect" multiple="multiple">';
            foreach ( $data as $id => $dat ) {
                $addclass = '';
                $addstyle = '';
                $add = (in_array( $id, $value )) ? (' selected="selected"') : ('');
                if ( is_array( $dat ) ) {
                    if ( isset( $dat['class'] ) )
                        $addclass = ' class="' . $dat['class'] . '"';
                    if ( isset( $dat['style'] ) )
                        $addstyle = ' style="' . $dat['style'] . '"';
                    $dat = $dat['val'];
                }
                $html .= '<option value="' . $id . '"' . $add . $addclass . $addstyle . '>' . ($dat) . '</option>';
            }
            $html .= '</select>';
            return $html;
        }

        private function multiselectFixedForDirectUsageBuild( $name, $value, $data ) {
            if ( is_array( $data ) && isSet( $data['data'] ) ) {
                $options = $data;
                $data = $data['data'];
                unset( $options['data'] );
            } else
                $options = array();

            $data = $this->parseSelectString( $data );
            if ( !is_array( $value ) ) {
                if ( empty( $value ) )
                    $value = array();
                elseif ( $value[0] == ',' && $value[count( $value ) - 1] == ',' )
                    $value = explode( ',', substr( $value, 1, -1 ) );
                else
                    $value = explode( ',', $value );
            }
            if ( !isAdmin() )
                $name .= '[]';
            $html = '<select name="' . $name . '[]" size="100" class="multiselect" multiple="multiple">';
            foreach ( $data as $id => $dat ) {
                $addclass = '';
                $addstyle = '';
                $add = (in_array( $id, $value )) ? (' selected="selected"') : ('');
                if ( is_array( $dat ) ) {
                    if ( isset( $dat['class'] ) )
                        $addclass = ' class="' . $dat['class'] . '"';
                    if ( isset( $dat['style'] ) )
                        $addstyle = ' style="' . $dat['style'] . '"';
                    $dat = $dat['val'];
                }
                $html .= '<option value="' . $id . '"' . $add . $addclass . $addstyle . '>' . ($dat) . '</option>';
            }
            $html .= '</select>';
            return $html;
        }

        private function multiselect2Build( $name, $value, $eig ) {
            $html = '';
            if ( is_array( $eig ) && isSet( $eig['data'] ) ) {
                $options = $eig;
                $eig = $eig['data'];
                unset( $options['data'] );
            } else
                $options = array();
            $txt_val = '';
            $butVal = '+';
            if ( $value == '0' )
                $value = '';
            elseif ( $value != '' ) {
                $spl = explode( ',', $value );
                $txt_val = '';
                foreach ( $spl as $s ) {
                    if ( isset( $eig[$s] ) && $eig[$s] != '' ) {
                        $txt_val .= (is_array( $eig[$s] )) ? ',' . $eig[$s]['val'] : ',' . $eig[$s];
                    }
                }
                $txt_val = substr( $txt_val, 1 );
                if ( is_array( $eig ) && count( $eig ) > 0 && in_array( current( $eig ), $spl ) )
                    $butVal = '-';
            }
//		$html .= '<input type="text" name="'.$name.'txt" disabled="disabled" value="'.$txt_val.'" class="multiSelect" />'; // disabled="disabled"
            if ( isSet( $options['tagBuild'] ) ) {
                $html .= '<div class="tags showOnJs" style="display:none">';
                $html .= '<input type="text" name="js' . $name . '" class="multiSelValue js" style="display:none" value="' . $value . '" />';
                foreach ( $eig as $i => $t ) {
                    if ( strpos( $value, ',' . $i . ',' ) !== false ) {
                        $active = ' active';
                        $style = '';
                    } else {
                        $active = '';
                        $style = ' style="text-decoration: line-through"';
                    }
                    $html .= '<a href="#" ' . $style . ' class="addremovethis' . $active . '" id="id' . $i . '">' . $t['val'] . '</a> ';
                }
                $selectData = array();
                foreach ( $eig as $i => $eig ) {
                    $selectData[',' . $i . ','] = $eig;
                }
                $html .= '</div>';
                $html .= '<div class="noJs">' . $this->selectBuild( $name . '', $value, array( '' => 'Alle' ) + $selectData ) . '</div>';
            } elseif ( isSet( $options['checkBuild'] ) ) {
                $html .= '<div class="tags showOnJs" style="display:none">';
                $html .= '<input type="text" name="js' . $name . '" class="multiSelValue js" style="display:none" value="' . $value . '" />';
                foreach ( $eig as $i => $t ) {
                    if ( strpos( $value, ',' . $i . ',' ) !== false ) {
                        $active = ' active';
                        $sel = ' checked="checked"';
                    } else {
                        $active = '';
                        $sel = '';
                    }
                    $html .= '<div class="checkbox"><input id="id' . $i . '" class="addremovethisC' . $active . '" type="checkbox"' . $sel . ' /><span class="checkDesc">' . $t['val'] . '</span></div>';
//				$html .= '<a href="#" '.$style.' class="addremovethis'.$active.'" id="id'.$i.'"><input type="checkbox" />'.$t['val'].'</a> ';
                }
                $selectData = array();
                foreach ( $eig as $i => $eig ) {
                    $selectData[',' . $i . ','] = $eig;
                }
                $html .= '</div>';
                $html .= '<div class="noJs">' . $this->selectBuild( $name . '', $value, array( '' => 'Alle' ) + $selectData ) . '</div>';
            } else {
                $escapedName = menuepunkte::setTitelIntern( $name );
                $html .= '<input type="text" name="' . $name . '" class="multiSelValue" value="' . $value . '" />';
                $html .= '<textarea name="' . $escapedName . 'txt" rows="5" cols="50" class="multiSelect multiSelVis">' . $txt_val . '</textarea>'; // disabled="disabled"
                $html .= $this->selectBuild( $escapedName . 'sel', '', $eig, 'multisel' );
                $html .= '<input type="button" class="addremove" name="' . $escapedName . 'but" value="' . $butVal . '" />';
            }
            return $html;
        }

        private function uploadBuild( $name, $value, $eig ) {
            $html = '<input type="file" name="' . $name . '" class="upload" value="" />';
            return $html;
        }

        private function tagMultiSelectBuild( $name, $value, $eig ) {
            $html = '';
            if ( $eig['maintag'] > 0 )
                addWhere( 'parent', '=', $eig['maintag'], 'i' );
            $tags = buildSelectArray( 'tags', array( 'title_intern', 'title' ), true );
            asort( $tags, SORT_STRING );
            foreach ( $tags as $t ) {
                $html .= '<a href="#" style="text-decoration: line-through">' . $t['val'] . '</a> ';
            }
            return $html;
        }

        private function tagsBuild( $name, $value, $eig ) {
            if ( isSet( $eig['maintag'] ) && $eig['maintag'] > 0 )
                addWhere( 'parent', '=', $eig['maintag'], 'i' );
            $tags = buildSelectArray( 'tags', array( 'title_intern', 'title' ), true );
//		asort($tags,SORT_STRING);
            $data = array();
            if ( isSet( $eig['tagBuild'] ) && $eig['tagBuild'] ) {
                $data['data'] = $tags;
                $data['tagBuild'] = true;
            } elseif ( isSet( $eig['checkBuild'] ) && $eig['checkBuild'] ) {
                $data['data'] = $tags;
                $data['checkBuild'] = true;
            } else {
                $data = $tags;
            }
            return $this->multiselectBuild( $name, $value, $data );
        }

        private function selectBuild( $name, $value, $data, $class = '' ) {

            if ( !is_array( $data ) ) {
                $data = explode( ',', $data );
                $ndata = array();
                foreach ( $data as $i => $d ) {
                    if ( $d != '' )
                        $ndata[$i] = $d;
                }
                $data = $ndata;
            }

            if ( count( $data ) > 15 ) {
                $class .= " autocomplete";
            }

            $html = '<select name="' . $name . '" class="' . $class . '">';

            foreach ( $data as $id => $dat ) {
                $addclass = '';
                $addstyle = '';
                $add = ((string) $value == (string) $id) ? (' selected="selected"') : ('');
                if ( is_array( $dat ) ) {
                    if ( isset( $dat['class'] ) )
                        $addclass = ' class="' . $dat['class'] . '"';
                    if ( isset( $dat['style'] ) )
                        $addstyle = ' style="' . $dat['style'] . '"';
                    $dat = $dat['val'];
                }
                $html .= '<option value="' . $id . '"' . $add . $addclass . $addstyle . '>' . ($dat) . '</option>';
            }
            $html .= '</select>';
            return $html;
        }

        private function selectTxtBuild( $name, $value, $data, $class = '' ) {
            if ( !is_array( $data ) ) {
                $data = explode( ',', $data );
                $ndata = array();
                foreach ( $data as $i => $d ) {
                    if ( $d != '' )
                        $ndata[$i] = $d;
                }
                $data = $ndata;
            }
            $newData = array();
            $el = $this->getElement( $name );
            $first = $el['pflicht'];
            foreach ( $data as $d ) {
                if ( $first ) {
                    $id = '0';
                    $first = false;
                } else
                    $id = $d;
                $newData[$id] = $d;
            }
            return $this->selectBuild( $name, $value, $newData, $class );
        }

        private function pageBuild( $name, $value, $data, $class = '' ) {

            mys::getObj()->select( "menuepunkte", "title,title_intern" );

            $newData = array();
            foreach ( mys::getObj()->getRows() as $row ) {
                $newData["/{$row["title_intern"]}.html"] = array( "val" => $row["title"] );
            }

            return $this->selectBuild( $name, $value, $newData, $class );
        }

        private function editorBuild( $name, $value, $data ) {
            $html = '<div style="width: 100%;"><textarea name="' . $name . '" class="editor">' . $value . '</textarea></div>';
            //$html .= '<script type="text/javascript">delete CKEDITOR.instances[\''.$name.'\'];CKEDITOR.replace(\''.$name.'\');</script>'; //delete CKEDITOR.instances[\''.$name.'\'];
            return $html;
        }

        private function bbCodeBuild( $name, $value, $data ) {
            $html = '<div style="width: 100%;"><textarea name="' . $name . '" class="editorBBC">' . $value . '</textarea></div>';
            //$html .= '<script type="text/javascript">delete CKEDITOR.instances[\''.$name.'\'];CKEDITOR.replace(\''.$name.'\');</script>'; //delete CKEDITOR.instances[\''.$name.'\'];
            return $html;
        }

        private function newsletterBuild( $name, $value, $data ) {
            $html = '<div style="width: 100%;"><textarea name="' . $name . '" class="editorNewsletter">' . $value . '</textarea></div>';
            //$html .= '<script type="text/javascript">delete CKEDITOR.instances[\''.$name.'\'];CKEDITOR.replace(\''.$name.'\');</script>'; //delete CKEDITOR.instances[\''.$name.'\'];
            return $html;
        }

        private function codeeditorBuild( $name, $value, $data, $placeHolder = null ) {
            $addclass = ' id="codeEditor"';
            $html = '<div><textarea  ' . $placeHolder . ' name="' . $name . '"' . $addclass . '">' . $value . '</textarea></div>';
            return $html;
        }

        private function textareaBuild( $name, $value, $data, $placeHolder = null ) {
            if ( isset( $data['allowtabs'] ) && $data['allowtabs'] )
                $addclass = ' class="allowtabs"';
            else
                $addclass = '';
            $placeHolder = (!empty( $placeHolder )) ? "placeholder=\"{$placeHolder}\"" : "";
            $html = '<div><textarea  ' . $placeHolder . ' name="' . $name . '"' . $addclass . '">' . $value . '</textarea></div>';
            return $html;
        }

        private function dateiBuild( $name, $value, $data ) {
            $add = (isSet( $data['allowFolder'] ) && $data['allowFolder'] == true) ? '&art=hybrid' : '';
            if ( isSet( $data['note'] ) && $data['note'] != '' )
                $add .= '&note=' . $data['note'];
            $dm_id = 'dm' . str_replace( array( '[', ']' ), '_', $name );
            $html = '<input type="text" class="dm_autocomplete" name="' . $name . '" id="' . $dm_id . '" value="' . $value . '" /><input type="button" onclick="openFileBrowser(\'' . $GLOBALS['cms_rootdir'] . 'admin/index.php?mm=Dateimanager&amp;retid=' . $dm_id . $add . '\')" value="Server durchsuchen">';
            return $html;
        }

        private function folderBuild( $name, $value, $data ) {
            $dm_id = 'dm' . str_replace( array( '[', ']' ), '_', $name );
            $add = '';
            if ( isSet( $data['note'] ) && $data['note'] != '' )
                $add = '&note=' . $data['note'];
            $html = '<input type="text" class="" name="' . $name . '" id="' . $dm_id . '" value="' . $value . '" /><input type="button" onclick="openFileBrowser(\'' . $GLOBALS['cms_rootdir'] . 'admin/index.php?mm=Dateimanager&amp;retid=' . $dm_id . '&art=folder' . $add . '\')" value="Server durchsuchen">';
            return $html;
        }

        private function bildBuild( $name, $value, $data ) {
            $add = '';
            $dm_id = 'dm' . str_replace( array( '[', ']' ), '_', $name );
            $add = (isSet( $data['allowFolder'] ) && $data['allowFolder'] != true) ? '&art=hybrid' : '';
            if ( isSet( $data['note'] ) && $data['note'] != '' )
                $add .= '&note=' . $data['note'];

            if ( isset( $data['maxwidth'] ) )
                $add .= '&amp;maxwidth=' . $data['maxwidth'];
            if ( isset( $data['maxheight'] ) )
                $add .= '&amp;maxheight=' . $data['maxheight'];
            if ( isset( $data['defaultpic'] ) && $data['defaultpic'] != '' && $value == '' ) {
                if ( $data['defaultpic'][count( $data['defaultpic'] ) - 1] == '/' ) {
                    $dm = new dateimanager();
                    $files = $dm->getdateien( $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $data['defaultpic'], false, 'images' ); //TODO: rekursiv !?
                    if ( count( $files ) > 0 ) {
                        mt_srand( (double) microtime() * 1000000 );
                        $rand = mt_rand( 0, count( $files ) - 1 );
                        $value = $data['defaultpic'] . $files[$rand]['name'];
                    }
                } else
                    $value = $data['defaultpic'];
            }
            $html = '<input type="text" class="dm_autocomplete" name="' . $name . '" id="' . $dm_id . '" value="' . $value . '" /><input type="button" onclick="openFileBrowser(\'' . $GLOBALS['cms_rootdir'] . 'admin/index.php?mm=Dateimanager&amp;retid=' . $dm_id . $add . '\')" value="Server durchsuchen">';
            return $html;
        }

        private function htmlBuild( $name, $value, $data ) {
            if ( is_array( $data ) && isSet( $data['html'] ) )
                return $data['html'];
            return $data;
        }

        private function formularBuild( $name, $value, formular $data ) {
//            new dBug($data->elements);
            if ( is_array( $value ) && count( $value ) > 0 ) {
                foreach ( $value as $name => $val ) {
                    if ( is_array( $val ) ) {
                        $newVal = array();
                        $x = 1;
                        foreach ( $val as $i => $v ) {
                            $newVal[$x] = $v;
                            $x++;
                        }
                        $value[$name] = $newVal;
                    }
                }
                $data->connectData( 0, false, $value );
            }
            return (string) $data->toTable();
        }

        private function simpleCheckBuild( $name, $value, $data ) {
            $checked = ($value == '1') ? ' checked="checked"' : '';
//		var_dump($data);
            $html = '<input type="checkbox" name="' . $name . '"' . $checked . ' class="checkbox" />' . $data . '<br />';
            return $html;
        }

        private function checkBuild( $name, $value, $data ) {
            if ( !is_array( $data ) && strpos( $data, ',' ) )
                $data = explode( ',', $data );
            elseif ( !is_array( $data ) )
                $data = array( $data );
            if ( !is_array( $value ) )
                $value = array( $value );
            $html = '';
            $i = 0;
            foreach ( $data as $i => $desc ) {
                if ( in_array( $desc, $value ) )
                    $check = ' checked="checked"';
                else
                    $check = '';
                $val = $i;
                $i++;
                $html .= '<div class="check"><input type="checkbox" class="checkbox" name="' . $name . '[]" value="' . $val . '"' . $check . ' /><span class="checkVal">' . $desc . '</span></div>';
            }
            return $html;
        }

        private function radioBuild( $name, $value, $data ) {
            if ( !is_array( $data ) && strpos( $data, ',' ) )
                $data = explode( ',', $data );
            elseif ( !is_array( $data ) )
                $data = array( $data );
            $html = '';
            foreach ( $data as $i => $desc ) {
                if ( $value == $desc )
                    $check = ' checked="checked"';
                else
                    $check = '';
                $html .= '<input id="'. md5($desc.$name) . '"class="radio" type="radio" name="' . $name . '" value="' . $desc . '"' . $check . ' /><label class="radio" for="'. md5($desc.$name) .'">' . $desc . '</label>';
            }
            return $html;
        }

        private function databaseBuild( $name, $value, $data ) {
            if ( isset( $data['selectName'] ) && $data['selectName'] ) {
                $newdata = array();
                foreach ( $data['dbArray'] as $i => $d )
                    $newdata[$d] = $d;
                $data['dbArray'] = $newdata;
            }
            if ( isset( $data['addAllDB'] ) && $data['addAllDB'] ) {
                $data['dbArray'] = array( '0' => '[Alle Datenbanken]' ) + $data['dbArray'];
            }
            $html = $this->selectBuild( $name, $value, $data['dbArray'] );
            $html .= ' <span class="link newdia db">Neue Datenbank anlegen</span>';
            return $html;
        }

        private function filterBuild( $name, $value, $data ) {
            if ( $value != '' && $value != '0' )
                $value = unserialize( $value );

            $html = '';
            $fieldnames = array( '' => '[Bitte auswählen]' );
            $vars = array();
            if ( isSet( $data['dbformname'] ) ) {
                $dbformname = $data['dbformname'];

                if ( isset( $_GET['change' . $dbformname] ) && (int) $_GET['change' . $dbformname] > 0 )
                    $dbid = (int) $_GET['change' . $dbformname];
                else
                    $dbid = $this->elements[$dbformname]['value'];
            }else {
                $dbid = $data['dbId'];
            }
            if ( $dbid > 0 )
                addWhere( 'id', '=', $dbid, 'i' );
            setLimit( 1 );
            $dbs = new DatabaseConnector( $dbid );
            $tmp_ = $dbs->getDatabases();
            $db = array_pop($tmp_);
            $fields = $dbs->getFields();
            foreach ( $fields as $f ) {
                $fieldnames[$f['name']] = array(
                    'val' => $f['name'],
                    'class' => $f['type']
                );
                if ( $f['type'] == 'select' ) {
                    if ( !in_array( $f['name'], $vars ) ) {
                        $html .= '<div id="var' . $f['name'] . '" class="var">' . $f['eig'] . '</div>';
                        $vars[] = $f['name'];
                    }
                }
            }

            $rel = array( '=' => 'gleich', '!=' => 'ungleich', '<' => 'vor', '>' => 'nach', 'enthaelt', 'enthaelt nicht', "in" => "in" );
            $data['form'] = new formular();
            $data['form']->addElement( 'Feld', 'fieldname', 'select', 0, $fieldnames );
            $data['form']->addElement( 'Relation', 'relation', 'select', '=', $rel );
            $data['form']->addElement( 'Wert', 'value', 'text', '' );

            foreach ( $fields as $f ) {
                if ( $f['type'] == 'tags' ) {
//					$tagsSys = new TagSystem();
                    if ( $f['eig']['maintag'] > 0 )
                        addWhere( 'parent', '=', $f['eig']['maintag'] );
//					$tgs = $tagsSys->getArrayList('title_intern,title');
                    $tags = array( '[TAG]' => '[Ausgewählter Tag]' ) + buildSelectArray( 'tags', array( 'title_intern', 'title' ), true );
//					var_dump($tgs);
//					$tags = array();
//					$tags['[TAG]'] = '[Ausgew&auml;hlter Tag]';
//					foreach($tgs as $t){
//						$tags[$t['title_intern']] = $t['title'];
//					}
                    $tagform = new formular();
                    $tagform->addElement( '', 'tagname[]', 'select', '0', $tags );

                    $data['form']->addOnChangeSetForm( 'fieldname', $f['name'], 'value', $tagform );
                }
                elseif ( $f['type'] == 'select' || $f['type'] == 'selectTxt' ) {
                    $eig = $f['eig'];

                    $selform = new formular();
                    $selform->addElement( '', 'tagname[]', $f['type'], '0', $eig );

                    $data['form']->addOnChangeSetForm( 'fieldname', $f['name'], 'value', $selform );
                }
            }
            $this->elements[$name]['data'] = $data;

            $html = $this->formularBuild( $name, $value, $data['form'] );
            return $html;
        }

        private function pwchangeBuild( $name, $value, $data ) {
            $html = '';
            if ( isSet( $data['type'] ) )
                $type = $data['type'];
            else
                $type = "text";
            if ( $this->connectid > 0 || !isSet( $data['noConf'] ) || !$data['noConf'] ) {
                $html .= '<input type="' . $type . '" name="' . $name . '" value="" />';
                $html .= '</div><div class="formcontent password"><span class="fieldname">Passwort wiederholen:</span>';
            }
            $html .= '<input type="' . $type . '" name="' . $name . '" value="" />';
            //$html .= '<input type="text" name="'.$name.'" value="'.$value.'" />';
            if ( $type == "text" )
                $html .= '<input type="button" onclick="setrandpass(\'' . $name . '\')" value="Zufallspasswort" />';
            return $html;
        }

        private function dbConBuild( $name, $value, $data ) {
            $html = $this->selectBuild( $name, $value, $data['selectData'] );
            if ( !isSet( $data['allowNew'] ) || $data['allowNew'] )
                $html .= '<span class="link newdia" id="' . $data['class'] . '">Neu</span>';
            return $html;
        }

        private function dataTemplateBuild( $name, $value, $data ) {
            if ( isSet( $data['dbformname'] ) ) {
                $dbformname = $data['dbformname'];
                if ( isset( $_GET['change' . $dbformname] ) && (int) $_GET['change' . $dbformname] > 0 )
                    $dbid = (int) $_GET['change' . $dbformname];
                else {
                    $dbid = key( $this->elements[$dbformname]['data']['dbArray'] );
                    $dbid = $this->elements[$dbformname]['value'];
                }
            }if ( isSet( $data['dbid'] ) )
                $dbid = $data['dbid'];
//		echo $dbid.'!!';
//		if($dbid > 0) addWhere('id', '=', $dbid,'i');
//		setLimit(1);
            $html = '';
            $tpl = new DataTemplate( $dbid );
            $data = $tpl->getData();
            $first = true;
            foreach ( $data as $d ) {
                if ( ($first && $value == '0') || $value == $d['id'] )
                    $addclass = ' selected';
                else
                    $addclass = '';

                if ( $first ) {
                    $first = false;
                    if ( $value == '0' )
                        $value = $d['id'];
                    $html .= '<input type="text" name="' . $name . '" id="' . $name . '" value="' . $value . '" />';
                }
                $html .= '<div class="tplsel' . $addclass . ' sel' . $name . '" onclick="$(\'#' . $name . '\').val(\'' . $d['id'] . '\');$(\'.selected.sel' . $name . '\').removeClass(\'selected\');$(this).addClass(\'selected\');">';
                $html .= $d['name'] . '<br />';
                $html .= $d['image'];
                $html .= '</div>';
            }
            if ( $first )
                $html .= '<input type="text" name="' . $name . '" id="' . $name . '" value="" />';
//		$html .= $tpl;
            $html .= ' <span class="link newdia tpl" id="d' . $dbid . '">Neues Template anlegen</span>';
            return $html;
        }

        private function dataRecordBuild( $name, $value, $data ) {
            $html = '';
            if ( isSet( $data['dbId'] ) ) {
                $dbid = $data['dbId'];
            } else {
                $dbformname = $data['dbformname'];
                if ( isset( $_GET['change' . $dbformname] ) && (int) $_GET['change' . $dbformname] > 0 )
                    $dbid = (int) $_GET['change' . $dbformname];
                else {
                    $dbid = key( $this->elements[$dbformname]['data']['dbArray'] );
                    $dbid = $this->elements[$dbformname]['value'];
                }
            }
            /*
             * TODO: Multiple FIELDIDS
             */
            if ( $dbid > 0 )
                addWhere( 'id', '=', $dbid, 'i' );
//		else addWhere('dbType', '=', '0','i');
            setLimit( 1 );
            select( 'databases', 'name,dbType,fieldid' );
            $dbRow = getRow();
//		if(isSet($data['valueAsText']) && $data['valueAsText'])
//			$defaultId = $dbRow['fieldid'];
//		else
            $defaultId = 'id';
            if ( isSet( $data['dbFields'] ) )
                $what = $data['dbFields'];
            else
                $what = array( $defaultId, $dbRow['fieldid'] );
            if ( $dbRow['dbType'] == 1 )
                $dbName = "user";
            elseif ( $dbRow['dbType'] == 2 )
                $dbName = "frontEndUser";
            else
                $dbName = 'data_' . $dbRow['name'];
            $dat = buildSelectArray( $dbName, $what );
            if ( isSet( $data['addEmpty'] ) && !empty( $data['addEmpty'] ) ) {
                $dat = array( '' => $data['addEmpty'] ) + $dat;
            }

            if ( (!isSet( $data['allowNew'] ) || $data['allowNew']) && isAdmin() )
                $html .= ' <span class="link newdia dataRecord" id="d' . $dbid . '">Neuen Datensatz anlegen</span>';

            if ( isSet( $data['valueAsText'] ) && $data['valueAsText'] ) {
                if ( $value != '' && $value > 0 ) {
                    addWhere( 'id', '=', $value );
                    setLimit( 1 );
                    select( $dbName, $dbRow['fieldid'] );
                    $row = getRow();
                    $value = $row[$dbRow['fieldid']];
                }
                return $this->selectTxtBuild( $name, $value, $dat ) . $html;
            } else
                return $this->selectBuild( $name, $value, $dat ) . $html;
        }

        private function dbFieldBuild( $name, $value, $data ) {
            if ( isSet( $data['dbId'] ) )
                $dbid = $data['dbId'];
            else {
                $dbformname = $data['dbformname'];
                if ( isset( $_GET['change' . $dbformname] ) && (int) $_GET['change' . $dbformname] > 0 )
                    $dbid = (int) $_GET['change' . $dbformname];
                else {
                    $dbid = key( $this->elements[$dbformname]['data']['dbArray'] );
                    $dbid = $this->elements[$dbformname]['value'];
                }
            }
            $html = '';
            addWhere( "id", "=", $dbid );
            setLimit( 1 );
            select( 'databases', 'fields' );
            $row = getRow();
            $fields = unserialize( $row['fields'] );
            $dataFields = array(
                'id' => 'id',
                'erstellt' => 'erstellt',
                'published' => 'Veröffentlicht'
            );
            foreach ( $fields['fieldname'] as $field ) {
                $dataFields[$field] = $field;
            }
            return $this->selectBuild( $name, $value, $dataFields );
        }

        private function gridBuild( $name, $value, $data ) {
            $html = '';
            $html .= '<div class="gridBuild" id="' . $name . '">';
            $html .= '<span class="newGridEl">Neues Element</span>';
            $html .= '<div class="a4">';
            if ( is_array( $value ) ) {
                foreach ( $value as $gridEl ) {
                    $html .= '<div class="gridElement" style="width:' . $gridEl['width'] . 'px;height:' . $gridEl['height'] . 'px;top:' . $gridEl['top'] . ';left:' . $gridEl['left'] . '">';
                    $html .= '<div class="content">';
                    $html .= $gridEl['data'];
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="gridBuildstatic"></div>';
            return $html;
        }

        private function buttonBuild( $name, $value, $data ) {
            $onclick = (isSet( $data['onclick'] )) ? ' onclick="' . $data['onclick'] . '"' : '';
            $dataId = (isSet( $data['id'] )) ? ' id="' . $data['id'] . '"' : '';

            return '<input type="button" name="' . $name . '" value="' . $value . '"' . $onclick . $dataId . ' />';
        }

        public function setDoBackup( $doBackup ) {
            $this->doBackup = $doBackup;
        }

        public function isEdit() {
            return ($this->connectid > 0 );
        }

        public function setSucessMessage( $sucessMessage ) {
            $this->sucessMessage = $sucessMessage;
        }

        public function getFormId() {
            return $this->formid;
        }

        public function getPosted() {
            return $this->posted;
        }

        public function setPosted( $posted ) {
            $this->posted = $posted;
        }

        public function getDataid() {
            return $this->dataid;
        }

        private function parseSelectString( $val ) {
            if ( !is_array( $val ) ) {
                $data = explode( ',', $val );
                $ndata = array();
                foreach ( $data as $i => $d ) {
                    if ( strpos( $d, '|' ) !== false ) {
                        $d = explode( '|', $d, 2 );
                        $ndata[$d[0]] = $d[1];
                    } elseif ( $d != '' )
                        $ndata[$i] = $d;
                }
                return $ndata;
            }else {
                $nData = array();
                foreach ( $val as $i => $d ) {
                    if ( is_array( $d ) )
                        return $val;
                    if ( strpos( $d, '|' ) !== false ) {
                        $d = explode( '|', $d, 2 );
                        $nData[$d[0]] = $d[1];
                    } else
                        $nData[$i] = $d;
                }
                return $nData;
            }
            return $val;
        }

    }
