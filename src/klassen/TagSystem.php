<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TagSystem
 *
 * @author F.Ehrlich
 */
class TagSystem {

	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;

	public function __construct() {
		$this->dbc = new DataEditor('tags');



		$seiten = array('' => '[Standard]') + buildSelectArray("menuepunkte", array('title_intern', 'title'), true);
		$list = buildSelectArray('tags', array('id', 'title'), true);
		$list = array(-1 => '[Nichts]') + $list;

		$this->dbc->setAddLevel();

		$this->dbc->form->addElement('Tag Name', 'title');

		$this->dbc->form->addElement('Sichbarkeit', 'visible', 'select', 1, array('unsichtbar', 'normal'));
		$this->dbc->form->addElement('Unterordnen', 'parent', 'select', -1, $list);
		$this->dbc->form->addElement('Aktuelle Priori&auml;t', 'prio', 'text');
		$this->dbc->form->addElement('Anzeigen in', 'showin', 'select', '', $seiten);
		$this->dbc->form->addElement('Anzeige vererben', 'vererbe', 'check', '', array('Anzeige vererben (nur n&auml;chste Ebene!)'), false);
//		$this->dbc->form->removeElement('mainid');
		//$this->dbc->form->removeElement('visible');
		$this->dbc->form->useTab('Admin');
		$this->dbc->form->addElement('Level', 'level', 'text');
		$this->dbc->form->addElement('interner Titel', 'title_intern', 'text');
		$this->dbc->form->addElement('Order Nr', 'ordernr', 'text');

//		$this->dbc->form->removeElement('level');
//		$this->dbc->form->removeElement('title_intern');
//		$this->dbc->form->removeElement('ordernr');
//		$this->dbc->form->removeElement('showin');
		$this->dbc->setInvisible('level');
		$this->dbc->setInvisible('title_intern');
		$this->dbc->setInvisible('ordernr');
		$this->dbc->setInvisible('showin');
		$this->dbc->setOrderBy("ordernr");

		$this->dbc->AddQuickAdd();

		if (count($this->dbc->quickAddData) > 0) {
			$lines = $this->dbc->quickAddData;
//			var_dump($lines);
			setLimit(1);
			select('tags', 'ordernr', 'ordernr DESC');
			$row = getRow();
			$anordnung = $row['ordernr'] + 1;
			$insert = array();
			$mainids = array(0);
			$insert['level'] = 0;
			foreach ($lines as $line) {
				$insert['title'] = trim($line);
				$insert['ordernr'] = $anordnung;
				$level = 0;
				$next = $line[0];
				while ($next == "\t") {
					$level++;
					$next = $line[$level];
				}
				if ($insert['level'] < $level)
					$mainids[] = $lastinsert;
				elseif ($insert['level'] > $level) {
					$anz = $insert['level'] - $level;
					for ($i = 0; $i < $anz; $i++)
						array_pop($mainids);
				}
				$insert['parent'] = $mainids[count($mainids) - 1];
				$insert['level'] = $level;
				$insert['erstellt'] = time();

				if ($level > 0) {
					addWhere('id', '=', $insert['parent']);
					select('tags', 'title');
					$row = getRow();
					$altTitle = $row['title'] . '_' . $insert['title'];
				}else
					$altTitle = '';
				$insert['title_intern'] = menuepunkte::setTitelIntern($insert['title'], 'tags', 0, $altTitle);


				$insert['prio'] = 5;
				$insert['visible'] = 1;

				$parastr = 'ssiiisii';
				$anordnung++;
				$lastinsert = insertArray('tags', $insert, $parastr);
			}
		}elseif($this->dbc->form->posted ) {
				$altTitle = '';
				if ($_POST['parent'] > 0) {
					addWhere('id', '=', $_POST['parent'], 'i');
					select('tags', 'title_intern');
					$row = getRow();
					if ($row)
						$altTitle = $row['title_intern'] . '_' . $_POST['title'];
				}
//				var_dump($_POST);
//				var_dump($_GET);
				$id = (isSet($_GET['edit']) && $_GET['edit'] > 0)?$_GET['edit']:0;
				
				$_POST['title_intern'] = menuepunkte::setTitelIntern($_POST['title'], 'tags', $id, $altTitle);
			}
//		$this->dbc
	}

	public function __toString() {
		return (string) $this->dbc;
	}

}

?>