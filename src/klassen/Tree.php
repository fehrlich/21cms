<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tree
 *
 * @author F.Ehrlich
 */
class Tree {
	public $parent;
	public $content = '';
	private $class = '';
	public $Nodes = array();
	private $id;
	private $LinkNodes = array();
	private $activeId = 0;
	private $isActive = false;
	private $seperator = false;
	private $start = true;
	private $firstNode = true;

	function __construct($content = "", $parent = -1, $id = 0, $class = '') {
		$this->content = $content;
		$this->parent = $parent;
		$this->id = $id;
		$this->class = $class;
	}

	public function setSeperator($seperator) {
		if($seperator === false) $this->seperator = false;
		else{
			if(!is_array($seperator)){
				if (strpos($seperator, ',') !== false)
					$seperator = explode(',', ','.$seperator);
				else
					$seperator = array($seperator);
			}
			$this->seperator = $seperator;
		}
	}

	public function setStart($start) {
		$this->start = $start;
	}

	/**
	 * Set the Active Id to set class
	 * @param mixed $id Sets Active Id
	 */
	function setactiveId($id) {
		$this->activeId = $id;
	}

	function getList() {
		foreach ($this->LinkNodes as $n) {
			echo 'id:' . $n->id . ' content:' . $n->content . ' parent:' . $n->parent . "<br/>\n";
		}
	}

	/**
	 * Gets Node From id
	 * @param int $id
	 */
	function getNode($id) {
		if (isSet($this->LinkNodes[$id]))
			return $this->LinkNodes[$id];
		else
			return false;
	}

	/**
	 * Gets Node From content
	 * @param str $content
         * @return Tree
	 */
	function getNodeByContent($content) {
		foreach ($this->LinkNodes as $n) {
                    if(strpos($n->content, $content) !== false)
                       return $n;
                }
                return null;
	}

	/**
	 * Adds an Node to the tree
	 * @param int $id ID of node
	 * @param content $content content which will be cast as string
	 * @param mixed $parent ID of parent node or -1
	 */
	function addNode($id, $content, $parent = -1) {
		$newNode = new Tree($content, $parent, $id);
		$newNode->setStart($this->firstNode);
		$this->firstNode = false;
		$this->LinkNodes[$id] = &$newNode;
		$newNode->setactiveId($this->activeId);
		if (!is_int($parent) || $parent == -1 || $parent == '' || $parent == $this->id || !isset($this->LinkNodes[$parent])) {
			$this->Nodes[$id] = $newNode;
		} else {
			$this->LinkNodes[$parent]->Nodes[] = $newNode;
		}
	}

	function delNode($delId) {
		$array = array();
		if ($this->content != '') {
			// if ($this->id == $delId)
			// 	unset($this);
			return true;
		}
		if (count($this->Nodes) > 0) {
			foreach ($this->Nodes as $id => $node) {
				$node->delNode($delId);
			}
		}
		return false;
	}

	function getSingleItem() {
		return $this->content;
	}

	function getHtml() {
                $oId = 0;
		$ids = $this->activeId;
		if (!is_array($ids))
			$ids = array($ids);
		foreach ($ids as $id) {
			while (isSet($this->LinkNodes[$id]) && $oId != $id) {
                                $oId = $id;
				$this->LinkNodes[$id]->isActive = true;
				$id = $this->LinkNodes[$id]->parent;
			}
		}
		$str = '';
		if ($this->content != '') {
			if ($this->seperator && !$this->start) {
				$str .= '<li class="split">' . current($this->seperator) . '</li>';
			}

            $class = "";

            if ($this->isActive) {
				$class .= 'active';
            }

            if ( count($this->Nodes) > 0) {
                $class .= " hasSubmenu";
            }

            if ( !empty( $class )) {
                $class = ' class="' . $class . '"';
            }

			$str .= '<li id="node' . $this->id . '"' . $class . '>' . $this->content;
		}
		$shift = false;

		if (count($this->Nodes) > 0) {
			$str .= '<ul' . (($this->class != '') ? ' class="' . $this->class . '"' : '') . '>';
			$first = true;
			foreach ($this->Nodes as $id => $node) {
				if($this->seperator !== false && !$shift && count($this->seperator) > 1){
					$shift = true;
					array_shift($this->seperator);
				}
				$node->setSeperator($this->seperator);
				$node->setStart($first);
				$str .= $node;
				$first = false;
			}
			$str .= '</ul>';
		}
		if ($this->content != '')
			$str .= '</li>';
		return $str;
	}

	/**
	 *
	 * @return string Liste im Htmlformat
	 */
	function __toString() {
		return $this->getHtml();
	}

	function getArray() {
		$array = array();
		if ($this->content != '') {
			$array[$this->id] = $this;
		}
		if (count($this->Nodes) > 0) {
			foreach ($this->Nodes as $id => $node) {
				$array += $node->getArray();
			}
		}
		return $array;
	}

	public function getId() {
		return $this->id;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getContent() {
		return $this->content;
	}

	public function getClass() {
		return $this->class;
	}
}
?>
