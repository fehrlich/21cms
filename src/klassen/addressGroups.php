<?php

class addressGroups {

    public function __toString() {

        $this->dbsys = new \DataEditor('addressGroups');

        $this->dbsys->form->setFormAction('ajax.php?kl=addressGroups' . buildGet('edit,new'));
        $this->dbsys->form->addElement('name', 'name', 'text');
        $this->dbsys->form->addElement('Kommentar', 'Kommentar', 'text');

        $addDbc = new \DatabaseConnector();
        addWhere('dbType', '=', '1');
        $addForm = $addDbc->getForm();
        if ($addForm) {
            $addElements = $addForm->form->getElements();
            $dontAdd = array('id', 'public', 'erstellt', 'published', 'autor', 'title_intern');
            foreach ($addElements as $el) {
                if (!\in_array($el['name'], $dontAdd))
                    $this->dbsys->form->addElementArrays($el);
            }
        }

        $this->dbsys->enableDot();

        return $this->dbsys->__toString();
    }

}

?>
