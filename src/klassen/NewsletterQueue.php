<?php

    class NewsletterQueue {

        public function __toString() {

            $this->dbsys = new \DataEditor('newsletterQueue');

            $this->dbsys->form->addElement('empfaenger', 'from','text');
            $this->dbsys->form->addElement('empfaenger', 'subject','text');
            $this->dbsys->form->addElement('empfaenger', 'to','text');
            $this->dbsys->form->addElement('jobHash', 'jobHash', 'text');

            return $this->dbsys->__toString(); 
        }
    }