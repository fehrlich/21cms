<?php

/**
 * Description of PageRights
 *
 * @author Franz Ehrlich <Franz.Ehrlich@21advertise.de>
 */
class PageRights {

	static private $handle = null;
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;
	private $allowedPages = false;


	public function __construct() {

		PageRights::$handle = $this;
		$this->dbc = new DataEditor('contentRights');

		$groups = buildSelectArray("userGroups", array('id', 'name'));
		$pages = buildSelectArray("menuepunkte", array('id', 'title'),true);

		$yesNo = array(0 => "Nicht erlaubt", 1 => "Erlaubt");
		$this->dbc->form->addElement('Anwenden auf', 'groups', 'multiselect', '', $groups);
		$this->dbc->form->addElement('Darf nur folgende Seiten bearbeiten', 'pages', 'multiselect', '', $pages);
		$this->dbc->form->addElement('Darf neue Seiten anlegen', 'createPages', 'select', '', $yesNo);
		$this->dbc->form->addElement('Darf alle Seiten bearbeiten', 'editAll', 'select', '', $yesNo);
	}

	/**
	 * static function for retriving the MessageSystem handle
	 * @return PageRights  retrive PageRights objecthandle
	 */
	static public function getObj() {

		if (PageRights::$handle === null) {
			new PageRights();
		}

		return PageRights::$handle;
	}

	public function __toString() {
		return (string) $this->dbc;
	}

	public function isAllowed($seitenid = 0) {
		if ($seitenid == 0 && isSet($GLOBALS['seitenid']))
			$seitenid = $GLOBALS['seitenid'];
		if ($this->allowedPages === false) {
			$this->allowedPages = array();
			addWhere('groups', 'LIKE', '%,' . \cms\session::getObj()->getGroupId() . ',%');
			select('contentRights', 'pages,editAll');
			$this->allowedPages = getRows();
		}

		if (count($this->allowedPages) == 0)
			return true;
		else {
			foreach ($this->allowedPages as $row) {

				if (isset($row["editAll"]) && $row["editAll"] == 1) {
					return true;
				}
				if (isset($row["pages"]) && (strpos($row['pages'], ',' . $seitenid . ',') !== false)) {
					return true;
				}
			}
		}

		return false;
	}

	public function canCreatePages() {
		addWhere('groups', 'LIKE', '%,' . \cms\session::getObj()->getGroupId() . ',%');
		select('contentRights', 'createPages');
		$rows = getRows();

		if (count($rows) == 0) {
			return false;
		} else {
			return $rows[0]["createPages"];
		}
	}

}

?>
