<?php
    class sendNewsletter  {

        private $form = null;
        private $recipients = array();
        private $frontEndUser = false;
        private $groups = false;
        private $sendCount = 0;


        public function __construct() {

            ignore_user_abort(true);
            set_time_limit(0);

            $groups = buildSelectArray("addressGroups", array('id','name'));
            $groups[] = "FrontEndUser";
            $user = buildSelectArray("addressUser", array('id','email'));

            natcasesort($user);
            natcasesort($groups);

            select("newsletterTemplates","id,title,datum","id desc");
            $templates = array();
            while($row = getRow()) $templates[$row["id"]] = date("d.m.y",$row["datum"]) ." - ". $row["title"];
            $this->form = new formular('',$GLOBALS['cms_roothtml'].'admin/index.php?mm=sendNewsletter&send=true');

            $this->form->addElement('Template', 'template','select','0',$templates);
            $this->form->addElement('Gruppen', 'groups','multiselectFixedForDirectUsage','',$groups);
            $this->form->addElement('Als gesendet markieren', 'marksended',  FormType::SIMPLECHECKBOS,'',array(),false);

            $this->form->setSaveButton("Abschicken", true);
        }

        public function __toString() {

            $html = "<div class='newsletter'>";

            if (isset($_GET["send"])) {

                $this->send();

				if(isSet($_POST['marksended'])){
					addWhere('id', '=', $_POST['template']);
					updateArray('newsletterTemplates', array(
						'sent' => time()
					),'i');
				}

                cms\session::getObj()->set("sendCount", $this->sendCount);
                return header("Location: /admin/index.php?mm=sendNewsletter&result=true");
            }

            if ( isset($_GET["result"])) {
                return cms\session::getObj()->get("sendCount"). " Empfänger wurden zur Mailqueue hinzugefügt.";
            }

            $html .= $this->form->__toString();

            $html .= "</div>";

            return $html;
        }

        public function send() {

            $l = new Logger();
            $l->debug("starting newsletter...");

            $jobHash = md5(microtime(1));

            $groups_data = $this->form->getElement("groups");

            if ( empty($groups_data["value"]) ) {
                return 0;
            }

            // reading template and from mails
            $tplId = $this->form->getElement("template");
            $tplId = $tplId["value"];
            addWhere("id", "=", $tplId);
            select("newsletterTemplates", "title,html,datum,author,fromMail");
            $template = getRow();
            mys::getObj()->cleanup()->clearWhere();

            if (empty($template["fromMail"])) {
                $FromMail = UserConfig::getObj()->getContactMail();

                if (empty($FromMail)) {
                    $FromMail = \cms\session::getObj()->getFormatedEmail();
                }

            } else {
                $FromMail = $template["fromMail"];
            }


            // reading users and groups
            foreach ( $groups_data["value"] as $value ) {
                $l->debug("fetch for group id {$value}");
                addWhere('groups', 'LIKE', "%,{$value},%", "s","OR");
            }

            select('addressUser', "email,token,authenticated");

            foreach( getRows() as $user ) {

                if ( $user["authenticated"] == 0 ) {
                    continue;
                }

                ++$this->sendCount;

                $tplId = $this->form->getElement("template");
                $tplId = $tplId["value"];

                $l->debug("user {email} added to mailqueue",$user);

                insertArray("newsletterQueue", array(
                    "jobhash" => $jobHash,
                    "from" => $FromMail,
                    "to" => $user["email"],
                    "subject" => $template["title"],
                    "content" => str_replace("[token]",$user["token"],$template["html"]),
                ), "sssss");
            }

            $l->debug("added {$this->sendCount} to mailqueue");
        }

//        public function send() {
//
//            $groups_data = $this->form->getElement("groups");
//
//            if ( empty($groups_data["value"] )) {
//                return;
//            }
//
//            foreach ( $groups_data["value"] as $value ) {
//                addWhere('groups', 'LIKE', "%,{$value},%", "s", "OR");
//            }
//
//            select('addressUser', "email");
//            while ( $mail = getRow() ) {
//                $this->recipients[] = $mail["email"];
//            }
//
//            $this->recipients = array_unique($this->recipients);
//
//            mys::getObj()->cleanup()->clearWhere();
//

//
//            foreach ( $this->recipients as $email ) {
//                $this->sendCount++;
//                $mail = new mimeMail();
//                $mail->setFromComplete($FromMail);
//                $mail->setSubject($template["title"]);
//                $mail->addRecipient($email);
//                $mail->setBody($template["html"]);
//                $mail->send();
//                unset($mail);
//            }:
//        }
    }
