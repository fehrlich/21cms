<?php
/**
 * Description of frontendSession
 *
 * @author F.Ehrlich
 */
class frontendSession {
	static private $handle = null;

	private $hash = null;
	private $userId = 0;
	private $data = array();
	private $timeout = 86400;
	private $username = '';
	private $userEmail = '';
	private $groupId = 0;

	public function __construct() {
		frontendSession::$handle = $this;
		$this->checkSession();
	}
	public function getUserId() {
		return $this->userId;
	}
	public function getGroupId() {
		if ($this->groupId != 0) {
			return $this->groupId;
		}
		$res = mys::getObj()->cleanup()->query("select `group` from frontEndUser where id = {$this->userId} LIMIT 1");
		$data = $res->fetch_array();
		$res->close();
		return $this->groupId = $data["group"];
	}
	public function getUserEmail() {
		if ($this->userEmail !== '') {
			return $this->userEmail;
		}

		$res = mys::getObj()->cleanup()->query("select email from frontEndUser where id = {$this->userId} LIMIT 1");
		$data = $res->fetch_array();
		$res->close();
		return $this->userEmail = $data["email"];
	}

	public function getUserName() {
		if ($this->username !== '') {
			return $this->username;
		}

		$res = mys::getObj()->cleanup()->query("select loginname from frontEndUser where id = {$this->userId} LIMIT 1");
		$data = $res->fetch_array();
		$res->close();
		return $this->username = $data["loginname"];
	}
	/**
	 * static function for retriving the session handle
	 * @return frontendSession  retrive session objecthandle
	 */

	static public function getObj() {

		if (frontendSession::$handle === null) {
			new frontendSession();
		}

		return frontendSession::$handle;
	}

	private function checkSession() {
		if (!isset($_COOKIE["fid"]) && !isset($_POST['fid'])) {
			return $this->startSession();
		}
		
		$this->hash = (isSet($_POST['fid']))?$_POST['fid']:$_COOKIE["fid"];
		$this->resumeSession();
	}

	public function destroySession() {
		setcookie("fid",null,time() - 60*60*24*365*10);

		addWhere("session_id", "=", $this->hash,"s");
		delete("sessions");
	}

	/**
	 * starts new session
	 * @return bool successfully or not
	 */

	private function startSession() {

		$this->hash = sha1(microtime() + rand(0,10000));
		$this->time = time();
		$this->expire = $this->time + $this->timeout;

		if (!setcookie("fid", $this->hash, time() + $this->expire, "/")) {
			return false;
		}


		insertArray("sessions", array(
				"logintime" => time(),
				"session_id" => $this->hash,
				"timeout" => (time() +60*60*24),
				"useragent" => $_SERVER["HTTP_USER_AGENT"],
				"ip" => $_SERVER["REMOTE_ADDR"],
				"hostname" => gethostbyaddr($_SERVER["REMOTE_ADDR"])
				),"isisss");


		return true;
	}

	private function resumeSession() {

		if (strlen($this->hash) != 40) {
			return $this->startSession();
		}

		$this->hash = $this->hash;
		addWhere("session_id", "=", $this->hash);
		select("sessions", "*");

		$data = getRow();
//		var_dump($data);


		if (empty($data)) {
			return $this->startSession();
		}

		if ($data["timeout"] < time())
			return $this->startSession();

		$this->data = unserialize($data["vars"]);
		$this->hash = $data["session_id"];
		$this->userId = $data["user_id"];
		$this->expires = $data["timeout"];

		return true;
	}
	public function isLoggedIn() {
		if ($this->userId != 0) return true;
		return false;
	}
	public function setUserId($id) {
		if (!is_int($id)) return false;

		$this->userId = $id;

		addWhere("session_id", "=", $this->hash);
		updateArray("sessions",array("user_id" => $id),"i");

		return true;
	}

	public function getUserRow(){
            $res = \mys::getObj()->cleanup()->query("select * from frontEndUser where id = {$this->userId} LIMIT 1");
            $data = $res->fetch_assoc();
            $res->close();
            return $data;
	}
	
	public function getSessionId() {
		return $this->hash;
	}
        
        public function getData() {
            return $this->data;
        }
        public function setData($name, $value){
            $this->data[$name] = $value;
            addWhere('session_id', '=', $this->hash);
            updateArray('sessions', array(
                'vars' => serialize($this->data)
            ),'s');
        }
        public function getDataVal($name){
            if(!isSet($this->data[$name])) return '';
            return $this->data[$name];
        }
}
?>
