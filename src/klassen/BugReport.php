<?php
class ticket{
	public $type = 'defect';
	public $priority = 'major';
	public $component = 'backend';
	public $milestone = '';
	public $owner = '';
	public $cc = '';
	public $reporter = '';
}
/**
 * Description of FrontEndUser
 *
 * @author Franz
 */
class BugReport {

	/**
	 * Formula for Bug reporting
	 * @var formular
	 */
	private $form;

	public function __construct() {

		$this->form = new formular('',$GLOBALS['cms_roothtml'].'admin/index.php?mm=BugReport&new=1');

		$this->form->addElement('Fehler Kurzbeschreibung', 'title', 'text');
		$this->form->addElement('Wie kam es zu dem Fehler ?', 'error1', 'textarea');
		$this->form->addElement('Wie wirkt sich der Fehler aus ?', 'error2', 'textarea');
		$this->form->addElement('Gibt es eine Fehlermeldung ?', 'error3', 'textarea');
		$this->form->addElement('Zus&auml;tzlicher Kommentar', 'error4', 'textarea');
		$this->form->addElement('Aufllösung', 'res', 'html', '', "<input type=\"text\" id=\"setRes\" name=\"res\" />");
		$this->form->addElement('Browser', 'browser', 'html', '', "<input type=\"text\" id=\"setBrowser\" name=\"browser\" />");
		$this->form->addElement('Flash', 'flash', 'html', '', "<input type=\"text\" id=\"setFlash\" name=\"flash\" />");
		$this->form->setSaveButton("Abschicken", true);
	}

	private function extendDescritpion(){
		$description = "'''".'Informationen: '."'''\n\n";
		$version = 'unknown';

		if(file_exists($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].'upgrade/version'))
			$version = file_get_contents($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].'upgrade/version');

		$description .= 'Angemeldeter User:' . \cms\session::getObj()->getUserName() . "\n";
		$description .= 'CMS Version:' . $version . "\n";
		$description .= 'User Agent:' . $_SERVER['HTTP_USER_AGENT'] . "\n";
		$description .= 'JS Browsre:' . ($_POST['browser']) . "\n";
		$description .= 'REQUEST_URI:' . ((isSet($_SERVER['REQUEST_URI']))?$_SERVER['REQUEST_URI']:'') . "\n";
		$description .= 'HTTP_REFERER:' . ((isSet($_SERVER['HTTP_REFERER']))?$_SERVER['HTTP_REFERER']:'') . "\n";
		$description .= 'Max. Auflösung:' . ($_POST['res']) . "\n";
		$description .= 'Hat Flash:' . ($_POST['flash']) . "\n\n";

		$description = ($description);
		return $description;
	}

	private function postBugreportToTrac($user,$pw,$description,$title,$opt){
		$xmlrpc_client = new xmlrpc_client('/login/xmlrpc', 'ssl://svn.21cms.de', 443);
		$xmlrpc_client->setCredentials($user, $pw);
		$xmlrpc_client->setSSLVerifyHost(0);
		$xmlrpc_client->setSSLVerifyPeer(false);
		$xmlrpc_client->setAcceptedCompression("deflate");


		$xmlrpc_msg = new xmlrpcmsg('ticket.create', array(
					new xmlrpcval(utf8_decode($title), 'string'),
					new xmlrpcval(utf8_decode($description), 'string'),
					new xmlrpcval($opt, 'struct'),
					new xmlrpcval(true, 'boolean')
				));
		$xmlrpc_resp = $xmlrpc_client->send($xmlrpc_msg);

		if ($xmlrpc_resp == False)
			die('error message');

		if (!$xmlrpc_resp->faultCode()) {
	//				$msg .= 'Danke f&uuml;r ihren Fehlerreport';
		}
	}

	public function codeBlock($txt){
		return "\n{{{\n".$txt."\n}}}\n";
	}

	public function __toString() {
		$user = 'Kunde';
		$pw = 'xofibu88';
		$milestone = $GLOBALS['trac_milestone'];
		$msg = '';
		$description = '';
		include('../klassen/xmlrpc/xmlrpc.inc');

		if(isSet($_POST['errormsg'])){
			$description .= "'''".'User Message: '."'''".$this->codeBlock($_POST['usermsg']);
			$description .= "\n\n";
			$description .= "'''".'Error: '."'''".$this->codeBlock($_POST['errormsg']);
			$description .= "\n\n";
			$description .= $this->codeBlock($this->extendDescritpion());
			$title = ('Automatischer Bugreport [Bitte ändern]');
			$opt = array(
				'milestone' => $milestone,
				$milestone => $milestone,
				'type' => 'defect',
				'defect' => 'defect',
				'priority' => 'major',
				'major' => 'major',
				'component' => 'backend',
				'backend' => '',
				'reporter' => $user,
				$user => $user,
				'owner' => '',
				'' => ''
			);
			if($milestone != '')
				$this->postBugreportToTrac($user,$pw,$description,$title,$opt);
		   else {
			   $mail = new mimeMail();
				$mail->setFrom('info@21cms.de', "Das CMS");
				$mail->setSubject("CMS Fehlerreport von ".$_SERVER['HTTP_HOST']);
				$mail->addRecipient('franz.ehrlich@googlemail.com');
				$mail->addRecipient('masternobi@aol.com');
				$mail->setBody($description);
				$mail->send();
		   }
		   return 'Ok';
		}
		elseif (isSet($_GET['new']) && $_GET['new'] == '1') {
			if ($this->form->posted) {
				$description = '\'\'\'Wie kam es zu dem Fehler ?\'\'\'' . "\n\n";
				$description .= $this->codeBlock( $_POST['error1']) . "\n\n";
				$description .= '\'\'\'Wie wirkt sich der Fehler aus ?\'\'\'' . "\n\n";
				$description .= $this->codeBlock( $_POST['error2']) . "\n\n";
				$description .= '\'\'\'Gibt es eine Fehlermeldung ?\'\'\'' . "\n\n";
				$description .= $this->codeBlock( $_POST['error3']) . "\n\n";
				$description .= '\'\'\'Zusätzlicher Kommentar\'\'\'' . "\n\n";
				$description .= $this->codeBlock( $_POST['error4']) . "\n\n";
				$description .= '\'\'\'System Informationen:\'\'\'' . "\n\n";

				$description =  $description;
				$description .= $this->codeBlock($this->extendDescritpion());
				
				$msg = 'Vielen Dank f&uuml;r ihre Fehlermeldung.';

				$opt = array(
					'milestone' => $milestone,
					$milestone => $milestone,
					'type' => 'defect',
					'defect' => 'defect',
					'priority' => 'major',
					'major' => 'major',
					'component' => 'backend',
					'backend' => '',
					'reporter' => $user,
					$user => $user,
					'owner' => '',
					'' => ''
				);
				
				$title = (strip_tags($_POST['title']));

				$this->postBugreportToTrac($user,$pw,$description,$title,$opt);
			}
			$form = (string) $this->form;
			if ($form == 'Ok')
				$form = '';
			return $msg . $form;
		}else {
			$msg .= '<a href="'.$GLOBALS['cms_roothtml'].'admin/index.php?mm=BugReport&new=1">Fehler melden</a>';
			$xmlrpc_client = new xmlrpc_client('/login/xmlrpc', 'ssl://svn.21cms.de', 443);
//			$xmlrpc_client->setDebug(1);
			$xmlrpc_client->setCredentials($user, $pw);
			$xmlrpc_client->setSSLVerifyHost(0);
			$xmlrpc_client->setSSLVerifyPeer(false);
			$xmlrpc_client->setAcceptedCompression("deflate");
//			echo 'milestone='.$milestone.'';
			$xmlrpc_msg = new xmlrpcmsg('ticket.query', array(
						new xmlrpcval('milestone='.$milestone.'&reporter='.$user, 'string') // AND milestone = ' . $milestone . '
//						new xmlrpcval('status!=closed', 'string') // AND milestone = ' . $milestone . '
//						new xmlrpcval('status!=closed&milestone=' . $milestone . '', 'string') // AND milestone = ' . $milestone . '
					)); //, new xmlrpcval(2, 'int')

			$xmlrpc_resp = $xmlrpc_client->send($xmlrpc_msg);


			if ($xmlrpc_resp == False)
				die('error message');

			$ids = array();
			if (!$xmlrpc_resp->faultCode()) {
				foreach ($xmlrpc_resp->val->me['array'] as $val) {
					$ids[] =  $val->me['int'];
				}
			}

			$multicall = array();

			$newTable = new Table(array("id", "Titel", "Status"));

			foreach ($ids as $id) {
				$multicall[] = new xmlrpcmsg('ticket.get', array(
					new xmlrpcval($id, 'int') // AND milestone = ' . $milestone . '
				));
			}

			if (count($multicall) > 0) {
				$xmlrpc_resp = $xmlrpc_client->multicall($multicall);

				if ($xmlrpc_resp == False)
					die('error message');

				foreach ($xmlrpc_resp as $i => $resp) {
					if (!$resp->faultCode()) {
						$id = $ids[$i];
						$attr = $resp->val->me['array'][3]->me['struct'];
						$status = $attr['status']->me['string'];
						$title = $attr['summary']->me['string'];
						$type = $attr['type']->me['string'];

						$newTable->addContent(array($id,$title,$status));
					}
				}
			}

			return $msg.$newTable;
		}
	}

}

?>