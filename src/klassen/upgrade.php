<?php
    class upgrade {

        private $versionFile = '';
        private $version = '';
        private $checkout = '';
        private $export = '';
        private $updater = 'http://svn.21cms.de/svn/21cms/branches/__updater/';
        private $path = '';
        private $ver = '';
        private $upgrader = '';
        private $repo = 'http://svn.21cms.de/svn/21cms/tags';

        public function __construct() {
            $this->path = realpath(dirname(__FILE__) . "/..");
            $this->export = $this->path . "/upgrade/export";
            $this->checkout = $this->path . "/upgrade/checkout";
            $this->versionFile =  $this->path . "/upgrade/version";
            $this->upgrader =  $this->path . "/upgrade/upgrade.php";
            if(file_exists($this->versionFile))
				$this->version = file_get_contents ($this->versionFile);

            svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_USERNAME, 'easteregg');
            svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_PASSWORD, 'fcartwentyone');

            svn_checkout($this->updater,$this->checkout);
            svn_export($this->checkout,$this->export);

            @unlink($this->upgrader);
            rename($this->export . "/upgrade.php",$this->upgrader);

            $this->rrmdir($this->checkout . "/");
            $this->rrmdir($this->export . "/");
        }

        public function  __toString() {
            $html = "<h1>Aktuelle Version: {$this->version}</h1>";
            $html .= "<h2 style=\"color:red\">!!!CAREFUL!!! - experimentell!!!</h2>";
            $html .= "<table style=\"width: 100%\"><tr><th>Version</th><th>rev</th><th>date</th><th>install</th></tr>";
            $data = svn_ls($this->repo);
            
            
            $html .= "<tr>";
            $html .= "<td>trunk</td>";
            $html .= "<td>trunk by drunk</td>";
            $html .= "<td>i don't know, neither do i care</td>";
            $html .= "<td><a href='/upgrade/upgrade.php?install=trunk'>install</a></td>";
            $html .= "</tr>";
            
            foreach($data as $d) {

                $html .= "<tr>";
                $html .= "<td>{$d["name"]}</td>";
                $html .= "<td>{$d["created_rev"]} by {$d["last_author"]}</td>";
                $html .= "<td>{$d["time"]}</td>";
                $html .= "<td><a href='/upgrade/upgrade.php?install={$d["name"]}'>install</a></td>";
                $html .= "</tr>";

            }

            $html .= "<table>";

            return $html;
        }


        private function rcopy($src,$dst) {
            $dir = opendir($src);
            @mkdir($dst);
            while(false !== ( $file = readdir($dir)) ) {
                if (( $file != '.' ) && ( $file != '..' )) {
                    if ( is_dir($src . '/' . $file) ) {
                        $this->rcopy($src . '/' . $file,$dst . '/' . $file);
                    }
                    else {
                        copy($src . '/' . $file,$dst . '/' . $file);
                    }
                }
            }
            closedir($dir);
        }

        private function rrmdir($dir) {
           if (is_dir($dir)) {
             $objects = scandir($dir);
             foreach ($objects as $object) {
               if ($object != "." && $object != "..") {
                 if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
               } 
             }
             reset($objects);
             rmdir($dir);
           }
         }
    }

    

?>