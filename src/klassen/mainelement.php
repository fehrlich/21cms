<?php
/**
 * Description of mainelement
 *
 * @author F.Ehrlich
 */
abstract class mainelement extends element {
	protected $treeview = false;
	protected $strucktur = true;
	protected $usemainid = true;
	
	public function __construct($id = 0,$data = array(),$container = '', $mainid = 0,$deletable = true,$buildform = true){
		$this->hassub = true;
		parent::__construct($id,$data,$container, $mainid,$deletable,$buildform);

		if($GLOBALS['interface'] == 'ajax' && $this->strucktur 
				&& isset($_GET['el']) && $_GET['el'] == get_class($this) && $buildform){
			$treedata = $this->gettree('*',-1,true,true);
			if(!is_string($treedata))
				$treedata->setSeperator(false);
			$class = (!$this->treeview)?' listOnly':'';
			$this->form->useTab('Struktur');
			$this->form->addHtml('<div class="treesort'.$class.'" id="'.get_class().'">'.$treedata.'</div>');
			$this->form->startSerialize('eldata');
		}
	}
	
//	public function addStructure(){
//			$treedata = $this->gettree();
//			$this->form->useTab('Struktur');
//			$this->form->addHtml('<div class="treesort" id="'.get_class().'">'.$treedata.'</div>');
//			$this->form->startSerialize('eldata');
//	}

	/**
	 * builds a Tree of all subelemends
	 * @param mixed $what Mysql What-Getter or Array of fetched data
	 * @return Tree html Tree of Subelements
	 */
	public function getTree($what = '*',$activeid = -1,$listView = false,$forceNotEmpty = false) {
		//TODO: Menu Dynamisch
		//if($mainid == -1) $mainid = $this->id;
		$subelement = $this->getSubName();
		$data = new Tree('',$this->id,'menu');
		$data->setactiveId($activeid);
		if($subelement == '') $tab = 'elements';
		else $tab = call_user_func_array(array($subelement, 'getTabName'),array());
		if(is_array($what)){
			$tmps = $what;
		}else{
			if($this->id != 0 && $this->usemainid)
				addWhere('mainid', '=', $this->id);
//			$tab = call_user_func_array(array($subelement, 'getTabName'),array());
			if(!isAdmin()) addWhere('visible', '=', '1');
			elseif($tab != 'tags') addWhere('visible', '!=', 'a');
			select($tab,$what,'ordernr');
			$tmps = array();
			while($row = getrow()){
				$tmps[$row['id']] = $row;
			}
		}
		
		foreach($tmps as $row){
			if($tab == 'elements') $subdata = unserialize($row['eldata']);
			else $subdata = $row;
			if($subelement == '' && isSet($row['klasse'])) $subelement = $row['klasse'];
			if(isSet($row['verlinkung']) && $row['verlinkung'] != '-1'){
				if(isset($tmps[$row['verlinkung']])){
					$row['title_intern'] = $tmps[$row['verlinkung']]['title_intern'];
				}elseif($row['verlinkung'] == '0'){
					$row['title_intern'] = 'index';
				}
				else{
					addWhere('id', '=', $row['verlinkung'],'i');
					setLimit(1);
					select('menuepunkte','title_intern');
					$row2 = getRow();
					$element = new $subelement($row['id'],$row2,0,true);
					unset($row2);
				}
				$element = new $subelement($row['id'],$subdata,0,true,false,false);
			}else{
				$element = new $subelement($row['id'],$subdata,0,true,false,false);
			}
			if($listView) $element->setListStyle(true);
			if (method_exists($element, "setMainElement"))
                                $element->setMainElement($this);
			if($forceNotEmpty && ($element->__toString() == "" || $element->__toString() == '<a href="#"></a>')){
				$element = '<a href="#"> </a>';
			}
           	$data->addNode($row['id'], $element, $row['parent']);
		}
		return $data;
	}

	/**
	 *
	 * @param string $what Mysql What-Getter
	 * @return array list of subelements
	 */
	public function getArrayList($what = '*',$idaskey=false,$orderBy = 'ordernr',$checkVisibility = false) {
		$subelement = $this->getSubName();
		$rows = array();
		
		if($this->id != 0 && $this->usemainid) addWhere('mainid', '=', $this->id);
		if($subelement == '') $tab = 'elements';
		else $tab = call_user_func_array(array($subelement, 'getTabName'),array());
		
		select($tab,$what,$orderBy);
		$x = -1;
		while($row = getrow()){
			if(isset($row['id'])){
				if(count($row) == 2){
					$x = $row['id'];
					next($row);
					$row = $row[key($row)];
				}elseif($idaskey)
					$x = $row['id'];
				else
					$x++;
			}else $x++;
			if(!$checkVisibility || !isSet($row['visible']) || $row['visible'] != '0' || isAdmin())
				$rows[$x] = $row;
		}
		return $rows;
	}

	/**
	 * builds mysqldata from json object
	 * @param array $treedata
	 * @param int $anordnung the used ordernr
	 * @param int $level the used level
	 * @param int $parent the used parentid
	 * @return int the next ordernr
	 */
	private function parse_treedata($treedata,$anordnung=0,$level=0,$parent=-1){
		foreach($treedata as $data){
			if(isset($data['attr']['id'])){
				$id = substr($data['attr']['id'], 4);
				$mysqldata = array();
				$mysqldata['ordernr'] = $anordnung;
				$mysqldata['level'] = $level;
				$mysqldata['parent'] = $parent;
				addWhere('id', '=', $id);
				$subelement = $this->getSubName();
				if($subelement == '' && isSet($data['data']))
					$subelement = $data['data'];
				$tab = call_user_func_array(array($subelement, 'getTabName'),array());
				updatearray($tab,$mysqldata,'iii');
				$anordnung++;
				if(isset($data['children']) && is_array($data['children'])){
					$anordnung = $this->parse_treedata($data['children'], $anordnung, $level+1, $id);
				}
			}
		}
		return $anordnung;
	}

	public function formPost(){
		if(isSet($_POST['treeAnordnung'])){
			if(checkVar('treeAnordnung')){
//                            if(isAdmin()) echo $_POST['treeAnordnung'].'!';
				$treedata = unserialize(utf8_decode($_POST['treeAnordnung']));
				$this->parse_treedata($treedata);
			}
			else return 'Error: Fehlende Parameter';
		}
		return parent::formpost();
	}

	/**
	 * returns a subelement
	 * @param int $mainid used mainid
	 * @return mixed subelement
	 */	
	public static function getSub($mainid) {
		//TODO: dynamic
//		$thisclass = get_called_class();
		$thisclass = $_GET['el'];
		$name = call_user_func_array(array($thisclass, 'getsubname'),array());
		return new $name(0,array(),$mainid);
	}

	/**
	 * must return the name of the subelement
	 */
	abstract static public function getSubName();
	static public function getTabName(){
		return 'elements';
	}
}
?>
