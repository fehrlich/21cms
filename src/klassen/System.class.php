<?php

    class System {

        public static function getTempPath() {
            return "/content/temp";
        }

        public static function cleanTemp() {
            self::_recursivelyDelete(CMS_ROOT . self::getTempPath());
        }

        public static function generateTempPath($input) {

            $fileExt = explode(".",$input);
            $fileExt = array_pop($fileExt);

            return static::getTempPath() ."/". sha1("TEMP_" . $input) .".". $fileExt;
        }

        private static function _recursivelyDelete($path) {

            $DI = new DirectoryIterator($path);

            foreach($DI as $entry) {

                if ($entry->isDot()) continue;

                if ($entry->isDir()) {
                    self::_recursivelyDelete($entry->getPathname());
                    rmdir($entry->getPathname());
                } else {
                    unlink($entry->getPathname());
                }
            }
        }
    }