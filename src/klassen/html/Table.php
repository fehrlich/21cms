<?php
class Table {

    private $header;
    private $content;
    private $styles;
    private $added;
    private $fulllink;
    private $dragsort;
    private $headerNames;
    private $trTitles;
    public $class = '';
    public $tdheadstyle;
    public $tdheadClass;
    public $id;
    private $marker = false;
    
    private $newLinkButtonId = "";
    private $excelExport = false;

    public function enableDot() {
        $this->marker = true;
    }
    
    public function setExcelExport($excelExport) {
        $this->excelExport = $excelExport;
    }
    
    public function __construct($head = array(), $sortable = false, $dragsort = false, $id = "") { //,$headerNames = array()
        if ($dragsort) {
            $head = array_merge(array('S'), $head);
        }
        $this->header = $head;
//		$this->headerNames = $head;
        $this->added = 0;
        $this->dragsort = $dragsort;
        $this->id = $id;
    }

    public function addNewLinkButtonId($idadd) {
        $this->newLinkButtonId = $idadd;
    }

    public function addContent($data, $style = '', $fulllinelink = false, $level = 0, $title = '') {
        if ($this->dragsort) {
            $data = array_merge(array('<input type="hidden" class="tableorder" name="tableorder' . $this->id . '[]" value="' . $this->added . '" />#' . $this->added), $data);
        }
        $this->added++;
        $this->content[] = $data;
        $this->contentEig[] = array(
            'level' => $level
        );
        $this->styles[] = $style;
        $this->fulllink[] = $fulllinelink;
        $this->trTitles[] = $title;
    }

    public function getTableRow($index = 0, $class = '', $title = '') {
        $spalten = count($this->header);
        $html = '';
        $contentEig = $this->contentEig[$index];
        $data = $this->content[$index];
        $class = ($class == '') ? '' : ' class="' . $class . '"';

        $tdclass = ($contentEig['level'] > 0) ? ' class="level' . $contentEig['level'] . '"' : '';
        if ($this->trTitles[$index] != '')
            $this->trTitles[$index] = ' title="' . $this->trTitles[$index] . '"';

        if ($this->fulllink[$index])
            $html .= '<tr class="fulllinelink"' . $this->trTitles[$index] . '>';
        else
            $html .= '<tr' . $class . '' . $this->trTitles[$index] . '>';
        $sp = count($data);
        if ($spalten != $sp) {
            $colspan = ' colspan="' . round($spalten / $sp) . '"';
        } else
            $colspan = '';
        if ($this->newLinkButtonId != ''){
            $hoveract = '';
            if(in_array('Aktionen', $this->header)){
//                echo $index;
//                new dBug($this->header);
//                new dBug($this->content); 
                $hoveract = $this->content[$index][count($this->header)-1];
                $hoveract = str_replace('>Edit<', '><img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/icons/appedit.png" alt="[e]" /><', $hoveract);
                $hoveract = str_replace('>Delete<', '><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appdel.png" alt="[X]" /><', $hoveract);
            }
            $html .= '<td><div class="hoverAction">'.$hoveract.'</div></td>';
        }
        foreach ($data as $i => $name) {
            if ($name == '')
                $name = '&nbsp;';
            if ($this->styles[$index] != '')
                $style = ' style="' . $this->styles[$index] . '"';
            else
                $style = '';
            if (isSet($this->header[$i]) && $this->header[$i] == 'Aktionen')
                $tdclass = ' class="aktionen"';
            $html .= '<td' . $tdclass . $colspan . $style . '>' . $name . '</td>';
        }

        if ($this->marker) {
            $html .= "<td class=\"marker\"></td>";
        }

        $html .= '</tr>';
        return $html;
    }

    public function deleteRowsContainerHtml() {
        $html = '<div class="deleteRows">';
//            $html .= '<p>Ansicht anpassen:</p>';
//            foreach($this->header as $name){
//			$html .= '<input type="checkbox" name="deleteCol" class="deleteCol" id="delete'.$name.'" checked="checked" value="1" /> '.$name;
//		}
        $html .= '</div>';
        return $html;
    }
    
    public function toExcel(){
        include($_SERVER['DOCUMENT_ROOT'].'/klassen/PHPExcel.php');
//        include($_SERVER['DOCUMENT_ROOT'].'/klassen/PHPExcel/Autoloader.php');
//        PHPExcel_Autoloader::Register();
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle('export');
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        foreach($this->header as $i => $h){
            if($h != 'Aktionen'){
                $cell = $sheet->setCellValueByColumnAndRow($i, 1, $h,true);   
    //            new dBug($cell->getColumn());
    //            $cell->getColumnDimension()->setAutoSize(true);
    //            $sheet->setCellValueByColumnAndRow($i+1, 1, $h);   
                $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);     
            }
        }
        foreach($this->content as $i => $row){
            foreach($row as $col => $d){
                if(count($row)-1 != $col){
                    $sheet->setCellValueByColumnAndRow($col, $i+2, strip_tags($d));
                }
            }
        }
//        foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
//            $col->setAutoSize(true);
//            echo 'asd';
//        }
//        $objPHPExcel->getActiveSheet()->calculateColumnWidths();
//        foreach($this->content as $i => $row){
//            foreach($row as $col => $d){
//                $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
//                $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
//            }
//        }
        
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=export.xlsx');
        
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('php://output');
    }
    
    public function toHtml(){
        
        if(isSet($_GET['exelExport'.$this->id])){
            $this->toExcel();
            exit();
        }
        $style = '';
        //$width = ' width="100%"';
        //if($style != '') $style = ' style="'.$style.'"';
        if ($this->dragsort)
            $this->class = 'sortable';
        $class = 'sort';
        if ($this->class != '')
            $class .= ' ' . $this->class;

        if ($this->marker) {
            $class .= ' markTable';
        }

        $class = ' class="' . $class . '"';
//                $html = $this->deleteRowsContainerHtml();
        $html = '';
        if (!isSet($_GET['mm']))
            $tableId = 'tableX';
        else
            $tableId = 'table' . $_GET['mm'];
        $html .= '<table border="0" id="table' . $tableId . '" cellpadding="3" cellspacing="0"' . $style . $class . '>'; // class="sortierbar"
        $html .= '<thead>';
        $html .= '<tr class="headdiv">';
        $x = 0;
        $anz = count($this->header) - 1;
        $addClass = '';
        $addAttr = '';
        if ($this->newLinkButtonId != '' || $this->excelExport){
            $html .= '<th class="fakeheader">';
            if($this->newLinkButtonId != '')
                $html .= '<span id="' . $this->newLinkButtonId . '" class="link newRow"><img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/icons/appadd.png" alt="[+]" /></span>';
            if($this->excelExport)
                $html .= '<a href="'.$_SERVER['REQUEST_URI'].'&exelExport'.$this->id.'='.$this->id.'" class="link newRow"><img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/icons/fileTypes/file_extension_xls.png" alt="[xls]" style="width: 16px;height:16px" /></a>';
            $html .= '</th>';
        }
//        if ($this->excelExport != '')
        foreach ($this->header as $name) {
            if ($x == $anz) {
                //$addAttr = ' width="100%"';
                $addClass = ' last';
            }
            if (isset($this->tdheadstyle[$x]) && $this->tdheadstyle[$x] != '')
                $style = ' style="' . $this->tdheadstyle[$x] . $addClass . '"';
            else
                $style = '';
            if (isset($this->tdheadClass[$x]) && $this->tdheadClass[$x] != '')
                $class = ' class="' . $this->tdheadClass[$x] . $addClass . '"';
            else
                $class = '';
            $rowNr = '';
            if ($class == '') {
                $rowNr = ' class="col' . ($x + 1) . $addClass . '"';
            }
            $html .= '<th' . $style . $class . $rowNr . $addAttr . '>' . $name . '</th>'; // class="sortierbar"
            $x++;
        }
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $even = false;
        if ($this->added > 0) {
            foreach ($this->content as $index => $data) {

                $class = ($even) ? 'even' : '';
                $even = !$even;
                $html .= $this->getTableRow($index, $class);
            }
        }
        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    public function __toString() {
        return $this->toHtml();
    }

}

?>
