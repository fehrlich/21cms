<?php

    /**
     * Creates an Connecter to visualize a databases editor
     *
     * @author F.Ehrlich
     */
    class DataEditor {

        private $mysqlTable;
        private $fields;
        private $dontShow = array();
        private $cols = array();
        private $selcols = array();
        private $where = null;
        private $data;
        private $dataIds;

        /**
         *
         * @var Table Table for Displaying Table
         */
        private $table;
        private $head;
        private $formElements;
        private $addLevel = "";
        private $addQuickadd = "";
        private $parentName = "";
        private $orderNr = "";
        private $allowEdit = true;
        private $allowNew = true;
        private $allowDel = true;
        private $orderBy = '';
        private $dataTitleId = '';
        private $limit = 0;
        private $marker = false;
        private $tableShowInvisible = true;
        private $tableShowNoMysql = false;
        private $tableShowPasswords = false;

        /**
         * Formular zum Editieren der Daten
         * @var formular
         */
        public $form;
        public $id;
        public $edit = false;
        public $new = false;
        public $del = false;
        public $quickAddData = array();

        public function setDataTitleId( $dataTitleId ) {
            $this->dataTitleId = $dataTitleId;
        }

        public function setAddLevel( $addLevel = 'level', $parentName = 'parent', $orderNr = 'ordernr' ) {
            $this->addLevel = $addLevel;
            $this->parentName = $parentName;
            $this->orderNr = $orderNr;
            if ( $this->form->posted ) {
                $this->onPost();
            }
        }

        public function AddQuickAdd() {
            $this->addQuickadd = true;

            $old_tab = $this->form->getUsedTab();
            $this->form->useTab( 'Quickadd' );
            $this->form->addElement( 'Quickadd', 'quickadd', 'textarea', '', array(
                'allowtabs' => true
                ), false );
            $this->form->useTab( $old_tab );
            if ( $this->form->posted ) {
                $datas = explode( "\n", $_POST['quickadd'] );
                foreach ( $datas as $i => $data ) {
                    $d = trim( $data );
                    if ( $d != "" )
                        $this->quickAddData[] = $data;
                }
            }
        }

        public function setAllowEdit( $bool ) {
            $this->allowEdit = $bool;
        }

        public function setAllowNew( $bool ) {
            $this->allowNew = $bool;
        }

        public function setAllowDel( $bool ) {
            $this->allowDel = $bool;
        }

        public function getMysqlTable() {
            return $this->mysqlTable;
        }

        public function setOrderBy( $orderBy ) {
            $this->orderBy = $orderBy;
        }

        public function enableDot() {
            $this->marker = true;
        }

        /**
         * Creates an Connecter to visualize a databases editor
         * @param string $mysqlTable Table that should be edited
         * @param string $newString Text for adding an new row
         * @param string $editString Text for editing an new row
         */
        public function __construct( $mysqlTable, $newString = "Neu", $editString = "Editieren" ) {

            if ( intern === true && !rights::getObj()->isAdmin() ) {
                $this->where = array( "author", "=", \cms\session::getObj()->getUserId() );
            }


            $this->mysqlTable = $mysqlTable;
            $this->form = new formular( $mysqlTable );
            if ( isSet( $_POST['quickadd'] ) && $_POST['quickadd'] != "" )
                $this->form->noDataInput = true;
            $this->form->setFormAction( 'ajax.php' . buildGet( 'mm,kl,db,new,edit,inter', true ) );
            if ( isset( $_GET['edit'] ) ) {
                $this->setEdit( $_GET['edit'] );
            }
            if ( isset( $_GET['new'] ) || (isset( $_POST['name'] ) && $_POST['name'] == 'new') ) {
                $this->new = true;
            }
            if ( isset( $_GET['del'] ) ) {
                $this->del = true;
                $this->id = (int) $_GET['del'];
            }
            $this->fields = array();
        }

        public function setEdit( $id ) {
            $this->edit = true;
            $this->id = (int) $id;
        }

        public function addMysqlCol( $name ) {
            $this->selcols[] = $name;
        }

        public function addCol( $name, $html ) {
            $this->cols[$name] = $html;
        }

        public function parseRow( $str, $data ) {
            foreach ( $data as $key => $val ) {
                $str = str_replace( '[DB_' . $key . ']', $val, $str );
            }
//		foreach($this->selcols as $name){
//			$str = str_replace('[DB_'.$name.']', $data[$name], $str);
//		}
            return $str;
        }

        public function setInvisible( $name ) {
            if ( !is_array( $name ) )
                $this->dontShow[] = $name;
            else {
                foreach ( $name as $n ) {
                    $this->dontShow[] = $n;
                }
            }
        }

        /**
         * returns all fields of this connector
         * @return array fields of database
         */
        public function getFields( $alsoinvisible = false, $alsoNoMysql = true, $alsoPasswords = true ) {
            $tmp = array();
            if ( count( $this->fields ) == 0 ) { // || $alsoinvisible
                $elements = $this->form->getElements();
                $elements[] = array( 'name' => 'id' );
//			$elements[] = array('name' => 'title_intern');
                $tmp = array( 'id' ); //','title_intern'
                foreach ( $elements as $el ) {
                    if ( isSet( $el['art'] ) && $el['art'] == 'editor' )
                        $this->dontShow[] = $el['name'];
                    if ( !in_array( $el['name'], $this->dontShow ) && $el['name'] != 'id' )
                        if ( $alsoNoMysql || !isSet( $el['mysql'] ) || $el['mysql'] )
                            if ( $alsoPasswords || !isSet( $el['art'] ) || ($el['art'] != 'password') && $el['art'] != 'pwchange' )
                                $tmp[] = $el['name'];
                }
                $this->fields = $tmp;
            } else
                $tmp = $this->fields;
            return $tmp;
        }

        public function getFieldDescription( $alsoinvisible = false, $alsoNoMysql = true, $alsoPasswords = true ) {
            $tmp = array();
//		if(count($this->fields) == 0 || $getDescription){ // || $alsoinvisible
            $elements = $this->form->getElements();
//			$elements[] = array('name' => 'id');
//			$elements[] = array('name' => 'title_intern');
            $tmp = array( 'id' ); //','title_intern'
            foreach ( $elements as $el ) {
                if ( isSet( $el['art'] ) && $el['art'] == 'editor' )
                    $this->dontShow[] = $el['name'];
                if ( !in_array( $el['name'], $this->dontShow ) )
                    if ( $alsoNoMysql || !isSet( $el['mysql'] ) || $el['mysql'] )
                        if ( $alsoPasswords || !isSet( $el['art'] ) || ($el['art'] != 'password') && $el['art'] != 'pwchange' )
                            if ( $el['serto'] == '' )
                                $tmp[] = (isSet( $el['desc'] )) ? $el['desc'] : $el['name'];
            }
            return $tmp;
        }

        /**
         * get all rows from specific table
         * @return array rows
         */
        public function getData( $alsointern = true, $force = false ) {
            if ( is_array( $this->data ) && !$force )
                return $this->data;

            $data = array();
            $this->dataIds = array();
            $dontAdd = array( 'id' );
            $head = array();
            $head2 = $this->getFields();
            $head2 = array_merge( $head2, $this->selcols );


            if ( !isSet( $this->formElements ) )
                $this->formElements = $this->form->getElements();
            $what = '';
            $changeSelectValue = array();
            $changeMultiSelectValue = array();

            foreach ( $this->formElements as $el ) {
                if ( isset( $el['art'] ) ) {
                    if ( $el['serto'] != '' )
                        $dontAdd[] = $el['name'];
                    if ( $el['art'] == 'dbCon' ) {
                        $dontAdd[] = $el['name'];
                        addJoin( $el['data']['table'], $el['name'], $el['data']['what'][0], $el['data']['what'][1], 'left', $el['name'] );
                    } elseif ( $el['art'] == 'database' ) {
                        $dontAdd[] = $el['name'];
                        addJoin( 'databases', $el['name'], 'id', 'name', 'left', $el['name'] );
                    } elseif ( $el['art'] == 'dataRecord' ) {
                        if ( isSet( $el['data']['dbformname'] ) ) {
                            $dbElName = $el['data']['dbformname'];
                            if ( isSet( $this->formElements[$dbElName] ) ) {
                                $dbEl = $this->formElements[$dbElName];
                                if ( isSet( $dbEl['data']['dbArray'][$dbEl['value']] ) ) {
                                    /**
                                     * Fix: erweiterte DB !?
                                     */
                                    $dontAdd[] = $el['name'];
                                    addJoin( 'data_' . $dbEl['data']['dbArray'][$dbEl['value']], $el['name'], 'id', 'title_intern', 'left', $el['name'] );
                                }
                            }
                        } else {
                            $dbId = $el['data']['dbId'];
                            $dontAdd[] = $el['name'];
                            mys::getObj()->startSubQuery();
                            addWhere( 'id', '=', $dbId );
                            select( 'databases', 'name,dbType' );
                            mys::getObj()->endSubQuery();
                            $row = getRow();
                            $title = "title_intern";
                            if ( $row['dbType'] == 1 ) {
                                $row['name'] = 'user';
                                $title = "loginname";
                            } elseif ( $row['dbType'] == 2 ) {
                                $row['name'] = 'frontEndUser';
                                $title = "loginname";
                            } else
                                $row['name'] = 'data_' . $row['name'];
//                                        $GLOBALS['mysql_debug'] = true;
                            addJoin( $row['name'], $el['name'], 'id', $title, 'left', $el['name'] );
                        }
                    }
                    elseif ( $el['art'] == 'select' ) {
                        if ( !is_array( $el['data'] ) )
                            if ( strpos( $el['data'], ',' ) !== false )
                                $el['data'] = explode( ',', $el['data'] );
                        $changeSelectValue[] = array(
                            'name' => $el['name'],
                            'data' => $el['data']
                        );
                    }
                    elseif ( $el['art'] == 'multiselect' ) {
                        if ( isset( $el["data"] ) ) {
                            foreach ( $el["data"] as $search => $replace ) {
                                $changeMultiSelectValue["search"][] = $search;
                                if ( isSet( $replace['val'] ) )
                                    $replace = $replace['val'];
                                $changeMultiSelectValue["replace"][] = $replace;
                            }
                            $changeMultiSelectValue["name"] = $el["name"];
                        }
                    }
                }
            }
            foreach ( $head2 as $h ) {
                if ( !in_array( $h, $dontAdd ) )
                    $head[] = $h;
            }

            $what = implode( ',', $head );
//		echo $what.'!!';
            if ( !in_array( 'id', $head ) )
                $what = 'id,' . $what;
            if ( $this->addLevel != '' )
                if ( !in_array( $this->addLevel, $head ) )
                    $what = $what . ',' . $this->addLevel;

            if ( substr( $this->mysqlTable, 0, 5 ) == 'data_' && !in_array( 'title_intern', $head ) )
                $what = 'title_intern,' . $what;


//		$GLOBALS['mysql_debug'] = true;
            if ( $this->where !== null && is_array( $this->where ) && $this->mysqlTable == "databases" )
                addWhere( $this->where[0], $this->where[1], $this->where[2] );

            select( $this->mysqlTable, $what, $this->orderBy );

            while ( $row = getRow() ) {
                foreach ( $changeSelectValue as $changeVal ) {
                    /**
                     * FIX: VBU FrontEndUser ....
                     */
                    if ( isSet( $row[$changeVal['name']] ) && isSet( $changeVal['data'][$row[$changeVal['name']]] ) )
                        $row[$changeVal['name']] = $changeVal['data'][$row[$changeVal['name']]];
                }
                foreach ( $head as $h ) {
                    if ( !isSet( $row[$h] ) )
                        $row[$h] = '';
                }
                if ( !empty( $changeMultiSelectValue ) ) {

                    if ( isset( $row[$changeMultiSelectValue["name"]] ) ) {
                        $row[$changeMultiSelectValue["name"]] = str_replace(
                            $changeMultiSelectValue["search"], $changeMultiSelectValue["replace"], $row[$changeMultiSelectValue["name"]] );

                        $row[$changeMultiSelectValue["name"]] = substr( $row[$changeMultiSelectValue["name"]], 1, -1 );
                    }
                }

                $data[] = $row;
                $this->dataIds[$row['id']] = $row;
            }

            $this->data = $data;
            return $data;
        }

        public function setData( $data ) {
            $this->data = $data;
        }

        public function getRowById( $id ) {
            return $this->dataIds[$id];
        }

        public function getRowByIndex( $index ) {
            return $this->data[$index];
        }

        public function getTableRow( $index = 0, $class = '' ) {
            if ( !isSet( $this->table ) ) {
                $idadd = '';
                if ( isSet( $_GET['mm'] ) )
                    $idadd .= $_GET['mm'] . '-';
                elseif ( isSet( $_GET['kl'] ) )
                    $idadd .= $_GET['kl'] . '-';
                if ( isset( $_GET['db'] ) )
                    $idadd .= $_GET['db'];
                $this->buildTable( $idadd );
                addWhere( 'id', '=', $this->form->dataid );
//			echo $this->form->dataid.'!!!';
                $data = $this->getData( false );
//			var_dump($data);
//			if(count($data) > 0)
                $this->setRow( 0, $data[0] );
            }
            return $this->table->getTableRow( $index, $class );
        }

        public function setRow( $id, $row ) {
            $idadd = '';
            if ( isSet( $_GET['mm'] ) )
                $idadd = $_GET['mm'] . '-';
            elseif ( isSet( $_GET['kl'] ) )
                $idadd = $_GET['kl'] . '-';
            if ( isset( $_GET['db'] ) )
                $idadd .= $_GET['db'];
            $tablerow = array();
            foreach ( $this->head as $i => $rowname ) {
                if ( isset( $row[$rowname] ) ) {
                    $name = $rowname;
//				echo $name.'!';
                    $content = $row[$rowname];
//				if(!in_array($name, $this->dontShow)){
                    if ( isset( $this->formElements[$name] ) ) {
                        if ( $this->formElements[$name]['art'] == 'tags' ) {
                            $spl = explode( ',', substr( $content, 1, -1 ) );
                            $content = implode( ', ', $spl );
                        } elseif ( $this->formElements[$name]['art'] == 'bild' ) {
                            $content = '<img src="' . $content . '" style="max-width: 50px" />';
                        } elseif ( $this->formElements[$name]['art'] == 'time' ) {
                            if ( $content != 0 )
                                $content = date( 'd.m.Y H:i:s', $content );
                            else
                                $content = '-';
                        } elseif ( $this->formElements[$name]['art'] == 'date' ) {
                            $content = (int) $content;
                            if ( $content != 0 )
                                $content = date( 'd.m.Y', $content );
                            else
                                $content = '-';
                        }elseif ( $this->formElements[$name]['art'] == 'serTable' ) {
                            $content = unserialize( $content );
                            $table = new Table( array( 'Eigenschaft', 'Wert' ) );
                            foreach ( $content as $key => $c ) {
//							$head[] = $key;
                                $table->addContent( array( $key, $c ) );
                            }
                            $content = $table->__toString();
                        }
                    }
                    $tablerow[] = $content;
                }
            }
            foreach ( $this->cols as $name => $content ) {
                $tablerow[] = $this->parseRow( $content, $row );
            }

            if ( $this->allowEdit && $this->allowDel && (
                (isset( $row["Autor"] ) && $row["Autor"] == \cms\session::getObj()->getUserId()) || rights::getObj()->isAdmin() || (isset( $_GET["db"] ) && rights::getObj()->getDbRights( (int) $_GET["db"], "edit" ) && rights::getObj()->getDbRights( (int) $_GET["db"], "delete" ))
                ) ) {
                $tablerow[] = '<span class="link editRow" id="' . $idadd . '-' . $row['id'] . '">Edit</span> <span class="link delRow" id="' . $idadd . '-' . $row['id'] . '">Delete</span>';
            } elseif ( $this->allowEdit && (!isSet( $_GET["db"] ) || rights::getObj()->getDbRights( (int) $_GET["db"], "edit" )) ) {
                $tablerow[] = '<span class="link editRow" id="' . $idadd . '-' . $row['id'] . '">Edit</span>';
            } elseif ( $this->allowDel && (!isSet( $_GET["db"] ) || rights::getObj()->getDbRights( (int) $_GET["db"], "delete" )) ) {
                $tablerow[] = '<span class="link delRow" id="' . $idadd . '-' . $row['id'] . '">Delete</span>';
            } else {
                $tablerow[] = '';
            }

            foreach ( $this->selcols as $delname ) {
                unset( $tablerow[$delname] );
            }

            if ( $this->addLevel != '' ) {
                $row = $this->getRowById( $tablerow[0] );
                $level = $row[$this->addLevel];
            } else
                $level = 0;
//		if($this->get)

            $this->table->addContent( $tablerow, '', false, $level ); //,$this->getTitle($row)
        }

        public function getTitle( $data ) {
            if ( $this->dataTitleId != '' ) {
                $title = '';
                $spl = explode( ' ', $this->dataTitleId );
                foreach ( $spl as $t ) {
                    $title .= $data[$t];
                }
                return $title;
            }
            return '';
        }

        public function buildTable( $newButtonId = "" ) {
//		$this->head = $tbuildTable(his->getFields(true,false,false);
            $head = $this->getFieldDescription( $this->tableShowInvisible, $this->tableShowNoMysql, $this->tableShowPasswords );
            $this->head = $this->getFields( $this->tableShowInvisible, $this->tableShowNoMysql, $this->tableShowPasswords );

            $headclass = array();
            foreach ( $this->cols as $name => $content ) {
                $this->head[] = $name;
                $head[] = $name;
            }
            $headclass[count( $this->head )] = '{sorter: false}';
            $head[] = 'Aktionen';
//		var_dump($this->head);
            $this->table = new Table( $head );
            $this->table->addNewLinkButtonId( $newButtonId );
            $this->table->setExcelExport( true );
            if ( $this->marker )
                $this->table->enableDot();
            $this->table->tdheadClass = $headclass;
            $this->formElements = $this->form->getElements();
        }

        public function setLimit( $limit ) {
            $this->limit = $limit;
        }

        /**
         * creates table to interact with the table
         * @return string html source
         */
        public function htmlOverview( $onlyTable = false ) {
            $html = '';
            $idadd = '';
            if ( !$onlyTable && ((isset( $_GET["db"] ) && rights::getObj()->getDbRights( (int) $_GET['db'], "create" )) || rights::getObj()->isAdmin() && $this->allowNew) ) {
                $idadd = $_GET['mm'] . '-';
                if ( isset( $_GET['db'] ) )
                    $idadd .= $_GET['db'];
                $html .= '<span class="link newRow" id="' . $idadd . '">Neues Element</span>';
            }

            $this->buildTable( $idadd );
            if ( $this->limit > 0 ) {
                if ( isset( $_GET['page'] ) && $_GET['page'] > 1 )
                    $limit = (($_GET['page'] - 1) * $this->limit) . ',' . $this->limit;
                else
                    $limit = $this->limit;
                setLimit( $limit );
            }
            $data = $this->getData();
            $delcols = array_keys( $this->selcols );

            foreach ( $data as $id => $row ) {
                $this->setRow( $id, $row );
            }
            $html .= $this->table->toHtml();
            if ( $this->limit > 0 ) {
                if ( $this->where !== null && is_array( $this->where ) && $this->mysqlTable == "databases" )
                    addWhere( $this->where[0], $this->where[1], $this->where[2] );
                $count = dbCheck( $this->mysqlTable );
                $pages = ceil( $count / $this->limit );
                if ( $pages > 0 ) {
                    $html .= '<br />Seite:';
                    for ( $i = 1; $i <= $pages; $i++ ) {
                        if ( isset( $_GET['page'] ) && $_GET['page'] == $i )
                            $act = true;
                        else
                            $act = false;
                        if ( $act )
                            $html .= '<b>';
                        $html .= ' <a href="' . $GLOBALS['cms_roothtml'] . 'admin/index.php?' . $_SERVER['QUERY_STRING'] . '&page=' . $i . '">' . $i . '</a>';
                        if ( $act )
                            $html .= '</b>';
                    }
                }
            }
            return $html;
        }

        public function delete( $id = 0 ) {
            if ( $id == 0 )
                $id = $this->id;
            addWhere( 'id', '=', $id, 'i' );
            delete( $this->mysqlTable );
        }

        public function getHtml() {
            $html = '';
            if ( $this->edit ) {
                $this->form->connectData( $this->id );

                $html .= $this->form;

                if ( $this->form->posted ) {
                    if ( $html != 'Ok' )
                        return 'FORMERROR!:' . $html;
                    $html .= ':EL' . $this->getTableRow( 0, 'edit' );
                }
                return $html;
            } elseif ( $this->del ) {
                $this->delete();
                $html .= 'Ok';
                $html .= ':EL';
                return $html;
            } elseif ( $this->new ) {
                $html .= $this->form->__toString();
                if ( $this->form->posted ) {
                    if ( $html != 'Ok' )
                        return 'FORMERROR:' . $html;
                    if ( $this->addQuickadd ) {
                        $html .= ':CONT' . $this->htmlOverview( true );
                    } else
                        $html .= ':EL' . $this->getTableRow( 0, 'edit' );
                }
                return $html;
            } else
                $html = '<div class="var" id="editnewconent"></div>';

            return $html . $this->htmlOverview();
        }

        public function __toString() {
            return $this->getHtml();
        }

        private function onPost() {
            if ( $this->parentName != "" ) {
//			if ($_POST[$this->parentName] != '-1') {
//				addWhere('id', '=', $_POST[$this->parentName], 'i');
//				addWhere('parent', '=', $_POST[$this->parentName], 'i', 'OR');
//				select($this->mysqltab, 'max('.$this->orderNr.') as max', '', true, true);
//				$row = getRow();
//				$_POST[$this->orderNr] = $row['max('.$this->orderNr.') as max'] + 1;
//			} else {
//				addWhere('id', '=', $_POST[$this->parentName], 'i');
//				select($this->mysqlTable, $this->addLevel);
//				$parentrow = getRow();
////				var_dump($parentrow);
//				$_POST[$this->addLevel] = $parentrow[$this->addLevel] + 1;
//
//				addWhere($this->orderNr, '>=', $_POST[$this->orderNr], 'i');
//				updatearray($this->mysqlTable, array($this->orderNr => $this->orderNr + '+1'));
//			}

                if ( $_POST[$this->parentName] != '-1' ) {
                    addWhere( 'id', '=', $_POST[$this->parentName], 'i' );
                    addWhere( $this->parentName, '=', $_POST[$this->parentName], 'i', 'OR' );
                }

                select( $this->mysqlTable, 'max(' . $this->orderNr . ') as max', '', true, true );
                $row = getRow();
                $_POST[$this->orderNr] = $row['max(' . $this->orderNr . ') as max'] + 1;

                if ( $_POST[$this->parentName] != '-1' ) {
                    addWhere( 'id', '=', $_POST[$this->parentName], 'i' );
                    select( $this->mysqlTable, 'level' );
                    $parentrow = getRow();
                    $_POST[$this->addLevel] = $parentrow[$this->addLevel] + 1;

                    addWhere( $this->orderNr, '>=', $_POST[$this->orderNr], 'i' );
                    updatearray( $this->mysqlTable, array( $this->orderNr => $this->orderNr . '+1' ) );
                }
            }
        }

        public function setTableShowInvisible( $tableShowInvisible ) {
            $this->tableShowInvisible = $tableShowInvisible;
        }

        public function setTableShowNoMysql( $tableShowNoMysql ) {
            $this->tableShowNoMysql = $tableShowNoMysql;
        }

        public function setTableShowPasswords( $tableShowPasswords ) {
            $this->tableShowPasswords = $tableShowPasswords;
        }

    }