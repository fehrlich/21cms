<?php
    class newsletterTemplates {

        public function __toString() {

            $template = null;
            $html = '';


            if (file_exists(CMS_ROOT . "/tpl/newsletter/")) {

                $selected = (isset($_GET["template"])) ? $_GET["template"] : '';
                $selected = str_replace(".", "", $selected);

                $files = glob(CMS_ROOT . "/tpl/newsletter/*.html");


                if (!empty($files)) {
                    $dropdown = 'Template auswählen: <select id="newsletterSelect">';
                    foreach($files as $file)  {

                        $file = array_pop(explode("/",$file));
                        $file = explode(".",$file,2);
                        $file = $file[0];

                        if (empty($selected) && $GLOBALS['interface'] != 'ajax') {
                            $selected = $file;
                        }

                        $s = ($file == $selected) ? " selected=\"selected\" " : "";
                        $dropdown .= "<option {$s} value=\"{$file}\">{$file}</option>";
                    }
                    $dropdown .= '</select>';
                    $html .= $dropdown . "<br /><br />";
                }





                if (file_exists(CMS_ROOT . "/tpl/newsletter/{$selected}.html")) {
                  \cms\session::getObj()->set("newsletterTemplate",$selected);
                }


            }

            if (\cms\session::getObj()->get("newsletterTemplate") !== null && file_exists(CMS_ROOT . "/tpl/newsletter/". \cms\session::getObj()->get("newsletterTemplate") .".html")) {
                $template = file_get_contents(CMS_ROOT . "/tpl/newsletter/". \cms\session::getObj()->get("newsletterTemplate") .".html");
            }


            $this->dbsys = new \DataEditor('newsletterTemplates');

            $this->dbsys->form->setFormAction('ajax.php?kl=newsletterTemplates'.buildGet('edit,new'));
            $this->dbsys->form->addElement('name', 'title', 'text');
            $this->dbsys->form->addElement('absender', 'fromMail', 'text');
            $this->dbsys->form->addElement('Html', 'html', 'newsletter',$template);
            $this->dbsys->form->addElement('Datum', 'datum', 'date');
            $this->dbsys->form->addElement('im Archiv?', 'public', 'select','0',array(0 => "nein",1 => "ja"));
//            $this->dbsys->addCol('Versendet', '[DB_sent]');
            $this->dbsys->form->addElement('Versendet', 'sent', FormType::DATE,'',array(),false); 
            $this->dbsys->setTableShowNoMysql(true);
            $this->dbsys->setOrderBy('id DESC');

            $addDbc = new \DatabaseConnector();
            addWhere('dbType','=','1');
            $addForm = $addDbc->getForm();
            if($addForm){
                $addElements = $addForm->form->getElements();
                $dontAdd = array('id','public','published','title_intern');
                foreach($addElements as $el){
                        if(!\in_array($el['name'],$dontAdd))
                                $this->dbsys->form->addElementArrays($el);
                }
            }

            $this->dbsys->enableDot();
            $this->dbsys->setInvisible('html');
            return ($GLOBALS['interface'] != 'ajax') ? $html . $this->dbsys->__toString() : $this->dbsys->__toString();

        }
    }
?>