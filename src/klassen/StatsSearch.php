<?php

class StatsSearch {

	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;

	function  __construct() {
		$this->dbc = new DataEditor('stats_search');
		$this->dbc->setAllowDel(false);
		$this->dbc->setAllowEdit(false);
		$this->dbc->setAllowNew(false);

		$this->dbc->form->addElement("Suchterm", "search", "text");
		$this->dbc->form->addElement("Anzahl", "anz", "text");
	}
	function __toString() {
		$mys = mys::getObj()->cleanup();

		$sql = 'SELECT search,COUNT(*) as anz FROM stats_search GROUP BY search ORDER BY anz DESC';
		$res = $mys->query($sql);
		$rows = array();
		if ($res !== false) {
			while ($row = $res->fetch_assoc()) {
				$row['search'] = htmlentities($row['search'],null,'UTF-8');
				$rows[] = array('id' => 0)+$row;
			}
		}
		
		$this->dbc->setData($rows);
		return (string)$this->dbc;
	}
}
?>