<?php
/**
 * Description of Languages
 *
 * @author XzenTorXz
 */
class Languages {
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;

	public function getLanguages(){
		$cfg = config::getObj();
		$lang = $cfg->get('languages');
		if($lang == false){
			$lang = array();
			$cfg->set('languages', $lang);
		}
		return $lang;
	}

	public function __construct(){
	}

	public static function getLang($short = true) {
        if (isset($_GET["lang"])) $_GET['lang'] = \strtolower($_GET['lang']);

        if (isSet($_GET['lang']) && $_GET['lang'] != '') {
			if (!isSet($GLOBALS['languages'])) {
                return '';
            } elseif (!isSet($GLOBALS['languages'][$_GET['lang']])) {
                return '';
            } else {
				if ($short) {
                    return $_GET['lang'];
                } else {
                    return $GLOBALS['languages'][$_GET['lang']];
                }
			}
		}

        return '';
	}
	public function __toString(){
		$html = '<h1>Sprachen</h1>';
		$f = new formular();
		$cfg = config::getObj();
		$lang = $this->getLanguages();
		$f->addElement('Neue Sprache', 'name');
		$f->addElement('K&uuml;rzel', 'short');
		if($f->posted && isSet($_POST['name']) && $_POST['name'] != ''){
			$html .= $this->newLanguage($_POST['name'],$_POST['short']);
		}
		$f->setSaveButton('Hinzuf&uuml;gen', true);
		$html .= $f;

		return (string) $html;
	}

	private function newLanguage($name,$short){
        $short = strtolower($short);
		$name = menuepunkte::setTitelIntern($name);
		$lang = $this->getLanguages();
		if($name == '') return 'Bitte einen Namen der Sprache eingeben';
		if(in_array($name,$lang) || isSet($lang[$short])) return 'Diese Sprache existiert schon';
		$lang[$short] = $name;
		config::getObj()->set('languages', $lang);
		config::getObj()->save();
		/*ADD COL Elements*/
		mys::getObj()->query('ALTER TABLE `elements` ADD COLUMN `searchtxt_'.$name.'` TEXT NULL AFTER `searchtxt`');
		mys::getObj()->query('ALTER TABLE `menuepunkte` ADD COLUMN `title_'.$name.'` TEXT NULL AFTER `title`');
		mys::getObj()->query('ALTER TABLE `dataTemplates` ADD COLUMN `html_'.$name.'` TEXT NULL AFTER `html`');
	}
}
?>
