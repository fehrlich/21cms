<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tag
 *
 * @author F.Ehrlich
 */
class Tag extends subelement {

    protected $treeview = true;

    public function formBuild() {
            $this->form->addElement('Tag Name', 'title');
            $m = new menu();
            $seiten = array('' => '[Standard]');
            $s = $m->getArrayList('title_intern',false);
            $seiten = mysql2selectArray($s,$seiten);

            $this->form->addElement('Aktuelle Priori&auml;t', 'prio','text');
            $this->form->addElement('Anzeigen in', 'showin','select','',$seiten);
            $this->form->addElement('Anzeige vererben', 'vererbe','check','',array('Anzeige vererben (nur n&auml;chste Ebene!)'),false);
            $this->form->removeElement('mainid');
            //$this->form->removeElement('visible');
            $this->form->useTab('Admin');
            $this->form->addElement('interner Titel', 'title_intern', 'text');
    }

    public function formPost(){
            $altTitle =  '';
            if($_POST['parent'] > 0){
                    addWhere('id', '=', $_POST['parent'],'i');
                    select('tags', 'title_intern');
                    $row = getRow();
                    if($row)
                            $altTitle =  $row['title_intern'].'_'.$_POST['title'];
            }
			if(!isSet($this->data['id']))
					$this->data['id'] = 0;
            $_POST['title_intern'] = menuepunkte::setTitelIntern($_POST['title'],'tags', $this->data['id'], $altTitle);
            $t = parent::formPost();
            if(isset($_POST['vererbe'])){
//			echo $_POST['showin'].'!!!';
//			$this->setShowIn($this->form->dataid,$_POST['showin']);
                    $data['showin'] = $_POST['showin'];
                    addWhere('parent', '=', $this->form->dataid,'i');
                    updateArray('tags',$data,'s');
            }
			return $t;
    }

    public static function buildLink($name,$title_intern,$size='',$color = ''){
            if($GLOBALS['interface'] == 'admin') $add = 'admin/';
            else $add = '';
            $addsize = ($size == '')?'':'font-size:'.$size.'px;';
            $addcolor = ($color == '')?'':'color:'.$color;
            $addcss = ($addsize == '' && $addcolor == '')?'':' style="'.$addsize.$addcolor.'"';
            $link = '<a href="'.$GLOBALS['cms_roothtml'].$add.'Tag/'.$title_intern.'.html"'.$addcss.'>'.$name.'</a>';
            return $link;
    }

    public function getInline() {
//		$title = htmlentities(utf8_decode($this->data['title']));
            return Tag::buildLink($this->data['title'],$this->data['title_intern']);
    }
    public function getMainName() {
            return 'TagSystem';
    }

    static public function getTabName(){
            return 'tags';
    }
}
?>
