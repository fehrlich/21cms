<?php

    class UserConfig {

        /**
         *
         * @var UserConfig config
         */
        static private $handle;
        private $cropProperties = array();
        private $userTemplates = array();
        private $gobalMaxWidth = 1280;
        private $mainContentWidth = 0;
        private $contactMail = '';
        private $publishRequestMail = '';
        private $disableFlash = false;
        private $javascriptEmailProtection = false;
        private $disableMetatagGenerator = false;
        private $loginSite = false;
        private $captchaKey = '';
        private $captchaKeyPublic = '';
        private $contentClasses = array();
        private $links = array();
        private $adminDBListLimit = 100;

        /**
         *
         * @var bool
         */
        private $disableMetatagPublisher = false;

        public function __construct() {

            UserConfig::$handle = $this;
            $this->contactMail = 'info@' . $_SERVER['HTTP_HOST'];
        }

        /**
         *
         * @return UserConfig config
         */
        public static function getObj() {
            if (UserConfig::$handle == null) {
                new UserConfig ();
            }

            return UserConfig::$handle;
        }

        /**
         *
         * @param type $name
         * @param type $width
         * @param type $height
         * @param type $forceHeight
         * @return \UserConfig
         */
        public function addCropPropertie($name, $width, $height, $forceHeight = false) {
            $this->cropProperties[$name] = array($width, $height, $forceHeight);
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $html
         * @return \UserConfig
         */
        public function addUserTemplate($name, $html) {
            $this->userTemplates[$name] = array(
                'name' => $name,
                'html' => $html
            );
            return $this;
        }

        public function getUserTemplate($name = '') {
            if ($name == '')
                return $this->userTemplates;
            else {
                if (isSet($this->userTemplates[$name]))
                    $ret = $this->userTemplates[$name];
                else
                    $ret = array('name' => $name, 'html' => 'no such template');
                return $ret;
            }
        }

        public function getUserTemplateHtml($name) {
            $ret = $this->getUserTemplate($name);
            if ($name == '') {
                $first = current($ret);
                return $first['html'];
            }
            return $ret['html'];
        }

        public function getCropProperties() {
            return $this->cropProperties;
        }

        public function getGobalMaxWidth() {
            if ($this->gobalMaxWidth !== false) {
                return $this->gobalMaxWidth;
            }
            return false;
        }

        /**
         *
         * @param type $gobalMaxWidth
         * @return \UserConfig
         */
        public function setGobalMaxWidth($gobalMaxWidth) {
            $this->gobalMaxWidth = $gobalMaxWidth;
            return $this;
        }

        public function getMainContentWidth() {
            return $this->mainContentWidth;
        }

        /**
         *
         * @param type $mainContentWidth
         * @return \UserConfig
         */
        public function setMainContentWidth($mainContentWidth) {
            $this->mainContentWidth = $mainContentWidth;
            return $this;
        }

        public function getContactMail() {
            return $this->contactMail;
        }

        /**
         *
         * @param type $contactMail
         * @return \UserConfig
         */
        public function setContactMail($contactMail) {
            $this->contactMail = $contactMail;
            return $this;
        }

        public function getPublishRequestMail() {
            return $this->publishRequestMail;
        }

        /**
         *
         * @param type $publishRequestMail
         * @return \UserConfig
         */
        public function setPublishRequestMail($publishRequestMail) {
            $this->publishRequestMail = $publishRequestMail;
            return $this;
        }

        public function getDisableFlash() {
            return $this->disableFlash;
        }

        /**
         *
         * @param type $disableFlash
         * @return \UserConfig
         */
        public function setDisableFlash($disableFlash) {
            $this->disableFlash = $disableFlash;
            return $this;
        }

        public function getJavascriptEmailProtection() {
            return $this->javascriptEmailProtection;
        }

        /**
         *
         * @param type $javascriptEmailProtection
         * @return \UserConfig
         */
        public function setJavascriptEmailProtection($javascriptEmailProtection) {
            $this->javascriptEmailProtection = $javascriptEmailProtection;
            return $this;
        }

        public function getDisableMetatagGenerator() {
            return $this->disableMetatagGenerator;
        }

        public function getDisableMetatagPublisher() {
            return $this->disableMetatagPublisher;
        }

        /**
         *
         * @param type $disableMetatagGenerator
         * @return \UserConfig
         */
        public function setDisableMetatagGenerator($disableMetatagGenerator) {
            $this->disableMetatagGenerator = $disableMetatagGenerator;
            return $this;
        }

        /**
         *
         * @param type $disableMetatagPublisher
         * @return \UserConfig
         */
        public function setDisableMetatagPublisher($disableMetatagPublisher) {
            $this->disableMetatagPublisher = $disableMetatagPublisher;
            return $this;
        }

        public function getLoginSite() {
            return $this->loginSite;
        }

        /**
         * return $this;
         * @param type $loginSite
         * @return \UserConfig
         */
        public function setLoginSite($loginSite) {
            $this->loginSite = $loginSite;
            return $this;
        }

        public function getCaptchaKey() {
            return $this->captchaKey;
        }

        /**
         *
         * @param type $captchaKey
         * @return \UserConfig
         */
        public function setCaptchaKey($captchaKey) {
            $this->captchaKey = $captchaKey;
            return $this;
        }

        public function getCaptchaKeyPublic() {
            return $this->captchaKeyPublic;
        }

        /**
         *
         * @param type $captchaKeyPublic
         * @return \UserConfig
         */
        public function setCaptchaKeyPublic($captchaKeyPublic) {
            $this->captchaKeyPublic = $captchaKeyPublic;
            return $this;
        }

        /**
         *
         * @param type $path
         * @param type $classname
         * @return \UserConfig
         */
        public function addCustomElement($path, $classname) {
            include_once CMS_ROOT . "/tpl/" . $path;
            $GLOBALS['elements'][$classname] = array('element');

            return $this;
        }

        function getContentClasses() {
            return $this->contentClasses;
        }

        /**
         *
         * @param type $contentClasses
         * @return \UserConfig
         */
        function setContentClasses($contentClasses) {
            $this->contentClasses = $contentClasses;
            return $this;
        }

        /**
         *
         * @param type $src
         * @param type $dest
         * @return \UserConfig
         */
        public function addRedirect($src, $dest) {
            $this->links[$src] = $dest;
            return $this;
        }

        public function getRedirects() {
            return $this->links;
        }

        function getAdminDBListLimit() {
            return $this->adminDBListLimit;
        }

        /**
         *
         * @param type $adminDBListLimit
         * @return \UserConfig
         */
        function setAdminDBListLimit($adminDBListLimit) {
            $this->adminDBListLimit = $adminDBListLimit;
            return $this;
        }
    }

    $userConfig = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . "tpl/config.php";

    if ( file_exists($userConfig) ) {
        include( $userConfig );
    }