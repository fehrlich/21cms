<?php
    class stats {

        private static $obj = null;
        private static $data = array();

        private static $widgets = array(
                                    "Besucherübersicht" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=index",
                                                "height" => "600px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 1
                                             ),
                                    "Weltkarte" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserCountryMap&actionToWidgetize=worldMap",
                                                "height" => "450px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 3
                                             ),
                                    "Besuchslänge" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerVisitDuration",
                                                "height" => "350px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 1
                                             ),
                                    "Suchmachinen" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getKeywords",
                                                "height" => "450px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 2
                                             ),
                                    "Externe Seiten" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getWebsites",
                                                "height" => "450px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 2
                                             ),
                                    "Besucherklicks" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage",
                                                "height" => "350px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 2
                                             ),
                                    "Einstiegsseiten" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getEntryPageUrls",
                                                "height" => "1500px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 3
                                             ),
                                    "Länderübesicht" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=GeoIP&actionToWidgetize=getGeoIPCountry",
                                                "height" => "500px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 1
                                             ),
                                    "Seitenübersicht" => array(
                                                "url" => "http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageUrls",
                                                "height" => "1500px",
                                                "width" => "100%",
                                                "context" => '<div id="widgetIframe"><iframe class="piwikIframe" width="{width}" height="{height}" src="{url}" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>',
                                                "column" => 2
                                             )


                                    );

        public function __construct() {
            self::$obj = $this;
            $this->getUserInformation();
        }

        static public function getObj() {
            if (self::$obj === null) {
                new self();
            }

            return self::$obj;
        }

        public function registerSearch($what) {
            static::$data["search"] = $what;
            insertArray("stats_search", static::$data,  "i" . str_repeat("s", count(static::$data)-1));

        }

        public function trackUser() {

        }

        public function getWidget($name) {


        }

        public function  __toString() {

            if(!isset($GLOBALS['piwik_token']) || !isset($GLOBALS['piwik_siteid']) || $GLOBALS['piwik_token'] == '' || $GLOBALS['piwik_siteid']  == '') return "Kein Authtoken gegeben!";


            return '<iframe style="min-height: 900px;" src="http://stats.21advertise.de/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite='.$GLOBALS["piwik_siteid"].'&period=week&date=yesterday&token_auth=' . $GLOBALS["piwik_token"] .'" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="100%"></iframe>';
        }

        public function getUserInformation() {
            @$this->data["requestTime"] = (string) $_SERVER["REQUEST_TIME"];
            @$this->data["userAgent"] = $_SERVER["HTTP_USER_AGENT"];
            @$this->data["request"] = $_SERVER["REQUEST_URI"];
            @$this->data["ip"] = $_SERVER["REMOTE_ADDR"];
            @$this->data["hostname"] = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
            @$this->data["referer"] = $_SERVER["HTTP_REFERER"];

            if (\cms\session::sessionExists()) {
                @$this->data["sessionId"] = \cms\session::getObj()->getSessionId();
            }
        }
    }
?>