<?php
/**
 * Description of dateimanager
 *
 * @author F.Ehrlich
 *
 */
class dateimanager {

	private $pfad;

	/**
	 * gets all files from an directory
	 * @param mixed $aktDir the used dir, fals for default folder
	 * @param boolean $rekursiv set recursiv
	 * @return array array of all files
	 */
	public function getdateien($aktDir = false, $rekursiv = false, $filter = '') { //, $filter = false
		if (!$aktDir)
			$aktDir = $this->pfad;
//		echo $aktDir;

		if (!is_dir($aktDir)) {
			$aktDir = str_replace('//', '/', $this->pfad . $aktDir);
			if (!is_dir($aktDir))
				return array();
		}
		$dateien = array();

		if ($handle = opendir($aktDir)) {
			$i = 0;
			while (false !== ($file = readdir($handle))) {
				if (substr($file, 0, 1) != '.') { // && substr($file,0,6) != "thump_"
					if (is_dir($aktDir . $file)) {
						if ($rekursiv) {
							$dateien = array_merge($dateien, $this->getdateien($aktDir . ($file) . '/', true));
						} else {
							$dateien[$i]['name'] = ($file);
							$dateien[$i]['art'] = '';
							$i++;
						}
					} else {
						preg_match('/(.*)\.(\w*)/', $file, $spl);
						$art = $spl[2];
						$name = $spl[1];

						if ($rekursiv)
							$dateien[$i]['name'] = ($aktDir . $file);
						else
							$dateien[$i]['name'] = ($file);

						if ($art == ''
							)$dateien[$i]['art'] = 'datei';
						else
							$dateien[$i]['art'] = $art;
						$i++;
					}
				}
			}
			closedir($handle);
		}
		if ($filter != '') {
			if ($filter == 'images')
				$reg_filter = '/gif|jpg|png|jpeg/';
			elseif ($filter == 'flv')
				$reg_filter = '/flv/';
			else
				$reg_filter = $filter;
			$tmp_dateien = array();
			foreach ($dateien as $datei) {
				if (preg_match($reg_filter, $datei['art']))
					$tmp_dateien[] = $datei;
			}
			$dateien = $tmp_dateien;
		}
		return $dateien;
	}

        public function uploadPost2(){
                
            $input = fopen("php://input", "r");
            $temp = tmpfile();
            $realSize = stream_copy_to_stream($input, $temp);
            fclose($input);

            if ($realSize != $this->getSize()){            
                return false;
            }

            $target = fopen($path, "w");        
            fseek($temp, 0, SEEK_SET);
            stream_copy_to_stream($temp, $target);
            fclose($target);
        }
        
	public function uploadPost() {
//            if (!empty($_FILES)) {

                checkVar('folder', 'd');
                checkVar('folder', 'd',true);
                
                if (isset($_GET['noflash']) && $_GET['noflash'] == '1') {
                    $noFlash = true;
                } else {
                    $noFlash = false;
                }


                if (!$noFlash) {
                    if(!isSet($_POST['folder']))
                        $pfad = $_GET['folder'];
                    else
                        $pfad = $_POST['folder'];
//                    $pfad = substr($_POST['folder'], $len);
                } else {
                    $pfad = $_GET['folder'];
                    if ($_GET["folder"] === "root" || $_GET["folder"] === "../") $pfad = "/";
                }
                
                $targetPath = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . '/dateien/' . $pfad . '/';
//                echo $targetPath.'!';
                if (!empty($_FILES)) {
                    $targetFile = str_replace('//', '/', $targetPath) . ($_FILES['Filedata']['name']);
                    $tempFile = $_FILES['Filedata']['tmp_name'];
                    move_uploaded_file($tempFile, $targetFile);
                }else{
                    $targetFile = str_replace('//', '/', $targetPath) . ($_GET['qqfile']);
                    $input = fopen("php://input", "r");
                    $temp = tmpfile();
                    $realSize = stream_copy_to_stream($input, $temp);
                    fclose($input);

                    if ($realSize != $_SERVER["CONTENT_LENGTH"]){            
//                        return false;
                        echo 'Dateigröße stimmt nicht überein';
                    }

                    $target = fopen($targetFile, "w");        
                    fseek($temp, 0, SEEK_SET);
                    stream_copy_to_stream($temp, $target);
                    fclose($target);                    
                }


                $info = @getimagesize($targetFile);

                if ($info !== false && UserConfig::getObj()->getGobalMaxWidth() !== false) {

                    $img = image::create($targetFile,true);
		    if($img->getWidth() > UserConfig::getObj()->getGobalMaxWidth() || $img->getHeight() > UserConfig::getObj()->getGobalMaxWidth()) {
                        
			$img->resize(UserConfig::getObj()->getGobalMaxWidth(),0,true)
				->save($targetFile)
				->cleanup();
                    }
                }

                
                cms\file::load($targetFile,1)->close();

                if ($noFlash) {
                    header('Location: ' . $GLOBALS['cms_roothtml'] . 'admin/index.php?mm=Dateimanager');
                } else {
                    echo (json_encode(array('success'=>true)));;
                }
//            }
	}

	/**
	 *
	 * @param String $pfad
	 */
	public function __construct($pfad = 'dateien/') {
		$this->pfad = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $pfad;
	}

	/**
	 * copys an dir to destination
	 * @param String $source Pfad vom Quellverzeichnis
	 * @param String $destination Pfad vom Zielverzeichnis
	 * @return boolean sucess
	 */
	public function dir_copy($source, $destination) {
		$error = '';
                \cms\file::load($source,true)->copy($destination, true);
                echo 'Ok';
	}

	public function fileedit(){
		$path = '/dateien/'.$_GET['id'];
		$form = new formular('files','','','fileopt');
		$file = \cms\file::load($path);

		$form->addElement('Kommentar', 'comment','textarea');
		$form->addElement('Sortierungsnummer', 'sort','text');
        $form->addElement('public json aktivieren',"pubJson","select","",array(0 => "nein",1 => "ja"));

		$fileData = $file->get();

		$form->useTab('Zusätzlich');
                $form->startSerialize('data');
                if(is_array($fileData) && isSet($fileData['Tags']))
                        $tagVal = $fileData['Tags'];
                else
                        $tagVal = '';
//			$form->addElement('Tags', 'Tags', 'text', $tagVal);
                $form->addElement('Tags', 'Tags', FormType::TAGS, $tagVal);
                if(is_array($fileData)){
                        foreach($fileData as $id => $val){
                                if($id != 'Tags')
                                        $form->addElement($id, $id, 'text', $val);
                        }
                }
		$form->connectData($file->getId());
		echo $form->__toString();
	}

	/**
	 * crops an image from posted data
	 */
	public function crop() {
		if (checkVar(array('src', 'cropw', 'croph', 'cropx', 'cropy', 'solw'), 'diiiii')) {
//			include('../klassen/image.php');
			$pfad = dirname($_POST['src']);
			if (strlen($pfad) < 2)
				$pfad = '';
			if (checkVar('newname'))
				$newname = basename($_POST['newname']);
			else
				$newname = basename($_POST['src']);
			$oldname = $_POST['src'];
			if(strpos($oldname, '?')){
				$spl = explode('?',$oldname);
				$oldname = $spl[0];
			}
			if(strpos($newname, '?')){
				$spl = explode('?',$newname);
				$newname = $spl[0];
			}
                        image::create('dateien/' . $pfad ."/". basename($oldname))
                            ->crop($_POST['cropx'], $_POST['cropy'], $_POST['cropw'], $_POST['croph'], $_POST['solw'])
                            ->save('dateien/' . $pfad ."/". $newname)
                            ->cleanup();

			echo 'Ok';
		}else
			echo "Falsche Parameter";
	}

	/**
	 * delete a file
	 */
	public function delete() {
            if (checkVar('src', 'd')) {
                $source = $this->pfad . str_replace('&amp;', '&', $_POST['src']);
                cms\file::load($source,1)->delete();
                echo 'Ok';
            } else {
                echo "Falsche Parameter";
            }
	}

	/**
	 * copys an dir to destination from post data
	 */
	public function dircopy() {
            if (checkVar(array('to', 'src'), "dd")) {
                $source = slashFolderFix($this->pfad . $_POST['src']);
				if($source.'/' == $this->pfad)
					die('Root Verzeichniss kann nicht kopiert werden.');
                $_POST["to"] .= ($_POST['to'] != '') ? '/' : '';

                $destination = $this->pfad . $_POST['to'] . basename($source);
		\cms\file::load($source,true)->copy($destination, true);
                echo 'Ok';
            } else {
                echo 'Fehler bei der Uebergabe der Parameter';
            }
	}

	/**
	 * moves an dir to destination from post data
	 */
	public function dirmove() {
		if (checkVar(array('to', 'src'), "dd")) {			
                    $source = slashFolderFix($this->pfad . $_POST['src']);
                    if($source.'/' == $this->pfad)
                            die('Root Verzeichniss kann nicht verschoben werden.');
                    $destination = $this->pfad . $_POST['to'] . '/' . basename($source);

                    \cms\file::load($source, true)->move($destination, true);
                    echo 'Ok';
		}else
                    echo 'Error: Falsche Parameter';
	}
        
	public function checkOccurrence() {
		if (checkVar(array('src'), "d")) {			
                    $source = slashFolderFix($this->pfad . $_POST['src']);
                    if($source.'/' == $this->pfad)
                        die('Root Verzeichniss kann nicht &uuml;berpr&uuml;ft werden.');
                    
                    echo \cms\file::load($source, true)->checkFilePathOccurrence("");
		}else
                    echo 'Error: Falsche Parameter';
	}
        

	/**
	 * moves an dir to destination from post data and check its occurence
	 */
	public function savedirmove() {
		if (checkVar(array('to', 'src'), "dd")) {
                        $source = slashFolderFix($this->pfad . $_POST['src']);
			$source_pfad = slashFolderFix($this->pfad . $_POST['src']);
			if($source_pfad.'/' == $this->pfad)
				die('Root Verzeichniss kann nicht verschoben werden.');
			$destination = $this->pfad . $_POST['to'] . '/' . basename($source);
                        echo \cms\file::load($source, true)->checkFilePathOccurrence($destination,true,true,true);
		}else
			echo 'Error: Falsche Parameter';
	}

	/**
	 * deletes an dir to destination from post data
	 */
	public function remDir($path = '') {
		if ($path == '' && checkVar(array('src'), "dd")) {
			$first = true;
			$path = slashFolderFix($this->pfad . $_POST['src']);
			if($path.'/' == $this->pfad)
				die('Root Verzeichniss kann nicht gelöscht werden.');
		}else
			$first = false;
		if ($path == '') {
			echo 'Falsche Parameter';
			return;
		}
		if (!is_dir($path)) {
			\cms\file::load($path,true)->delete();
//			if (unlink($path))
				echo 'Ok';
//			else
//				echo 'Datei konnte nicht gel&ouml;scht werden';
			return;
		}
		$dir = @opendir($path);
		if (!$dir)
			echo 'Fehler beim &ouml;ffnen des Verzeichnisses';

		while (($entry = @readdir($dir)) !== false) {
			if ($entry == '.' || $entry == '..')
				continue;
			if (is_dir($path . '/' . $entry)) {
				$res = $this->remDir($path . '/' . $entry);
				if ($res == -1) { // dies duerfte gar nicht passieren
					@closedir($dir); // Verzeichnis schliessen
					return -2; // normalen Fehler melden
				} else if ($res == -2) { // Fehler?
					@closedir($dir); // Verzeichnis schliessen
					return -2; // Fehler weitergeben
				} else if ($res == -3) { // nicht unterstuetzer Dateityp?
					@closedir($dir); // Verzeichnis schliessen
					return -3; // Fehler weitergeben
				} else if ($res != 0) { // das duerfe auch nicht passieren...
					@closedir($dir); // Verzeichnis schliessen
					return -2; // Fehler zurueck
				}
			} else if (is_file($path . '/' . $entry) || is_link($path . '/' . $entry)) {
				// ansonsten loesche diese Datei / diesen Link
//				$res = @unlink($path . '/' . $entry);
				\cms\file::load($path . '/' . $entry,true)->delete();
				// Fehler?
//				if (!$res) {
//					@closedir($dir); // Verzeichnis schliessen
//					return -2; // melde ihn
//				}
			} else {
				@closedir($dir); // Verzeichnis schliessen
				echo 'Falscher Dateityp';
				return -3; // tut mir schrecklich leid...
			}
		}

		// schliesse nun das Verzeichnis
		@closedir($dir);

		// versuche nun, das Verzeichnis zu loeschen
		\cms\file::load($path . '/' . $entry,true)->delete();
//		$res = @rmdir($path);

		// gab's einen Fehler?
//		if (!$res) {
//			echo 'Fehler beim l&ouml;schen des Verzeichnises. Verzeichnis eventuell nicht leer';
//			return -2; // melde ihn
//		}
		if ($first)
			echo 'Ok';
	}

	/**
	 * renames a file or directory from post data
	 */
	public function rename() {
            if (checkVar(array('src', 'newname'), "ddd") && (!isSet($_POST['endung']) || checkVar(array('endung'), "d"))) {

				$source = slashFolderFix($this->pfad . $_POST['src']);
				if($source.'/' == $this->pfad)
					die('Root Verzeichniss kann nicht umbenannt werden.');
                
                if (isSet($_POST['endung']) && $_POST['endung'] != '') {
                    $destination = dirname($source) . '/' . $_POST['newname'] . '.' . $_POST['endung'];
                } else {
                    $destination = dirname($source) . '/' . $_POST['newname'];
                    if (!is_dir($source)) {
                        echo 'Konnte das Verzeichnis nicht finden';
                        return;
                    }
                }
                $fileHandle = cms\file::load($source,1);
                if(isSet($_GET['opt']) && $_GET['opt'] == 'save'){
                    echo $fileHandle->checkFilePathOccurrence($destination, true, true, true);
                }
                else{
                    $fileHandle->move($destination, true);
                    echo 'Ok';
                }

            } else {
                echo 'Error: Falsche Parameter';
            }
	}

	/**
	 * gets xmlfile with folder and files
	 * @param <type> $rel_pfad
	 */
	public function get_foldercontents($rel_pfad = '') {
		if(isSet($_GET['pfad'])) $_POST['pfad'] = $_GET['pfad'];
		if (checkVar(array('pfad'), "d")) {
			$rel_pfad = $_POST['pfad'];
			$bilder = array('jpg', 'gif', 'png');
			if ($rel_pfad == '/' || $rel_pfad == 'root')
				$rel_pfad = '';
			else {
				$rel_pfad .= '/';
				if (substr($rel_pfad, 0, 1) == '/')
					$rel_pfad = substr($rel_pfad, 1);
			}
			$pfad = $this->pfad . $rel_pfad;
			if (!is_dir($pfad))
				echo 'Dieser Pfad exestiert nicht!';
			else {
				$filter_nogo = array('.', '..', '__thumb1', '__thumb2', '__thumb3', '__thumb4');
//				echo $pfad.'!';
				$files = array_diff(scandir($pfad), $filter_nogo);
				//print_r($files);
				$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
				$xml .= '<!DOCTYPE el [<!ATTLIST name pfad CDATA #REQUIRED>]>';
				$xml .= '<struc>' . "\n";
				foreach ($files as $file) {
					//echo $pfad.$file."\n";;
					if (is_dir($pfad . $file)) {
						$xml .= "\t<el name=\"" . (str_replace('&', '&#38;', $file)) . "\" isdir=\"1\" pfad=\"" . (str_replace('&', '&#38;', $rel_pfad . $file)) . "\" />\n";
					} else {
						preg_match('/(.*)\.(\w*)/', $file, $spl);
						if (isset($spl[2]) && in_array(strtolower($spl[2]), $bilder))
							$art = 'img';
						elseif (isset($spl[2]) && $spl[2] == 'flv')
							$art = 'flv';
						else
							$art = $spl[2];
						$check = true;
						if (isSet($_GET['filter']) && $_GET['filter'] != "") {
							if ($_GET['filter'] == 'flv' && $art != 'flv')
								$check = false;
						}
						if ($check)
							$xml .= "\t<el name=\"" . (str_replace('&', '&#38;', $file)) . "\" isdir=\"0\" pfad=\"" . (str_replace('&', '&#38;', $rel_pfad . $file)) . "\" art=\"" . $art . "\" />\n";
					}
				}
				//$xml .= "<dirs>\n".$xml_dirs."</dirs>\n<files>\n".$xml_files."</files>\n";
				$xml .= '</struc>' . "\n";
				//echo $xml->asXML();
				header("content-type: text/xml");

				echo $xml;
			}
		}else
			echo 'Fehler bei der Uebergabe der Parameter';
	}
	/**
	 * gets xmlfile with folder and files of absolute path
	 * @param <type> $rel_pfad
	 */
	public function get_foldercontentsGlob($rel_pfad = '') {
		if(isSet($_GET['pfad'])) $_POST['pfad'] = $_GET['pfad'];
		if (checkVar(array('pfad'), "d")) {
			if(!is_dir($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].'dateien/'.$_POST['pfad']))
				$rel_pfad = dirname($_POST['pfad']);
			else
				$rel_pfad = $_POST['pfad']; 
			$filter_nogo = array('.', '..', '__thumb1', '__thumb2', '__thumb3', '__thumb4');
			$bilder = array('jpg', 'gif', 'png');
			if ($rel_pfad == '/' || $rel_pfad == 'root')
				$rel_pfad = '';
			else {
				$rel_pfad .= '/';
				if (substr($rel_pfad, 0, 1) == '/')
					$rel_pfad = substr($rel_pfad, 1);
			}

			$pfad = $this->pfad . $rel_pfad;
			$folders = explode('/',  substr($rel_pfad,0,-1));
			$xmlFolderAdd = '';
			$subPath = '';
			$level = 0;
			if(!is_dir($pfad)){
				echo 'Error:NoValidPath';
				return 'Error:NoValidPath';
			}
			$files = array_diff(scandir($this->pfad.$subPath), $filter_nogo);
			foreach ($files as $file) {
				if (is_dir($this->pfad.$subPath .'/'. $file)){
//					echo $subPath .'/'. $file.'!<br>';
					$xmlFolderAdd .= "\t<el name=\"" . (str_replace('&', '&#38;', $file)) . "\" level=\"".$level."\" isSubDir=\"0\" isdir=\"1\" pfad=\"" . (str_replace('&', '&#38;', $subPath . $file)) . "\" dirpfad=\"" . (substr(str_replace('&', '&#38;', $subPath),0,-1)) . "\" />\n";
				}
			}
			$level = 1;
			$folderCount = count($folders);
			foreach($folders as $folder){
				if($folder != ''){
					if($level > 1) $subPath .= '/'.$folder;
					else $subPath .= $folder;
					$files = array_diff(scandir($this->pfad.$subPath), $filter_nogo);
					foreach ($files as $file) {
						if (is_dir($this->pfad.$subPath .'/'. $file)){
	//					echo $subPath .'/'. $file.'!<br>';
							if($level == $folderCount) $isSubfolder = '1';
							else $isSubfolder = '0';
							$xmlFolderAdd .= "\t<el name=\"" . (str_replace('&', '&#38;', $file)) . "\" level=\"".$level."\" isdir=\"1\" isSubDir=\"".$isSubfolder."\" pfad=\"" . (str_replace('&', '&#38;', $subPath.'/' . $file)) . "\" dirpfad=\"" . (str_replace('&', '&#38;', $subPath)) . "\" />\n";
						}
					}
					$level++;
				}
			}
			if (!is_dir($pfad))
				echo 'Dieser Pfad exestiert nicht!';
			else {
				$files = array_diff(scandir($pfad), $filter_nogo);
				//print_r($files);
				$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
				$xml .= '<!DOCTYPE el [<!ATTLIST name pfad CDATA #REQUIRED>]>';
				$xml .= '<struc>' . "\n";
				$xml .= $xmlFolderAdd;
				foreach ($files as $file) {
					//echo $pfad.$file."\n";;
					if (is_dir($pfad . $file)) {
//						$xml .= "\t<el name=\"" . utf8_encode(str_replace('&', '&#38;', $file)) . "\" isdir=\"1\" pfad=\"" . utf8_encode(str_replace('&', '&#38;', $rel_pfad . $file)) . "\" />\n";
					} else {
						preg_match('/(.*)\.(\w*)/', $file, $spl);
						if (isset($spl[2]) && in_array(strtolower($spl[2]), $bilder))
							$art = 'img';
						elseif (isset($spl[2]) && $spl[2] == 'flv')
							$art = 'flv';
						else
							$art = 'file';
						$check = true;
						if (isSet($_GET['filter']) && $_GET['filter'] != "") {
							if ($_GET['filter'] == 'flv' && $art != 'flv')
								$check = false;
						}
						if ($check)
							$xml .= "\t<el name=\"" . (str_replace('&', '&#38;', $file)) . "\" isdir=\"0\" pfad=\"" . (str_replace('&', '&#38;', $rel_pfad . $file)) . "\" art=\"" . $art . "\" dirpfad=\"" . substr(str_replace('&', '&#38;', $rel_pfad),0,-1) . "\" />\n";
					}
				}
				//$xml .= "<dirs>\n".$xml_dirs."</dirs>\n<files>\n".$xml_files."</files>\n";
				$xml .= '</struc>' . "\n";
				//echo $xml->asXML();
				header("content-type: text/xml");

				echo $xml;
			}
		}else
			echo 'Fehler bei der Uebergabe der Parameter';
	}

	/**
	 * creates an directory
	 */
	public function mkdir() {
            if (checkVar(array('pfad', 'name'), "dd")) {
                mkdir($this->pfad . $_POST['pfad'] . '/' . $_POST['name']);
				\cms\file::load($this->pfad . $_POST['pfad'] . '/' . $_POST['name'],true);
//				echo 'ja';
                echo "Ok";
            } else {
                echo "Parameter Error";
            }
	}
        
        public function getFileInfo(){
            echo 'check';
            
            if (checkVar(array('pfad'), "d",true)) {
                $finfo = finfo_open();
                var_dump(finfo_file($finfo, $this->pfad.$_GET['pfad']));
            echo 'check';
                $file = \cms\file::load($this->pfad.$_GET['pfad'],true);
                var_dump($file->getFileInfo());
                var_dump($file->getData());
            }
        }
        

	public function __toString() {
		//$html = '<div id="dateimanager"><div id="filetree"><ul><li id="root" class="selected">root</li></ul></div><div id="newfolderdiv"><input type="button" id="newfolder" value="Neues Verzeichnis" /></div><div id="dateien">[DAT]</div></div>';
		$html = "<script type=\"text/javascript\">var sessionid = \"" . \cms\session::getObj()->getSessionId() . "\";</script>";
		if(isset($_GET['listview']) && $_GET['listview'] == '1')
			$add = ' class="listview"';
		else $add = '';
		//'.((UserConfig::getObj()->getDisableFlash())?'':'uploadify').'
                
                $html .= '<script type="text/html" id="dmFolderListElement">
                            <li data-bind="css:{selected:$root.selectedFolder().path()==path(),active:showSubFolders}">
                                <div class="actions" data-bind="ifnot: isRoot">
                                    <span class="act actRename" data-bind="click: renameDialog"><img title="Datei umbenennen" alt="[r]" src="images/icons/rename.png"></span>
                                    <span class="act actEdit" data-bind="click: editDialog"><img alt="[e]" src="images/icons/appedit.png"></span>                                    
                                    <span class="act actEdit" data-bind="click: deleteDialog"><img alt="[x]" src="images/icons/appdel.png"></span>                                    
                                </div>
                                <span data-bind="{text:name(),click:updateContent,event: { dblclick: commitFolder }}" />
                                <!-- ko if: showSubFolders -->                                
                                <ul data-bind="template: { name: \'dmFolderListElement\', foreach: subFolders }" />
                                <!-- /ko -->
                            </li>                     
                        </script>';
                
                $html .='<div id="dateimanager">
                    
                        <div id="folderDiv" class="filemanagerDiv">
                          <div id="folderHeader" class="fakeheader"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" data-bind="click: selectedFolder().newFolderDialog" /> Verzeichnis</div>
                          <div id="folderContent">
                            <div id="filetree2">
                                <ul data-bind="template: { name: \'dmFolderListElement\',data: rootFolder}" />
                                <ul data-bind="template: { name: \'dmFolderListElement\', foreach: rootFolder.subFolders }" />
                            </div>
                            <div id="newFolderButDiv"><input type="button" class="" id="newFolderBut" value="Neues Verzeichnis" data-bind="click: selectedFolder().newFolderDialog" /></div>
                          </div>
                        </div>
                        <div id="fileDiv" class="filemanagerDiv">
                          <div id="fileHeader" class="fakeheader"><div id="newFileDiv"></div> Dateien</div>
                          <div id="fileContent">
                            <div id="fileList2" data-bind="foreach:selectedFolder().files">
                              <div class="datei" data-bind="{click:selectFile,css:{selected:($root.selectedFile() && $root.selectedFile().path()==path())}, event: { dblclick: commitFile }}">
                                  <span class="dateiname" data-bind="{text:name,style:{width:isLoading()+\'%\'},css:{ isLoading: (isLoading() != 100)}}"></span>
                              </div>
                            </div>
                            <div id="uploadButDiv"><input type="button" class="" id="uploadButton" value="UPLOAD" /></div>
                          </div>
                          <div id="fileListx"></div>
                        </div>
                        <div id="previewDiv" class="filemanagerDiv">
                          <div id="previewHeader" class="fakeheader">Vorschau</div>
                          <!-- ko ifnot: fileSelected -->
                          <div id="dateien2" data-bind="foreach:selectedFolder().files">
                            <div class="datei" data-bind="event: { dblclick: commitFile }">
                                <div class="dateiimg" data-bind="click: selectFile">
                                    <img class="dateithumb" data-bind="attr:{src: (icon())?\''.$GLOBALS['cms_rootdir'].'admin/images/icons/fileTypes/file_extension_\'+icon()+\'.png\':\''.$GLOBALS['cms_rootdir'].'dateien/\'+path()+addTime(),alt:name}" />
                                </div>
                                <span class="dateiname showImage" data-bind="{text:name,attr:{title:name}}"></span>
                                <div class="fileactions">
                                    <a class="act"  data-bind="attr:{href: \''.$GLOBALS['cms_rootdir'].'dateien/\'+path()}" target="_blank"><img title="Datei downloaden" alt="[d]" src="images/icons/download.png"><span class="desc">Download</span></a>
                                    <span class="act actren" data-bind="click: renameDialog"><img title="Datei umbenennen" alt="[r]" src="images/icons/rename.png"><span class="desc">Umbenennen</span></span>
                                    <!-- ko ifnot: icon -->
                                    <span class="act actedit" title="Bild bearbeiten" data-bind="click: imageEditDialog"><img alt="[e]" src="images/icons/imgedit.png"><span class="desc">Foto bearbeiten</span></span>
                                    <!-- /ko -->
                                    <span class="act actoptedit" title="Datei bearbeiten" data-bind="click: fileEditDialog"><img alt="[e]" src="images/icons/fileedit.png"><span class="desc">Datei bearbeiten</span></span>
                                    <span class="act actdel" title="Datei löschen" data-bind="click: fileDeleteDialog"><img alt="[x]" src="images/icons/filedelete.png"><span class="desc">Löschen</span></span>
                                </div>
                              </div>
                          </div>
                          <!-- /ko -->
                          <!-- ko if: fileSelected -->
                          <div id="previewImage">
                            <div class="dateiBig" data-bind="{attr: {id: selectedFile.name},event: { dblclick: selectedFile().commitFile }}">
                                <a href="#" class="backLink" data-bind="click: folderView">Zur&uuml;ck Zur Ordneransicht</a>
                                <div class="dateiimg" style="">
                                    <img class="dateithumb" data-bind="attr:{src: (selectedFile().icon())?\''.$GLOBALS['cms_rootdir'].'admin/images/icons/fileTypes/file_extension_\'+selectedFile().icon()+\'.png\':\''.$GLOBALS['cms_rootdir'].'dateien/\'+selectedFile().path(),alt:name}" />
                                </div>
                                <div class="fileactions">
                                    <a class="act"  data-bind="attr:{href: \''.$GLOBALS['cms_rootdir'].'dateien/\'+selectedFile().path}" target="_blank"><img title="Datei downloaden" alt="[d]" src="images/icons/download.png"><span class="desc">Download</span></a>
                                    <span class="act actren" data-bind="click: selectedFile().renameDialog"><img title="Datei umbenennen" alt="[r]" src="images/icons/rename.png"><span class="desc">Umbenennen</span></span>
                                    <span class="act actedit" title="Bild bearbeiten" data-bind="click: selectedFile().imageEditDialog"><img alt="[e]" src="images/icons/imgedit.png"><span class="desc">Foto bearbeiten</span></span>
                                    <span class="act actoptedit" title="Datei bearbeiten" data-bind="click: selectedFile().fileEditDialog"><img alt="[e]" src="images/icons/fileedit.png"><span class="desc">Datei bearbeiten</span></span>
                                    <span class="act actdel" title="Datei löschen" data-bind="click: selectedFile().fileDeleteDialog"><img alt="[x]" src="images/icons/filedelete.png"><span class="desc">Löschen</span></span>
                                </div>
                            </div>
                          </div>
                            <div id="fileInfo">
                                <span>Dateiname:</span>
                                <span class="info" data-bind="text:selectedFile().name"></span>
                                <span>Dateigr&ouml;&szlig;e:</span>
                                <span class="info" data-bind="text:selectedFile().size"></span>
                                <span>Info:</span>
                                <span class="info" data-bind="text:selectedFile().info"></span>
                            </div>
                          <div id="dateienx"></div>
                        <!-- /ko -->
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    ';
//                $html .='<div id="dateimanager">
//                    
//                        <div id="folderDiv" class="filemanagerDiv">
//                          <div id="folderHeader" class="fakeheader"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" id="newfolder" /> Verzeichnis</div>
//                          
//                             <ul data-bind="template: { name: \'dmFolderListElement\', foreach: rootFolder.subFolders }" />
//                        <div id="filetree">
//                            <li id="root" class="selected"><a href="#">root</a></li>
//                          </div>
//                        </div>
//                        <div id="fileDiv" class="filemanagerDiv">
//                          <div id="fileHeader" class="fakeheader"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" id="newfile" /> Dateien</div>
//                          <div id="uploadDiv">
//                            <div id="fileInputDiv"><input id="fileInput" name="Filedata" class="'.((UserConfig::getObj()->getDisableFlash())?'':'uploadify').'" type="file" /></div>
//                          </div>
//                          <div id="fileList"></div>
//                        </div>
//                        <div id="previewDiv" class="filemanagerDiv">
//                          <div id=previewHeader" class="fakeheader">Vorschau</div>
//                          <div id="dateien"></div>
//                        </div>
//                    </div>
//                    ';
//		$html .= '<div id="dateimanager"'.$add.'>
//			<div id="filetree">
//				<ul>
//					<li id="root" class="selected">root</li>
//				</ul>
//			<div id="newfolderdiv"><input type="button" id="newfolder" value="Neues Verzeichnis" /></div>
//			<div id="folderrenamediv"><input type="button" id="renameFolder" value="Verzeichnis umbenennen" /></div>
//			<div id="foldereditdiv"><input type="button" id="editFolder" value="Verzeichnis editieren" /></div>
//			<div id="delfolderdiv"></div>
//			<div id="fileInputDiv"><input id="fileInput" name="Filedata" class="'.((UserConfig::getObj()->getDisableFlash())?'':'uploadify').'" type="file" /></div>
//			</div>
//			<div id="imageSizeSlider"></div>
//			<div id="actions"><input type="button" id="toggleListView" value="Zur Listenansicht wechseln" /></div>
//			<div id="dateien">
//				[DAT]
//			</div>
//		</div>';
		return $html;
	}

} 

?>