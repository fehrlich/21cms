<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gruppe
 *
 * @author F.Ehrlich
 */
class Gruppen {
    //put your code here
	/**
	 *
	 * @var DataEditor
	 */
	private $dbsys;
        private $sql;

	public function  __construct() {

            $groups = array(0 => "Nicht erlaubt",1 => "Erlaubt");

            $this->dbsys = new DataEditor('userGroups');
//            if(!isset($_GET['inter']) || $_GET['inter'] != 'dia')
//                    $this->dbsys->form->setSaveButton('Speichern');

            $this->dbsys->form->setFormAction('ajax.php?kl=Gruppen'.buildGet('edit,new'));
            $this->dbsys->form->addElement('Gruppenname', 'name','text');

            $this->dbsys->form->addElement('Erstellen', 'create','select','0', $groups);
            $this->dbsys->form->addElement('Bearbeiten', 'edit','select','0', $groups);
            $this->dbsys->form->addElement('Löschen', 'delete','select','0', $groups);
            $this->dbsys->form->addElement('Veröffentlichen', 'release','select','0', $groups);
            $this->dbsys->form->addElement('Administrator', 'admin','select','0', $groups);
			$this->dbsys->form->useTab('AdminMenue');
			$this->dbsys->form->startSerialize("menuRights");
			$menu = getAdminMenu();
			$menuValues = $menu->getArray();
			foreach($menuValues as $menupunkt){
				$this->dbsys->form->addElement($menupunkt->getSingleItem()." anzeigen", 'allow'.$menupunkt->getId(),  FormType::SIMPLECHECKBOS,'1',array(),true);
			}
			$this->dbsys->form->stopSerialize("menuRights");
	}

	public function  __toString() {
		return $this->dbsys->__toString();
	}
}
?>
