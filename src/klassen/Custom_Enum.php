<?php

abstract class Custom_Enum {

	private $_constants;  // array
	private $_index;  // string

	public function __construct() {
		$rc = new ReflectionClass($this);
		$this->_constants = $rc->getConstants();
	}

	public function __call($method, $args) {
		if (preg_match('/^is([a-zA-Z]*?)$/', $method, $matches)) {
			return $this->is($matches[1]);
		}
		$this->set($method);
		return $this;
	}

	public function __toString() {
		return (string) $this->getValue();
	}

	public function set($index) {
		$this->_index = strtoupper($index);
		return $this;
	}

	public function is($index) {
		return strtoupper($index) == $this->_index;
	}

	public function getValue() {
		return $this->_constants[$this->_index];
	}

}

class FormType extends Custom_Enum {

    const CODEEDITOR = "codeeditor";

	/**
	 * Simple textline<br>
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>style</var> = <i>string</i> wich fills the Style attribute of the input</li>
	 * </ul>
	 * * this id has to be set
	 */
	const TEXTLINE = 'text';

	/**
	 * Simple textline, wich needs to be an email or empty<br>
	 * data is not used
	 */
	const EMAIL = 'email';

	/**
	 * Simple textarea<br>
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>allowtabs</var> = <i>boolean</i> if it should disalbe tab for the next focus</li>
	 * </ul>
	 * * this id has to be set
	 */
	const TEXTAREA = 'textarea';

	/**
	 * Used for insert a mysql field.
	 * data is not used
	 */
	const MYSQLONLY = 'mysql';

	/**
	 * Field with date and time creates a unix timestamp
	 * data is not used
	 */
	const TIME = 'time';

	/**
	 * Field with Date gets converted to datepicker with jQuery
	 * data is not used
	 */
	const DATE = 'date';

	/**
	 * Simple hidden field
	 * data is not used
	 */
	const HIDDENFIELD = 'hidden';

	/**
	 * Field where the User can pick multiple Values<br>
	 * data should be an array where the ids are the values for the mysql table
	 * and the values the data wich the user see
	 */
	const MULTISELECT = 'multiselect';

	/**
	 * creates a simple upload.<br>
	 * only for specific need, used FILE instead
	 */
	const UPLOAD = 'upload';

	/**
	 * Creates a MULTISELECT for Tags<br>
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>maintag</var> = <i>int</i> id of the maintag</li>
	 * </ul>
	 * * this id has to be set
	 */
	const TAGS = 'tags';

	/**
	 * Simple Dropdown<br>
	 * data should be an array where the ids are the values for the mysql table
	 * and the values the data wich the user see.<br>
	 * <b>OR:</b> data can be string where the data is seperated by comma (,)
	 */
	const SELECT = 'select';

	/**
	 * Creates a Textarea wich is Converted to an CKEditor instance by jQuery
	 * data is not used
	 */
	const HTMLEDITOR = 'editor';

	/**
	 * Textline, with a button for a filemanager popup where the user can select
	 * a file or folder
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>allowfolder</var> = <i>boolean</i> set to true if the user also
	 *  can select Folders</li>
	 *	<li><var>note</var> = <i>string</i> displays a note on the top right of
	 * the filemanager</li>
	 * </ul>
	 * * this id has to be set
	 * <b>same as FILE</a>
	 */
	const DATEI = 'datei';

	/**
	 * Textline, with a button for a filemanager popup where the user can select
	 * a file or folder
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>allowfolder</var> = <i>boolean</i> set to true if the user also
	 *  can select a Folder</li>
	 *	<li><var>note</var> = <i>string</i> displays a note on the top right of
	 * the filemanager</li>
	 * </ul>
	 * * this id has to be set
	 * <b>same as DATEI</a>
	 */
	const FILE = 'datei';

	/**
	 * Textline, with a button for a filemanager popup where the user can select
	 * a folder
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>note</var> = <i>string</i> displays a note on the top right of
	 * the filemanager</li>
	 * </ul>
	 * * this id has to be set
	 * <b>same as DATEI</a>
	 */
	const FOLDER = 'folder';

	/**
	 * Textline, with a button for a filemanager popup where the user can select
	 * a image
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>allowfolder</var> = <i>boolean</i> set to true if the user also
	 *  can select a Folder</li>
	 *	<li><var>note</var> = <i>string</i> displays a note on the top right of
	 * the filemanager</li>
	 *	<li><var>maxwidth</var> = <i>int</i> the maximum width of the image</li>
	 *	<li><var>maxheight</var> = <i>int</i> the maximum height of the image</li>
	 *	<li><var>defaultpic</var> = <i>string</i> analog to value, but if the
	 * defaultpic is set as a folder a random pic from the folder is picked as
	 * defaultvalue.</li>
	 * </ul>
	 * * this id has to be set
	 * <b>same as DATEI</a>
	 */
	const BILD = 'bild';

	/**
	 * Simple Html Button wich execute a specific Javascript function
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>onclick</var> = <i>boolean</i> Javascript function for onclick
	 * event</li>
	 * * this id has to be set
	 * <b>same as DATEI</a>
	 */
	const BUTTON = 'button';

	/**
	 * create custom html inputs
	 * data should be a html string where a post with name id should be created
	 */
	const HTMLPLAIN = 'html';

	/**
	 * implement a nested formula
	 * data should be a formular element
	 */
	const FORMULA = 'formular';

	/**
	 * simple checkbox creates a 0 or 1 as value
	 * data should be a string with a description
	 */
	const SIMPLECHECKBOS = 'simpleCheck';


	/**
	 * list of checkbxes<br>
	 * data should be an array where the ids are the values for the mysql table
	 * and the values the data wich the user see.<br>
	 */
	const CHECKBOXARRAY = 'CHECK';

	/**
	 * simple dropdown filled with all user databases<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>addAllDB</var> = <i>boolean</i> a Value for ALL Databases is
	 * created</li>
	 *	<li><var>selectName</var> = <i>boolean</i> if set to true all values are
	 * the databasenames and not the ids</li>
	 *	<li><var>noreload</var> = <i>boolean</i> if set to true the field dont
	 * force a reload when the value is changed</li>
	 * </ul>
	 * * this id has to be set
	 */
	const DATABASE = 'database';


	/**
	 * creates a filterselection for a specific userdatabase. the formular needs a
	 * database field<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>dbformname</var>* = <i>string</i> name of the database field</li>
	 * </ul>
	 * * this id has to be set
	 */
	const DATAFILTER = 'filter';

	/**
	 * creates a templateselection for a specific userdatabase. the formular needs a
	 * database field<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>dbformname</var>* = <i>string</i> name of the database field</li>
	 * </ul>
	 * * this id has to be set
	 */
	const DATATEMPLATE = 'dataTemplate';

	/**
	 * creates a dropdown filled with all data of a specific userdatabase. the
	 * formular needs a database field<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>dbformname</var>** = <i>string</i> name of the database field</li>
	 * </ul>
	 *	<li><var>dbId</var>** = <i>string</i> the id of the database</li>
	 * </ul>
	 * ** One of this values need to be filled out
	 */
	const DATARECORD = 'dataRecord';

	/**
	 * creates a dropdown filled with all fields of a specific userdatabase. the
	 * formular needs a database field<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>dbformname</var>** = <i>string</i> name of the database field</li>
	 * </ul>
	 *	<li><var>dbId</var>** = <i>string</i> the id of the database</li>
	 * </ul>
	 * ** One of this values need to be filled out
	 */
	const DATAFIELD = 'dbField';

	/**
	 * creates a dropdown field where the user can select a value from a
	 * specific mysql table or create a new one.<br>
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>table</var>* = <i>string</i> mysql Table</li>
	 *	<li><var>class</var>* = <i>string</i> The Classname of the the class
	 * wich is used for data creation (for example the ?mm=ClassName value)</li>
	 *	<li><var>what</var>* = <i>array</i> what should be selected id 0 for the
	 * id and id 1 for the displayed value (for example array('id', 'title')</li>
	 * </ul>
	 * * this id has to be set
	 */
	const MYSQLCONNECTION = 'dbCon';

	/**
	 * creates 2 text input fields wich needs to be empty or equal, also creates
	 * a button for a random passwordsetting
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>type</var> = <i>string</i> set the type of the input fields
	 * should be text or password. if the type is not equal to text then there
	 * will be no random password button</li>
	 *	<li><var>noConf</var> = <i>boolean</i> if set to true there will be only
	 * one text input with no confirmation</li>
	 * </ul>
	 * * this id has to be set
	 */
	const PASSCHANGE = 'pwchange';

	/**
	 * creates a cridSystem where you can add Divs.
	 *
	 * data should be an array with following ids:
	 * <ul>
	 *	<li><var>gridSizeX</var> = <i>int</i> Size where the Divs snap on X axes
	 * default: 10</li>
	 *	<li><var>gridSizeY</var> = <i>int</i> Size where the Divs snap on Y axes
	 * default: 10</li>
	 * </ul>
	 * * this id has to be set
	 */
	const GRIDSYSTEM = 'grid';
}

?>