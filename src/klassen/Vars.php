<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vars
 *
 * @author Franz Ehrlich <franz.ehrlich@21advertise.de>
 */
class Vars {
	static function parseUserVars($string){
		$string = str_replace('[FS_User]', frontendSession::getObj()->getUserName(), $string);
		return $string;
	}
	static function parseTagVars($string,$addBefore = '',$addAfter = ''){
		if (preg_match_all('/\[TAGS(.*?)\]/', $string, $matches)) {
			foreach ($matches[1] as $i => $match) {
				$maintag = $match;
//							new dBug($maintag);
				if(!empty ($maintag)){
					$maintag = substr ($maintag, 1,-1);
					addWhere('parent', '=', $maintag,'i');
				}
				select('tags','title_intern');
				$tags = array();
				while($row = getRow()){
					$tags[] = $row['title_intern'];
				}
				$string = str_replace($matches[0][$i], $addBefore.implode(',',$tags).$addAfter, $string);
			}
		}
		return $string;
	}
}
?>
