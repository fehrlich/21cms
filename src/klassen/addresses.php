<?php
    class addresses {

        private static $handle = null;

        public function __construct () {
            self::$handle = $this;
        }

        public function __toString() {

            $groups = buildSelectArray("addressGroups", array('id','name'));

            $this->dbsys = new \DataEditor('addressUser');

            $this->dbsys->form->setFormAction('ajax.php?kl=addresses'.buildGet('edit,new'));
            $this->dbsys->form->addElement('Anrede', 'gender', 'select',0,array(0 => "[nicht gewählt]",1 =>"Herr",2 => "Frau"));
            $this->dbsys->form->addElement('Titel', 'title', 'text');
            $this->dbsys->form->addElement('Vorname', 'name','text');
            $this->dbsys->form->addElement('Nachname', 'lastname','text');
            $this->dbsys->form->addElement('Email', 'email', 'text');
            $this->dbsys->form->addElement('Heimatland', 'country', 'text');
            $this->dbsys->form->addElement('Adresse', 'address', 'text');
            $this->dbsys->form->addElement('Token', 'token', 'text',sha1(microtime(1)));
            $this->dbsys->form->addElement('Gruppe', 'groups','multiselect','', $groups);
            $this->dbsys->form->addElement('Auth', 'authenticated','select','0',array("Nein","Ja"));

            $addDbc = new \DatabaseConnector();
            addWhere('dbType','=','1');
            $addForm = $addDbc->getForm();
            if($addForm){
                $addElements = $addForm->form->getElements();
                $dontAdd = array('id','public','erstellt','published','autor','title_intern');
                foreach($addElements as $el){
                        if(!\in_array($el['name'],$dontAdd))
                                $this->dbsys->form->addElementArrays($el);
                }
            }

            $this->dbsys->enableDot();

            return $this->dbsys->__toString();
        }

		/**
		 *
		 * @return addresses
		 */
        static public function getObj() {
            if (self::$handle === null) {
                new self();
            }

            return self::$handle;
        }


        public function addUser($data = false,$group = false) {
            if (!$data || !is_array($data) || !isset($data["email"])) return false;
            filterArray($data,array("title","name","lastname","email","address","country","firm","gender"));

            if ($group !== false) $data["groups"] = "," . $group . ",";

            $data["token"] = sha1(microtime(1) . $data["email"]);
            $data["added"] = time();
            insertArray("addressUser",$data,str_repeat("s",count($data)));

            return $data["token"];
        }

        public function deleteUser($req) {
            mys::getObj()->cleanup()->clearWhere();
            if (is_integer($req)) {
                addwhere("id","=",$req);
            } elseif(strpos($req,"@") === false) {
                //keine email, also token
                addWhere("token","=",$req);
            } else {
                //also mail
                addWhere("email","=",$req);
            }

            delete("addressUser");
        }

        public function authUser($token) {
            if (strlen($token) != 40) return false;
            return mys::getObj()->cleanup()->clearWhere()->addWhere("token", "=", $token)->updateArray("addressUser",array("authenticated" => 1));
        }

        public function getUser($req) {
            mys::getObj()->cleanup()->clearWhere();
            if (is_integer($req)) {
                addwhere("id","=",$req);
            } elseif(strpos($req,"@") === false) {
                //keine email, also token
                addWhere("token","=",$req);
            } else {
                //also mail
                addWhere("email","=",$req);
            }

            setLimit(1);
            select('addressUser','token,email,id,name,lastname');

            $res = getRow();
            return (count($res) == 0) ? false : $res;
        }

        public function addGroup() {

        }

        public function addUserToGroup($user,$group) {

        }

        public function removeUserFromGroup($id) {

        }
    }
?>
