<?php
namespace cms;
/**
 * Description of User
 *
 * @author R.Heine
 */
    class session {

        static private $handle = null;

        private $hash = null;
        private $userId = 0;
        private $data = array();
        private $timeout = 86400;
        private $username = '';
        private $userEmail = '';
        private $groupId = 0;
        private $adminMenuRights = null;
        private $formatedEmail = null;



        public function __construct() {
            
            \cms\session::$handle = $this;
            $this->cleanupsessions();
            $this->checkSession();
        }

        /**
         * checks if the user is logged in
         * @return bool if user logged in
         */

        public function isLoggedIn() {
            if ($this->userId != 0) return true;
            return false;
        }

        private function checkSession() {
            if (!isset($_COOKIE["id"]) && !isset($_POST["sessionid"]) && !isset($_GET["sessionid"])) {
                return $this->startSession();
            }

            if (isset($_GET["sessionid"])) {
                $this->hash = $_GET["sessionid"];
                return $this->resumeSession();
            }
            if (isset($_POST["sessionid"])) {
                $this->hash = $_POST["sessionid"];
                return $this->resumeSession();
            }

            $this->hash = (isset($_POST["sessionid"])) ? $_POST["sessionid"] : $_COOKIE["id"];
            $this->resumeSession();
        }

        /**
         * static function for retriving the session handle
         * @return session  retrive session objecthandle
         */

        static public function getObj() {

            if(\cms\session::$handle === null) {
                new \cms\session();
            }

            return \cms\session::$handle;
        }

        /**
         * starts new session
         * @return bool successfully or not
         */

        private function startSession() {
            
            $this->hash = sha1(microtime() + rand(0,10000));
            $this->time = time();
            $this->expire = $this->time + $this->timeout;
            
            if (!setcookie("id", $this->hash, time() + $this->expire, "/")) { return false; }
            $_SERVER["HTTP_USER_AGENT"] = (isset($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : 'empty';
            insertArray("sessions", array(
                                        "logintime" => time(),
                                        "session_id" => $this->hash,
                                        "timeout" => (time() +60*60*24),
                                        "useragent" => $_SERVER["HTTP_USER_AGENT"],
                                        "ip" => $_SERVER["REMOTE_ADDR"],
                                        "hostname" => gethostbyaddr($_SERVER["REMOTE_ADDR"])
                                    ),"isisss");


            return true;
        }


        private function resumeSession() {
            
            if (strlen($this->hash) != 40) {
                return $this->startSession();
            }

            $this->hash = $this->hash;
            addWhere("session_id", "=", $this->hash);
            select("sessions", "*");

            $data = getRow();


            if (empty($data)) {
                return $this->startSession();
            }

            if ($data["timeout"] < time())
                return $this->startSession();

            $this->data = unserialize($data["vars"]);
            $this->hash = $data["session_id"];
            $this->userId = $data["user_id"];
            $this->expires = $data["timeout"];
            
            return true;
        }

        /**
         * stores data in the session
         * @param string $name identifier for the data
         * @param mixed $var data to store in the session
         */
        public function set($name,$var) {
            $this->data[$name] = $var;
        }

        /**
         * retrives stored data
         * @param string $name name of the datafield
         * @return mixed returns the requested data or null if not found
         */

        public function get($name) {
            if (isset($this->data[$name]))
                    return $this->data[$name];

            return null;
        }

        /**
         * ends session
         */

        public function destroySession() {
            
            if (isSet($GLOBALS['forum_integration']) && $GLOBALS['forum_integration'] != '') {
                $api = new \phpbb($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['forum_integration'], 'php');
                if ($api->user_logout() != 'SUCCESS') {
                    echo 'Logout im Forum fehlgeschlagen';
                }
            }

            setcookie("id",null,time() - 60*60*24*365*10);
            
            addWhere("session_id", "=", $this->hash,"s");
            delete("sessions");
        }

        public function  __destruct() {
             $data = serialize($this->data);
             \mys::getObj()->cleanup()->clearWhere();
             
             addWhere("session_id", "=", $this->hash);
             updateArray("sessions",array("timeout" => time()+ $this->timeout, "vars" => $data,"lastsite" => $_SERVER["REQUEST_URI"]),"iss");
        }

        public function getUserId() {
            return $this->userId; 
        }
        
        public function getFormatedEmail() {
            if ($this->formatedEmail !== null) {
                return $this->formatedEmail;
            }

            $res = \mys::getObj()->cleanup()->query("select name,email from user where id = {$this->userId} LIMIT 1");
            $data = $res->fetch_array();
            $res->close();
            return $this->formatedEmail = $data["name"] . '<' . $data["email"] . '>';
        }

        public function getUserEmail() {
            if ($this->userEmail !== '') {
                return $this->userEmail;
            }

            $res = \mys::getObj()->cleanup()->query("select email from user where id = {$this->userId} LIMIT 1");
            $data = $res->fetch_array();
            $res->close();
            return $this->userEmail = $data["email"];
        }

        public function getUserName() {
            if ($this->username !== '') {
                return $this->username;
            }

            $res = \mys::getObj()->cleanup()->query("select loginname from user where id = {$this->userId} LIMIT 1");
            $data = $res->fetch_array();
            $res->close();
            return $this->username = $data["loginname"];

        }

        public function getGroupId() {
            if ($this->groupId !== 0) {
                return $this->groupId;
            }
            $res = \mys::getObj()->cleanup()->query("select `group` from user where id = {$this->userId} LIMIT 1");
            $data = $res->fetch_array();
            $res->close();
            return $this->groupId = $data["group"];

        }

        public function setUserId($id) {
            if (!is_int($id)) return false;

            $this->userId = $id;

            addWhere("session_id", "=", $this->hash);
            updateArray("sessions",array("user_id" => $id),"i");

            return true;
        }


        public function getSessionId() {
            return $this->hash;
        }
        
        public function cleanupsessions() {
            \mys::getObj()->cleanup()->clearWhere()->query("DELETE FROM sessions where timeout < " . time());
        }

        static public function sessionExists() {
            return (self::$handle !== null);
        }

        public function getUserRow(){
                        $res = \mys::getObj()->cleanup()->query("select * from user where id = {$this->userId} LIMIT 1");
                        $data = $res->fetch_array();
                        $res->close();
                        return $data;
        }

        public function getAdminMenuRights(){
            if ($this->adminMenuRights === null) {
                $res = \mys::getObj()->cleanup()->query("select menuRights from userGroups where id = {$this->getGroupId()} LIMIT 1");
                $data = $res->fetch_array();
                $res->close();
                
                $this->adminMenuRights = \unserialize($data['menuRights']);
            }
            return $this->adminMenuRights;
        }
    }
?>
