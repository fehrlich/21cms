<?php

/**
 *
 * @author F.Ehrlich
 *
 */
class tpl {

	private $src;
	private $srcpfad;
	private $vars;
	public $htmlVars = array();
	private $container = array();
	public $adminjs = array();
	public $admincss = array();
	public $wenndann;
	public $addToolBar = false;

	function addCSS($css) {
		if (is_array($css)) {
			$this->admincss = array_merge($this->admincss, $css);
		} else {
			$this->admincss[] = $css;
		}
	}

	function addJS($js) {
		if (is_array($js)) {
			$this->adminjs = array_merge($this->adminjs, $js);
		} else {
			$this->adminjs[] = $js;
		}
	}

	private function addHeaderLine($line) {
		$this->src = str_replace('</head>', "\t" . $line . "\n" . '</head>', $this->src);
	}

	private function emailProtection() {
		if (preg_match_all('/href=\"mailto:(.*?)@(.*?)\"/', $this->src, $matches)) {
			foreach ($matches[0] as $i => $match) {

				$name = $matches[1][$i];
				$domain = $matches[2][$i];
				$subject = '';
				$body = '';
				$spl = explode('?', $domain);

				if (isSet($spl[1])) {
					$domain = $spl[0];
					$parameter = '';
					$spl2 = explode('&', $spl[1]);
					foreach ($spl2 as $s) {
						if (substr($s, 0, 8) == 'subject=')
							$subject = substr($s, 8);
						if (substr($s, 0, 5) == 'body=')
							$body = substr($s, 5);
					}
				}

				$this->src = str_replace($match, 'href="javascript:mt(\'' . $name . '\',\'' . $domain . '\',\'' . $subject . '\',\'' . $body . '\')"', $this->src);
			}
		}
	}

	private function setupMetaTags() {
		$mettags = "";
//          $mettags = "\t".'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>'."\n";
//          $mettags.= "\t".'<meta http-equiv="Content-Language" content="de" />'."\n";

//        if (!UserConfig::getObj()->getDisableMetatagPublisher()) $mettags.= "\t" . '<meta name="Publisher" content="21advertise.de" />' . "\n";
//        if (!UserConfig::getObj()->getDisableMetatagGenerator()) $mettags.= "\t" . '<meta name="Generator" content="21cms" />' . "\n";

//		$mettags.= "\t" . '<meta name="country" content="Germany" />' . "\n";



		if (isSet($GLOBALS['SEO_tags']))
			$mettags.= "\t" . '<meta name="keywords" content="' . strtolower(htmlspecialchars($GLOBALS['SEO_tags'], ENT_QUOTES, 'UTF-8')) . '" />' . "\n";
		if (isSet($GLOBALS['SEO_description']))
			$mettags.= "\t" . '<meta name="Description" content="' . htmlspecialchars($GLOBALS['SEO_description'], ENT_QUOTES, 'UTF-8') . '" />' . "\n";

		$this->src = str_replace('<head>', '<head>' . "\n" . $mettags, $this->src);
	}

	private function addRssFeed() {

		$sqlobj = mys::getObj()->cleanup();
		$res = $sqlobj->query("SELECT r.desc,r.name, d.`name` AS dbname FROM rss AS r INNER JOIN `databases` AS d ON r.`database` = d.id WHERE r.mainfeed = 1");
//          $rows = array();
//          if ($res !== false) {
//              while($row = $res->fetch_array())$rows[] = $row;
//	    }

		if ($res !== false) {
			while ($rssrow = $res->fetch_array()) {
				$src = '<link rel="alternate" type="application/rss+xml" title="' . htmlentities($rssrow['desc'], null, 'utf-8') . '" href="http://' . $_SERVER['HTTP_HOST'] . $GLOBALS['cms_roothtml'] . $rssrow['dbname']  . $rssrow['name'] . '.xml" />';
				$this->addHeaderLine($src);
			}
		}
	}

	private function checkCanonical() {
		if (!isSet($GLOBALS['SEO_canonical']) || $GLOBALS['SEO_canonical'] == '')
			return;
		$src = '<link rel="canonical" href="http://' . $_SERVER['HTTP_HOST'] . $GLOBALS['cms_roothtml'] . $GLOBALS['SEO_canonical'] . '.html" />';
		$this->addHeaderLine($src);
	}

	/**
	 * constructs a template element
	 * @param $srcpfad - Pfad zur index.html
	 * @return unknown_type
	 */
	function __construct($srcpfad = 'tpl/index.html', $scr = '') {
		$this->vars = array();
		$this->srcpfad = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $srcpfad;
		$this->src = $scr;
	}

	public function setSrcPath($srcpfad = 'tpl/index.html') {
		$this->srcpfad = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . $srcpfad;
	}

	public function setVar($varname, $content) {
		$this->vars[$varname] = $content;
	}

	public function getSrcString() {
		return $this->src;
	}

	/**
	 * gets the template source
	 */
	private function getSrc() {
		if (!file_exists($this->srcpfad)) {
			die("Unable to load tpl file: ".($this->srcpfad));
		}

		$this->src = file_get_contents($this->srcpfad);
	}

	public function setHtmlVar($name, $value) {
		$this->htmlVars[$name] = $value;
	}

	/**
	 * sets up global vars in html
	 */
	private function setVars() {

		$script = '<div id="vars">';
		if (isset($GLOBALS['seitenid']))
			$script .= '<div id="seitenid">' . $GLOBALS['seitenid'] . '</div>';
		$script .= '<div id="cmsroot">' . $GLOBALS['cms_roothtml'] . '</div>';
		$script .= '<div id="cmsLanguage">' . Languages::getLang(false) . '</div>';

		foreach ($this->htmlVars as $name => $value) {
			$script .= '<div id="' . $name . '">' . $value . '</div>';
		}

		$script .= '<div id="maxWidth">' . UserConfig::getObj()->getMainContentWidth() . '</div>';
		$script .= '</div>';
		$this->src = str_replace('</body>', $script . '</body>', $this->src);
	}

	/**
	 * gets the CVARS from template
	 * @param string $src template source
	 */
	public static function getCVARS($src = false) {
		if (!$src) {
			$srcpfad = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'tpl/index.html';
			$src = file_get_contents($srcpfad);
		}

		if (preg_match_all('/\[CVAR\((.*?)\)\]/', $src, $matches)) {

			foreach ($matches[1] as $i => $match) {

				$spl = explode(',', $match);
				$name = $spl[0];
				$typ = (isset($spl[1])) ? $spl[1] : 'text';
				$lokal = (isset($spl[2])) ? (boolean) $spl[2] : false;
				$dat = (isset($spl[3])) ? $spl[3] : '';
				$val = (isset($spl[4])) ? $spl[4] : '';
				$dat = explode('|', $dat);
				$newdat = array();
				foreach ($dat as $d) {
					$newdat[$d] = $d;
				}

				if (!isset($GLOBALS['CVARS'][$name])) {
					$GLOBALS['CVARS'][$name] = array();
					$GLOBALS['CVARS'][$name]['value'] = $val;
				}
				$GLOBALS['CVARS'][$name]['typ'] = $typ;
				$GLOBALS['CVARS'][$name]['lokal'] = $lokal;
				$GLOBALS['CVARS'][$name]['data'] = $newdat;
			}
		}
	}

	public function parseLinks() {
		$langLink = Languages::getLang();

        if ( $langLink != "") {
            $langLink .= "/";
        }

		$admin = (isAdmin()) ? "admin/" : '';
		if (preg_match_all('/\[LINK\((.*?)\)\]/', $this->src, $matches)) {
			foreach ($matches[1] as $name) {
				$this->src = str_replace('[LINK(' . $name . ')]', $GLOBALS['cms_roothtml'] . $admin . $langLink  . str_replace(' ', '%20', $name) . '.html', $this->src);
			}
		}
		if (preg_match_all('/\[LINKF\((.*?)\)\]/', $this->src, $matches)) {
			foreach ($matches[1] as $name) {
				$this->src = str_replace('[LINKF(' . $name . ')]', str_replace('//', '/', $GLOBALS['cms_roothtml'] . str_replace(' ', '%20', $name)), $this->src);
			}
		}
		if (preg_match_all('/\[LINKR\((.*?)\)\]/', $this->src, $matches)) {
			foreach ($matches[1] as $name) {
				$this->src = str_replace('[LINKR(' . $name . ')]', $GLOBALS['cms_roothtml'] . $admin . $langLink  . str_replace(' ', '%20', $name) . '.xml', $this->src);
			}
		}
		if (preg_match_all('/\[LINKA\((.*?)\)\]/', $langLink  . $this->src, $matches)) {
			foreach ($matches[1] as $name) {
				$add = isAdmin() ? 'admin/' : '';
				$this->src = str_replace('[LINKA(' . $name . ')]', $GLOBALS['cms_roothtml'] . $admin . $add . $langLink  . str_replace(' ', '%20', $name) . '.html', $this->src);
			}
		}
		if (preg_match_all('/\[HTTPLINK\((.*?)\)\]/', $this->src, $matches)) {
			foreach ($matches[0] as $i => $match) {
				$link = $matches[1][$i];
				if(substr($link,0,4) != 'http')
					$link = 'http://'.$link;
				$this->src = str_replace($match, $link, $this->src);
			}
		}

        if ( Languages::getLang() == "" || !isset($GLOBALS["languages"][strtolower(Languages::getLang())]) ) {
            $this->src = str_replace('[LANG]', "Deutsch", $this->src);
            $this->src = str_replace('[LANGSHORT]', "de", $this->src);
        } else {
            $this->src = str_replace('[LANG]', $GLOBALS["languages"][strtolower(Languages::getLang())], $this->src);
            $this->src = str_replace('[LANGSHORT]', strtolower(Languages::getLang()), $this->src);
        }


	}

	private function parseGlobalVars() {

        if (isset($_GET['tag']) && $_GET['tag'] != '') {
			$this->src = str_replace('[TAG]', str_replace('_', ' ', $GLOBALS['akt_tag']['title']), $this->src);
		} else {
			$this->src = str_replace('[TAG]', '', $this->src);
		}

        if ( !isSet($GLOBALS['SITE_TITLE'])) {
			$GLOBALS['SITE_TITLE'] = '';
        }

		$this->src = str_replace('[TITLE]', $GLOBALS['SITE_TITLE'] ?: "Home", $this->src);

                if (isSet($GLOBALS['akt_menuepunkt'])) {
                    $this->src = str_replace('[MENUNAME]', $GLOBALS['akt_menuepunkt']['title'], $this->src);

                    $id =  ($GLOBALS['akt_menuepunkt']['parent'] == -1 || $GLOBALS['akt_menuepunkt']['parent'] == 0)
                                ? $GLOBALS['akt_menuepunkt']['id'] : $GLOBALS['akt_menuepunkt']['parent'];
                    $this->src = str_replace('[MAINMENUNAME]', menu::getName($id),$this->src);
                    $this->src = str_replace('[PATHIDS]', 'i'.implode(' i',$GLOBALS['aktmenu']),$this->src);
                    $this->src = str_replace('[MAINMENUID]', $id,$this->src);
                }



		$this->src = str_replace(array('<h1></h1>', '<h1> 	</h1>', '<p></p>'), '', $this->src);
		$parser = new Parser();
		$this->src = $parser->parseTxt($this->src);

                if(isSet($GLOBALS['currentDataset']) && is_array($GLOBALS['currentDataset'])){
                    $dbc = $GLOBALS['currentDBC'];
                    $this->src = $dbc->parseData($this->src, $GLOBALS['currentDataset']);
                }else{
                    $this->src = DatabaseConnector::cleanTemplateVars($this->src);
                }

                if (preg_match_all('#\[IF\((.*?)\)\](.*?)?(\[ELSE\](.*?)?)?\[/IF\]#si', $this->src, $matches)) { //|\[IFEND\]
			foreach ($matches[0] as $i => $match) {

				$condString = $matches[1][$i];
				$condBool = true;
				$and = true;
				if(strpos($condString, '&') !== false){
					if(strpos($condString, '&amp;&amp;')) $str = '&amp;&amp;';
					elseif(strpos($condString, '&&')) $str = '&&';
					elseif(strpos($condString, '&amp;')) $str = '&amp;';
					else $str = '&';

					$conditons = explode($str,$condString);
				}
				elseif(strpos($condString, '|') !== false){
					if(strpos($condString, '||')) $str = '||';
					elseif(strpos($condString, '|')) $str = '|';

					$conditons = explode($str,$condString);
					$and = false;
					$condBool = false;
				}
				else $conditons = array($condString);

				foreach($conditons as $condString){
					$condString = trim($condString);
					if(strpos($condString, '==') !== false){
						$spl = explode('==',$condString);
						if(trim($spl[0]) == trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}
					elseif(strpos($condString, '!=') !== false){
						$spl = explode('!=',$condString);
						if(trim($spl[0]) != trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}
					elseif(strpos($condString, '=') !== false){
						$spl = explode('=',$condString);
						if(trim($spl[0]) == trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}elseif($condString != '')
						$condBoolTmp = true;
					else
						$condBoolTmp = false;

					if($and) $condBool = $condBool && $condBoolTmp;
					else $condBool = $condBool || $condBoolTmp;
				}
//				var_dump($condition);
				$then = $matches[2][$i];
				$else = $matches[4][$i];
				if ($condBool)
					$this->src = str_replace($match, $then, $this->src);
				else
					$this->src = str_replace($match, $else, $this->src);
			}
		}
                $this->src = str_replace('[FS_USER]', '',$this->src);
	}

	private function parseVars() {
		foreach ($this->vars as $name => $cont) {
			$this->src = str_replace(array('[' . $name . ']'), $cont, $this->src);
		}
	}

	/**
	 * parse template and creates contents
	 * @param boolean $admin admininteface
	 * @return Container Array
	 */
	public function preParse($admin = false, $webtpl = false) {
		if (!isSet($GLOBALS['akt_menuepunkt']) || $GLOBALS['akt_menuepunkt']['verlinkung'] != 'ajaxLink') {
			$this->getSrc();
		} else {
			$this->src = '[CONTAINER(2)]';
			if (isAdmin())
				$this->src .= '<div id="ajaxid" style="display:none">' . $GLOBALS['akt_menuepunkt']['id'] . '</div>';
		}

		$this->src = str_replace(
                    array('href="#', 'src="/', 'href="/', 'rel="/', 'href="http://', 'src="http://', 'rel="http://', 'href="https://', 'src="https://', 'rel="https://'), 
                    array('href"#' , 'src"/', 'href"/', 'rel"/', 'href"http://', 'src"http://', 'rel"http://', 'href"https://', 'src"https://', 'rel"https://'), 
                    $this->src
                );
		$this->src = str_replace('src="', 'src="' . $GLOBALS['cms_roothtml'] . 'tpl/', $this->src);
		$this->src = str_replace('href="', 'href="' . $GLOBALS['cms_roothtml'] . 'tpl/', $this->src);
		$this->src = str_replace(array('src"', 'href"', 'rel"'), array('src="', 'href="', 'rel="'), $this->src);

		$this->addRssFeed();

		foreach ($this->wenndann as $bed => $value) {
			if ($value) {
				$this->src = str_replace(array('[ON' . $bed . ']', '[/ON' . $bed . ']'), '', $this->src);
				$this->src = preg_replace('#\[NOTON' . $bed . '\](.*?)\[/NOTON' . $bed . '\]#mis', '', $this->src);
			} else {
				$this->src = preg_replace('#\[ON' . $bed . '\](.*?)\[/ON' . $bed . '\]#mis', '', $this->src);
				$this->src = str_replace(array('[NOTON' . $bed . ']', '[/NOTON' . $bed . ']'), '', $this->src);
			}
		}


		if (preg_match_all('/\[MAINCONTAINER\]/', $this->src, $matches)) {
			$tplName = 'none';

			if (isSet($GLOBALS['akt_menuepunkt']['userTpl'])) {
				$tplName = $GLOBALS['akt_menuepunkt']['userTpl'];
			} else {
				$tplName = current(UserConfig::getObj()->getUserTemplate());
				$tplName = $tplName['name'];
			}

			$this->src = str_replace('[MAINCONTAINER]', UserConfig::getObj()->getUserTemplateHtml($tplName), $this->src);
		}
		tpl::getCVARS($this->src);
		$this->parseVars();

		if (preg_match_all('/\[CONTAINER\((.*?)\)\]/', $this->src, $matches)) {
			addWhere('klasse', '=', 'container');
			addWhereBracket('(');
			addWhere('seitenid', '=', $GLOBALS['seitenid'], 'i');
			addWhere('seitenid', '=', '', 's', 'OR');
			addWhereBracket(')');
			select('elements', 'id,seitenid,eldata,container');
			$cont_data = getRows();
			foreach ($cont_data as $i => $v) {
				if (!isSet($contData[$v['container']]) || $v['seitenid'] != '0')
					$contData[$v['container']] = $v;
			}

			foreach ($matches[1] as $name) {
				// Container erstellen und inhalte holen
				$realid = 0;

				if (isset($contData[$name])) {
					$data = $contData[$name];
					$realid = $data['id'];
				} else {
					$data = array();
				}

				$cont = new container($name, $data, $realid);
				$cont->container = $name;
				$this->src = str_replace('[CONTAINER(' . $name . ')]', $cont->getContent(), $this->src);
			}

			unset($cont);
		}


		if (preg_match_all('/\[EL_(.*?)\((.*?)\)\]/', $this->src, $matches)) {
			foreach ($matches[0] as $i => $match) {

				//Container erstellen und inhalte holen
				$name = $matches[1][$i];
				$spl = explode(',', $matches[2][$i]);
				$id = $spl[0];
				if (isset($spl[1])) {
					$html = '';
					if ($admin) {
						addWhereprepare('container = ? AND ((visible = 2 AND dontShowOn NOT LIKE ?) OR seitenid = ? OR (visible = 3 AND visible_main IN (' . implode(',', $GLOBALS['aktmenu']) . ')))', array('el' . $id,'%,'.$GLOBALS['seitenid'].',%', $GLOBALS['seitenid']), 'ssi');
					} else {
						addWhereprepare('container = ? AND ((visible > 0 AND seitenid = ?) OR (visible = 2 AND dontShowOn NOT LIKE ?) OR (visible = 3 AND visible_main IN (' . implode(',', $GLOBALS['aktmenu']) . ')))', array('el' . $id, $GLOBALS['seitenid'],'%,'.$GLOBALS['seitenid'].',%'), 'ssi');
					}

					select('elements', 'id,eldata');
					$rows = getRows();
					foreach ($rows as $row) {
						$data = unserialize($row['eldata']);
						$el = new $name($row['id'], $data, '', 0, false);
						$html .= $el->getContent();
					}

					if ($html == '') {
						//DEBUGEN WEGEN CONTAINER
						$el = new $name(0, array(), 'el' . $id, 0, false);
						$html .= $el->getContent();
					}

					$this->src = str_replace('[EL_' . $name . '(' . $matches[2][$i] . ')]', $html, $this->src);
				} else {
					$html = '';
					//TODO: Ein Select fuer alle Elemente
					$GLOBALS['cmsdb']->clearWhere();
					addWhere('id', '=', $id);
					select('elements', 'eldata');
					$row = getRow();
					$data = unserialize($row['eldata']);
					$el = new $name($id, $data, '', 0, false);
					$html .= $el->getContent();
					$this->src = str_replace('[EL_' . $name . '(' . $matches[2][$i] . ')]', $html, $this->src);
				}
			}

			unset($el);
		}


		foreach ($GLOBALS['CVARS'] as $name => $var) {
			$this->src = preg_replace('#\[CVAR\(' . $name . '(.*?)\)\]#', $var['value'], $this->src);
		}

		/*
		 * Javascript
		 */
		if (isSet($GLOBALS['includeJs'])) {
			$script = '';
			foreach ($GLOBALS['includeJs'] as $name) {
				$script .= '<script src="' . $GLOBALS['cms_roothtml'] . $name . '" type="text/javascript"></script>';
			}
			$this->src = str_replace('</body>', $script . '</body>', $this->src);
		}
//			var_dump($_GET);
			if(isSet($_GET['para']) && $_GET['para'] == 'print'){
				$script = '<link rel="stylesheet" href="' . $GLOBALS['cms_roothtml'] . 'tpl/print.css" type="text/css" media="all" />';
				$this->src = str_replace('</head>', $script . '</head>', $this->src);
				$GLOBALS['SEO_canonical'] = substr(str_replace('-P_print', '', $_SERVER['REQUEST_URI']),1,-5);
				$this->src = str_replace('<body>',  '<body><script>window.print()</script>', $this->src);
			}
		if ($admin) {
			$this->setVars();
			$script = '';
			foreach ($this->adminjs as $pfad => $name) {
				if (substr($name, 0, 4) == 'http')
					$script .= '<script src="' . $name . '" type="text/javascript"></script>' . "\n";
				elseif (is_int($pfad))
					$script .= '<script src="' . $GLOBALS['cms_roothtml'] . 'admin/js/' . $name . '.js" type="text/javascript"></script>' . "\n";
				else
					$script .= '<script src="' . $GLOBALS['cms_roothtml'] . $pfad . $name . '.js" type="text/javascript"></script>' . "\n";
			}

			foreach ($GLOBALS['jsInclude'] as $name) {
				$script .= '<script src="' . $GLOBALS['cms_roothtml'] . 'js/' . $name . '" type="text/javascript"></script>' . "\n";
			}


			$this->src = str_replace('</body>', $script . '</body>', $this->src);
			$script = '';

			foreach ($this->admincss as $pfad => $name) {
				if (is_int($pfad)){
					$script .= '<link rel="stylesheet" href="' . $GLOBALS['cms_roothtml'] . 'admin/css/' . $name . '.css" type="text/css" />';
				}else
					$script .= '<link rel="stylesheet" href="' . $GLOBALS['cms_roothtml'] . $pfad . $name . '.css" type="text/css" />';
			}

			$this->addHeaderLine($script);
		} else {
			$script = '';
			foreach ($GLOBALS['jsInclude'] as $name) {
				$script .= '<script src="' . $GLOBALS['cms_roothtml'] . 'js/' . $name . '" type="text/javascript"></script>';
			}

			$this->src = str_replace('</body>', $script . '</body>', $this->src);
		}


		//load stats tracking
		if ($GLOBALS["piwik_siteid"] && $GLOBALS["piwik_url"] > 0 && !isAdmin()) {
			$code = '
			<script type="text/javascript">
			  var _paq = _paq || [];
			  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
			  _paq.push([\'trackPageView\']);
			  _paq.push([\'enableLinkTracking\']);
			  (function() {
				var u="//analytics.esichain.de/";
				_paq.push([\'setTrackerUrl\', u+\'piwik.php\']);
				_paq.push([\'setSiteId\', \'4\']);
				var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];
				g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'piwik.js\'; s.parentNode.insertBefore(g,s);
			  })();
			</script>';
			$this->src = str_replace('</body>', $code . '</body>', $this->src);
		}

		$this->parseGlobalVars();

		if ($this->addToolBar && $admin)
			$this->src = preg_replace('~<body(.*)?>~sU', '<body $1><div id="staticToolbox"></div><div id="admintoolbox"><div id="adminMenuTop">' . buildToolbar() . '</div></div><div id="websiteContent">', $this->src);
		if ($this->addToolBar && $admin)
			$this->src = preg_replace('~<body(.*)?>~sU', '<body $1><div id="debug">[DEBUG]</div></div>', $this->src);


		$this->parseLinks();
		if(UserConfig::getObj()->getJavascriptEmailProtection())
			$this->emailProtection();
		$this->checkCanonical();
		$this->setupMetaTags();
                $this->src = str_replace('[USER]', \cms\session::getObj()->getUserName(), $this->src);
                $this->src = str_replace('[BREADCRUMBS]', getAdminBreadCrumb(), $this->src);
		return $this->src;
	}

	public function __toString() {
		if ($this->src == '')
			$this->getSrc();
		$this->parseVars();

		return $this->src;
	}

}

?>
