<?php
/**
 * Creates an Connecter to interact with database templates
 *
 * @author F.Ehrlich
 */
class DataTemplate{
	private $id;
	private $dbid;
//	private $dataid;
	/**
	 * @var DataEditor
	 */
	private $datasys;

	/**
	 * Creates an Connecter to interact with database templates
	 * @param int $dbid database id
	 * @param int $id template id
	 */
	public function __construct($dbid = 0, $id = 0){ //, $dataid = 0
		if($dbid == 0 && isset($_GET['db'])) $dbid = $_GET['db'];
		$this->id = $id;
		$this->dbid = $dbid;
//		$this->dataid = $dataid;
		addWhere('visible', '!=', 'a');
		select('menuepunkte','title_intern');
		$mps = getRows();
		$m = array();
		$m[''] = '[Kein Link]';
		foreach($mps as $mp){
			$m[$mp['title_intern']] = $mp['title_intern'];
		}
		$this->datasys = new DataEditor('dataTemplates');
        $this->datasys->setInvisible("html");
		$this->datasys->form->addElement('Name', 'name', 'text');
		$this->datasys->form->addElement('Bild', 'image', 'datei');
		$this->datasys->form->addElement('Html', 'html', FormType::CODEEDITOR);
		$this->datasys->form->addElement('Als Vorschaulink anzeigen auf', 'preview', 'select', '',$m);

		if($dbid == 0){
			select('databases','id,name');
			$d = getRows();
			$data = mysql2selectArray($d);
			$this->datasys->form->addElement('Datenbank', 'dbid', 'select', '', $data);
		}
		else $this->datasys->form->addElement('d', 'dbid', 'hidden',$dbid);
        $this->datasys->form->setMultiLanguage(array('html'));
		if(!isset($_GET['inter']) || $_GET['inter'] != 'dia')
			$this->datasys->form->setSaveButton('Speichern');
	}

	public function getData(){
		if($this->dbid != 0){
			addWhere('dbid', '=', $this->dbid,'i');
		}
		return $this->datasys->getData();
	}

	public function htmlOverview(){
//		addWhere('type', '!=', 'pdf'); //Needs NOT NULL
		return $this->datasys->getHtml();
	}

	public function  __toString() {
        //if it crashes here, check the language columns in the database, most likley this is the error
        return $this->htmlOverview();
	}
}
?>
