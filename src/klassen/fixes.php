<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fixes
 *
 * @author F.Ehrlich
 */
class fixes {
    //put your code here


	public function rewriteFiles($dir){
	    $files = glob($dir . '*');
	    if(is_array($files)){
		foreach ($files as $file){
		    $isDir = is_dir($file);
		    if($isDir)
			$this->rewriteFiles ($file.'/');

		    \cms\file::load($file,true);
		}
	    }
	}
	private function fileSystem(){
	    $dir = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].'dateien/';
	    $this->rewriteFiles($dir);
	}
	private function createPublished(){
		select('databases','name,fieldid');
		$dbs = getRows();
		$mysql = mys::getObj();
		$html = '';
		foreach($dbs as $db){
//			$name = $db['']
			$sql = 'SHOW COLUMNS FROM `data_'.$db['name'].'` WHERE Field = "published"';
			$q = $mysql->query($sql);
			if($q->num_rows == 0){
				$sql = 'ALTER TABLE `data_'.$db['name'].'`	ADD COLUMN `published` INT(11) NOT NULL AFTER `erstellt`';
				$html .= 'Published added to:'.$db['name'];
				$mysql->query($sql);
			}
			$sql = 'UPDATE TABLE `data_'.$db['name'].'`	SET `published` = `erstellt` WHERE public = 1 AND published = 0';
			$mysql->query($sql);
		}
		return $html;
	}

	private function rewriteTitle(){
		mys::getObj()->clearWhere();
		select('databases','name,fieldid');
		$dbs = getRows();
//		$dbs[] = array('name' => 'menuepunkte');
		$count = 0;
		foreach($dbs as $db){
			$mysqladd = str_replace(' ', ',', $db['fieldid']);
			select('data_'.$db['name'],'id,title_intern,'.$mysqladd);
			$datas = array();
//			echo $mysqladd.':';
			while($row = getRow()){
				$spl = explode(',', $mysqladd);
				$fieldid = '';
				foreach($spl as $eig){$fieldid .= ' '.$row[$eig];}
				$fieldid = substr($fieldid, 1);
//				echo $fieldid.'!';
				$newtitle = menuepunkte::setTitelIntern($fieldid);
				if($newtitle != $row['title_intern']){
					$data['title_intern'] = $newtitle;
					$datas[$row['id']] = $data;
					$count++;
				}
			}
			foreach($datas as $id => $data){
				addWhere('id', '=', $id,'i');
				updateArray('data_'.$db['name'], $data,'s');
				$count++;
			}
		}


//		echo "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>Go:";
		updateArray('menuepunkte', array('title_intern' => ''),'s');
		addJoin('menuepunkte', 'parent', 'id', 'title', 'left', 'parentname');
		select('menuepunkte','id,title,title_intern','ordernr');
		$mps = getRows();
		$datas = array();
		foreach($mps  as $row){
			if($row['parentname'] != ''){
				$altname = menuepunkte::setTitelIntern($row['parentname']).'_'.menuepunkte::setTitelIntern($row['title']);
			}else $altname = '';
			$newtitle = menuepunkte::setTitelIntern($row['title'],'menuepunkte',$row['id'],$altname);
//			echo ':'.$newtitle.'<br />';
			if($newtitle != $row['title_intern']){
				$data['title_intern'] = $newtitle;
				$datas[$row['id']] = $data;
				$count++;
				addWhere('id', '=', $row['id'],'i');
				updateArray('menuepunkte', $data,'s');
			}
		}
		foreach($datas as $id => $data){
			$count++;
		}
		select('tags','id,title,title_intern');
		$datas = array();
		while($row = getRow()){
			$newtitle = menuepunkte::setTitelIntern($row['title']);
			if($newtitle != $row['title_intern']){
				$data['title_intern'] = $newtitle;
				$datas[$row['id']] = $data;
				$count++;
			}
		}
		foreach($datas as $id => $data){
			addWhere('id', '=', $id,'i');
			updateArray('tags', $data,'s');
			$count++;
		}
		return $count.' wurden geupdatet!';
	}

	private function rewriteTags(){
//		addJoin('tags', 'parent', 'maintagName','title_intern');
		addJoin('tags','parent','id','title','inner');
		select('tags','id,title_intern','t.ordernr');
		$tags = getRows();
		foreach($tags as $tag){
//			var_dump($tag);
			$title_int = menuepunkte::setTitelIntern($tag['title_intern'], 'tags', $tag['id'], $tag['title'].'_'.$tag['title_intern']);
			if($title_int != $tag['title_intern']){
				echo $tag['title_intern'].'=='.$title_int.'<br>';
				$upd = array();
				$upd['title_intern'] = $title_int;
				addWhere('id', '=', $tag['id']);
				updateArray('tags', $upd,'s');
			}
		}
		return '
			';
	}
	private function TagsTwentyone(){
//		addJoin('tags', 'parent', 'maintagName','title_intern');
//		addJoin('tags','parent','id','title','inner');
		select('tags2','id,title_intern,title,prio,parent,ordernr,level,visible,erstellt,showin');
		$tags = getRows();
		select('databases');
		$databases = getRows();
		$x = 0;
		$newTags = array();
		foreach($tags as $tag){
//			if($tag['id'] > 9){
				$new_title = menuepunkte::setTitelIntern($tag['title']);
				$newprio = $tag['prio'];
				if(isSet($newTags[$new_title])){
					if($newTags[$new_title]['prio'] > $tag['prio'])
						$newprio = $newTags[$new_title]['prio'];
				}
				$newTags[$new_title] = array(
					'title_intern' => $new_title,
					'title' => $tag['title'],
					'showin' => $tag['showin'],
					'erstellt' => $tag['erstellt'],
					'visible' => $tag['visible'],
					'level' => '0',
					'ordernr' => $x,
					'prio' => $newprio
				);
	//			foreach($databases as $db){
	//				$dbname = 'data_'.$db['name'];
	//				echo $dbname.'!';
	//				$fields = unserialize($db['fields']);
	//				if(in_array('Tags', $fields['fieldname'])){
	//					addWhere('id,Tags', 'LIKE', '%,'.$tag['title_intern']."',%");
	//					select($dbname,'id,Tags');
	//					$items = getRows();
	//					foreach($items as $item){
	//						$newTags = str_replace(','.$tag['title_intern'].',', ','.$new_title.',', $item['Tags']);
	//						addWhere('id', '=', $item['id']);
	//						updateArray($dbname, array(
	//							'Tags' => $newTags
	//						), 's');
	//
	//					}
	//				}
	//			}
				$x++;
//			}
		}
		addWhere('id', '>', '0');
		delete('tags');
		foreach($newTags as $t){

			insertArray('tags', $t, 'sssiiiii');
		}
		return '';
	}

    private function setTokensOnAddressesWithoutTokens() {
        mys::clearWhere();
        $res = mys::getObj()->query('SELECT id FROM addressUser WHERE token = "" OR token IS NULL');

        while($result = mysqli_fetch_assoc($res)) {
            mys::clearWhere();
            addWhere("id","=",$result["id"]);
            updateArray("addressUser", array("Token" => sha1(microtime(1) . rand(0,9999))),"s");
        }
    }

	public function  __toString() {
		$html = '<h1>Fixes</h1>';

		if(isset($_POST)){
			foreach($_POST as $k => $p){
				if(substr($k, 0, 3) == 'do_'){
					$f = substr($k,3);
					if(method_exists($this, $f))
						$html .= $this->{$f()};
				}
			}
		}
		$form = new formular();
		$form->addElement('Submit', 'Datenbanken, Tags und Menue title_intern neu erstellen','submit');
		$html .= '<form action="'.$_SERVER['PHP_SELF'].'?mm=fixes" method="post">';
		$html .= '<input type="submit" name="do_rewriteTitle" value="Datenbanken und Menue title_intern neu erstellen" /><br />';
		$html .= '<input type="submit" name="do_rewriteTags" value="Tags title_intern neu erstellen" /><br />';
		$html .= '<input type="submit" name="do_createPublished" value="PublishedDatum anlegen (<15.09.10)" /><br />';
		$html .= '<input type="submit" name="do_fileSystem" value="DateiSystem und DB abgleichen" /><br />';
		$html .= '<input type="submit" name="do_TagsTwentyone" value="Tags (Twentyone) fix" /><br />';
		$html .= '<input type="submit" name="do_setTokensOnAddressesWithoutTokens" value="fix empty tokens on addresses" /><br />';
		$html .= '</form>';

		return $html;
	}
}
?>
