<?php

/**
 * Creates an Connecter to interact with database templates
 *
 * @author F.Ehrlich
 */
require('fPdf/fpdf.php');

class PdfTemplate {

	private $id;
	private $dbid;
//	private $dataid;
	/**
	 * @var DataEditor
	 */
	private $datasys;

	/**
	 * Creates an Connecter to interact with database templates
	 * @param int $dbid database id
	 * @param int $id template id
	 */
	public function __construct($dbid = 0, $id = 0) { //, $dataid = 0
		if ($dbid == 0 && isset($_GET['db']))
			$dbid = $_GET['db'];
		$this->id = $id;
		$this->dbid = $dbid;

		$this->datasys = new DataEditor('dataTemplates');
		$this->datasys->form->addElement('Name', 'name', 'text');
		$this->datasys->form->addElement('Bild', 'image', 'datei');
		$this->datasys->form->addElement('Datenbank', 'dbId', 'database');
		$this->datasys->form->addElement('type', 'type', FormType::MYSQLONLY, 'pdf');
		$this->datasys->form->addElement('Pdf', 'html', FormType::GRIDSYSTEM, '', array('gridSizeX' => 10, 'gridSizeY' => 10));
		$this->datasys->setInvisible('html');
		if (isSet($_GET['d']) && $_GET['d'] != '') {
			PdfTemplate::generatePdf(5);
		}
	}

	public function getData() {
		if ($this->dbid != 0) {
			addWhere('dbid', '=', $this->dbid, 'i');
		}
		return $this->datasys->getData();
	}

	public function htmlOverview() {
		addWhere('type', '=', 'pdf');
		$this->datasys->getData();
		return (string) $this->datasys;
//		return '<div class="gridBuild"></div>';
	}

	public function __toString() {
		return $this->htmlOverview();
	}

	/**
	 *
	 * @param <type> $tplId
	 * @param Parser $parser
	 * @param <type> $name
	 * @param <type> $dest
	 * @return file File handle
	 */
	static public function generatePdf($tplId, $parser = null, $name = '', $dest = '') {
		$name = $parser->parseTxt($name);
		addWhere('id', '=', $tplId);
		select('dataTemplates');
		$row = getRow();
		$elements = unserialize($row['html']);
		$faktor = 5;
		$pdf = new FPDF_BBCODE();
		$pdf->AddPage();
		$pdf->SetFont('Arial', '',8);
		$pdf->SetMargins(0, 0);
		$pdf->pixelFaktor = $faktor;
//	$pdf->SetAutoPageBreak(false);
		foreach ($elements as $el) {
			$rawData = $el['data'];
			if (substr($el['left'], -2) == 'px')
				$el['left'] = substr($el['left'], 0, -2);
			if (substr($el['top'], -2) == 'px')
				$el['top'] = substr($el['top'], 0, -2);
			$top = round($el['top'] / ($faktor));
			$pdf->SetXY(round($el['left'] / $faktor), $top);
			$maxCellHeight = round($el['height'] / $faktor);
			$pdf->setCellDimensions(round($el['width'] / $faktor), round($el['height'] / $faktor));
			if (isSet($parser))
				$el['data'] = $parser->parseTxt($el['data']);
			$pdf->BBCode(strip_tags($el['data']));
			$diff = ($pdf->GetY()-$top)-$maxCellHeight;
			if($diff > 5){
				$error = array();
				if (preg_match_all('/\[FORM_(.*?)\]/', $rawData, $matches)) {
					foreach ($matches[1] as $i => $match) {
						$error[] = $match;
					}
				}
				if(count($error) > 0){
					$pdf->Close();
					return $error;
				}
			}
//			echo $diff.'!!';
			
		}
		$pdf->Output($name, $dest);
		$file = \cms\file::load($name, true);
		return $file;
	}
}

class FPDF_BBCODE extends FPDF {

	var $pixelFaktor = 1;
	var $ubb_x;
	var $ubb_y;
	var $last_x;
	var $last_y;
	var $last_h_text;
	var $last_h_bild;
	var $font_size;
	var $defaultColor;
	var $defaultSize;
	var $cellWidth;
	var $cellHeight;
	var $lastPattern;

	public function __construct() {
		$this->lastPattern = '';
		parent::__construct();
	}

	function BBCode($str) {
		if (!$this->ubb_x)
			$this->ubb_x = $this->x;
		if (!$this->ubb_y)
			$this->ubb_y = $this->y;

		if (!$this->last_x)
			$this->last_x = $this->x;
		if (!$this->last_y)
			$this->last_y = $this->y;

		if (!$this->last_h_text)
			$this->last_h_text = 0;
		if (!$this->last_h_bild)
			$this->last_h_bild = 0;

		if (!$this->font_size)
			$this->font_size = 8;
		if (!$this->defaultColor)
			$this->defaultColor = array(0, 0, 0);
		if (!$this->defaultSize)
			$this->defaultSize = 8;

		$this->parseString($str);
	}

	function parseTagsRecursive($input) {
		$regex = '#\[(\w*)(=.[^\]]*)?]((?:[^[]|\[(?!/?.*?])|(?R))*)\[/(.[^\]]*?)]#';
		if (is_array($input)) {
			if ($input[2] != '')
				$input[2] = substr($input[2], 1);
			$inputAdapter = array();
			for ($i = 2; $i < count($input); $i++) {
				$inputAdapter[$i - 2] = $input[$i];
			}
			$input[1] = strtolower($input[1]);
			if ($input[1] == 'img')
				return $this->BBCode_img($inputAdapter);
			elseif ($input[1] == 'size')
				$this->BBCode_size($inputAdapter);
			elseif ($input[1] == 'color')
				$this->BBCode_color($inputAdapter);
			elseif ($input[1] == 'b') {
				$this->BBCode_bold($inputAdapter);
			} elseif ($input[1] == 'br')
				$this->BBCode_br($inputAdapter);
			elseif ($input[1] == 'i')
				$this->BBCode_italic($inputAdapter);
			elseif ($input[1] == 'u')
				$this->BBCode_underline($inputAdapter);
			elseif ($input[1] == 'text') {
				$this->BBCode_text($inputAdapter);
			}
			if (preg_match($regex, $input[3]) == 0) {
				$this->BBCode_text($inputAdapter);
				return '';
			}
			$oldStr = $input[0];
			$input = $input[3];
		} else {
			if (!preg_match($regex, $input)) {
				$this->BBCode_text(array(1 => $input));
				return '';
			}
		}
		$nextTag = strpos($input, '[');
		if ($nextTag > 0) {
			$this->BBCode_text(array(1 => substr($input, 0, $nextTag)));
			$rest = preg_replace_callback($regex, array(&$this, 'parseTagsRecursive'), substr($input, $nextTag), 1);
		} else {
			$rest = preg_replace_callback($regex, array(&$this, 'parseTagsRecursive'), $input, 1);
		}
		if (strlen($this->stripLineBreaks($rest)) > 0) {
			$this->parseTagsRecursive($rest);
		}
		return '';
	}

	function parseString($str) {
		$str = str_replace(array("\r\n", "\r", "\n", "[BR][/BR]", "[BR]"), 'LINEBREAK', $str);
		$output = $this->parseTagsRecursive($str);
		return;
	}

	function stripLineBreaks($str) {
		return str_replace('LINEBREAK', '', $str);
	}

	function BBCode_br() {
		$this->SetXY($this->last_x, $this->GetY());
	}

	function BBCode_img($str) {
		$file = $_SERVER['DOCUMENT_ROOT'] . $str[1];
		if (!file_exists($file) || !is_file($file)) {
			return;
		}
		$size = @GetImageSize($file);
                $twidth = $size[0] / $this->pixelFaktor;
                $theight = $size[1] / $this->pixelFaktor;
                if($twidth > $this->cellWidth){
                    $width = $this->cellWidth;
                    $height = $theight * ($width/$twidth);
                }else{
                    $width = $twidth;
                    $height = $theight;
                }
                if($height > $this->cellHeight){
                    $twidth = $width;
                    $theight = $height;
                    $height = $this->cellHeight;
                    $width = $twidth * ($height/$theight);
                }
		$this->Image($file, $this->ubb_x, $this->ubb_y, $width, $height);

		$this->ubb_x += ( $size[0] / 72 * 25.4) / $this->pixelFaktor;
		$this->SetXY($this->ubb_x, $this->GetY());
		$this->last_h_bild = $size[1] / 72 * 25.4;
	}

	function BBCode_text($str) {
		$text = utf8_decode($str[1]);
		$spl = explode("LINEBREAK", $text);
		$anz = count($spl);
		$this->SetFillColor(150, 150, 150);
		$this->setX($this->last_x);
		if ($anz > 0) {
			$this->MultiCell($this->cellWidth, $this->font_size - 5, $spl[0], 0, "L", 0);
			for ($i = 1; $i < $anz; $i++) {
				$this->BBCode_br();
				$this->MultiCell($this->cellWidth, $this->font_size - 5, $spl[$i], 0, "L", 0);
			}
		} else {
			$this->MultiCell($this->cellWidth, $this->font_size - 5, $text, 0, "L", 0);
		}
		$this->ubb_x = $this->x + 1;
		$this->ubb_y = $this->y;
		$this->last_h_text = $this->font_size;
		$this->SetFont('', '');
		$this->SetTextColor($this->defaultColor[0], $this->defaultColor[1], $this->defaultColor[2]);
		$this->SetFontSize($this->defaultSize);
	}

	function BBCode_color($str) {
		$colorString = $str[0];
		$colors = explode(',', substr($colorString, 4, -1));
		if(!isset($colors[1])){
			if(substr($colorString, 0,1) == '#')
				$colorString = substr ($colorString, 1);
			$colors[0] = hexdec(substr($colorString, 0, 2));
			$colors[1] = hexdec(substr($colorString, 2, 2));
			$colors[2] = hexdec(substr($colorString, 4, 2));
		}
		$this->SetTextColor((int) $colors[0], (int) $colors[1], (int) $colors[2]);
	}

	function BBCode_size($str) {
		$this->SetFontSize($str[0] - 6);
	}

	function BBCode_bold($str) {
		$this->SetFont('', 'b');
	}

	function BBCode_italic($str) {
		$this->SetFont('', 'i');
	}

	function BBCode_underline($str) {
		$this->SetFont('', 'u');
	}

	function SetXY($x, $y) {
		parent::SetXY($x, $y);
		$this->ubb_x = $x;
		$this->ubb_y = $y;
		$this->last_x = $x;
		$this->last_y = $y;
	}

	function setCellDimensions($w, $h) {
		$this->cellWidth = $w;
		$this->cellHeight = $h;
	}
}
?>