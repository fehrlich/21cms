<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MessageSystem
 *
 * @author Franz Ehrlich <Franz.Ehrlich@21advertise.de>
 */
class MessageSystem {
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;
	static private $handle = null;

	

	public function __construct(){
		MessageSystem::$handle = $this;
		$this->dbc = new DataEditor('messages');
//		$this->dbc->

		$users = buildSelectArray("user", array('id','loginname')); //array("Alle")+
//		$this->dbc->form->addElement('User', 'user', 'text');
//		$this->dbc->form->addElement('Inhalt', 'icon', 'select','0',array('Information', 'Warnung', 'Error'));
//		$this->dbc->form->addElement('passwort', 'password', 'pwchange','',array('noConf' => true));
		$this->dbc->setAllowEdit(false);
		if(isSet($_GET['id'])) $id = $_GET['id'];
		else $id = '';
       	$this->dbc->form->addElement('An', 'to_user', 'select', $id,$users);
		$this->dbc->form->addElement('Nachricht', 'message', 'textarea');
		$this->dbc->form->addElement('Von', 'from_user', 'hidden',\cms\session::getObj()->getUserId());
		$this->dbc->form->addElement('Time', 'time', 'hidden',time());
		$this->dbc->form->addElement('Time', 'icon', 'hidden','0');
//		$this->dbc->form->addElement('User', 'to_user', 'dbCon','',array(
//			'table' => 'User',
//			'class' => 'User',
//			'what'  => array('id','loginname')
//		));
	}

	public function getMessages($alsoReaded = false){
//		$GLOBALS['mysql_debug'] = true;
//		addWhereBracket('(');
       	addWhere('to_user', '=', \cms\session::getObj()->getUserId(),'i');
//		addWhere('from_user', '=', '0', 'i', 'OR');
//		addWhereBracket(')');
		if(!$alsoReaded){
			addWhere('read', '=', '0','i');
		}

		addJoin('user', 'from_user', 'id','name');
       	$messages = $this->dbc->getData();
		mys::getObj()->clearWhere();
//		var_dump($messages);
//		var_dump($messages);
		$html = '';
		foreach($messages as $row){
			if($row['icon'] == '1') $icon = 'warning.png';
			if($row['icon'] == '2') $icon = 'error.png';
			else $icon = 'information.png';
			
			$html .= '<div class="message">';
			$html .= '<div class="id">'.$row['id'].'</div>';
			$html .= '<div class="to_user">'.$row['to_user'].'</div>';
			$html .= '<div class="from_user">'.$row['name'].'</div>';
			$html .= '<div class="time">'.$row['time'].'</div>';
			$html .= '<div class="msg"><img src="'.$GLOBALS['cms_roothtml'].'admin/images/icons/'.$icon.'" alt="" class="msgIcon" /><div class="txt">'.nl2br($row['message']).'</div></div>';
			$html .= '</div>';
		}
		return $html;
   	}

	public function setRead($id = 0){
		if($id == 0) $id = $_POST['id'];
		addWhere('to_user', '=', \cms\session::getObj()->getUserId(), 'i');
		addWhere('id', '=', $id,'i');
		updateArray("messages", array('read' => '1'));
		echo 'Ok';
   	}

	public function __toString(){
		addWhere('to_user', '=', \cms\session::getObj()->getUserId());
		$ret = (string) $this->dbc->__toString();
		mys::getObj()->clearWhere();
       	return $ret;
	}

	/**
	 * static function for retriving the MessageSystem handle
	 * @return MessageSystem  retrive MessageSystem objecthandle
	 */

	static public function getObj() {

		if (MessageSystem::$handle === null) {
			new MessageSystem();
		}

		return MessageSystem::$handle;
	}
}
?>
