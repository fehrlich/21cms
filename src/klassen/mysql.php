<?php
class mys extends mysqli {
	private $where = array();
	private $where_str = array();
	private $addWhereStr = '';
	private $where_values = array();
	private $whereStartBrackets = array();
	private $whereEndBrackets = array();
	private $sqlHistory = array();
	/**
	 *
	 * @var MySQLi_STMT
	 */
	private $con = false;
	private $row = array();
	private $where_art = 0;
	private $limit = '';
	private $innerJoin = array();
        private $groupBy = null;
        private $backup = array();

        private $joinTableNumber = 1;

	static public $handle = null;


        public function startSubQuery(){
            $this->backup["where"] = $this->where;
            $this->backup["addWhereStr"] = $this->addWhereStr;
            $this->backup["where_str"] = $this->where_str;
            $this->backup["where_values"] = $this->where_values;
            $this->backup["whereStartBrackets"] = $this->whereStartBrackets;
            $this->backup["whereEndBrackets"] = $this->whereEndBrackets;
            $this->backup["innerJoin"] = $this->innerJoin;
            $this->backup["limit"] = $this->limit;
            $this->backup["where_art"] = $this->where_art;
            $this->backup["groupBy"] = $this->groupBy;
            $this->backup["joinTableNumber"] = $this->joinTableNumber;
//                $this->innerJoin = array();
//		$this->limit = '';
//		$this->where_art = 0;
//                $this->groupBy = null;
            $this->clearWhere();
        }
        public function endSubQuery(){
            $this->where = $this->backup["where"];
            $this->addWhereStr = $this->backup["addWhereStr"];
            $this->where_str = $this->backup["where_str"];
            $this->where_values = $this->backup["where_values"];
            $this->whereStartBrackets = $this->backup["whereStartBrackets"];
            $this->whereEndBrackets = $this->backup["whereEndBrackets"];
            $this->innerJoin = $this->backup["innerJoin"];
            $this->limit = $this->backup["limit"];
            $this->where_art = $this->backup["where_art"];
            $this->groupBy = $this->backup["groupBy"];
            $this->joinTableNumber = $this->backup["joinTableNumber"];
        }

	public function throwError($msg = false,$errornr = false) {
		if(!$msg) $msg = $this->error;
		if(!$errornr) $errornr =  $this->errno;
		$msg .= $msg."\n".' Last Query:'.array_pop($this->sqlHistory);
		if($GLOBALS['interface'] == 'ajax') die('MySQL Error: '.$msg);
		else throw new exception('MySQL Error: '.$msg,$errornr);
	}
	public function __construct() {

		if(!isSet($GLOBALS['mysql_port'])) $GLOBALS['mysql_port'] = '3306';

        try {
            parent::__construct($GLOBALS['mysql_host'],$GLOBALS['mysql_user'], $GLOBALS['mysql_pass'],$GLOBALS['mysql_db'],$GLOBALS['mysql_port']);
        } catch (mysqli_sql_exception $e) {
            throw new Exception($e->getMessage());
        }

		if ($this->connect_errno)
			$this->throwError(mysqli_connect_error(), mysqli_connect_errno());

                self::$handle = $this;
	}

	public function changeDatabase($dbname = ''){
		if($dbname == '') $dbname = $GLOBALS['mysql_db'];
		if($this->con != false) $this->con->free_result();
		parent::select_db($dbname);
	}

	/**
	 * set the Limit of a query
	 * @param string $limit limit
	 */
	public function setLimit($limit){
		if($limit == '') $this->limit = '';
		else $this->limit = ' LIMIT '.$limit;
	}
	//$joinTable,$mainTableCol,$joinTableCol,$selectWhat,$joinType
	public function addJoin($joinTable, $mainTableCol, $joinTableCol,$selectWhat = '', $joinType = 'inner',$rewriteColName = ''){
		if($selectWhat == '') $selectWhat = $joinTableCol;
		$this->innerJoin[] = array(
			'joinTable' => $joinTable,
			'mainTableCol' => $mainTableCol,
			'joinTableCol' => $joinTableCol,
			'rewriteColName' => $rewriteColName,
			'selectWhat' => $selectWhat,
			'joinType' => strtoupper($joinType)
		);
                $this->joinTableNumber++;
                return $this->joinTableNumber;
	}

	/**
	 * update an table based on an array
	 * @param string $table tablename
	 * @param array $data key sets fieldname and value sets content
	 * @param string $para string of security parameters
	 */
	public function updateArray($table,$data,$para = ''){
		if($this->con != false) $this->con->free_result();
		$where = '';
		$sql = 'UPDATE `'.$table.'` SET ';
		$add = '';
		$types = '';
		$values = array();
		//if(count($data) > 0) $
		$values[] = $para;
		foreach($data as $name => $value){
			if($para != ''){
				$sql .= $add.'`'.addslashes($name).'` = ?';
				$values[] = &$data[$name];
			}
			else $sql .= $add.'`'.addslashes($name).'` = '.$value;
			if($add == '') $add = ',';
		}
		foreach($this->where as $w) {
			$where .= $w[4].$w[0].' '.$w[1].' ?';
			$types .= $w[3];
			$values[] = &$w[2];
		}
		$values[0] = $para.$types;
		$anz = count($values);
		for($i=1;$i<$anz;$i++) {
			if(is_array($values[$i])) $values[$i] = '';
			$values[$i] =  &$values[$i];
		}
		if($where == '') $where = '1';
		$sql .= ' WHERE '.$where;
		$this->con = $this->prepare($sql);
		if(isSet($GLOBALS['mysql_debug']) && $GLOBALS['mysql_debug']) echo $sql.var_dump($values,1);
		$this->sqlHistory[] = $sql.print_r($values,1);
		if (!$this->con) $this->throwError();
		if(count($values) > 1) call_user_func_array(array($this->con, 'bind_param'),$values);
		$this->con->execute();
		if($this->errno) $this->throwError();
			$this->clearWhere();

		return mysqli_affected_rows(mys::getObj());
	}

	/**
	 * deletes elements from an table
	 * @param string $table tablename
	 */
	public function delete($table){ //,$data = array()
		if($this->con != false) $this->con->free_result();
		$where = '';
		$sql = 'DELETE FROM `'.$table.'`';
		$add = '';
		$types = '';
		$values = array();
		$values[] = '';
		foreach($this->where as $w) {
			$where .= $w[4].$w[0].' '.$w[1].' ?';
			$types .= $w[3];
			$values[] = &$w[2];
		}
		$values[0] = $types;
		$anz = count($values);
		for($i=1;$i<$anz;$i++) {
			$values[$i] =  &$values[$i];
		}
		$sql .= ' WHERE '.$where.$this->limit;
		$this->con = $this->prepare($sql);//.' WHERE '.$where
		if (!$this->con) $this->throwError();
//		echo $sql;
		call_user_func_array(array($this->con, 'bind_param'),$values);
		$this->con->execute();
		if($this->errno) $this->throwError();
		$this->clearWhere();
	}

	/**
	 * count an selection
	 * @param string $table tablename
	 * @return int number of rows
	 */
	public function dbCheck($table){ //,$data = array()
		$this->select($table,'count(*)','',true,true);
		while($row = $this->getrow())$anz = $row['count(*)'];
		return $anz;
	}

	/**
	 * count an selection
	 * @param string $table tablename
	 * @param array $data key sets fieldname and value sets content
	 * @param string $para string of security parameters
	 * @return int id of the inserted element
	 */
	public function insertArray($table,$data,$para,$updateOnDublicate = false){ //,$data = array()
		if($this->con != false) $this->con->free_result();
		$sql = 'INSERT INTO `'.$table.'` (';
		$add = '';
		$values = array();
		//if(count($data) > 0) $
		$values[] = $para;
		foreach($data as $name => $value){
			$sql .= $add.'`'.addslashes($name).'`';
			$values[] = &$data[$name];
			if(is_array($data[$name])) $data[$name] = '';
			if($add == '') $add = ',';
		}
		$sql .= ') VALUES (?'.str_repeat(',?',count($data)-1).')';

                if($updateOnDublicate){
                    $para .= $para;
                    $values[0] = $para;
                    $sql .= ' ON DUPLICATE KEY UPDATE ';
                    $updateSQL = '';
                    foreach($data as $name => $value){
                        if($updateSQL != '') $updateSQL .= ', ';
                        $updateSQL .= '`'.addslashes($name).'` = ?';
                        $values[] = &$data[$name];
                        if(is_array($data[$name])) $data[$name] = '';
                    }
                    $sql .= $updateSQL;
                }

		if($GLOBALS['mysql_debug'])
			echo $sql.print_r($values);

		$this->sqlHistory[] = $sql.print_r($values,true);
		$this->con = $this->prepare($sql);
		if (!$this->con) $this->throwError();
		call_user_func_array(array($this->con, 'bind_param'),$values);
		$this->con->execute();
		if($this->errno) $this->throwError();
		return $this->insert_id;
	}

/**
	 * add an WHERE statement
	 * @param string $what fieldname
	 * @param string $data relation
	 * @param string $value relation
	 * @param char $type char for security parameter
	 */
	public function addWhere($what, $op, $value, $type='s',$logik = 'AND',$table = 't') {
		$logik = !count($this->where) ? '' : ' '.$logik.' ';
		if(substr($what,0,1) == '"' || substr($what,0,1) == "'"){
			$this->where[] = $logik.$what.' '.$op.' '.$value;
			$this->where_art = 0;
		}else{
			$this->where[] = array($what,$op,$value,$type,$logik,$table);
			$this->where_art = 0;
		}
		return $this;
	}

	public function addWhereBracket($bracket) {
		if($bracket == '('){
			if(!isSet($this->whereStartBrackets[count($this->where)]))
				$this->whereStartBrackets[count($this->where)] = array();
			$this->whereStartBrackets[count($this->where)][] = $bracket;
		}if($bracket == ')'){
			if(!isSet($this->whereEndBrackets[count($this->where)-1]))
				$this->whereEndBrackets[count($this->where)-1] =  array();
			$this->whereEndBrackets[count($this->where)-1][] = $bracket;
		}
	}

	/**
	 * add a whole query str for WHERE statement
	 * @param string $prepare_str query string
	 * @param array $values array of values
	 * @param string $para string of security parameters
	 */
	public function addWherePrepare($prepare_str, $values, $para) {
		$this->where_str = $prepare_str;
		if(count($values) > 0) $this->where_values = array_merge(array($para), $values);
		else $this->where_values = array();
		$this->where_art = 1;
	}

	/**
	 * make an mysql select
	 * @param string $tab tablename
	 * @param string $what selected fields
	 * @param string $order order of the selection
	 * @param boolean $assoc if it should return an assoc array
	 * @param boolean $skipslashes if the addslahses should be skipped
	 */
	public function select($tab, $what = '*', $order = '', $assoc = true, $skipslashes = false) {
		if($this->con != false) $this->con->free_result();
		$this->row = array();
		$where = '';
		$types = '';
		$values = array();

		$arg = array();
		$argh= array();
		$nextArgh = 0;
		$tab = $this->real_escape_string($tab);
		$what = $this->real_escape_string($what);
		//$res = range(0,count($what_spl)-1);

		if($this->where_art == 1) {
			$where = $this->where_str;
			//$values = &$this->where_values;
			if(count($this->where_values) > 0){
				$values[0] = $this->where_values[0];
				$anz = count($this->where_values);
				for($i=1;$i<$anz;$i++) {
					$values[$i] =  &$this->where_values[$i];
				}
			}
		}else {
			$values[] = '';
			foreach($this->where as $i => $w) {
				$startBracket = (isSet($this->whereStartBrackets[$i]))? ' '.implode('', $this->whereStartBrackets[$i]):'';
				$endBracket = (isSet($this->whereEndBrackets[$i]))? ' '.implode('',$this->whereEndBrackets[$i]):'';
				if(!is_array($w)){
					if($startBracket != ''){
						if(strpos($w, 'OR') === false && strpos($w,'AND') === false)
							$w = $startBracket.$w;
						else
							$w = str_replace(array('AND','OR'), array('AND '.$startBracket,'OR '.$startBracket), $w);
					}
					$where .= ' '.$w.$endBracket;
				}
				elseif($w[1] == 'IN'){
					$anz = count($w[2]);
					$t = '';
					for($i=0;$i<$anz;$i++){
						$t .= ',?';
						$values[] = $w[2][$i];
						$types .= $w[3];
					}
					$t = '('.substr($t,1).')';
					if($w[5] != '') $w[5] = $w[5].'.';
					$where .= $w[4].$startBracket.$w[5].$w[0].' IN '.$t.$endBracket;
				}elseif($w[1] == 'MATCH'){
					if($w[5] != '') $w[5] = $w[5].'.';
					if($skipslashes) $where .= $w[4].$startBracket.' '.$w[0].' '.$w[1].' ?'.$endBracket;
					else $where .= $w[4].$startBracket.'MATCH('.$w[5].$w[0].') AGAINST(? IN BOOLEAN MODE) > 0.9'.$endBracket;
					$types .= $w[3];
					$values[] = $w[2];
				}else{
					if($w[5] != '') $w[5] = $w[5].'.';
					if($skipslashes) $where .= $w[4].$startBracket.' '.$w[0].' '.$w[1].' ?'.$endBracket;
					else $where .= $w[4].$startBracket.$w[5].$w[0].' '.$w[1].' ?'.$endBracket;
					$types .= $w[3];
					$values[] = $w[2];
				}
			}
			if($this->addWhereStr != ''){
				$where .= ' '.$this->addWhereStr;
			}
			$values[0] = $types;
			$anz = count($values);
			for($i=1;$i<$anz;$i++) {
				$values[$i] =  &$values[$i];
			}
		}

		if($what != '*'){
			$what_spl = explode(',', $what);
			$resanz = count($what_spl);
			for($i=0;$i<$resanz;$i++) {
                            $asSplit = explode(' AS ', $what_spl[$i]);
                            if(isSet($asSplit[1])) $ass = str_replace ('`', '', $asSplit[1]);
                            else $ass = $assoc ?$what_spl[$i]:$i;
                            $this->row[$ass] = '';
                            $argh[$i] = &$this->row[$ass];
                            if(!$skipslashes) {
                                if (preg_match('~^distinct (.*)~',$what_spl[$i],$hit)) {
                                    $what_spl[$i] = 'distinct t.`'.$hit[1].'`';
                                } else {
                                    if(substr($what_spl[$i], 0,1) == '`')
                                        $what_spl[$i] = $what_spl[$i];
                                    else
                                        $what_spl[$i] = 't.`'.$what_spl[$i].'`';
                                }
                            }
			}
			$nextArgh = $i;
			$what = implode(',', $what_spl);
		}
                $skipPreTable = false;
                if(substr($order, 0,5) == 'UNIX_') $skipPreTable = true;
                elseif(substr($order, 0,3) == 'ABS') $skipPreTable = true;

		if($order != '') $order = ' ORDER BY '.(($skipPreTable)?'':'t.').$order;
		if($where == '') $where = '1';
		if(is_array($this->innerJoin) && count($this->innerJoin) > 0){
			$frommadd = '';
			foreach($this->innerJoin as $tableNr => $join){
				$tableNr = $tableNr+2;
				$newColName = ($join['rewriteColName'] != '')? $join['rewriteColName']:$join['selectWhat'];
				$rewriteName = ($join['rewriteColName'] != '')? ' AS `'.$join['rewriteColName'].'`':'';
				if($join['rewriteColName'] != ''){
					$join['rewriteColName'] = '';
				}
				if(strpos($join['selectWhat'], ',')){
					$join_what_arr = explode(',', $join['selectWhat']);
					foreach($join_what_arr as $j_w){
						$what .= ',t'.$tableNr.'.`'.$j_w.'`';
						$argh[$nextArgh] = &$this->row[$j_w];
						$nextArgh++;
					}
				}else{
					$what .= ',t'.$tableNr.'.`'.$join['selectWhat'].'`'.$rewriteName;
					$argh[$nextArgh] = &$this->row[$newColName];
					$nextArgh++;
				}
				$frommadd .= ' '.$join['joinType'].' JOIN `'.$join['joinTable'].'` AS t'.$tableNr.' ON t.`'.$join['mainTableCol'].'` = t'.$tableNr.'.`'.$join['joinTableCol'].'`';
			}
		}else $frommadd = '';

                if ($this->groupBy !== null) {
                    $this->groupBy = " group by {$this->groupBy} ";

                }

		$sql = 'SELECT '.$what.' FROM `'.$tab.'` AS t '.$frommadd.' WHERE '.$where.$this->groupBy.$order.$this->limit;

		if($GLOBALS['mysql_debug'])
			echo $sql.var_dump($values,1).'<br>'."\n";
		$this->sqlHistory[] = $sql.print_r($values,1).'<br>'."\n";
		$this->con = $this->prepare($sql);
		if(!$this->con) $this->throwError();

		if ($this->errno) $this->throwError();
//		if(count($values) > 1) $this->con->bind_param($values);
		if(count($values) > 1) call_user_func_array(array($this->con, 'bind_param'),$values);
		//if ($this->errno) $this->throwerror();

		$this->con->execute();
		//if ($this->errno) $this->throwerror();
		if(substr($what,0,1) == '*'){
			$meta = $this->con->result_metadata();
			$fields = $meta->fetch_fields();

			foreach($fields as $field){
				$this->row[$field->name] = '';
				$argh[$nextArgh] = &$this->row[$field->name];
				$nextArgh++;
			}
		}

        $this->con->store_result();
		call_user_func_array (array($this->con, 'bind_result'),$argh);
		//if ($this->errno) $this->throwerror();
		$this->clearWhere();
		$this->innerJoin = array();
	}

	/**
	 * clear all WHERE statements
	 */
	public function clearWhere(){
		$this->where = array();
		$this->addWhereStr = '';
		$this->where_values = array();
		$this->whereStartBrackets = array();
		$this->whereEndBrackets = array();
                $this->innerJoin = array();
		$this->limit = '';
		$this->where_art = 0;
                $this->groupBy = null;
                $this->joinTableNumber = 1;

                return $this;
	}

	/**
	 * get the next row of an selection
	 */
	public function getRow() {
		if(!$this->con->fetch()) return false;
		$row = array();
		foreach($this->row as $id => $val){
			$row[$id] = $val;
		}
		return $row;
	}

	/**
	 * get all rows as Array
	 */
	public function getRows() {
		$rows = array();
		while($row = $this->getRow()){
			$rows[]= $row;
		}
		return $rows;
	}

	/**
	 *
	 * @return mys Mysql Singleton
	 */
        static public function getObj() {
            if (self::$handle === null) {
                new mys();
            }

            return self::$handle;
        }

		public function query($sql, $resultmode = null){
			$ret = parent::query($sql,$resultmode);
			$this->sqlHistory[] = $sql.'<br>';
//				$this->con = $ret;
			return $ret;
		}

        public function cleanup() {
            if($this->con != false) $this->con->free_result();
            return $this;
        }

        public function addGroupBy($str) {
            $this->groupBy = $str;
        }

		public function getSqlHistory() {
			return $this->sqlHistory;
		}
    }

/**
 * add a whole query str for WHERE statement
 * @param string $prepare_str query string
 * @param array $values array of values
 * @param string $para string of security parameters
 */
function addWhere($what, $op, $value, $type='s',$logik = 'AND',$table = "t") {
	mys::getObj()->addWhere($what, $op, $value, $type,$logik,$table);
}
/**
 * make an mysql select
 * @param string $tab tablename
 * @param string $what selected fields
 * @param string $order order of the selection
 * @param boolean $assoc if it should return an assoc array
 * @param boolean $skipslashes if the addslahses should be skipped
 */
function select($tab, $what = '*', $order = '',$assoc = true,$skipslashes = false) {
	mys::getObj()->select($tab, $what, $order, $assoc, $skipslashes);
}
/**
 * get the next row of an selection
 */
function getRow() {
	return mys::getObj()->getRow();
}
/**
 * get all rows as Array
 */
function getRows() {
	return mys::getObj()->getRows();
}
/**
 * add a whole query str for WHERE statement
 * @param string $prepare_str query string
 * @param array $values array of values
 * @param string $para string of security parameters
 */
function addWherePrepare($prepare_str, $values = array(), $types = '') {
	return mys::getObj()->addWherePrepare($prepare_str, $values, $types);
}
/**
 * count an selection
 * @param string $table tablename
 * @param array $data key sets fieldname and value sets content
 * @param string $para string of security parameters
 * @return int id of the inserted element
 */
function insertArray($table,$data,$parastr,$updateOnDublicate = false){
	return mys::getObj()->insertArray($table,$data,$parastr,$updateOnDublicate);
}
/**
 * update an table based on an array
 * @param string $table tablename
 * @param array $data key sets fieldname and value sets content
 * @param string $para string of security parameters
 */
function updateArray($table,$data,$para = ''){
	return mys::getObj()->updateArray($table,$data,$para);
}
/**
 * deletes elements from an table
 * @param string $table tablename
 */
function delete($table){
	return mys::getObj()->delete($table);
}
/**
 * count an selection
 * @param string $table tablename
 * @return int number of rows
 */
function dbCheck($table){
	return mys::getObj()->dbCheck($table);
}
/**
 * set the Limit of a query
 * @param string $limit limit
 */
function setLimit($limit){
	return mys::getObj()->setLimit($limit);
}
function addJoin($joinTable,$mainTableCol,$joinTableCol, $selectWhat = '',$joinType = 'inner',$rewriteColName = ''){
	return mys::getObj()->addJoin($joinTable,$mainTableCol,$joinTableCol,$selectWhat,$joinType,$rewriteColName);
}
function addWhereBracket($bracket){
	return $GLOBALS['cmsdb']->addWhereBracket($bracket);
}
function addGroupBy($str) {
    mys::getObj()->addGroupBy($str);
}
$GLOBALS['cmsdb'] = new mys();
$GLOBALS['cmsdb']->set_charset('utf8');
$GLOBALS['mysql_debug'] = false;
//if(!$cmsdb) die('VERBINDUNG FEHLGESCHLAGEN');
?>