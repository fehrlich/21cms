<?php
    class sendmail {
        private $recipiens = array();
        private $copy = array();
        private $bcopy = array();
        private $subject = '';
        private $from = "21cms.de";
        private $body = '';

        private function checkMail($mail) {
            #return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $mail);
            return true;
        }

        public function addRecipient($mail) {
            if ($this->checkMail($mail)) {
                $this->recipiens[] = $mail;
                return true;
            }

            return false;
        }

        /*
        public function addCopy($mail) {

            if ($this->checkMail($mail)) {
                $this->copy[] = $mail;
                return true;
            }

            return false;

        }

        public function addBlindCopy($mail) {

            if ($this->checkMail($mail)) {
                $this->bcopy[] = $mail;
                return true;
            }

            return false;

        }
        */
        
        public function subject($sub) {
            $this->subject = $sub;
            return true;
        }

        public function body($text) {
            $this->body = $text;
        }

        public function send() {
            $header = "From: {$this->from}\r\n";

            foreach($this->recipiens as $mail) {
                mail($mail,$this->subject,$this->body,$header);
            }
        }

        public function setFrom($from) {
            $this->from = $from;

        }
    }
?>