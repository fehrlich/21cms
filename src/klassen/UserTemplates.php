<?php

class UserTemplates {
        static private $lorem = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean ulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat';
	static function getImageOnLeft() {
		$html = '';
		$html .= '<img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/dummyImg.png" alt="" style="margin: 0 10px 10px 0;float:left" />';
		$html .= '<p>'.self::$lorem.'</p>';
		return $html;
	}
	static function getImageOnRight() {
		$html = '<img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/dummyImg.png" alt="" style="margin: 0 10px 10px;float:right" />';
		$html .= '<p>'.self::$lorem.'</p>';
		return $html;
	}

	static function getTwoColumns() {
		$html = '<div class="floatdiv">';
		$html .= '<p class="row1">'.self::$lorem.'</p>';
		$html .= '<p class="row2">'.self::$lorem.'</p>';
		$html .= '</div>';
		return $html;
	}

	static function getThreeColumns() {

                $html = '<div class="floatdiv">';
		$html .= '<div class="row1">'.self::$lorem.'</div>';
                $html .= '<div class="row2">'.self::$lorem.'</div>';
                $html .= '<div class="row3">'.self::$lorem.'</div>';
		$html .= '</div>';
		return $html;
	}

	static function getTemplatesOverview(){
		$html = '<div id="templates">';
		$html .= '<div class="template"><img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/img_on_left.gif" alt=""><div id="img_on_left" class="tplHtml">'.UserTemplates::getImageOnLeft().'</div></div>';
		$html .= '<div class="template"><img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/img_on_right.gif" alt=""><div id="img_on_right" class="tplHtml">'.UserTemplates::getImageOnRight().'</div></div>';
		$html .= '<div class="template"><img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/2_col.gif" alt=""><div id="2Col" class="tplHtml">'.UserTemplates::getTwoColumns().'</div></div>';
//		$html .= '<div class="template"><img src="'.$GLOBALS['cms_roothtml'].'admin/images/tpls/2_col.gif" alt=""><div id="2Col" class="tplHtml">'.UserTemplates::getThreeColumns().'</div></div>';
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		return $html;
	}

}

?>
