<?php

/**
 * Creates an Connecter to interact with cms intern userdatabases
 *
 * @author F.Ehrlich
 */
class DatabaseConnector {

	private $database;
	private $id;
	private $tpl;
	private $fields;
	private $data;
	private $fieldid;
	private $fieldid_mysql;
	private $databaseName = "";
	private $overWriteLinks = "";
	private $where = null;
	private $dbType = 0;
	private $triggerDataId = 0;

	/**
	 * Creates an Connecter to interact with cms intern databases
	 * @param int $database database id
	 * @param int $tpl template id
	 * @param int $dataid dataid
	 */
	public function __construct($database = 0, $tpl = 0, $dataid = 0) {



		if ($database == 0)
			if (isset($_GET['db']))
				$database = (int) $_GET['db'];
		if ($dataid == 0)
			if (isset($_GET['data']))
				$dataid = (int) $_GET['data'];
		if ($tpl == 0 && isset($_GET['tpl']))
			$dtpl = (int) $_GET['tpl'];
		if ($database == 0 && isset($_GET['el']) && isset($_GET['id']) && $_GET['el'] == 'DatenListe') {
		    addWhere('id', '=', $_GET['id'], 'i');
		    setLimit(1);
		    select('elements', 'eldata');
		    $row = getRow();
		    $dat = unserialize($row['eldata']);
		    $database = (isset($dat['database'])) ? $dat['database'] : 0;
		}
//		echo $database.'!!!!';
		$this->database = $database;
		$this->id = $dataid;
		$this->tpl = $tpl;
		$this->fieldid_mysql = false;
		$this->fields = array();
	}

	/**
	 * Return all possible Types for data colums
	 * @return array with Types
	 */
	public function getFullIndexTypes(){
		$fullIndexes = array('textarea', 'text', 'editor', 'email', 'tags', 'bild', 'datei');
		return $fullIndexes;
	}

	/**
	 * set the template for html data
	 * @param int $tpl id of the template
	 */
	public function setTpl($tpl) {
		$this->tpl = $tpl;
	}

	/**
	 * delete the current data
	 * id must be set!
	 */
	public function delete() {
//		$GLOBALS['mysql_debug'] = true;
		$dbname = $this->getDatabaseName();
		addWhere('id', '=', $this->database);
		$dbRow = $this->getDatabases('dbType');
		if(!isSet($dbRow['dbType']) || $dbRow['dbType'] == '0'){
			$this->triggerEvent(Event::DEL,$this->id);
			addWhere('id', '=', $this->id, 'i');
			delete("data_" . $dbname);
		}
//		$GLOBALS['mysql_debug'] = false;
		echo 'Ok';
	}

	/**
	 * lieftert Formular zum erstellen von Datenbanken
	 * @return DataEditor DBEditor Element von Typ Datenbank
	 */
	private function getDBForm() {

		$dbedit = new DataEditor('databases');
//		$fieldform = new formular();
		$fieldform = new formular('','','','dbFields',$dbedit->form->getPosted());
		$fieldform->addElement('Name', 'fieldname', 'text');
		$fieldform->addElement('Typ', 'type', 'select', '0', DatabaseConnector::getTypesForm());
		$fieldform->addElement('Eigenschaften', 'eig', 'text', '');
		$fieldform->addElement('Standard', 'default', 'text', '');
//		$fieldform->addElement('Optionen', 'options[]', 'check', '', array('Pflicht', 'Mehrsprachig'));
//		$fieldform->addElement('Pflicht', 'pflicht', 'simpleCheck', 'Pflicht');
		$fieldform->addElement('Mehrsprachig', 'multilang', 'simpleCheck','','');
		$fieldform->addElement('Suchmultiplikator', 'searchmul', 'select','0',array('1x','2x','5x','10x'));
		$fieldform->addElement('Key', 'identifiers', 'select', '', array('[Kein Key]','Beschreibung', 'Tags'));

//		$opt = new formular();
//		$opt->addElement('Mehrsprachig', 'multiLang[]', 'simpleCheck', '','Mehrsprachig');
//		$opt->addElement('Pflicht', 'pflicht[]', 'simpleCheck', '', 'Pflicht');
//
//		$fieldform->addElement('Key', 'options', 'formular', '', $opt);

		$tags = buildSelectArray('tags', array('id', 'title'), true);
		$tags[0] = '[Keinen]';
		$tagform = new formular();
		$tagform->addElement('Unterordnen', 'maintag[]', 'select', '0', $tags);
		$tagform->addElement('Exklusiv', 'exclusive[]', 'check');
		$fieldform->addOnChangeSetForm('type', 'tags', 'eig', $tagform);

		$bildform = new formular();
		$bildform->addElement('Maximale Breite', 'maxwidth[]', 'text');
		$bildform->addElement('Maximale H&ouml;he', 'maxheight[]', 'text');
		$bildform->addElement('Standard Bild', 'defaultpic[]', 'datei','',array(
			'allowFolder' => true,
			'note' => 'Wenn Sie ein Verzeichnis wählen wird jedes mal ein zuf&auml;lliges Bild aus diesem Verzeichnis gew&auml;lt'
		));
		$fieldform->addOnChangeSetForm('type', 'bild', 'eig', $bildform);

                $foreignIdForm = new formular();
                $foreignIdForm->addElement("Datenbank", 'dbId', 'database');
                $fieldform->addOnChangeSetForm('type', FormType::DATARECORD, 'eig', $foreignIdForm);
//		if(!isset($_GET['inter']) || $_GET['inter'] != 'dia')
//			$dbedit->form->setSaveButton('Speichern');
		$dbedit->form->setFormAction('ajax.php?kl=DatabaseConnector' . buildGet('edit,new'));
		$dbedit->form->addElement('Name', 'name', 'text', '');
		$dbedit->form->addElement('Typ', 'dbType', 'select', '',array('neue Datenbank','Erweiterung für User','Erweiterung für FrontEndUser'));
		$dbedit->form->addElement('Felder', 'fields', 'formular', null, $fieldform);
		$dbedit->form->addElement('Feld idtentifier', 'fieldid', 'text', 'Titel');
		$dbedit->setInvisible('fields');

		$dbedit->form->usetab('Events_Aktionen');
		$dbedit->form->addElement('EventAktionen', 'eventActions', FormType::FORMULA,'', Action::getEventActionForm());
//		$dbedit->__toString();
		$dbedit->setInvisible('eventActions');
//		$els = $dbedit->form->getElements();
//		$el2 = $els['fields']['data']->getElements();
//		if($el2['searchmul']['value'] == '0'){
//
//			$el2['searchmul']['value'] = array(0,0,0,0,0,0);
//		}
		return $dbedit;
	}

	static public function getTypesForm() {
		return array(
			'text' => 'Textzeile',
			'textarea' => 'Textbereich',
			'editor' => 'Formatierbarer Text',
			'email' => 'Emailfeld',
			'date' => 'Datum',
			'time' => 'Zeit',
			'select' => 'Auswahl DropDown',
			'selectTxt' => 'Auswahl DropDown(text)',
			'page' => 'Seitenauswahl',
			'multiselect' => 'Multi Auswahl DropDown', 
			'tags' => 'Tags',
			'bild' => 'Bild',
			'datei' => 'Datei',
			FormType::DATARECORD => 'Daten aus anderer DB'
		);

	}

	/**
	 * returns all databases in an array
	 * @param string $what what fields should be selected
	 * @return array all selected databases
	 */
	public function getDatabases($what = '*') {

		if ($this->where !== null && is_array($this->where))
			addWhere($this->where[0], $this->where[1], $this->where[2]);

		select('databases', $what);
		$rows = array();
		while ($row = getRow()) {
//			if($this->database < 1 && isSet($row['id']))
//				$this->database = $row['id'];

			if ($what == 'id,name') {
				$rows[$row['id']] = $row['name'];
			} else {
			    if(isSet($row['dbType'])) $this->dbType = $row['dbType'];
			    if(isSet($row['name'])) $this->databaseName = $row['name'];
				if (isset($row['fields'])) {
					$field_array = array();
					$fields = unserialize($row['fields']);
					foreach ($fields['fieldname'] as $i => $n) {
						$field['name'] = $n;
						$field['type'] = $fields['type'][$i];
                                                if(!isSet($fields['eig'][$i])) $field['eig'] = '';
						else $field['eig'] = $fields['eig'][$i];
						$field['default'] = $fields['default'][$i];
						if(isSet($fields['identifiers'][$i]))
							$field['identifiers'] = $fields['identifiers'][$i];
						else
							$field['identifiers'] = 0;
//						$field['options'] = $fields['options'][$i];
						$field['options'] = array();

						if(isSet($fields['multilang'][$i])) $field['multilang'] = $fields['multilang'][$i];
						else $field['multilang'] = '0';
						if(in_array($field['type'], $this->getFullIndexTypes())){
							if(isSet($fields['searchmul'][$i])) $field['searchmul'] = $fields['searchmul'][$i];
							else $field['searchmul'] = '0';
						}else{
							$field['searchmul'] = false;
						}

						$field_array[] = $field;
					}
					$row['fields'] = $field_array;
					$this->fields = $field_array;
					if($this->dbType == 2){
					    $this->fields[] = array(
						  'name' => 'email',
						  'type' => 'email',
						  'eig' => "",
						  'default' => '',
						  'identifiers' => "0",
						  'options' => array(),
						  'multilang' => '0',
						  'searchmul' => '0'
					    );
					    $this->fields[] = array(
						  'name' => 'loginname',
						  'type' => 'text',
						  'eig' => "",
						  'default' => '',
						  'identifiers' => "0",
						  'options' => array(),
						  'multilang' => '0',
						  'searchmul' => '0'
					    );
					    $this->fields[] = array(
						  'name' => 'password',
						  'type' => 'password',
						  'eig' => "",
						  'default' => '',
						  'identifiers' => "0",
						  'options' => array(),
						  'multilang' => '0',
						  'searchmul' => '0'
					    );
					    $this->fields[] = array(
						  'name' => 'desc',
						  'type' => 'textarea',
						  'eig' => "",
						  'default' => '',
						  'identifiers' => "0",
						  'options' => array(),
						  'multilang' => '0',
						  'searchmul' => '0'
					    );
					    $this->fields[] = array(
						  'name' => 'group',
						  'type' => 'dbCon',
						  'eig' => array(
								'table' => 'frontEndGroups',
								'class' => 'FrontEndGroups',
								'what'  => array('id','name'),
								'sort'  => true
						  ),
						  'default' => '',
						  'identifiers' => "0",
						  'options' => array(),
						  'multilang' => '0',
						  'searchmul' => '0'
					    );
					}

					if(isSet($row['fieldid']))
					    $this->fieldid = $row['fieldid'];
					if (strpos($this->fieldid, ' '))
						$this->fieldid_mysql = str_replace(' ', ',', $this->fieldid);
					else
						$this->fieldid_mysql = $this->fieldid;
				}
				if (isset($row['id']))
					$rows[$row['id']] = $row;
				else
					$rows[] = $row;
			}
		}
		return $rows;
	}

	/**
	 * Returns a list of all fields of the database
	 * @return array fields
	 */
	public function getFields() {
		if (count($this->fields) == 0) {
			addWhere('id', '=', $this->database, 'i');
			setLimit(1);
			$dbs = $this->getDatabases('name,fields,fieldid,dbType');
		}
		return $this->fields;
	}

        public static function getDatabaseNameFromId($id){

        }


	/**
	 * Returns the name of the Database for mysql add data_ before
	 * @return string name
	 */
	public function getDatabaseName() {
		if ($this->databaseName == "") {
			addWhere('id', '=', $this->database);
			select('databases', 'name,dbType');
			$row = getRow();
			if($row['dbType'] == 1) $row['name'] = 'user';
			elseif($row['dbType'] == 2) $row['name'] = 'frontEndUser';
			$this->databaseName = $row['name'];
		}
		return $this->databaseName;
	}


	public function formedit() {
		if ($this->database > 0) {
			$form = $this->getForm();
		} else {
			$form = $this->getDBForm();
		}
		$form->new = true;
		return (string) $form->getHtml();
	}

	public function formpost() {
		$trigger = false;
		$ret = '';
		if ($this->database > 0) {
			$form = $this->getForm();
			$mainid = (isSet($_GET['nextMainId'])) ? $_GET['nextMainId'] : 0;
			$mainid = ($mainid == 0 && isSet($_GET['get']) && $_GET['get'] == 'sub' && isSet($_GET['id'])) ? $_GET['id'] : $mainid;
			if($form->form->posted){
				if(!$form->edit) $trigger = Event::NEW_ENTRY;
				else $trigger = Event::EDIT;
			}
			$form->new = true;
			if ($mainid > 0) {
				$ret = $form->getHtml();

				$ret = 'Ok';
				$dantenList = new DatenListe($mainid);
				$ret .= ':CONT' . $dantenList;
			}
		} else {
			$form = $this->getDBForm();
			$form->new = true;
		}
		if($ret == '') $ret = (string) $form;
		if($trigger) $this->triggerEvent ($trigger,$form->form->getDataid());

		return $ret;
	}

	/**
	 * returns all data from an specific database in an array
	 * @return array all data from an database
	 */
	public function getData($dbname='', $what = '*', $order = '', $skipPublic = false) {
		$html = '';
		$fieldid = false;
		if ($dbname == '') {
                        mys::getObj()->startSubQuery();
			addWhere('id', '=', $this->database, 'i');
//			echo $this->database.'!!!!';
			$dbs = $this->getDatabases('name,dbType'); //,fields
                        mys::getObj()->endSubQuery();
			$db = $dbs[0];
			$dbname = $db['name'];
			$this->databaseName = $dbname;
		}
		if ($what == 'fieldid') {
			$what = $this->getFieldId();
			$fieldid = true;
		}
		$fields = $this->getFields();
		$collect = ($what == '*');
		if($collect){
		    $what = 'id';
		    if($this->dbType == 0) $what .= ',title_intern,erstellt,public,published,autor';
		}
                foreach($fields as $i => $field){
                    if($field['type'] == 'dataRecord'){
                        mys::getObj()->startSubQuery();
                        $dbId = $field['eig']['dbId'];
//                        echo $dbId.'!';
                        $fieldDbc = new DatabaseConnector($dbId);
                        addWhere('id', '=', $dbId);
                        $fieldDb = $fieldDbc->getDatabases('name,fields,fieldId');
                        $fields[$i]['eig']['dbName'] = 'data_'.$fieldDb[0]['name'];
                        $fields[$i]['eig']['fieldId'] = ''.$fieldDb[0]['fieldId'];
                        $fields[$i]['eig']['fields'] = $fieldDbc->getFields();
                        mys::getObj()->endSubQuery();
                    }
                }
		foreach($fields as $field){
		    $add = true;
		    if($field['type'] == 'dbCon'){
			$add = false;
			addJoin($field['eig']['table'], $field['name'], $field['eig']['what'][0], $field['eig']['what'][1], 'left',$field['name']);
		    }
		    if($field['type'] == 'dataRecord'){
			$add = false;
                        $tableNumber = addJoin($field['eig']['dbName'], $field['name'], 'id', $field['eig']['fieldId'],'left',$field['name']);

                        if(substr($field['eig']['dbName'], 0,5) == 'data_'){
                            $joinDbName = substr($field['eig']['dbName'], 5);
                            foreach ($field['eig']['fields'] as $subField){
                                $what .= ',`t'.$tableNumber.'`.`'.$subField['name'].'` AS `'.$joinDbName.'.'.$subField['name'].'`';
                            }
                        }
		    }
		    if($collect && $add) {
                        if (isset($field["multilang"]) && $field["multilang"] == 1 && Languages::getLang(false) != '') {
                            $what .= ','.$field['name'] ."_". Languages::getLang(false);
                        } else {
                            $what .= ','.$field['name'];
                        }
                    }
		}
		if($this->dbType == 0){
		    if (!$skipPublic)
			addWhere('public', '=', '1');
		    select('data_' . $dbname, $what, $order);
		}elseif($this->dbType == 1)
		    select('user', $what, $order);
		elseif($this->dbType == 2)
		    select('frontEndUser', $what, $order);

		$rows = array();
		while ($row = getRow()) {
			if ($fieldid)
				$row['fieldid'] = $this->getFieldId($row);
			$rows[] = $row;
		}
		$this->data = $rows;
		return $rows;
	}

	public function getTitle($data) {
		$title = '';
		$spl = explode(' ', $this->fieldid);
        $first = true;
		foreach ($spl as $t) {
					if($first) $first = false;
					else $title .= ' ';
                    //language select
                    if (isset($data[$t . "_" . Languages::getLang(false)]) && Languages::getLang(false) != '') {
                        $title .= $data[$t . "_" . Languages::getLang(false)];
                    } else {
                        $title .= $data[$t];
                    }
		}
		return $title;
	}

        public function parseField($field,$data,$rss,$joinTable = ''){
            $langAdd = '';
            if(isSet($_GET['lang']) && $_GET['lang'] != '' && isSet($GLOBALS['languages'][$_GET['lang']])){
                $langAdd = '_'.$GLOBALS['languages'][$_GET['lang']];
            }

            $name = $field['name'];
            if(isSet($field['multilang']) && $field['multilang'] == '1')
                    $name .= $langAdd;
            if($joinTable != '')
                $name = $joinTable.'.'.$name;
            if(isSet($data[$name])){
                if ($field['type'] == 'datum' || $field['type'] == 'date' || $field['name'] == 'erstellt' || $field['name'] == 'published') {
                        $data[$name] = (int) $data[$name];
                        if($data[$name] > 0){
                            if ($rss) $val = date('r', $data[$name]);
                            else $val = date('d.m.Y', $data[$name]);
                        }else $val = '-';
                }
                elseif ($field['type'] == 'time') {
                        $data[$name] = (int) $data[$name];
                        if($data[$name] > 0){
                            if ($rss) $val = date('r', $data[$name]);
                            else $val = date('d.m.Y H:i', $data[$name]);
                        }else $val = '-';
                }
                elseif ($field['type'] == 'multiselect') {
                        $spl = explode(',', substr($data[$name],1,-1));
                        $val = implode(', ',$spl);
                }
                elseif ($field['type'] == 'tags') {
                        if ($rss)
                                $val = substr($data[$name], 1, -1);
                        else {
                                $spl = explode(',', substr($data[$name], 1, -1));
                                $val = '';
                                $add = ' ';
                                //				addWhere('title', $op, $value)
                                //				echo $data[$name].'!'.$field['eig']['maintag'];
                                $title_int = array();
                                if ($field['eig']['maintag'] > 0) {
                                        addWhere('parent', '=', $field['eig']['maintag'], 'i');
                                        addWhere('title', 'IN', $spl);
                                        select('tags', 'title_intern,title');
                                        $rows = getRows();
                                        foreach ($rows as $r) {
                                                $title_int[$r['title']] = $r['title_intern'];
                                        }
                                }else{
                                    addWhere('title_intern', 'IN', $spl);
                                    select('tags','title,title_intern');
                                    $tagTitles = getRows();
                                    foreach($tagTitles as $row){
                                        $tagTitlesArray[$row['title_intern']] = $row['title'];
                                    }
                                }
                                foreach ($spl as $tag) {
                                        if (!isSet($title_int[$tag]))
                                                $title_int[$tag] = $tag;
                                        $val .= $add . Tag::buildLink($title_int[$tag],$tag);
                                        $add = ' | ';
                                }
                                $val = substr($val, 1);
                        }
                }elseif ($field['type'] == 'textarea') {
                        $val = nl2br(htmlentities($data[$name],null,'utf-8'));
                }elseif ($field['type'] == 'bild' || $field['type'] == 'datei') {
                        $val = str_replace(' ', '%20',$data[$name]);
                }
    //			elseif($field['type'] != 'editor')  $val = htmlentities($data[$name],ENT_COMPAT,'UTF-8');
                else
                        $val = $data[$name];
            }else $val = '';
            $val = str_replace('=', '||=||', $val);
            return $val;
        }

	public function parseData($html, $data, $rss = false,$nr = 0) {
		$std_val = array(array(
				'name' => 'erstellt',
				'type' => 'time'
			),array(
				'name' => 'published',
				'type' => 'time'
			), array(
				'name' => 'autor',
				'type' => 'text'
			), array(
				'name' => 'id',
				'type' => 'text'
				));

		$langAdd = '';
		if(isSet($_GET['lang']) && $_GET['lang'] != '' && isSet($GLOBALS['languages'][$_GET['lang']]))
			$langAdd = '_'.$GLOBALS['languages'][$_GET['lang']];

		$html = str_replace('[NR]', $nr, $html);
		$html = str_replace('[INDEX]', $nr, $html);
		$fields = array_merge($std_val, $this->getFields());
		foreach ($fields as $field) {
                        if($field['type'] == 'dataRecord'){
                            mys::getObj()->startSubQuery();
                            $dbId = $field['eig']['dbId'];
                            $fieldDbc = new DatabaseConnector($dbId);
                            addWhere('id', '=', $dbId);
                            $fieldDb = $fieldDbc->getDatabases('name,fields,fieldId');
                            $field['eig']['dbName'] = 'data_'.$fieldDb[0]['name'];
                            $field['eig']['fieldId'] = ''.$fieldDb[0]['fieldId'];
                            $field['eig']['fields'] = $fieldDbc->getFields();

                            $joinDbName = $fieldDbc->getDatabaseName();
                            foreach($field['eig']['fields'] as $subField){
                                $val = $this->parseField($subField, $data, $rss,$joinDbName);
                                $html = str_replace('[DB_' . $joinDbName.'.'.$subField['name'] . ']', $val, $html);
                            }
                        }
			$val = $this->parseField($field, $data, $rss);
			$html = str_replace('[DB_' . $field['name'] . ']', $val, $html);
		}
		if($this->dbType == 0)
		    $html = str_replace('[DB_title_intern]', $data['title_intern'], $html);

		if (preg_match_all('/\[LINK\((.[^)]*?),(.[^)]*?),(.[^)]*?)(,(.[^)]*?))?\)\]/', $html, $matches)) {
			foreach ($matches[0] as $i => $match) {
				$id = $matches[1][$i];
				$dbname = $matches[2][$i];
				$seite = $matches[3][$i];
				if(isSet($matches[5][$i]) && $matches[5][$i] != ''){
					$para = '-P_'.$matches[5][$i];
				}else $para = '';
				if ($id != $this->id && $id != 'this') {
					addWhere('id', '=', $id, 'i');
//					select('data_'.$dbname,'Titel);
					select('data_' . $dbname, 'title_intern');
					$dat = getRow();
//					$title = $this->getFieldId($dat); #$dat['Titel'];
					$title = $dat['title_intern'];
				}else
					$title = $data['title_intern'];#['Titel'];
				if ($seite == 'this') {
					if($this->overWriteLinks != '')
						$seite = $this->overWriteLinks;
					else
						$seite = $GLOBALS['akt_menuepunkt']['title_intern'];
				}
//				$title = menuepunkte::setTitelIntern($title);

                                $lang = (Languages::getLang() != "") ? Languages::getLang() . "/" : "";
				$link = $GLOBALS['cms_root'] . $lang . $dbname . '/' . $seite . '-' . $title .$para. '.html';
				$html = str_replace($match, $link, $html);
			}
		}
		if (preg_match_all('/\[CHECKFILE\((.*?)\)\]/', $html, $matches)) {
			foreach ($matches[1] as $i => $match) {
				if($match != '' && file_exists(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].$match)))
					$rep = '1';
				else $rep = '';
				$html = str_replace($matches[0][$i], $rep, $html);
			}
		}

		if (preg_match_all('#\[IF\((.*?)\)\](.*?)?(\[ELSE\](.*?)?)?\[/IF\]#si', $html, $matches)) { //|\[IFEND\]
			foreach ($matches[0] as $i => $match) {

				$condString = $matches[1][$i];
				$condBool = true;
				$and = true;
				if(strpos($condString, '&') !== false){
					if(strpos($condString, '&amp;&amp;')) $str = '&amp;&amp;';
					elseif(strpos($condString, '&&')) $str = '&&';
					elseif(strpos($condString, '&amp;')) $str = '&amp;';
					else $str = '&';

					$conditons = explode($str,$condString);
				}
				elseif(strpos($condString, '|') !== false){
					if(strpos($condString, '||')) $str = '||';
					elseif(strpos($condString, '|')) $str = '|';

					$conditons = explode($str,$condString);
					$and = false;
					$condBool = false;
				}
				else $conditons = array($condString);

				foreach($conditons as $condString){
					$condString = trim($condString);
					if(strpos($condString, '==') !== false){
						$spl = explode('==',$condString);
						if(trim($spl[0]) == trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}
					elseif(strpos($condString, '!=') !== false){
						$spl = explode('!=',$condString);
						if(trim($spl[0]) != trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}
					elseif(strpos($condString, '=') !== false){
						$spl = explode('=',$condString);
						if(trim($spl[0]) == trim($spl[1]))
							$condBoolTmp = true;
						else $condBoolTmp = false;
					}elseif($condString != '')
						$condBoolTmp = true;
					else
						$condBoolTmp = false;

					if($and) $condBool = $condBool && $condBoolTmp;
					else $condBool = $condBool || $condBoolTmp;
				}
//				var_dump($condition);
				$then = $matches[2][$i];
				$else = $matches[4][$i];
				if ($condBool)
					$html = str_replace($match, $then, $html);
				else
					$html = str_replace($match, $else, $html);
			}
		}

        if (preg_match_all(''
                . '#\[INARRAY\((.*)?\),\((.*?)\)\](.*)\[/INARRAY\]#xi' //selector
                , $html, $matches,PREG_SET_ORDER)) {

            foreach ($matches as $inArraySelector) {
                if (count($inArraySelector) != 4) {
                    //illegal data set
                    continue;
                }

                $query = $inArraySelector[0];
                $givenValues = array_map("trim",explode(",",$inArraySelector[1]));
                $searchingForValues = array_map("trim",explode(",",$inArraySelector[2]));
                $textToDisplay = $inArraySelector[3];

                $allFound = true;

                foreach($searchingForValues as $value) {
                    if (!in_array($value,$givenValues)) {
                        $allFound = false;
                    }
                }

                $html = str_replace($query, ($allFound) ? $textToDisplay : "", $html);
            }
        }

		if (preg_match_all('#\[COMMENTSYS\]#si', $html, $matches)) { //|\[IFEND\]
			include_once $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'klassen/Comments.php';
			include_once $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'klassen/formular.php';
			foreach ($matches[0] as $i => $match) {

//				$form = (string) Comments::getUserForm($this->database, $data['id']);

				$str = Comments::getUserHtml($this->database, $data['id']);

				$html = str_replace($match, $str, $html);
			}
		}
		if (preg_match_all('#\[COMMENT_COUNT\]#si', $html, $matches)) { //|\[IFEND\]
			foreach ($matches[0] as $i => $match) {
				addWhere('dbId', '=', $this->database);
				addWhere('dataId', '=', $data['id']);
				$anz = dbCheck('comments');
				$new = $anz . ' Kommentar';
				if ($anz != 1)
					$new .= 'e';
				$html = str_replace($match, $new, $html);
			}
		}
		$html = str_replace('||=||', '=', $html);
                if (preg_match_all('/\[EL_(.*?)\((.*?)\)\]/', $html, $matches)) {
			foreach ($matches[0] as $i => $match) {

				//Container erstellen und inhalte holen
				$name = $matches[1][$i];
				$spl = explode(',', $matches[2][$i]);
				$id = $spl[0];
				if (isset($spl[1])) {
					$elHtml = '';
					if ($admin) {
						addWhereprepare('container = ? AND ((visible = 2 AND dontShowOn NOT LIKE ?) OR seitenid = ? OR (visible = 3 AND visible_main IN (' . implode(',', $GLOBALS['aktmenu']) . ')))', array('el' . $id,'%,'.$GLOBALS['seitenid'].',%', $GLOBALS['seitenid']), 'ssi');
					} else {
						addWhereprepare('container = ? AND ((visible > 0 AND seitenid = ?) OR (visible = 2 AND dontShowOn NOT LIKE ?) OR (visible = 3 AND visible_main IN (' . implode(',', $GLOBALS['aktmenu']) . ')))', array('el' . $id, $GLOBALS['seitenid'],'%,'.$GLOBALS['seitenid'].',%'), 'ssi');
					}

					select('elements', 'id,eldata');
					$rows = getRows();
					foreach ($rows as $row) {
						$data = unserialize($row['eldata']);
						$el = new $name($row['id'], $data, '', 0, false);
						$elHtml .= $el->getContent();
					}

					if ($elHtml == '') {
						//DEBUGEN WEGEN CONTAINER
						$el = new $name(0, array(), 'el' . $id, 0, false);
						$elHtml .= $el->getContent();
					}

					$html = str_replace('[EL_' . $name . '(' . $matches[2][$i] . ')]', $elHtml, $html);
				} else {
					$elHtml = '';
					//TODO: Ein Select fuer alle Elemente
					$GLOBALS['cmsdb']->clearWhere();
					addWhere('id', '=', $id);
					select('elements', 'eldata');
					$row = getRow();
					$data = unserialize($row['eldata']);
					$el = new $name($id, $data, '', 0, false);
					$elHtml .= $el->getContent();
					$html = str_replace('[EL_' . $name . '(' . $matches[2][$i] . ')]', $elHtml, $html);
				}
			}

			unset($el);
		}
		return $html;
	}

	public function getTemplate() {
		if($this->tpl > 0){
//			if(is_int($this->tpl)){
			addWhere('id', '=', $this->tpl, 'i');
			setLimit(1);
			select('dataTemplates');
			$tpl = getRow();
			$lang = Languages::getLang(false);
			if($lang != '' && isSet($tpl['html_'.$lang]) && $tpl['html_'.$lang] != '')
				$tpl['html'] = $tpl['html_'.$lang];
		}else{
			$tpl = array(
				'id' => 9999,
				'name' => 'Tabelle',
				'html' => $this->tpl,
				'image' => '',
				'preview' => ''
			);
		}
		return $tpl;

	}

	/**
	 * returns all data formatted with an specific template
	 * @return string html source
	 */
	public function htmlShowData() {
		if (is_array($this->data))
			$data = $this->data;
		else
			$data = $this->getData();
		$tplrow = $this->getTemplate();
		$html = '';
		$nr = 1;
		foreach ($data as $d) {
			if (isAdmin ()) {
				$html .= '<div class="element">'; // title=""
				$el_indistr = get_class($this) . '-' . $this->database . '-' . $d['id'];
				if (PageRights::getObj()->isAllowed()) {
					$html .= '<div class="admineig" style="display:none">'; // title=""

					$html .='<div class="button edit" id="' . $el_indistr . '-edit" title="' . get_class($this) . ' &auml;ndern"><img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/icons/appedit.png" alt="[e]" /></div>';
					$html .= '<div class="button del" id="' . $el_indistr . '-del" title="' . get_class($this) . ' l&ouml;schen"><img src="' . $GLOBALS['cms_rootdir'] . 'admin/images/icons/appdel.png" alt="[X]" /></div>';
					//				$html .='<div class="editRow" id="'.$el_indistr.'-edit" title="'.get_class($this).' &auml;ndern"><img src="images/icons/appedit.png" alt="[e]" /></div>';
					//				$html .= '<div class="delRow" id="'.$el_indistr.'-del" title="'.get_class($this).' l&ouml;schen"><img src="images/icons/appdel.png" alt="[X]" /></div>';
					$html .= '</div>'; //get_class($this).
				}
			}
			$tmphtml = $tplrow['html'];
			if (isset($d['id']))
				$this->id = $d['id'];
			$html .= $this->parseData($tmphtml, $d,false,$nr);
			if (isAdmin ())
				$html .= '</div>'; // title=""
			$nr++;
		}
		return $html;
	}

	public function getDB() {
		$html = '';
		if ($this->database > 0)
			addWhere('id', '=', $this->database, 'i');
		setLimit(1);
		$dbs = $this->getDatabases('name,fields,fieldid,dbType');
                if(!isSet($dbs[0])) return false;
		return $dbs[0];
	}

	public function getFieldId($row = array(), $fieldid = '') {
		if ($fieldid != '')
			$this->fieldid_mysql = $fieldid;
		if (!$this->fieldid_mysql)
			$this->getDB();
		if (count($row) == 0)
			return $this->fieldid_mysql;
		$id = '';
		$spl = explode(',', $this->fieldid_mysql);

		foreach ($spl as $eig) {
			//language select

                        if (isset($row[$eig . "_" . Languages::getLang(false)]) && Languages::getLang(false) != '') {
                            $id .= ' ' . $row[$eig . "_" . Languages::getLang(false)];
                        } else {
                            $id .= ' ' . $row[$eig];
                        }

		}
		return substr($id, 1);
	}

	/**
	 *
	 * @param type $db
	 * @return DataEditor
	 */
	public function getForm($db = false) {
		if (!$db)
			$db = $this->getDB();
                if(!$db) return false;

		if($this->dbType == 1)
		    $dbsys = new DataEditor('user');
		elseif($this->dbType == 2)
		    $dbsys = new DataEditor('frontEndUser');
		else
		    $dbsys = new DataEditor('data_' . $db['name']);

		if ($this->id > 0)
			$dbsys->setEdit($this->id);

		$mainid = (isSet($_GET['get']) && $_GET['get'] == 'sub' && isSet($_GET['id'])) ? $_GET['id'] : 0;
		if ($mainid > 0) {
			addWhere('id', '=', $mainid);
			setLimit(1);
			select('elements', 'eldata');
			$fielterRow = getRow();
			$fielterRow = unserialize($fielterRow['eldata']);
			$fielterRow = unserialize($fielterRow['filter']);
			$filters = flipArray($fielterRow);
			foreach ($filters as $filter) {
				foreach ($db['fields'] as $fieldid => $field) {
					if ($field['name'] == $filter['fieldname']) {
						if (is_array($filter['value']))
							$val = $filter['value']['tagname'];
						else
							$val = $filter['value'];

						if ($filter['relation'] == 0)
							$db['fields'][$fieldid]['default'] = $val;
						if ($filter['relation'] == 4)
							$db['fields'][$fieldid]['default'] = ',' . $val . ',';
//						echo $db['fields'][$fieldid]['default'].'!';
//						if($filter['relation'] == 2)  $db['fields'][$fieldid] = $val;
//						if($filter['relation'] == 3)  $db['fields'][$fieldid] = $val;
					}
				}
			}
		}
//		if(!isset($_GET['inter']) || $_GET['inter'] != 'dia')
//			$dbsys->form->setSaveButton('Speichern');
		$dbsys->form->setFormAction('ajax.php?kl=DatabaseConnector' . buildGet('mm,db,new,edit,c'));
		$multilang = array();
		$invisible = array();
		foreach ($db['fields'] as $field) {
			if($field['type'] == 'multiselect'){
			  $tmp = explode(',',$field['eig']);
			  $field['eig'] = array();
			  foreach($tmp as $v){
				$field['eig'][$v] = $v;
			  }
			}

			$dbsys->form->addElement($field['name'], $field['name'], $field['type'], $field["default"], $field['eig']);
			if(isSet($field['multilang']) && $field['multilang'] == '1'){
				$multilang[] = $field['name'];
			}
			if ($field['type'] == 'editor')
				$dbsys->setInvisible($field['name']);
		}

        if (rights::getObj()->isAdmin() || rights::getObj()->getContentRights('release')) {
			$dbsys->form->addElement('Ver&ouml;ffentlichen', 'public', 'select', '1', array('Nein', 'Ja'));
		} else {
			//TODO: Post auswerten
			$dbsys->form->addElement('Anfrage auf Ver&ouml;ffentlichung', 'public_request', 'select', '0', array('Nein', 'Ja'), false);
		}

        if(count($multilang) > 0){
			$dbsys->form->setMultiLanguage($multilang);
			foreach($multilang as $elName){
				foreach($GLOBALS['languages'] as $lid=>$lang){
					$dbsys->setInvisible($elName.'_'.$lid);

				}
			}
		}


//                if (rights::getObj()->isAdmin()) {
		$dbsys->form->useTab('Admin');
		$dbsys->form->addElement('Erstellt', 'erstellt', 'time', time());
		$dbsys->form->addElement('Ver&ouml;ffentlicht', 'published', 'time', '0');
		if ($dbsys->form->posted) {
//			if($this->dbType == 1)
//				$_POST['loginname'] = menuepunkte::setTitelIntern($this->getFieldId($_POST),$dbsys->getMysqlTable());
//			elseif($this->dbType == 2)
//				$_POST['loginname'] = menuepunkte::setTitelIntern($this->getFieldId($_POST),$dbsys->getMysqlTable());
//			else
			if($this->dbType == 0){
				if(isSet($_GET['edit']) && $_GET['edit'] > 0)
					$id = $_GET['edit'];
				else $id = 0;
				$_POST['title_intern'] = menuepunkte::setTitelIntern($this->getFieldId($_POST),$dbsys->getMysqlTable(),$id);

				if(isset($_POST['public']) && $_POST['public'] == '1' && isSet($_POST['published']) && $_POST['published'] == '0')
					$_POST['published'] = time();
			}
		}
		$dbsys->form->addElement('interner Titel', 'title_intern', 'text');
		$dbsys->form->addElement('Autor', 'autor', 'text', \cms\session::getObj()->getUserId());

//                } else {
//                    $dbsys->form->addElement('', 'Autor', 'hidden',session::getObj()->getUserId());
//                }
		$dbsys->setInvisible(array('title_intern', 'id'));
//		$dbsys->addMysqlCol('title_intern');
		addWhere('dbid', '=', $this->database, 'i');
		addWhere('preview', '!=', '', 's');
		select('dataTemplates');
		while ($row = getRow()) {
			$dbsys->addcol('Vorschau', '<a href="' . $GLOBALS['cms_roothtml'] . $db['name'] . '/' . $row['preview'] . '-[DB_title_intern].html">Vorschau</a>');
		}
		$dbsys->setDataTitleId($this->fieldid);
		return $dbsys;
	}

	/**
	 * returns edit form for a specific database
	 * @return string html source
	 */
	public function htmlShowDatabase() {
		$html = '';
		$db = $this->getDB();
		$dbsys = $this->getForm($db);
		$dbsys->setOrderBy('erstellt DESC');

//                new dBug($this->getFieldId());
//                $inject['QUICKLINKS'] = '<input type="text" value=""'
                if(UserConfig::getObj()->getAdminDBListLimit() > 0){
                    $dbsys->setLimit(UserConfig::getObj()->getAdminDBListLimit());
                }
		if ($dbsys->form->posted && isSet($_POST['public_request']) && $_POST['public_request'] == '1' && !rights::getObj()->isAdmin()) {
			$titel = $this->getFieldId($_POST);
			$msg = 'Diese Email wurde von <b>' . $_SERVER['HTTP_HOST'] . '</b> versendet.<br />';
			$msg .= 'Der User <b>' . \cms\session::getObj()->getUserName() . '</b> m&ouml;chte den Artikel <b>' . $titel . '</b> freigeben<br /><br />';

			addWhere('dbid', '=', $this->database, 'i');
			addWhere('preview', '!=', '', 's');
			select('dataTemplates');
			$links = array();
			while ($row = getRow()) {
				$links[] = 'http://' . $_SERVER['HTTP_HOST'] . $GLOBALS['cms_roothtml'] . $db['name'] . '/' . $row['preview'] . '-' . menuepunkte::setTitelIntern($titel) . '.html';
			}
			if (count($links) == 1) {
				$msg .= 'Vorschaulink: <a href="' . $links[0] . '">' . $titel . '</a>';
			} elseif (count($links) > 1) {
				$msg .= 'Vorschaulinks: ';
				foreach ($links as $i => $link) {
					$msg .= ' <a href="' . $link . '">' . $i . '</a>';
				}
			}

			$mail = new mimeMail();
			$mail->setSubject('Freigabeanfrage');
			$mail->setFrom(\cms\session::getObj()->getUserEmail(), \cms\session::getObj()->getUserName());
			$mail->setBody($msg);
			if(UserConfig::getObj()->getPublishRequestMail() != '')
				$mail->addRecipient(UserConfig::getObj()->getPublishRequestMail());
			else echo 'Error:PublishRequestMail wurde in der UserConfig nicht gesetzt';
//					var_dump($dbsys);

			$mail->send();

			unset($obj);
		}

//		if (!$dbsys->edit && !$dbsys->new && !$dbsys->del)
//			$html .= '<h1>' . $db['name'] . '</h1>';


                if(isset($_POST['quicksearch']) && $_POST['quicksearch'] != ''){
                    addWhere($this->getFieldId(), 'LIKE', '%'.$_POST['quicksearch'].'%');
                }


		$html .= $dbsys->getHtml();

		if($dbsys->form->posted){
			if($dbsys->new) $this->triggerEvent (Event::NEW_ENTRY,$dbsys->form->getDataid());
			else if($dbsys->edit) $this->triggerEvent (Event::EDIT,$dbsys->form->getDataid());
			else if($dbsys->del) $this->triggerEvent (Event::DEL,$dbsys->form->getDataid());
		}else{
                    $html .= '<span id="activateQuicksearch" data-fieldname="'.$this->getFieldId().'"></span>';
                }
		return $html;
	}

	private function getSqlType($type) {
		if ($type == 'textarea' || $type == 'editor' || $type == 'multiselect')
			$sqltype = ' TEXT NOT NULL';
		elseif ($type == 'foreignId')
			$sqltype = ' INT';
		elseif ($type == 'datei' || $type == 'bild')
			$sqltype = ' VARCHAR(128) NOT NULL';
		else
			$sqltype = ' VARCHAR(128) NOT NULL';
		return $sqltype;
	}

	/**
	 * returns a list of database with edit functions
	 * @return string html source
	 */
	public function htmlDBOverview() {
		$html = '';
//		$dbs = $this->getDatabases('id,name');
//		foreach($dbs as $id => $name){
//			$html .= '<a href="'.buildGet('mm',true,true).'&amp;db='.$id.'">'.$name.'</a><br>';
//		}
		$fullIndexes = $this->getFullIndexTypes();
		$dbedit = $this->getDBForm();
		if ($dbedit->del) {
			addWhere('id', '=', $dbedit->id, 'i');
			select('databases', 'name,dbType');
			$row = getRows();
			if($row[0]['dbType'] == '0'){
				$sql = 'DROP TABLE `data_' . $row[0]['name'] . '`';
				$con = $GLOBALS['cmsdb']->prepare($sql);
				$con->execute();
			}
		}
		if ($dbedit->form->posted && $dbedit->new && checkVar(array('name','dbType')) && $_POST['dbType'] == '0') {
			$elements = $dbedit->form->getElements();
			$sqladd = '';
			$searchmul = array();
			$fields_new = $elements['fields']['data']->getElements();
			foreach ($fields_new['fieldname']['value'] as $id => $name) {
				$sqltype = $this->getSqlType($fields_new['type']['value'][$id]);
				$sqladd .= '`' . addslashes($name) . '` ' . $sqltype . ",\n";
                                if(isSet($fields_new['multilang']['value'][$id]) && $fields_new['multilang']['value'][$id] == '1'){
					foreach($GLOBALS['languages'] as $lId => $lang)
						$sqladd .= '`' . addslashes($name) . '_'.$lang.'` ' . $sqltype . ",\n";
				}
				$searchmul[$fields_new['searchmul']['value'][$id]][] = $name;
			}
			$sql = 'CREATE TABLE `data_' . addslashes($_POST['name']) . '`(
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`public` INT NOT NULL,
				`erstellt` INT NOT NULL,
				`published` INT NOT NULL,
				`autor` VARCHAR(64) NOT NULL,
				`title_intern` VARCHAR(128) NOT NULL,
				' . $sqladd . '
				PRIMARY KEY (`id`)
			)';
			$con = $GLOBALS['cmsdb']->prepare($sql); //.' WHERE '.$where
			if (!$con)
				$GLOBALS['cmsdb']->throwError();
			$con->execute();
			if ($GLOBALS['cmsdb']->errno)
				die($GLOBALS['cmsdb']->error);
			/*
			 * FULLINDEX erstellen
			 */
			for($i=0;$i<4;$i++){
				if(isSet($searchmul[$i])){
					$fullIndex = $searchmul[$i];

					$c = 0;
					$indexes = array();
					foreach($fullIndex as $ind){
						if($c < 16){
							$indexes[] = $ind;
						}
						$c++;
					}
					$sql = "ALTER TABLE `data_" . addslashes($_POST['name']) . "` ADD FULLTEXT INDEX `Search".($i+1)."` (`" . implode('`,`', $indexes) . "`);";

					$con = $GLOBALS['cmsdb']->prepare($sql); //.' WHERE '.$where
					if (!$con)
						$GLOBALS['cmsdb']->throwError();
					$con->execute();
					if ($GLOBALS['cmsdb']->errno)
						die($GLOBALS['cmsdb']->error);
				}
			}
		}
		if ($dbedit->form->posted && $dbedit->edit || ($dbedit->new && checkVar(array('name','dbType')) && $_POST['dbType'] != '0')) {
			addWhere('id', '=', $dbedit->id, 'i');
			$db = $this->getDatabases();
			$fields_old = $db[$dbedit->id]['fields'];
			$elements = $dbedit->form->getElements();
			$newTableName = addslashes($_POST['name']);
			if($db[$dbedit->id]['name'] != $newTableName && $_POST['dbType'] == '0'){
				$sqlstr = 'ALTER TABLE `data_'.$db[$dbedit->id]['name'].'` RENAME TO `data_'.$newTableName.'`';

				$con = $GLOBALS['cmsdb']->prepare($sqlstr); //.' WHERE '.$where
				if (!$con)
					$GLOBALS['cmsdb']->throwError();
				$con->execute();
				if ($GLOBALS['cmsdb']->errno)
					die($GLOBALS['cmsdb']->error);
			}
			$sqladd = '';
			$fields_new = $elements['fields']['data']->getElements();
			$fieldnames = $fields_new['fieldname']['value'];
			array_pop($_POST['tableorderdbFields']);
			$fullIndex = array();
			if($_POST['dbType'] == '0')
				$table = '`data_' . addslashes($_POST['name']) . '`';
			elseif($_POST['dbType'] == '1')
				$table = '`user`';
			elseif($_POST['dbType'] == '2')
				$table = '`frontEndUser`';
			$searchmul = array();
			foreach ($_POST['tableorderdbFields'] as $x => $id) {
				$newid = (int) $x + 1;
				$oldid = $x;
				$addLang = false;
				$delLang = false;

				if (!isSet($fields_old[$oldid]['multilang']) || $fields_old[$oldid]['multilang'] == '0') { //!isSet($fields_old[$oldid]['multilang'][$oldid]) ||
					if (isSet($fields_new['multilang']['value'][$newid]) && $fields_new['multilang']['value'][$newid] == '1')
						$addLang = true;
				}elseif ($fields_old[$oldid]['multilang'] == '1') {
					if (!isSet($fields_new['multilang']['value'][$newid]) || $fields_new['multilang']['value'][$newid] == '0')
						$delLang = true;
				}

				if ($delLang) {
					foreach ($GLOBALS['languages'] as $lId => $lang)
						$sql[] = 'ALTER TABLE '.$table . ' DROP COLUMN `' . addslashes($fields_old[$oldid]['name']) . '_' . $lang . '`';
				}

				if ($addLang) {
					$sqltype = $this->getSqlType($fields_new['type']['value'][$newid]);
					foreach ($GLOBALS['languages'] as $lId => $lang)
						$sql[] = 'ALTER TABLE '.$table . ' ADD COLUMN `' . addslashes($fieldnames[$newid]) . '_' . $lang . '` ' . $sqltype;
				}

				if (!isset($fields_old[$oldid]['name'])) {
					$sqltype = $this->getSqlType($fields_new['type']['value'][$newid]);
					$sql[] = 'ALTER TABLE '.$table . ' ADD COLUMN `' . addslashes($fieldnames[$newid]) . '` ' . $sqltype;

					$sqlexec = true;
					if (in_array($fields_new['type']['value'][$newid], $fullIndexes)) {
						$searchmul[$fields_new['searchmul']['value'][$newid]][] = addslashes($fieldnames[$newid]);
						$fullIndex[] = addslashes($fieldnames[$newid]);
					}
				} elseif (!isset($fields_new['type']['value'][$newid])) {
					$sql[] = 'ALTER TABLE '.$table . ' DROP COLUMN `' . addslashes($fields_old[$oldid]['name']) . '`';

					$sqlexec = true;
				} else {//if($fields_old[$oldid]['name'] != $fieldnames[$newid]){
					$sqltype = $this->getSqlType($fields_new['type']['value'][$newid]);
					$sql[] = 'ALTER TABLE '.$table . ' CHANGE COLUMN `' . addslashes($fields_old[$oldid]['name']) . '` `' . addslashes($fieldnames[$newid]) . '` ' . $sqltype;
					$sqlexec = true;
					if (!$delLang && !$addLang && isSet($fields_new['multilang']['value'][$newid]) && $fields_new['multilang']['value'][$newid] == '1') {
						foreach ($GLOBALS['languages'] as $lId => $lang)
							$sql[] = 'ALTER TABLE '.$table . ' CHANGE COLUMN `' . addslashes($fields_old[$oldid]['name']) . '_' . $lang . '` `' . addslashes($fieldnames[$newid]) . '_' . $lang . '` ' . $sqltype;
					}

					if (in_array($fields_new['type']['value'][$newid], $fullIndexes)) {
						$fullIndex[] = addslashes($fieldnames[$newid]);
						if(!isSet($searchmul[$fields_new['searchmul']['value'][$newid]]))
							$searchmul[$fields_new['searchmul']['value'][$newid]] = array();
						$searchmul[$fields_new['searchmul']['value'][$newid]][] = addslashes($fieldnames[$newid]);
					}
				}
			}
                        if($_POST['dbType'] == '0'){
                            mysqli_report(MYSQLI_REPORT_STRICT);
                            for($i=1;$i<5;$i++){
                                    $sqlx = "ALTER TABLE " . $table . " DROP INDEX `Search".$i."`";
                                    $con = mys::getObj()->prepare($sqlx);
                                    if ($con !== false)
                                            $con->execute();
                            }
                            mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX );
                        }
			foreach ($sql as $s) {
				$con = $GLOBALS['cmsdb']->prepare($s); //.' WHERE '.$where
				if (!$con)
					$GLOBALS['cmsdb']->throwError();
				$con->execute();
				if ($GLOBALS['cmsdb']->errno)
					die($GLOBALS['cmsdb']->error);
			}
			/**
			 * Create SearchIndex
			 */
//			if (count($fullIndex) > 0) {
			if($_POST['dbType'] == '0'){
				for($i=0;$i<4;$i++){
					if(isSet($searchmul[$i])){
						$fullIndex = $searchmul[$i];
						$c = 0;
						$indexes = array();
						foreach($fullIndex as $ind){
							if($c < 16){
								$indexes[] = $ind;
							}
							$c++;
						}
						$sql = "ALTER TABLE " . $table . " ADD FULLTEXT INDEX `Search".($i+1)."` (`" . implode('`,`', $indexes) . "`);";
						$con = $GLOBALS['cmsdb']->prepare($sql); //.' WHERE '.$where
						if (!$con)
							$GLOBALS['cmsdb']->throwError();
						$con->execute();
						if ($GLOBALS['cmsdb']->errno)
							die($GLOBALS['cmsdb']->error);
					}
				}
			}
//			}
		}
//		if(!isset($sqlexec))
		$html .= $dbedit->getHtml();
		return $html;
	}

	public function getHtml() {
		if ($this->database > 0)
			return $this->htmlShowDatabase();
//		addWhere('id', '=', '11');
//		select('databases');
//		$rows = getRows();
//		echo '<br><br><br><br><br>';
//		foreach($rows as $row){
//			$f = unserialize($row['fields']);
//			var_dump(unserialize($f['options']));
//			var_dump($f);
//		}
//		var_dump($_POST);
		return $this->htmlDBOverview();
		return (string) $dbedit;
	}

	public function __toString() {
		return $this->getHtml();
	}

	public function setOverWriteLinks($overWriteLinks) {
	 $this->overWriteLinks = $overWriteLinks;
	}
	public function getDatabaseId(){
		return $this->database;
	}

	public function triggerEvent($name,$dataId,$eventData = false){
//		echo 'TRIGGER: '.$name;
		$this->triggerDataId = $dataId;
		if(!$eventData){
			addWhere('id', '=', $this->database);
			select('databases','eventActions');
			$row = getRow();
			$eventData = $row['eventActions'];
		}

		Action::triggerEvent($name, $eventData,$this);
	}
	public function parseStr($str){
		addWhere('id', '=', $this->triggerDataId);
		if ($this->dbType == 0)select('data_' . $this->databaseName);
		elseif ($this->dbType == 1) select('user');
		elseif ($this->dbType == 2) select ('frontEndUser');

		$data = getRow();
		if ($this->dbType == 2){
			if(isSet($_POST['password']))
				$data['password'] = $_POST['password'];
		}
		return $this->parseData($str, $data);
	}
        public static function cleanTemplateVars($html){
            $html = preg_replace('/\[DB_(.*?)\]/', '', $html);
            return $html;
        }

    public function fetchRawData() {
        return $this->data;
    }

    public function setRawData(array $data) {
        $this->data = $data;
    }

    public function addField($name, $type) {
        $this->fields[] = [ "name" => $name, "type" => $type];
    }

    }

?>
