<?php
/**
 * Image Klasse zum bearbeiten von Bildern
 */
class image{
	private $name;
	private $pfad;
	private $img;
	private $width;
	private $height;
	private $typ;
	private $html;
	private $destimg;
	public $error;

        private $autosave = true;
        private $absolutepath = false;

	/**
	 * Erstellt ein neues Bild zum bearbeiten
	 * @param String $pfad Pfad zur Datei
	 * @param String $name Dateiname
	 * @param String $dname Neuer Dateiname
	 */
	public function __construct($pfad, $absolutepath = false,$createImage = true){

            $this->pfad = ($absolutepath)? $pfad : $_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir']."/".$pfad;
            $this->absolutepath = $absolutepath;

            if (!file_exists($this->pfad) || !is_readable($this->pfad)) {
                return false;
            }

            $info = @getimagesize($this->pfad);
            if($info === false)
                return false;

            $this->width = $info[0];
            $this->height = $info[1];
            $this->typ = $info[2];
            $this->html = $info[3];

            unset($info);

            if ($createImage) {
                switch ($this->typ) {
                    case 1 : $this->img = imagecreatefromgif($this->pfad); break;
                    case 2 : $this->img = imagecreatefromjpeg($this->pfad); break;
                    case 3 : $this->img = imagecreatefrompng($this->pfad); imagealphablending($this->img, true); break;
                }
                if(!$this->img) $error = "Image konnte nicht geladen werden";
            }    
            
            
	}

        public static function create($pfad, $absolutepath = false,$createImage = true) {
            return new image($pfad,$absolutepath,$createImage );
        }

	/**
	 * Gibt einen Bereich aus dem Bild zurück
	 * @param int $x Position X
	 * @param int $y Position Y
	 * @param int $width Crop Breite
	 * @param int $height Crop Höhe
	 * @param int $dwidth Finale Breite
	 * @param int $dheight Finale Höhe
	 * @return Boolean Success
	 */
	public function crop($x,$y,$width,$height,$dwidth =0,$dheight=0){
		
            if ($this->error != '')
                return false;

            if ($dwidth == 0) {
                $dwidth = $width;
            } elseif ($dheight == 0) {
                $dheight = $this->getPropheight($width, $dwidth,$height);
            }

            if ($dheight== 0) {
                $dheight = $height;
            }

            $this->destimg = imagecreatetruecolor($dwidth,$dheight);
            imagealphablending($this->destimg, false);
            $transparent = imagecolorallocatealpha($this->destimg, 255, 255, 255, 127);
            imagefill($this->destimg, 0, 0, $transparent);
            imagesavealpha($this->destimg, true);




            if(!$this->destimg) {
                $this->error = 'Es konnte kein GD Image Stream erstellt werden';
                return false;
            }

            if(!imagecopyresampled($this->destimg,$this->img,0,0,$x,$y,$dwidth, $dheight,$width,$height)) {
                $this->error = 'Kopie der Image Instanz fehlgeschlagen';
                return false;
            }

            $this->img = $this->destimg;
            return $this;
	}
	/**
	 * Verkleinert das Bild
	 * @param int $width Finale Breite
	 * @param int $mheight Maximale Finale Höhe
	 * @return Boolean Success
	 */
	public function resize($maxWidth,$maxHeight = 0,$doNotForce = false){
		
            if($this->error != '')
                return false;

            if ($maxWidth > $this->width && $maxHeight > $this->height && $doNotForce) {
                return $this;
            }

            if ($maxWidth < $this->width) {
                $faktor = $this->width / $maxWidth;
                $newHeight = $this->height / $faktor;
                $newWidth = $maxWidth;

                if ($newHeight > $maxHeight && $maxHeight > 0) {
                    $faktor = $this->height / $maxHeight;
                    $newHeight = $maxHeight;
                    $newWidth = $this->width / $faktor;
                }
            } else {
                $faktor = $this->height / $maxHeight;
                $newHeight = $maxHeight;
                $newWidth = $this->width / $faktor;
            }

            $this->destimg = imagecreatetruecolor($newWidth,$newHeight);
            imagealphablending($this->destimg, false);
            $transparent = imagecolorallocatealpha($this->destimg, 255, 255, 255, 127);
            imagefill($this->destimg, 0, 0, $transparent);
            imagesavealpha($this->destimg, true);

            
            if (!$this->destimg) {
                $this->error = 'Es konnte kein GD Image Stream erstellt werden';
                return false;
            }

            if (!imagecopyresampled($this->destimg,$this->img,0,0,0,0,round($newWidth),round($newHeight),$this->width,$this->height)) {
                $this->error = 'Kopie der Image Instanz fehlgeschlagen';
                return false;
            }

            $this->img = $this->destimg;
            return $this;
	}

	/**
	 * Gibt die Resoltierente Höhe zurück
	 * @param int $w Orginale Breite
	 * @param int $w2 Finale Breite
	 * @param int $h Orginale Höhe
	 * @return int Finale Höhe
	 */
	private function getPropheight($w,$w2,$h){
            $faktor = ($w)/$w2;
            return round($h/$faktor);
	}

	/**
	 * Speichert das neue Bild
	 * @return Boolean Success
	 */
	public function save($pfad = null,$options = array()){


            if ($pfad !== null) {
                $pfad = ($this->absolutepath)? $pfad : $_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir']."/".$pfad;
            } else {
                $pfad = $this->pfad;
            }

            $this->destimg = $this->img;

            if($this->error != '') return false;

            switch ($this->typ) {
                case 1: imagegif($this->img,$pfad); break;
                case 2: imagejpeg($this->img,$pfad,(isset($options["jpeg_quality"])) ? $options["jpeg_quality"] : 95);break;
                case 3: imagepng($this->img,$pfad);break;
            }
			
            if (!$this->destimg) {
                throw new Exception('Neue Bild konnte nicht gespeichert werden');
            } else {
                new \cms\file($pfad,true);
                return $this;
            }
	}

        public function cleanup() {
        }

        public function grayScale() {
            imagefilter($this->img, IMG_FILTER_GRAYSCALE);
            return $this;
        }
	public function getWidth() {
	    return $this->width;
	}

	public function getHeight() {
	    return $this->height;
	}
}
?>