<?php
    class sitemap {
        
        private $sites = array();
        private $baseUrl = '';
        
        public function getData() {
            
            //get siteInfo
            
            $this->baseUrl = "http://{$_SERVER["HTTP_HOST"]}/";
            
            //fetch menu
            addWhere("visible","=","1");
            select("menuepunkte","title_intern,erstellt");
            while($row = getRow()) $this->sites[] = $row;
            
            mys::getObj()->cleanup()->clearWhere();
            //fetch datatemplates
            $dbs = array();
            $res = mys::getObj()->query("SELECT t.preview,d.name FROM `dataTemplates` as t inner join `databases` as d on t.dbid = d.id where `preview` <> \"\"");
            while($row = $res->fetch_array()) {
                $dbs[] = $row;
            }
            
            mys::getObj()->cleanup()->clearWhere();
            foreach($dbs as $data) {
                $res = mys::getObj()->query("SELECT erstellt,title_intern FROM data_{$data["name"]} WHERE public = 1");
                while($row = $res->fetch_array()) {
                    $this->sites[] = array("title_intern" => "{$data["name"]}/{$data["preview"]}-{$row["title_intern"]}","erstellt" => $row["erstellt"]);
                }
            }
            
            return true;
            
        }
        
        public function getSitemap() {
            
            $this->getData();
            
            $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            $xml .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
            
            
            foreach ($this->sites as $site) {
                
                $date = date("Y-m-d",$site["erstellt"]);
                
                $xml .= "  <url>\n";
                $xml .= "    <loc>{$this->baseUrl}{$site["title_intern"]}.html</loc>\n";
                $xml .= "    <lastmod>{$date}</lastmod>\n";
                $xml .= "  </url>\n";
            }
            
            $xml .= "</urlset>\n";
            
            
            return $xml;
        }
        
    }
?>