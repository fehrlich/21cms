<?php
namespace cms;
/**
 * Description of User
 *
 * @author R.Heine
 */
class User {
	/**
	 *
	 * @var DataEditor
	 */
	private $dbsys;
        private $sql;

	public function  __construct() {

                $this->sql = \mys::getObj()->cleanup();
		$groups = \buildSelectArray('userGroups', array('id','name'));

		$this->dbsys = new \DataEditor('user');

		$this->dbsys->form->setFormAction('ajax.php?kl=User'.buildGet('edit,new'));
		$this->dbsys->form->addElement('Loginname', 'loginname','text');
		$this->dbsys->form->addElement('Passwort', 'pass','pwchange');
		$this->dbsys->form->addElement('Anzeige Name', 'name');
		$this->dbsys->form->addElement('Email', 'email', 'text');
		$this->dbsys->form->addElement('Gruppe', 'group','select','0', $groups);

		$addDbc = new \DatabaseConnector();
		addWhere('dbType','=','1');
		$addForm = $addDbc->getForm();
                if($addForm){
                    $addElements = $addForm->form->getElements();
                    $dontAdd = array('id','public','erstellt','published','autor','title_intern');
                    foreach($addElements as $el){
                            if(!\in_array($el['name'],$dontAdd))
                                    $this->dbsys->form->addElementArrays($el);
                    }
                }

		if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
			$forumGroups = \buildSelectArray('phpbb_groups', array('group_id','group_name'));

			$forumGroupId = '';
			if(isSet($_GET['edit']) && $_GET['edit'] > 0){
				\addWhere('id', '=', $_GET['edit']);
				\select('user', 'loginname');
				$row = \getRow();
				$oldUserName = $row['loginname'];
				\addWhere('username', '=', $oldUserName);
				\select('phpbb_users', 'group_id');
				$row = \getRow();
				$forumGroupId = $row['group_id'];
			}
			if($forumGroupId == '')$forumGroupId = 1;
			$this->dbsys->form->addElement('Forum', 'forumint','select',$forumGroupId,$forumGroups,false);
			if($this->dbsys->form->posted && isSet($_POST['forumint'])){
				$api = new \phpbb($_SERVER['DOCUMENT_ROOT'].$GLOBALS['forum_integration'], 'php');
				if(!$api->user_loggedin())
				    $api->user_login(array(
					    'username' => \cms\session::getObj()->getUserName(),
					    'password' => 'arnarn',
					    'admin' => '1',
					    'autologin' => true
				    ));
				$userData = array(
					'username' => $_POST['loginname'],
					'user_email' => $_POST['email'],
					'group_id' => $_POST['forumint']
				);
				if(isSet($_GET['edit']) && $_GET['edit'] > 0 && $forumGroupId > 0){
					if($_POST['loginname'] != $oldUserName){
						if($api->user_rename($oldUserName, $_POST['loginname']) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht geupdatet werden';
					}
					if($api->user_update($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht geupdatet werden';
					if(isSet($_POST['pass']) && $_POST['pass'] != '' && $_POST['pass'][0] == $_POST['pass'][1]){
						$userData['password'] = $_POST['pass'][0];
						if($api->user_change_password ($userData) != 'SUCCESS') echo 'Error: Das Passwort für den Forum User konnte nicht geändert werden';
					}
				}else{
					$userData['user_password'] = $_POST['pass'][0];
					if($api->user_add($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht angelegt werden';

				}
			}
			if($this->dbsys->del){
				if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
					$api = new \phpbb($_SERVER['DOCUMENT_ROOT'].$GLOBALS['forum_integration'], 'php');
					if(!$api->user_loggedin())
						$api->user_login(array(
							'username' => \cms\session::getObj()->getUserName(),
							'password' => 'arnarn',
							'admin' => '1',
							'autologin' => true
						));
					\addWhere('id', '=', $_GET['del']);
					\select('user', 'loginname');
					$row = \getRow();
					$userData = array(
						'username' => $row['loginname']
					);
					if($api->user_delete($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht gelöscht werden';
				}
			}
		}

		$this->dbsys->setInvisible('pass');
	}

	public function  __toString() {
		if(\cms\session::getObj()->getUserId() != '1'){
			addWhere('id', '!=','1');
		}
		return $this->dbsys->__toString();
	}
}
?>