<?php
/**
 * Description of User
 *
 * @author R.Heine
 */
    class rights {

        static private $handle = null;
        private $userId = 0;
        private $data = array();

        public function __construct() {
            self::$handle = $this;

            $this->userId = \cms\session::getObj()->getUserId();
            $this->adminCheck();
        }

        private function adminCheck() {
            $sql = mys::getObj()->cleanup();
            $res = $sql->query("SELECT r.admin FROM user AS u INNER JOIN userGroups AS r ON (u.group = r.id) WHERE u.id = {$this->userId} LIMIT 1");
            
            if ($res !== false) {
                $tmp = $res->fetch_array();
                $this->data["admin"] = $tmp["admin"];
                unset($tmp);
            } else {
                $this->data["admin"] = 0;
            }
        }
		/**
		 *
		 * @return rights
		 */
        static public function getObj() {
            if (self::$handle === null) {
                new rights();
            }

            return self::$handle;
        }

        public function getContentRights($what) {

            if ($this->isAdmin()) {
                return true;
            }

            if (!isset($this->data["contentRights"])) {
                
                $sql = mys::getObj()->cleanup();
                $res = $sql->query("SELECT r.create,r.edit,r.delete,r.release,r.admin
                             FROM user AS u
                             INNER JOIN userGroups AS r ON (u.group = r.id)
                             WHERE u.id = {$this->userId}
                             LIMIT 1");

                if ($res !== false) {
                    $this->data["contentRights"] = $res->fetch_array();
                    $res->close();
                } else {
                    $this->data["contentRights"]["create"] = false;
                    $this->data["contentRights"]["edit"] = false;
                    $this->data["contentRights"]["release"] = false;
                    $this->data["contentRights"]["delete"] = false;
                }

            }

            switch ($what) {
                case 'edit': return $this->data["contentRights"]["edit"]; break;
                case 'create': return $this->data["contentRights"]["create"]; break;
                case 'delete': return $this->data["contentRights"]["delete"]; break;
                case 'release': return $this->data["contentRights"]["release"]; break;
                default : return false;
            }
        }

        public function getDbRights($dbid,$what) {

            if ($this->isAdmin()) {
                return true;
            }


            if (!isset($this->data["dbRights"][$dbid])) {
                $sql  = mys::getObj()->cleanup();
                $res = $sql->query("SELECT d.edit,d.delete,d.create 
                                    FROM userDbRights AS d
                                    INNER JOIN user AS u ON u.group = d.groupId
                                    WHERE u.id = ". \cms\session::getObj()->getUserId() ." AND dbId = {$dbid} LIMIT 1");
                
                if ($res !== false) {
                    
                    $this->data["dbRights"][$dbid] = $res->fetch_array();
                    $res->close();
                } else {
                    $this->data["dbRights"][$dbid]["create"] = 0;
                    $this->data["dbRights"][$dbid]["edit"] = 0;
                    $this->data["dbRights"][$dbid]["delete"] = 0;
                }
                            
            }

            switch ($what) {
                case 'edit': return $this->data["dbRights"][$dbid]["edit"]; break;
                case 'create': return $this->data["dbRights"][$dbid]["create"]; break;
                case 'delete': return $this->data["dbRights"][$dbid]["delete"]; break;
                default : return false;
            }
        }

        public function isAdmin() {
            return $this->data["admin"];
        }

        public function isSuperAdmin() {
			return ($this->userId == 1);
        }
    }
?>
