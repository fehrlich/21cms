<?php

    class MailQueueRunner {

        private $data;

        public function readMailQueueItem() {

            setLimit(1);
            select("newsletterQueue");

            $this->data = getRow();

            if ( empty($this->data) ) {
                return false;
            }

            setLimit(1);
            addWhere("id","=",$this->data["id"]);
            delete("newsletterQueue");

            return true;
        }

        public function getData() {
            return $this->data;
        }

        public function send() {
            $mail = new mimeMail();
            $mail->setFromComplete($this->data["from"]);
            $mail->setSubject($this->data["subject"]);
            $mail->addRecipient($this->data["to"]);
            $mail->setBody($this->data["content"]);
            $mail->send();
        }
    }