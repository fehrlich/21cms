<?php
class RssFeeds {
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;
	private $data;
	private $dbName;

	private function getXMLTemplate($nr){
		$xml = '';
		//if($nr == 0){
			if(isset($this->data['atomFeed']) && $this->data['atomFeed'] == '1'){
				$addAtom = "\n\t\t\t".'<!--<ort>[DB_'.$this->data['ortId'].']</ort>';
				$addAtom .= "\n\t\t\t".'<nummer>[DB_id]</nummer>';
				$addAtom .= "\n\t\t\t".'<link>http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml'].$this->dbName.'/'.$this->data['showin'].'-[DB_title_intern].html</link>';
				$addAtom .= "\n\t\t\t".'<tags>[DB_'.$this->data['catId'].']</tags>';
				if(UserConfig::getObj()->getContactMail() != '') $addAtom .= "\n\t\t\t".'<kontakt>'.UserConfig::getObj()->getContactMail().'</kontakt>-->';
			}else{
				$addAtom = '';
			}

			$xml = '
		<item>
			<title>[DB_'.$this->data['titleId'].']</title>
			<description><![CDATA[[DB_'.$this->data['descId'].']'.$addAtom.']]></description>
			<guid isPermaLink="true">http://'.$_SERVER['HTTP_HOST'].'/'.$this->dbName.'/'.$this->data['showin'].'-[DB_title_intern].html</guid>
			<pubDate>[DB_published]</pubDate>
			<author>'.$this->data["authorEmail"].'</author>
			<link>http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml'].$this->dbName.'/'.$this->data['showin'].'-[DB_title_intern].html</link>
			<category>[DB_'.$this->data['catId'].']</category>
		</item>';

		return $xml;
	}

	private function getData($name){
		addWhere('name', '=', $name,'s');
		select('rss');
		$this->data = getRow();
	}

	public function xmlOutPut(){
		$order = null;
		$dbname = $_GET['db'];
		$this->dbName = $dbname;
		$this->getData($_GET['name']);
		if(isSet($this->data['atomFeed']) && $this->data['atomFeed']) {
			$addAtom1 = ' xmlns:atom="http://www.w3.org/2005/Atom"';
			$addAtom2 = "\n".'<atom:link href="http://'.$_SERVER['HTTP_HOST'].$GLOBALS['cms_roothtml'].$this->dbName.'/'.str_replace('+', '%20', urlencode($this->data['name'])).'.xml" rel="self" type="application/rss+xml" />'."\n\t\t";
//			$addAtom2 = '';
		} else {
			$addAtom1 = '';
			$addAtom2 = '';
		}
		$xml = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0"'.$addAtom1.'>
	<channel>
		<title>'.str_replace('&', '&amp;', $this->data['name']).'</title>
		<link>'.$this->data['link'].'</link>
		<description>'.str_replace('&', '&amp;', $this->data['desc']).'</description>
		<language>de-de</language>
		<generator>'.$this->data['link'].'</generator>
		<ttl>20</ttl>'.$addAtom2.'
		';
		if($this->data['image'] != ''){
			$xml .='<image>
			<title>'.str_replace('&', '&amp;', $this->data['name']).'</title>
			<url>http://'.$_SERVER['HTTP_HOST'].$this->data['image'].'</url>
			<link>'.$this->data['link'].'</link>
		</image>';
		}/*
		 * <width>105</width>
		 * <height>62</height>
		 */

		$dbs = new DatabaseConnector();
		addWhere('id', '=', $this->data['database'], 'i');
		setLimit(1);

        $tmp = $dbs->getDatabases('name,fields,fieldid');
		$db = array_pop($tmp);

        if ( empty($db) ) {
            http_response_code(404);
            return;
        }

		addWherePrepare('title_intern != title');
		select('tags');
		$tag_rows = getRows();
		$tags = array();
		foreach($tag_rows as $tag){
			$tags[$tag['title_intern']] = $tag['title'];
		}
		if($this->data['filter'] != '0'){
			$this->data['filter'] = unserialize($this->data['filter']);
			$filters = flipArray($this->data['filter']);

			foreach($filters as $filter){
				if($filter['fieldname'] != ''){
					$ignore = false;
					if($filter['value']['tagname'] == '[TAG]') {
						if(!isset($_GET['tag'])) $ignore = true;
						else{
							if(isSet($tags[$_GET['tag']])) $filter['value']['tagname'] = $tags[$_GET['tag']];
							else $filter['value']['tagname'] = $_GET['tag'];
						}
					}
					if(!$ignore){
						if(is_array($filter['value'])) {
							if(isset($tags[$filter['value']['tagname']]))
								$filter['value'] = $tags[$filter['value']['tagname']];
							else
								$filter['value'] = $filter['value']['tagname'];
						}


						if($filter['relation'] == '=' || $filter['relation'] == '!='){
							addWhere($filter['fieldname'], $filter['relation'], $filter['value']);
							$addW[] = array($filter['fieldname'], $filter['relation'], $filter['value'],'s');
						}else{
							addWhere($filter['fieldname'], 'LIKE', '%,'.$filter['value'].',%');
							$addW[] = array($filter['fieldname'], 'LIKE', '%,'.$filter['value'].',%','s');
						}
					}
				}
			}
		};
		if (isset($this->data['limit'])) setLimit($this->data['limit']);


		if (isset($this->data["sort"])) {
			$order = $this->data["sort"];
			if (isset($this->data["order"]) && $this->data["order"] != 0) {
				$order .= ' ';
				$order .= ($this->data["order"] == 1) ? "desc" : "asc";
			}
		}

		$data = $dbs->getData($db['name'],'*',$order);
		$xmltpl = $this->getXMLTemplate(0);
		foreach($data as $d){
			foreach($d as $id => $val){

					//strip tags extra für twentyone one job feed eingefügt. die wollten es so, die bekommen es so!
					$val = strip_tags($val,"<br><strong><ul><li><ol><em><q><p>");
					$d[$id] = htmlspecialchars($val,ENT_QUOTES,'UTF-8');
					$d[$id] = str_replace(array("&lt;","&gt;","&amp;nbsp;","&amp;uuml;","&amp;ouml;","&amp;auml;"),array("<",">"," ","ü","ö","ä"),$d[$id]);
			}
			$xml .= $dbs->parseData($xmltpl, $d,true);
		}
		$xml .= "\t".'</channel>';
		$xml .= '</rss>';

		return $xml;
	}
	function  __construct() {

		if(isAdmin()){
			$this->dbc = new DataEditor('rss');

			$this->dbc->form->addElement('Name', 'name', 'text');

			$this->dbc->form->addElement('Beschreibung', 'desc', 'text');
			$this->dbc->form->addElement('Link', 'link', 'text');
			$this->dbc->form->addElement('Bild', 'image', 'bild');

			$data = array('selectName' => true);
			$this->dbc->form->addElement('Datenbank', 'database', 'database');
			$this->dbc->form->addElement('Limit', 'limit', 'text');

			$this->dbc->form->addElement('Title Id', 'titleId', 'text');
			$this->dbc->form->addElement('Beschreibungs Id', 'descId', 'text');
			$this->dbc->form->addElement('Kategorie Id', 'catId', 'text');
			$this->dbc->form->addElement('Ort Id', 'ortId', 'text');
			$this->dbc->form->addElement('Author Email', 'authorEmail', 'text');
			$this->dbc->form->addElement("AtomFeed","atomFeed","simpleCheck",'',"AtomFeed");

			$m = new menu(0,array(),'',0,true,false);
			$seiten = array('' => '[Standard]');
			$s = $m->getArrayList('title_intern',false);
			$seiten = mysql2selectArray($s,$seiten);

			$this->dbc->form->addElement('Anzeigen auf', 'showin','select','',$seiten);
			$this->dbc->form->addElement('Listen Template', 'tpl', 'select','0',array('Normal'));

			$this->dbc->form->addElement('HauptFeed', 'mainfeed', 'simpleCheck');
			$this->dbc->form->useTab('Filter');
			$this->dbc->form->addElement('Filter', 'filter', 'filter', '0',array('dbformname' => 'database'));
			$this->dbc->form->setFormAction('ajax.php?kl=RssFeeds'.buildGet('edit,new'));
			$this->dbc->setInvisible('filter');

			$this->dbc->form->useTab("Sortierung");
			$this->dbc->form->addElement("Sortieren","sort","text");
			$this->dbc->form->addElement("Art","order","select",0,array("Aufsteigend","Absteigend"));


		}
	}
	function  __toString() {
		return (string) $this->dbc;
	}
}
?>