<?php

/**
 * Description of Parser
 *
 * @author XX
 */
class Parser {

    private $form;
    private $dbCon;
    private $userid;
    private $useAdminAuthor = false;

    public function __construct($form = null, $dbCon = null, $userid = null) {
        $this->form = $form;
        $this->dbCon = $dbCon;
        $this->userid = $userid;
    }

    public function useAdminAuthor($useAdminAuthor = true) {
        $this->useAdminAuthor = $useAdminAuthor;
    }

    public function preParser() {
        
    }

    public function sufParser() {
        if (preg_match_all('#\[IF\((.*?)\)\](.*?)?(\[ELSE\](.*?)?)?\[/IF\]#si', $this->txt, $matches)) { //|\[IFEND\]
            foreach ($matches[0] as $i => $match) {

                $condString = $matches[1][$i];
                $condBool = true;
                $and = true;
                if (strpos($condString, '&') !== false) {
                    if (strpos($condString, '&amp;&amp;'))
                        $str = '&amp;&amp;';
                    elseif (strpos($condString, '&&'))
                        $str = '&&';
                    elseif (strpos($condString, '&amp;'))
                        $str = '&amp;';
                    else
                        $str = '&';

                    $conditons = explode($str, $condString);
                }
                elseif (strpos($condString, '|') !== false) {
                    if (strpos($condString, '||'))
                        $str = '||';
                    elseif (strpos($condString, '|'))
                        $str = '|';

                    $conditons = explode($str, $condString);
                    $and = false;
                    $condBool = false;
                }
                else
                    $conditons = array($condString);

                foreach ($conditons as $condString) {
                    $condString = trim($condString);
                    if (strpos($condString, '==') !== false) {
                        $spl = explode('==', $condString);
                        if (trim($spl[0]) == trim($spl[1]))
                            $condBoolTmp = true;
                        else
                            $condBoolTmp = false;
                    }
                    elseif (strpos($condString, '!=') !== false) {
                        $spl = explode('!=', $condString);
                        if (trim($spl[0]) != trim($spl[1]))
                            $condBoolTmp = true;
                        else
                            $condBoolTmp = false;
                    }
                    elseif (strpos($condString, '=') !== false) {
                        $spl = explode('=', $condString);
                        if (trim($spl[0]) == trim($spl[1]))
                            $condBoolTmp = true;
                        else
                            $condBoolTmp = false;
                    }elseif ($condString != '')
                        $condBoolTmp = true;
                    else
                        $condBoolTmp = false;

                    if ($and)
                        $condBool = $condBool && $condBoolTmp;
                    else
                        $condBool = $condBool || $condBoolTmp;
                }
//				var_dump($condition);
                $then = $matches[2][$i];
                $else = $matches[4][$i];
                if ($condBool)
                    $this->txt = str_replace($match, $then, $this->txt);
                else
                    $this->txt = str_replace($match, $else, $this->txt);
            }
        }
    }

    public function parseTxt($txt) {
        $this->txt = $txt;
        if (isSet($this->form))
            $this->parseForm();
        if ($this->userid > 0)
            $this->parseUser();
        else
            $this->parseAuthor();
        $this->sufParser();
        $this->txt = str_replace("[SESSIONID]",\cms\session::getObj()->getSessionId(),$this->txt);
        return $this->txt;
    }

    public function parseUser() {
        addWhere('id', '=', $this->userid);
        select('frontEndUser');
        $data = getRow();
        foreach ($data as $i => $d) {
            $this->txt = str_replace('[FS_' . $i . ']', $d, $this->txt);
        }
        $this->txt = str_replace('[FS_User]', $data['loginname'], $this->txt);
    }

    private function parseForm() {
        $elements = $this->form->getElements();
        foreach ($elements as $el) {
            if (is_array($el['value'])) {
                $el['value'] = implode(',', $el['value']);
            }
//            if(isAdmin()) echo $el['name'].':'.$el['value'].'<br>';
            $this->txt = str_replace('[FORM_' . $el['name'] . ']', $el['value'], $this->txt);
            $this->txt = str_replace('[FORM_' . menuepunkte::setTitelIntern($el['desc']) . ']', $el['value'], $this->txt);
        }
        return $this->txt;
    }

    private function parseAuthor() {
        if (!$this->useAdminAuthor && class_exists('frontendSession', false) && frontendSession::getObj()->isLoggedIn()) {
            $data = frontendSession::getObj()->getUserRow();
            foreach ($data as $i => $d) {
                $this->txt = str_replace('[AUTHOR_' . $i . ']', $d, $this->txt);
                $this->txt = str_replace('[FS_' . $i . ']', $d, $this->txt);
            }
            $this->txt = str_replace('[FS_User]', frontendSession::getObj()->getUserName(), $this->txt);
            $this->txt = str_replace('[FS_LoggedIn]', '1', $this->txt);
        }else{
            $this->txt = str_replace('[FS_LoggedIn]', '', $this->txt);
        }
        if (class_exists('\cms\session', false) && \cms\session::getObj()->isLoggedIn()) {
            $data = \cms\session::getObj()->getUserRow();
            foreach ($data as $i => $d) {
                $this->txt = str_replace('[AUTHOR_' . $i . ']', $d, $this->txt);
            }
        }
    }

}

?>
