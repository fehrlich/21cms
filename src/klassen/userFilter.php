<?php

/**
 * Description of userFilter
 *
 * @author XX
 */
function sortKrits($krit1, $krit2) {
	$m = $krit1['sortnr'];
	$n = $krit2['sortnr'];
	if ($m == $n)
		return 0;
	return ($m > $n) ? 1 : -1;
}

class userFilter {

	private $filterKriteria;
	private $filterForm;
	private $filterUserKriteria = array();
	private $database = "";
	private $dbType = 0;
	private $useSession = false;
	private $maindata = array();

	public function __construct($filterKrit = array(), $data = array()) {
		$this->filterKriteria = $filterKrit;
		$this->filterForm = new formular('', preg_replace(array('/-Seite\d+/i','/-P_(.*)?\./i','/-P_(.*)?\-/i'), array('','.',''), $_SERVER['REQUEST_URI']));
		$this->filterForm->setSucessMessage('');
		$this->filterForm->setDoBackup(false);
		if (!isset($_SESSION))
			session_start();
		$this->maindata = $data;
	}

	public function addFilterKrit($name, $type, $data = array(), $mysqlName = '', $mysqlTable = 't', $desc = '') {
		if ($desc == '')
			$desc = $name;
		$mysqlName = ($mysqlName == '') ? $name : $mysqlName;
		$this->filterKriteria[] = array(
			'name' => $name,
			'desc' => $desc,
			'type' => $type,
			'data' => $data,
			'mysqlName' => $mysqlName,
			'mysqlTable' => $mysqlTable,
		);
	}

	/**
	 *
	 * @param formular $form
	 */
	public function addToForm(&$form) {
		$form->addElement('UserFilter aktivieren', 'shoUserFilter', FormType::SIMPLECHECKBOS);
		$form->addElement('Werte merken', 'useSession', FormType::SIMPLECHECKBOS);
		$form->addElement('Button text', 'BtnUserFilterTxt', FormType::TEXTLINE, 'Filtern');
		$x = 0;
		foreach ($this->filterKriteria as $krit) {
			$form->addElement('Filter "' . $krit['name'] . '" anzeigen', 'userForm' . $krit['name'], FormType::SIMPLECHECKBOS);
			$form->addElement('Filter "' . $krit['name'] . '" umbenennen', 'userFormTxt' . $krit['name'], FormType::TEXTLINE);
			$form->addElement('Sortierung "' . $krit['name'] . '"', 'userFormSort' . $krit['name'], FormType::TEXTLINE, $x);
			$form->addElement('Logik "' . $krit['name'] . '"', 'userFormLogic' . $krit['name'], FormType::SELECT, 0, array('ODER', 'UND'));
			$x++;
		}
		$form->addElement('Eigener UserData Datei Filter', 'ownFilter', FormType::TEXTAREA);
	}

	public function getFilterHtml($data = array()) {
		/*
		 * FIX: Dirty Hack for activJob
		 */
		if (isSet($_POST['submitquick'])) {
			foreach ($_SESSION as $i => $s) {
				if (substr($i, 0, 10) == 'userFilter')
					unset($_SESSION[$i]);
			}
		}

		if (count($data) > 0) {
			if (!isSet($data['shoUserFilter']) || $data['shoUserFilter'] != '1')
				return '';
                        if(isSet($data['useSession']) && $data['useSession'] == '1')
                            $this->useSession = true;
			$showAllKrit = false;
			$this->maindata = $data;
		}else {
			$showAllKrit = true;
		}
		$html = '<div class="userFilter">';
		$filterKritSort = array();
		$x = 0;
		foreach ($this->filterKriteria as $i => $el) {
			if (isSet($data['userFormSort' . $el['name']]))
				$this->filterKriteria[$i]['sortnr'] = $data['userFormSort' . $el['name']];
			else
				$this->filterKriteria[$i]['sortnr'] = $x;
			$x++;
		}
		$filterName = '';
		usort($this->filterKriteria, "sortKrits");
		if (isSet($_GET['para']) && substr($_GET['para'], 0, 1) == 'f' && strpos($_GET['para'], '_')) {
			$spl = explode('_', $_GET['para'], 2);
			$filterName = substr($spl[0], 1);
			$filterValue = $spl[1];
		}
		foreach ($this->filterKriteria as $el) {
			if ($showAllKrit || (isSet($data['userForm' . $el['name']]) && $data['userForm' . $el['name']] == '1')) {
				if ((isSet($data['userFormTxt' . $el['name']]) && $data['userFormTxt' . $el['name']] != '')) {
					$desc = $data['userFormTxt' . $el['name']];
				} elseif (isSet($el['desc']))
					$desc = $el['desc'];
				else
					$desc = $el['name'];

				if ($this->isFilterPostet() && $this->useSession) {
					if (isSet($_POST['filter' . $el['name']]))
						$_SESSION['userFilter_' . $el['name']] = $_POST['filter' . $el['name']];
				}

				if ($filterName != '' && $filterName == $el['name']) {
					$value = $filterValue;
					if($this->useSession)
                                            $_SESSION['userFilter_' . $filterName] = $filterValue;
				} elseif ($this->useSession && isset($_SESSION['userFilter_' . $el['name']]))
					$value = $_SESSION['userFilter_' . $el['name']];
				else
					$value = '';
//				echo $el['type'].'!';
				if ($el['type'] == 'tags') {
//					$el['data']['tagBuild'] = true;
					$el['data']['checkBuild'] = true;
					$this->filterForm->addElement($desc, 'filter' . $el['name'], 'tags', $value, $el['data'],true,false,'',false,'','',$el['sortnr']);
				} elseif ($el['type'] == 'multiselect') {
					if (!is_array($el['data']))
						$el['data'] = explode(',', $el['data']);
                                        sort($el['data']);
					foreach ($el['data'] as $i => $name) {
						$this->filterForm->addElement($name, 'filter' . $el['name'] . $i, FormType::SIMPLECHECKBOS, $value,array(),true,false,'',false,'','',$el['sortnr']+$i);
					}
				} elseif ($el['type'] == 'select') {

					if (!is_array($el['data']))
						$el['data'] = explode(',', $el['data']);
					$this->filterForm->addElement($desc, 'filter' . $el['name'], FormType::SELECT, $value, array('' => 'Alle') + $el['data'],true,false,'',false,'','',$el['sortnr']);
				} elseif ($el['type'] == 'selectTxt') {
					if (!is_array($el['data']))
						$el['data'] = explode(',', $el['data']);
					if ($this->database != '') {
						$newdata = array();
						$mys = mys::getObj();
						$res = $mys->query('SELECT `' . $el['name'] . '` FROM `' . $this->getDatabaseName() . '` WHERE 1 GROUP BY `' . $el['name'] . '`');
						if ($res !== false) {
							while ($row = $res->fetch_assoc()) {
								if ($row[$el['name']] != '')
									$newdata[$row[$el['name']]] = $row[$el['name']];
							}
						}
						$el['data'] = array('Alle') + $newdata;
					}
					$this->filterForm->addElement($desc, 'filter' . $el['name'], FormType::SELECT, $value, array('Alle') + $el['data'],true,false,'',false,'','',$el['sortnr']);
				} else {
					$this->filterForm->addElement($desc, 'filter' . $el['name'], $el['type'], $value, $el['data'],true,false,'',false,'','',$el['sortnr']);
				}
			}
		}
		if (isSet($data['ownFilter'])) {

			if (preg_match_all('/\[DB_VALUES\((.*?)\)\]/', $data['ownFilter'], $matches)) {
				foreach ($matches[1] as $i => $fieldName) {
					$newdata = array();
					$mys = mys::getObj();
					$res = $mys->query('SELECT `' . $fieldName . '` FROM `' . $this->getDatabaseName() . '` WHERE 1 GROUP BY `' . $fieldName . '` ORDER BY `' . $fieldName . '`');
					$str = '';
					$dbFields = array();
					if ($res !== false) {
						while ($row = $res->fetch_assoc()) {
							if ($row[$fieldName] != '') {
								if (strpos($row[$fieldName], ',') !== false) {
									$expFields = explode(',', $row[$fieldName]);
									foreach ($expFields as $f) {
										$dbFields[] = $f;
									}
								}else
									$dbFields[] = $row[$fieldName];
							}
						}
					}
					sort($dbFields);
					$data['ownFilter'] = str_replace('[DB_VALUES(' . $fieldName . ')]', implode(',', $dbFields), $data['ownFilter']);
				}
			}
			$data['ownFilter'] = Vars::parseTagVars($data['ownFilter'], 'Alle,');
			$lineSpl = explode("|", $data['ownFilter']);
			if (count($lineSpl) == 0)
				$lineSpl = array($lineSpl);
			foreach ($lineSpl as $line) {
				if ($line != '') {
					$formEl = explode(':', $line);
					$name = trim($formEl[0]);
					$type = trim($formEl[1]);
					$eig = trim($formEl[2]);
					$desc = (isset($formEl[3])) ? trim($formEl[3]) : $name;
					$mysql = (isset($formEl[4])) ? trim($formEl[4]) : '';
					$sortNr = (isset($formEl[5])) ? trim($formEl[5]) : '';
					$userKrit = array(
						'name' => $name,
						'type' => $type,
						'data' => $eig,
						'desc' => $desc,
						'customMysql' => $mysql,
						'sortnr' => $sortNr
					);


					if ($this->isFilterPostet()) {
						if ($this->useSession && isSet($_POST['filterOwn' . $name]))
							$_SESSION['userFilterOwn_' . $name] = $_POST['filterOwn' . $name];
					}
					if ($filterName != '' && $filterName == $name) {
						$value = $filterValue;
						if($this->useSession)
                                                    $_SESSION['userFilterOwn_' . $name] = $value;
					} elseif ($this->useSession && isset($_SESSION['userFilterOwn_' . $name]) && $filterName == '')
						$value = $_SESSION['userFilterOwn_' . $name];
					else
						$value = '';
//					if(strpos($name,','))
//						$names = explode(',',$name);
//					else $names = array($names);
//					foreach($names as $name){
					if ($type == 'check' && strpos($eig, ',')) {
						$spl = explode(',', $eig);
						foreach ($spl as $i => $s) {
							$this->filterForm->addElement($s, 'filterOwn' . $name . $i, FormType::SIMPLECHECKBOS, $value,array(),true,false,'',false,'','',$sortNr+$i);
						}
						$userKrit['size'] = count($spl);
					} else{
                                            $trueType = strpos($type, '(');
                                            if($trueType > 0)
                                                $type = substr ($type ,$trueType+1,-1);
                                            $this->filterForm->addElement($desc, 'filterOwn' . $name, $type, $value, $eig,true,false,'',false,'','',$sortNr);
                                        }
//						}
					$this->filterUserKriteria[] = $userKrit;
				}
			}
		}
		$this->filterForm->setSaveButton(isSet($data['BtnUserFilterTxt']) ? $data['BtnUserFilterTxt'] : 'Filtern', 'Speichern');
		$html .= $this->filterForm->show(true);
		$html .= '</div>';
		return $html;
	}

	public function setupAddWhere() {
		if ($this->isFilterPostet() || (isSet($_GET['para']) && substr($_GET['para'], 0, 1) == 'f')) {
			$krits = $this->filterKriteria;
			foreach ($this->filterUserKriteria as $k)
				$krits[] = $k;

			$filterName = '';
			if (isSet($_GET['para']) && substr($_GET['para'], 0, 1) == 'f' && strpos($_GET['para'], '_')) {
				$spl = explode('_', $_GET['para'], 2);
				$filterName = substr($spl[0], 1);
				$filterValue = $spl[1];
			}
			foreach ($krits as $krit) {
				$defaultValue = '';
				if($filterName == $krit['name']){
					$defaultValue == $filterValue;
				}
				$kritLogic = (isset($this->maindata['userFormLogic' . $krit['name']]) && $this->maindata['userFormLogic' . $krit['name']] == '1') ? 'AND' : 'OR';
				if (isSet($krit['customMysql']) && $krit['customMysql'] != '') {
					$realfirst = true;
					$el = $this->filterForm->getElement('filterOwn' . $krit['name']);
					if($el['value'] == '') $el['value'] = $defaultValue;
					if ($el['value'] != '') {
						$orStatements = explode('OR', $krit['customMysql']);
						addWhereBracket('(');
//						echo '---------(-----------';
						foreach ($orStatements as $orStatement) {
							$orStatement = trim($orStatement);
							$orStatement = str_ireplace('[Search]', $el['value'], $orStatement);
							$andStatements = explode('AND', $orStatement);
							$first = true;
							addWhereBracket('(');
							foreach ($andStatements as $andStatment) {
								$wheres = explode(' ', trim($andStatment), 3);
								$value = str_replacE("'", '', $wheres[2]);
								$logic = ($realfirst || !$first) ? 'AND' : 'OR';
//								echo '<br/>addWhere('.$wheres[0].',', $wheres[1].','.$value.','.$logic.')<br/>';
								if ($wheres[1] == 'IN') {
									$value = str_replace(array('(', ')'), '', $value);
									$value = explode(',', $value);
								}
								addWhere('' . $wheres[0] . '', $wheres[1], $value, 's', $logic);
								$realfirst = false;
								$first = false;
							}
							addWhereBracket(')');
						}
						addWhereBracket(')');
//						echo '---------)-----------';
					}
				} elseif ($krit['type'] == 'multiselect') {
					$i = 0;
					if (!is_array($krit['data']))
						$krit['data'] = explode(',', $krit['data']);
                                            sort($krit['data']);
//					new dBug($krit);
//					new dBug($this->filterForm->getElements());
					foreach ($krit['data'] as $i => $name) {
						$el = $this->filterForm->getElement('filter' . $krit['name'] . $i);
						if($el['value'] == '') $el['value'] = $defaultValue;
//						new dBug($el);
						if ($el['value'] == '1') {
							if (strpos($name, ',')) {
								addWhereBracket('(');
								$splitValues = explode(',', $name);
								$first = true;
								foreach ($splitValues as $v) {
									$logic = ($first) ? 'AND' : 'OR';
									addWhere('`' . $krit['name'] . '`', 'LIKE', '%,' . $v . ',%', 's', $logic);
									$first = false;
								}
								addWhereBracket(')');
							}else
								addWhere('`' . $krit['name'] . '`', 'LIKE', '%,' . $name . ',%');
						}
						$i++;
					}
				}elseif ($krit['type'] == 'tags') {
					$el = $this->filterForm->getElement('filter' . $krit['name']);
					if($el['value'] == '') $el['value'] = $defaultValue;
					$values = array();
					if (!is_array($el['value']) && substr($el['value'], 0, 1) == substr($el['value'], -1, 1) && substr($el['value'], 0, 1) == ',')
						$values = explode(',', substr($el['value'], 1, -1));
//					new dBug($krit);
//					new dBug($this->filterForm->getElements());
					$first = true;
					if (count($values) > 0) {
						addWhereBracket('(');
						foreach ($values as $val) {
							$logic = ($first) ? 'AND' : $kritLogic;
							$first = false;
//							echo $krit['name'].' LIKE '.'%,' . $val . ',%<br>';
							addWhere('`' . $krit['name'] . '`', 'LIKE', '%,' . $val . ',%', 's', $logic);
						}
						addWhereBracket(')');
					}
				} else {
					$el = $this->filterForm->getElement('filter' . $krit['name']);
					if($el['value'] == '') $el['value'] = $defaultValue;
					if ($el['value'] != '') {
						if ($krit['type'] == 'editor')
							addWhere('`' . $krit['mysqlName'] . '`', 'MATCH', '' . $el['value'] . '', 's', 'AND', $krit['mysqlTable']);
						elseif ($krit['type'] == 'select')
							addWhere('`' . $krit['mysqlName'] . '`', '=', '' . $el['value'] . '', 's', 'AND', $krit['mysqlTable']);
						else
							addWhere('`' . $krit['mysqlName'] . '`', 'LIKE', '%' . $el['value'] . '%', 's', 'AND', $krit['mysqlTable']);
					}
				}
			}
		}
	}

	public function filterFileResult($fileResult) {
		if (!$this->filterForm->posted || count($this->filterUserKriteria) == 0)
			return $fileResult;

		$filtertResults = array();
		$activeFilter = array();
		foreach ($this->filterUserKriteria as $krit) {
			$el = $this->filterForm->getElement('filterOwn' . $krit['name']);
			if($el['value'] == 'Alle')
				$el['value'] = '';
			if (isSet($krit['size'])) {
				for ($i = 0; $i < $krit['size']; $i++) {
					$el = $this->filterForm->getElement('filterOwn' . $krit['name'] . $i);
					if ($el['value'] != '' && $el['value'] != '0') {
						$krit['value'] = $el['desc'];
						$activeFilter[] = $krit;
					}
				}
			} elseif ($el['value'] != '') {
				$krit['value'] = $el['value'];
				$activeFilter[] = $krit;
			}
		}
		if(count($activeFilter) == 0)
			return $fileResult;

		foreach ($fileResult as $file) {
			$check = false;
			foreach ($activeFilter as $krit) {
				if (isSet($file['data'][$krit['name']]) && strpos($file['data'][$krit['name']], $krit['value']) !== false) {
					$check = true;
				}
			}
			if ($check)
				$filtertResults[] = $file;
		}

		return $filtertResults;
	}

	public function setDatabase($database) {
		$this->database = $database;
                addWhere('name', '=', $database);
                select('databases', 'dbType');
                $row = getRow();
                $this->dbType = $row['dbType'];
	}
        public function getDatabaseName(){
            if($this->dbType == '1')
                return 'user';
            if($this->dbType == '2')
                return 'frontEndUser';
            else
                return 'data_'.$this->database;
        }

	public function isFilterPostet() {
		return ($this->filterForm->posted || ($this->useSession && isSet($_SESSION['userFilterPosted']) && $_SESSION['userFilterPosted']));
	}
    public function getFilterForm() {
        return $this->filterForm;
    }
}

?>
