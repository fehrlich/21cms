<?php
/**
 * Description of FrontEndUser
 *
 * @author Franz
 */
class FrontEndUser {
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;
	private $trigger = false;
	private $triggerEventData = false;
	private $triggerDataId = false;
	/**
	 *
	 * @var DatabaseConnector dbc
	 */
	private $dataBaseConnector;
	
	public function __construct(){
		$this->dbc = new DataEditor('frontEndUser');

		$this->dbc->form->addElement('EmailAdresse', 'email', 'email');
		$this->dbc->form->addElement('LoginName', 'loginname', 'text','',array(),true,false,'',true);
		$this->dbc->form->addElement('passwort', 'password', 'pwchange','',array('noConf' => true));
		$this->dbc->form->addElement('Beschreibung', 'desc', 'textarea');
		$this->dbc->form->addElement('Gruppe', 'group', 'dbCon','',array(
			'table' => 'frontEndGroups',
			'class' => 'FrontEndGroups',
			'what'  => array('id','name')
		));
		$this->dbc->setDataTitleId('loginname');
		$this->dbc->form->addElement('Gesperrt', 'public', FormType::SELECT,'1',array('1' => 'Nein', '0' => 'Ja'));
                $this->dbc->enableDot();

		$addDbc = new \DatabaseConnector();
		addWhere('dbType','=','2');
		$addForm = $addDbc->getForm();
		$this->dataBaseConnector = $addDbc;
		if($addForm){
			$addElements = $addForm->form->getElements();
			$dontAdd = array('id','public','erstellt','published','autor','title_intern');
			foreach($addElements as $el){
					if(!\in_array($el['name'],$dontAdd))
							$this->dbc->form->addElementArrays($el);
			}
			if($this->dbc->form->posted && $this->dbc->form->checkValidation($this->dbc->edit)){
				if($this->dbc->new) $name = Event::NEW_ENTRY;
				if($this->dbc->edit) $name = Event::EDIT;
				if($this->dbc->del) $name = Event::DEL;
				addWhere('dbType','=','2');
				setLimit(1);
//				echo 'Trigger:'.$name.'!'.$addDbc->getDatabaseId().'!';
				select('databases','eventActions');
				$row = getRow();
				$data = $row['eventActions'];
				$this->trigger = $name;
				$this->triggerEventData = $data;
			}
		}
		if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
			$forumGroups = \buildSelectArray('phpbb_groups', array('group_id','group_name'));

			$forumGroupId = '';
			if(isSet($_GET['edit']) && $_GET['edit'] > 0){
				\addWhere('id', '=', $_GET['edit']);
				\select('frontEndUser', 'loginname');
				$row = \getRow();
				$oldUserName = $row['loginname'];
				\addWhere('username', '=', $oldUserName);
				\select('phpbb_users', 'group_id');
				$row = \getRow();
				$forumGroupId = $row['group_id'];
			}
			$this->dbc->form->addElement('Forum', 'forumint','select',$forumGroupId,$forumGroups,false);
//			new dBug($this->dbc->form->isEdit());
			if($this->dbc->form->posted && $this->dbc->form->checkValidation($this->dbc->edit)  && isSet($_POST['forumint'])){
				$api = new \phpbb($_SERVER['DOCUMENT_ROOT'].$GLOBALS['forum_integration'], 'php');
				if(!$api->user_loggedin())
						$api->user_login(array(
							'username' => \cms\session::getObj()->getUserName(),
							'password' => 'arnarn',
							'admin' => '1',
							'autologin' => true
						));
				$userData = array(
					'username' => $_POST['loginname'],
					'user_email' => $_POST['email'],
					'group_id' => $_POST['forumint']
				);
				if(isSet($_GET['edit']) && $_GET['edit'] > 0 && $forumGroupId > 0){
					if($_POST['loginname'] != $oldUserName){
						if($api->user_rename($oldUserName, $_POST['loginname']) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht geupdatet werden';
					}
					if($api->user_update($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht geupdatet werden';
					if(isSet($_POST['password']) && $_POST['password'] != '' && $_POST['password'][0] == $_POST['password'][1]){
						$userData['password'] = $_POST['password'][0];
						if($api->user_change_password ($userData) != 'SUCCESS') echo 'Error: Das Passwort für den Forum User konnte nicht geändert werden';
					}
				}else{
					$userData['user_password'] = $_POST['password'][0];
					if($api->user_add($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht angelegt werden';

				}
			}
			if($this->dbc->del){
				if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
					$api = new \phpbb($_SERVER['DOCUMENT_ROOT'].$GLOBALS['forum_integration'], 'php');
					if(!$api->user_loggedin())
						$api->user_login(array(
							'username' => \cms\session::getObj()->getUserName(),
							'password' => 'arnarn',
							'admin' => '1',
							'autologin' => true
						));
					\addWhere('id', '=', $_GET['del']);
					\select('frontEndUser', 'loginname');
					$row = \getRow();
					$userData = array(
						'username' => $row['loginname']
					);
					if($api->user_delete($userData) != 'SUCCESS') echo 'Error: Der Forum User konnte nicht gelöscht werden';
				}
			}
		}
	}
	
	public function __toString(){
		$ret = $this->dbc->__toString();
		$this->dataBaseConnector->triggerEvent($this->trigger, $this->dbc->form->getDataid(), $this->triggerEventData);
		return $ret;
	}
        public function getDbc() {
            return $this->dbc;
        }
}
?>
