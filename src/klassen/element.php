<?php
/**
 *
 * @author F.Ehrlich
 *
 */

abstract class element{
    protected $src = "";
    protected $id = "";
    protected $mainid = "";
	protected $deleteable = true;
	protected $editable = true;
	protected $mysqltab = '';

    protected $elart = 'normal';
    protected $data = array();
    protected $rowData = array();
    protected $hassub = false;
    protected $subelname = 'inhalt';
    protected $extraFormElements = array();
    protected $allowEdit = null;

    protected $listStyle = false;
	protected $addedClasses = '';
	/**
	 * form for adding and editing
	 * @var formular
	 */
    protected $form;
	public $container;

	public function getEvents() {
		return array();
	}

	public function overWriteEdit($editable){
		$this->allowEdit = $editable;
	}

	public function setRow($row){
		$this->rowData = $row;
	}
	public function setListStyle($listStyle){
		$this->listStyle = $listStyle;
	}

	public function getInlineList(){
		return get_class($this);
	}

	/**
	 * Creates new Element
	 * @param int $id Id of the Element
	 * @param array $data Data Array from row eldata
	 * @param string $container Containername
	 * @param int $mainid Id of the Parentelement
	 */
	public function __construct($id = 0,$data = array(),$container = '', $mainid = 0,$deletable = true,$buildform = true){
		$this->mysqltab = $this->getThisTabName();
		//$this->mysqltab = 'elements';
		$this->id = $id;
		$this->data = $data;
		$this->deleteable = $deletable;
		if($this->container == '' || $this->container == '0') $this->container = $container;
		if(isAdmin()){
			$this->admineig();

			$this->form = new formular($this->mysqltab);
			$this->form->setShowHelp(true);
			$this->form->usetab(get_class($this));
			//$this->action = $_SERVER['PHP_SELF'].'?el='.get_class($this).buildget('act,id');//.buildget('el,name,');
			if($GLOBALS['interface'] == 'ajax' && isset($_GET['el']) && $buildform){ // && $_GET['el'] == get_class($this)
				if(isset($_GET['c']) && $_GET['c'] != '0') $cont = $_GET['c'];
				else $cont = $container;
				$this->form->addelement('Sichbarkeit', 'visible','select',1, array('unsichtbar', 'normal', 'immer', 'untergeordnet'));

				addWhere('visible', '!=', 'a');
				$data = buildSelectArray('menuepunkte', array('id','title'), true);
				$this->form->addelement('Unterordnen', 'visible_main','select',$GLOBALS['seitenid'],$data);
				$this->form->addelement('Außer auf', 'dontShowOn','multiselect',$GLOBALS['seitenid'],$data);
				$this->form->addOnlyShowOn('visible_main', 'visible', '3');
				$this->form->addOnlyShowOn('dontShowOn', 'visible', '2');

                $contentClasses = UserConfig::getObj()->getContentClasses();
                if ( !empty($contentClasses) ) {
                    $this->form->addelement('User Css Klasse', 'css_class2','select','',$contentClasses);
                }

				if (rights::getObj()->isAdmin()) {
					$this->form->usetab('Admin');
					//$this->form->addelement('Id', 'id','text',$this->id);
					$this->form->addelement('Seitenid', 'seitenid','text',$GLOBALS['seitenid']);
					$this->form->addelement('Container', 'container','text', $cont);
					$this->form->addelement('Erstellungsdatum', 'erstellt','text');
					$this->form->addelement('Suchtext', 'searchtxt','textarea');
                    if ( $this->mysqltab == 'elements' ) {
                        $this->form->addelement('Css Klasse', 'css_class','text');
                    }

				} else {
					$this->form->addelement('', 'seitenid','hidden',$GLOBALS['seitenid']);
					$this->form->addelement('', 'container','hidden', $cont);
				}
				if($mainid > 0 && (!isSet($_GET['el']) || $_GET['el'] != 'container'))
					$this->form->addelement('MainId', 'mainid','text',$mainid);
				$this->form->addmysqldata('klasse',get_class($this));
				$this->form->usetab('Events_Aktionen');

				$this->form->addElement('EventAktionen', 'eventActions', FormType::FORMULA,'', Action::getEventActionForm($this->getEvents()));

				$this->form->usetab(get_class($this));
				$this->form->startserialize('eldata');
				$this->formbuild();
			}
		}
	}

	/**
	 * Generates Buttons for the Admininterface (New, Edit, Delete)
	 */
	public function adminEig(){
		$this->src = '';
		if(isSet($this->allowEdit)) $check = $this->allowEdit;
		else $check = PageRights::getObj()->isAllowed();
		if($check && !$this->listStyle){
			$el_indistr = get_class($this).'-'.$this->container.'-'.$this->id;
			$this->src = '<div class="admineig" style="display:none">'; // title=""
			if($this->hassub && rights::getObj()->getContentRights("create")) $this->src .=	'<div class="button sub" id="'.$el_indistr.'-sub" title="Neues Element"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" /></div>';
			if($this->editable && rights::getObj()->getContentRights("edit")) $this->src .=	'<div class="button edit" id="'.$el_indistr.'-edit" title="'.get_class($this).' &auml;ndern"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appedit.png" alt="[e]" /></div>';
			if($this->deleteable && rights::getObj()->getContentRights("delete")) $this->src .=	'<div class="button del" id="'.$el_indistr.'-del" title="'.get_class($this).' l&ouml;schen"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appdel.png" alt="[X]" /></div>';
			$this->src .= '</div>'; //get_class($this).
		}
	}

	/**
	 * is called while editing the Element
	 */
	public function formEdit(){
		if($this->id > 0){
			$this->form->connectData($this->id,false); //,$this->data
		}
		echo $this->form->__toString();
	}

	/**
	 * is called while creating a new element
	 */

	public function formNew(){
		echo $this->form;
	}

	/**
	 * returns content as html
	 * @param array $data
	 * @return string Html Source
	 */
	public function getContent($data = array()){
		$this->getData($data);
		$src = '';
		if(isAdmin()) $this->admineig();

        $addclass = "";

        if ( isSet($this->rowData['css_class']) && $this->rowData['css_class'] != '') {
            $addclass .= ' ' . $this->rowData['css_class'];
        }

        if ( isSet($this->rowData['css_class2']) && $this->rowData['css_class2'] != '' && $this->rowData['css_class2'] != 'default') {
            $addclass .= ' ' . $this->rowData['css_class2'];
        }

        if((!isset($_GET['inter']) || $_GET['inter'] != 'dia') && !$this->listStyle) $src = '<div class="element '.((method_exists($this, 'getSub'))?'mainelement ':'').get_class($this).$addclass.$this->addedClasses.'">';
		$src .= $this->src;
		$src .= $this->getInline($this->data);
		if((!isset($_GET['inter']) || $_GET['inter'] != 'dia') && !$this->listStyle)  $src .= '</div>';
		return $src;
	}

	/**
	 *
	 * @return String html source
	 */
	public function  __toString() {
		return $this->getContent();
	}

	/**
	 * gets intern content without cms formatting (wrapping)
	 * @param <type> $data html source
	 */

	public abstract function getInline();

	/**
	 * user to set up formular data
	 */
	public abstract function formBuild();

	/**
	 * is called when form is submitted
	 */
	public function formPost(){
		if($this->id > 0 && $_GET['art'] != 'New') $this->form->connectData($this->id,true); //,$this->data
//		foreach($extraFormElements as $name){
//			if(in_array($name, $this->form->mysqlrow))
//					$this->form->addElement($name, $name)
//		}
		if(isset($_POST['erstellt']) && $_POST['erstellt'] == '') $_POST['erstellt'] = time();
		$src = $this->form->__toString();
		$this->data = array();
		if($GLOBALS['interface'] == 'ajax'){
			if($src != 'Ok')
				return 'FORMERROR:'.$src;
			if(method_exists($this, 'getMainName')){
				$elname = $this->getMainName();
				$mid = ($_GET['nextMainId'] > 0)? $_GET['nextMainId']:$_GET['id'];
				$container = new $elname($mid);
			}else{
				if($_POST['container'] == "" || substr($_POST['container'], 0, 2) == 'el'){
//					print_r($this);
//					addWhere('id', '=', $this->id);
//					select('elements','id,eldata,container');
					$this->getData();
					if($this->id == 0)
						$this->id = $this->form->getDataid();
					$src .= ':EL'.$this->getContent();
					return $src;
				}
				$cont = $_POST['container'];
				addWhere('klasse', '=', 'container');
				addWhereBracket('(');
				addWhere('seitenid', '=', $GLOBALS['seitenid'],'i');
				addWhere('seitenid', '=', '','s','OR');
				addWhereBracket(')');
				select('elements','id,eldata,container');
				$cont_data = getRow();
				include_once('../klassen/container.php');
//				var_dump($cont_data);
				$container = new container($cont,$cont_data);
			}
			$src .= ':CONT'.$container->getContent();
		}
		if($this->id > 0 && $_GET['art'] != 'New') $this->triggerEvent('Edit');
		else $this->triggerEvent('New');
//		>$this->afterPost();
//		echo $src.'!';
		return $src;
	}

	public function getData($data = array()){
		$checklang = true;
		if(count($data) == 0 && count($this->data) == 0 && $this->id > 0){

			addWhere('id', '=', $this->id);
			if($this->mysqltab == 'elements') $what = 'eldata';
			else $what = '*';
			select($this->mysqltab,$what);
			$row = getrow();
			if($what == 'eldata')
				$data = unserialize($row['eldata']);
			else
				$data = $row;
			$this->data = $data;
			$checklang = true;
		}elseif(count($data) > 0){
			$this->data = $data;
			$checklang = true;
		}
		if($checklang){
			$lang = Languages::getLang(false);

            if (isAdmin() && empty($lang)) {
                $lang = \cms\session::getObj()->get("currentLanguage");
            }

			$langSize = strlen($lang)+1;
			if($lang != '' && $lang != 'invalid'){
				if($this->data){
					foreach($this->data as $id => $d){
						if(substr($id,strlen($id)-$langSize) == '_'.$lang){
							$main_key = substr($id,0,strlen($id)-$langSize);
							if(isSet($this->data[$main_key])){
								if(strlen($d) > 0) $this->data[$main_key] = $d;
//								else $this->data[$main_key] = 'Sry no "'.$lang.'"'.$this->data[$main_key];
							}
						}
					}
				}
			}
		}
		return $this->data;
	}

	/**
	 * is called when element is supposed to be deleted
	 */
	public function delete() {
		addWhere('id', '=', $this->id);
		delete($this->mysqltab);
		$this->triggerEvent('Delete');
		echo 'Ok';
	}
	/**
	 * return TableName
	 */
	public function getThisTabName(){
		return 'elements';
	}
	public static function getTabName(){
		return 'elements';
	}
	public static function createPseudoElement($elHtml,$className,$id,$isMain = false){
		$html = '';
		if(!isset($_GET['inter']) || $_GET['inter'] != 'dia') $html = '<div class="element '.(($isMain)?'mainelement ':'').$className.'">';


		$check = PageRights::getObj()->isAllowed();
		if($check && isAdmin()){
			$el_indistr = $className.'-0-'.$id;
			$html .= '<div class="admineig" style="display:none">'; // title=""
			if($isMain) $isMain .=	'<div class="button sub" id="'.$el_indistr.'-sub" title="Neues Element"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" /></div>';
			if(rights::getObj()->getContentRights("edit")) $html .=	'<div class="button edit" id="'.$el_indistr.'-edit" title="'.$className.' &auml;ndern"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appedit.png" alt="[e]" /></div>';
			if(rights::getObj()->getContentRights("delete")) $html .=	'<div class="button del" id="'.$el_indistr.'-del" title="'.$className.' l&ouml;schen"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appdel.png" alt="[X]" /></div>';
			$html .= '</div>'; //get_class($this).
		}
		$html .= $elHtml;
		$html .= '</div>';
		return $html;
	}
	public function addClass($name){
		$this->addedClasses .= ' '.$name;
	}
	public function triggerEvent($name){
		addWhere('id', '=', $this->id);
		select($this->mysqltab,'eventActions');
		$row = getRow();
		$data = $row['eventActions'];
		Action::triggerEvent($name, $data,$this);
	}
        public function getTemplate(){
            return '';
        }
        static function load($id){
            addWhere('id', '=', $id);
            select('elements');
            $row = getRow();
            //($id = 0,$data = array(),$container = '', $mainid = 0,$deletable = true,$buildform = true){
//            echo $row['klasse'];
            $klasse = new $row['klasse']($row['id'],  unserialize($row['eldata']),$row['container']);
            return $klasse;
        }
}
?>
