<?php
class product{
    public $price = 0;
    public $title = "";
    public $text = "";
    public $amount = 1;
    
    public $dbName = "";
    public $dataTitle = 0;
    public $options = array();
    public $weight = 0;
    
    function __construct($dbName, $dataTitle, $options,$price, $weight, $title = "",$text = "") {
        $this->dbName = $dbName;
        $this->dataTitle = $dataTitle;
        $this->options = $options;
        
        $this->price = $price;
        $this->weight = $weight;
        $this->title = $title;
        $this->text = $text;
    }
    public function getPrice() {
        if(shop::getObj()->overwritePriceFunction != ''){
            $func = shop::getObj()->overwritePriceFunction;
            return $func($this->dbName, $this->dataTitle, $this->options);
        }
        return $this->price;
    }

    public function getTitle() {
        if(shop::getObj()->nameFunction != ''){
            $func = shop::getObj()->nameFunction;
            return $func($this);
        }
        return $this->title;
    }
    
    public function getText() {
        return $this->text;
    }
    public function getWeight() {
        return $this->weight;
    }
    
    public function toObject(){
        return array(
            'name' => $this->getTitle(),
            'weight' => $this->getWeight(),
            'options' => $this->options,
            'price' => $this->getPrice(),
        );
    }
}
?>
