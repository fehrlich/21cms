<?php
class shop{
    static private $handle = null;
    
    public $overwritePriceFunction = "";
    public $nameFunction = "";
    public $overwriteTextFunction = "";
    public $overwriteMailFunction = "";
    public $overwriteProductWeight = "";
    public $cartProductTplFunction = "";
    private $shipment = "";
    private $payment = "";

    public function __construct() {
        shop::$handle = $this;
    }
    
    /**
     *
     * @return shop Shop Klasse
     */
    static public function getObj() {

        if(shop::$handle === null) {
            new shop();
        }

        return shop::$handle;
    }
    
    function overwritePriceFunction($functionName){
        $this->overwritePriceFunction = $functionName;
    }
    function overwriteNameFunction($functionName){
        $this->nameFunction = $functionName;
    }
    function overwriteTextFunction($functionName){
        $this->overwriteTextFunction = $functionName;
    }
    function overwriteMailFunction($functionName){
        $this->overwriteMailFunction = $functionName;
    }

    public function overwriteProductWeight($functionName) {
        $this->overwriteProductWeight = $functionName;
    }
    public function overwriteCartProductTplFunction($functionName) {
        $this->cartProductTplFunction = $functionName;
    }
    
    function getProductPrice($db,$article,$options = array()){
        $function = $this->overwritePriceFunction;
        if($function != "" && function_exists($function)){
            return $function($db,$article,$options);
        }  else {
            return 0;
        }
    }
    
    
    public function getProductWeight($db, $article) {
        $function = $this->overwriteProductWeight;
        if($function != "" && function_exists($function)){
            return $function($db,$article);
        }  else {
            return 0;
        }        
    }
    
    function getProductName($db,$internerTitle){
        $function = $this->nameFunction;
        if($function != "" && function_exists($function)){
            return $function($db,$internerTitle);            
        }else{
            $dbc = new DatabaseConnector();
            addWhere('name', '=', $db);
            $dbc->getDatabases();
            addWhere('title_intern', '=', $internerTitle);
            $data = $dbc->getData($db);
            return $dbc->getFieldId($data[0]);            
        }
    }
    
    function getProductText($db,$internerTitle){
        $function = $this->overwriteTextFunction;
        if($function != "" && function_exists($function)){
            return $function($db,$internerTitle);            
        }else{
            $dbc = new DatabaseConnector();
            addWhere('name', '=', $db);
            $dbc->getDatabases();
            addWhere('title_intern', '=', $internerTitle);
            $data = $dbc->getData($db);
            return "";            
//            return $dbc->getFieldId($data[0]);            
        }
    }
    
    public function addProductToCart($db,$title,$options) {
        $product = new product($db, $title, $options, $this->getProductPrice($db, $title),$this->getProductWeight($db, $title), $this->getProductName($db, $title), $this->getProductText($db, $title));
        cart::getObj()->addProduct($product);
//        cart::getObj()->addProduct($db,$title,$options,$this->getProductPrice($db, $title));
    }

    public function addOrChangeArticle($db, $id,$title,$options) {
        $product = new product($db, $title, $options, $this->getProductPrice($db, $title),$this->getProductWeight($db, $title), $this->getProductName($db, $title), $this->getProductText($db, $title));
        cart::getObj()->addOrChangeProduct($product, $id);        
    }
    
    public function setShipment($shipment) {
        $this->shipment = $shipment;
        frontendSession::getObj()->setData('cmsshop_shipment', $shipment);
        return json_encode(array(
            'shipment' => $shipment,
            'price' => cart::getObj()->getShippingPrice()
        ));
    }

    public function setPayment($payment) {
        $this->payment = $payment;
        frontendSession::getObj()->setData('cmsshop_payment', $payment);
    }

    
    public function getShipment() {
        $shipment = frontendSession::getObj()->getDataVal('cmsshop_shipment');
        if($shipment != '') return $shipment;
        
        return $this->shipment;
    }

    public function getPayment() {
        $payment = frontendSession::getObj()->getDataVal('cmsshop_payment');
        if($payment != '') return $payment;
        return $this->payment;
    }
    public function appendProductOptions($id, $options) {
//        cart::getObj()->get
        cart::getObj()->appendProductOptions($id, $options); 
    }
    public function deleteProductOptions($id, $options) {
//        cart::getObj()->get
        cart::getObj()->deleteProductOptions($id, $options); 
    }
    
    public function sendMailToAdmin(){
         $function = $this->overwriteMailFunction;
        if($function != "" && function_exists($function)){
            return $function();
        }  else {
            $adress = UserConfig::getObj()->getContactMail();
            $mail = new mimeMail();
            die('not yet implemented');
            //TODO: default SendMail
        }
    }
}
?>
