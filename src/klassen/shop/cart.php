<?php
class cart{
    static private $handle = null;
    
    /**
     *
     * @var product[]
     */
    private $products = array();
    private $shippingPrice = 0;

    public function getHtml($emptyMsg) {
        $products = frontendSession::getObj()->getDataVal('cmsshop_products');
        
        if(!is_array($products) || count($products) == 0) return $emptyMsg;
        $html = '';
        foreach($products as $i => $product){
            $html .= '<div class="cartProduct">';
            if(shop::getObj()->cartProductTplFunction != ''){// && false
                $func = shop::getObj()->cartProductTplFunction;
                $html .= $func($product);
            }else{
                $html .= '<b>'.$product->getTitle().' - '.$product->getPrice().'€</b> <a class="deleteFromCart" id="cart'.$i.'" href="#[LINK]">[X]</a>';
                $html .= '<p>'.$product->getText().'</p>';
            }
            $html .= '</div>';
        }                
//        $html .= '<br /><br /><a href="/Kasse.html">Zur Kasse</a>';
        return $html;
    }
    
    public function deleteItem($id){
        unset ($this->products[$id]);
        frontendSession::getObj()->setData('cmsshop_products',$this->products);
        
    }
    
    public function __construct() {
        $products = frontendSession::getObj()->getDataVal('cmsshop_products');
        if(is_array($products))
            $this->products = $products;
        cart::$handle = $this;
    }
    
    /**
     *
     * @return cart Shop Klasse
     */
    static public function getObj() {

        if(cart::$handle === null) {
            new cart();
        }

        return cart::$handle;
    }
    
    
    static function getCartLink(){
        return "";
    }
    
    function addProduct2($db,$title,$options,$price,$productTitle = "",$productText = ""){
        if($productTitle == ""){
            $dbc = new DatabaseConnector();
            addWhere('name', '=', $db);
            $dbc->getDatabases();
            addWhere('title_intern', '=', $title);
            $data = $dbc->getData($db);
            $productTitle = $dbc->getFieldId($data[0]);            
        }
        $this->products[] = new product($db, $title, $options,$price,$productTitle,$productText);
        frontendSession::getObj()->setData('cmsshop_products',$this->products);
    }
    function addProduct($product){
        $this->products[] = $product;
        frontendSession::getObj()->setData('cmsshop_products',$this->products);
    }

    public function addOrChangeProduct($product,$id) {
        //get Old Options
        foreach($this->products[$id]->options as $optId => $options){
            if(!isSet($product->options[$optId])) $product->options[$optId] = $options;
        }
        $this->products[$id] = $product;
        frontendSession::getObj()->setData('cmsshop_products',$this->products);
    }

    public function appendProductOptions($id, $options) {
        if(!isSet($this->products[$id])) die('Artikel nicht vorhanden');
//        print_r($options);
        foreach($options as $key => $value){
            if(!isSet($this->products[$id]->options[$key])) $this->products[$id]->options[$key] = array();// die('Diese Option ist nicht vorhanden');
            $this->products[$id]->options[$key][] = $value;
        }
        print_r($this->products);
        frontendSession::getObj()->setData('cmsshop_products',$this->products);
    }
    
    public function deleteProductOptions($id, $options) {
        $searchIn = ($options['delete'])?$options['delete']:false;
        $array = ($searchIn)?$this->products[$id]->options[$searchIn]:$this->products[$id]->options;
        if(!isSet($this->products[$id])) die('Artikel nicht vorhanden');
//        print_r($options);
        $searchFor = (isSet($options['key']))?$options['key']:$options['value'];
        $searchForKey = (isSet($options['key']))?'key':'value';
        foreach($array as $key => $value){
            echo $$searchForKey.'=='.$searchFor."\n";
            if($$searchForKey == $searchFor){
                if($searchIn) unset($this->products[$id]->options[$searchIn][$key]);
                else unset($this->products[$id]->options[$key]);
                echo 'DELETED: '.$key.' IN '.$searchIn;
                print_r($this->products[$id]->options['files']);
                frontendSession::getObj()->setData('cmsshop_products',$this->products);
                return "ok";
            }
        }
    }
    
    public function getProducts() {
        return $this->products;
    }
    /**
     * 
     * @param type $id
     * @return product
     */
    public function getProduct($id) {
        if(!isSet($this->products[$id])) return false;
        return $this->products[$id];        
    }

    public function getWeight() {
        $weight = 0;
        foreach($this->products as $product){
            $weight += $product->getWeight();
        }
        return $weight;
    }
    public function getShippingPrice() {
        if(shop::getObj()->getShipment() == 'abhohlung'){
            return 0;
        }
        
        $p = frontendSession::getObj()->getDataVal('cmsshop_shippingPrice');
        if($p != '') return $p;
        return $this->shippingPrice;
    }

    public function setShippingPrice($shippingPrice) {
        frontendSession::getObj()->setData('cmsshop_shippingPrice', $shippingPrice);
        $this->shippingPrice = $shippingPrice;
    }
}
?>
