<?php
/**
 *
 * @author F.Ehrlich
 *
 */
class container extends mainelement { //mainelement
	protected $hassub = true;
	private $name;
	private $anordnung = 0;
	private $overlimit = 0;
	protected $deleteable = false;

	public $sektor = '';


	public function getInline($data = array()){
            $html = '';
            if (isAdmin()) {
                addWhereprepare('klasse != \'container\' AND mainid = 0 AND container = ? AND ((visible = 2 AND dontShowOn NOT LIKE ?) OR seitenid = ?  OR (visible = 3 AND visible_main IN ('.implode(',', $GLOBALS['aktmenu']).')))', array($this->name,'%,'.$GLOBALS['seitenid'].',%',$GLOBALS['seitenid']), 'ssi');
            } else {
                addWhereprepare('klasse != \'container\' AND mainid = 0 AND container = ? AND ((visible > 0 AND seitenid = ?) OR (visible = 2 AND dontShowOn NOT LIKE ?) OR (visible = 3 AND visible_main IN ('.implode(',', $GLOBALS['aktmenu']).')))', array($this->name,$GLOBALS['seitenid'],'%,'.$GLOBALS['seitenid'].',%'), 'sis');
            }
            $order = 'erstellt ASC';
            if (isset($this->data["orderby"])){
				if($this->data['orderby'] == 1)
					$order = 'erstellt DESC';
				elseif($this->data['orderby'] == 2)
					$order = 'ordernr';
            }

            select('elements', 'id,klasse,eldata,css_class,css_class2',$order);

            $rows = getRows();
            foreach($rows as $row){
                if($row['eldata'] != '') {
                    $unser = unserialize($row['eldata']);
                    if($unser === false) {
                        echo 'Element "'.$row['id']."' konnte nicht serialisiert werden!<br />";
                    }
                } else $unser = array();

                $el = new $row['klasse']($row['id'], $unser,$this->name);
                $el->setRow($row);
                $html .= $el->getContent($unser);
            }

            return $html;
	}

	public function getTree($what = '*',$activeid = -1,$listView = false,$forceNotEmpty = false) {
		$GLOBALS['seitenid'] = $_GET['m'];
		addWhere('id', '=', $_GET['m'], 'i');
		$add_txt = '';
		select('menuepunkte');
		$row = getRow();
		$GLOBALS['akt_menuepunkt'] = $row;
		preparation();

		$subelement = $this->getSubName();
		$data = new Tree('',$this->id,'menu');
		$data->setactiveId($activeid);
		if($subelement == '') $tab = 'elements';
		else $tab = call_user_func_array(array($subelement, 'getTabName'),array());
		if(is_array($what)){
			$tmps = $what;
		}else{
//			addWhere('container', '=', $_GET['c']);
//			$tab = call_user_func_array(array($subelement, 'getTabName'),array());
//			if(!isAdmin()) addWhere('visible', '=', '1');
//			elseif($tab != 'tags') addWhere('visible', '!=', 'a');
//			$GLOBALS['mysql_debug'] = true;
			if($this->usemainid)
				addWhereprepare('klasse != \'container\' AND mainid = 0 AND container = ? AND ((visible = 2 AND dontShowOn NOT LIKE ?) OR seitenid = ?  OR (visible = 3 AND visible_main IN ('.implode(',', $GLOBALS['aktmenu']).')))', array($_GET['c'],'%,'.$GLOBALS['akt_menuepunkt']['id'].',%',$GLOBALS['akt_menuepunkt']['id']), 'ssi');
			else
				addWhereprepare('klasse != \'container\' AND mainid = 0 AND container = ?', array($_GET['c']), 's');

			select($tab,$what,'ordernr');
			$tmps = array();
			while($row = getrow()){
				$tmps[$row['id']] = $row;
			}
		}
		$resetSubElement = false;
		foreach($tmps as $row){
			if($tab == 'elements') $subdata = unserialize($row['eldata']);
			else $subdata = $row;
			if($subelement == '' && isSet($row['klasse'])){
				$resetSubElement = true;
				$subelement = $row['klasse'];
			}
			if(isSet($row['verlinkung']) && $row['verlinkung'] != '-1'){
				if(isset($tmps[$row['verlinkung']])){
					$row['title_intern'] = $tmps[$row['verlinkung']]['title_intern'];
				}elseif($row['verlinkung'] == '0'){
					$row['title_intern'] = 'index';
				}
				else{
					addWhere('id', '=', $row['verlinkung'],'i');
					setLimit(1);
					select('menuepunkte','title_intern');
					$row2 = getRow();
					$element = new $subelement($row['id'],$row2,0,true);
					unset($row2);
				}
				$element = new $subelement($row['id'],$subdata,0,true,false,false);
			}else{
				$element = new $subelement($row['id'],$subdata,0,true,false,false);
			}
			if($listView) $element->setListStyle(true);
//			$txt = '<a href="#" class="tooltip" onclick="$.tooltip({delay: 0,showURL: false,track: true,positionRight: true,bodyHandler: function() {return \'asdasddsa\';}">'.$element->getInlineList().'</a>';
			$txt = '<a href="#" class="tooltip" id="tool'.$row['id'].'">'.$element->getInlineList().'</a>';
			$add_txt .= '</div><div id="toolcontenttool'.$row['id'].'" class="tooldiv" style="background-color: #fff;position:absolute;max-height:250px;border:1px solid red;overflow:hidden;display:none;">'.$element->getContent().'';

			$data->addNode($row['id'], $txt, $row['parent']);
			if($resetSubElement) $subelement = '';
		}
//		$add_txt ='asd';
		return $data.$add_txt;
	}

	/**
	 *
	 * @param String $name Containername
	 * @param Boolean $data
	 * @param Int $limit Inhalte Limit
	 * @param Int $anordnung 0 - Letzte zuletzt <br>1 - Letzte Zuerst
	 * @param Int $overlimit
	 * @param $name Containername
	 */
	function __construct($name, $data = array(), $realid = 0){//$limit = 0, $anordnung = 0, $overlimit = 0
		if($GLOBALS['interface'] == 'ajax'){
			$name = $_GET['c'];
			$realid = $_GET['id'];
		}
		$this->container = $name;
		if($GLOBALS['interface']){
			if(!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'container') $this->subelname = 'inhalt';
		}
		if(isset($data['eldata'])) $data = unserialize($data['eldata']);
		parent::__construct($realid,$data);
		$this->name = $name;
//		$this->data = $data;
	}

	public static function getSub($id){

		if(!isset($_GET['el']) || $_GET['el'] == '' || $_GET['el'] == 'container') $sub = 'inhalt';
		else $sub = $_GET['el'];
		return new $sub(0,true,'',$id);
	}

	public function formSub(){
		echo $this->subel->formEdit();
	}
	public function  __toString() {
		//echo $this->src;
	}
	public function formBuild() {
		$data = $this->getData();
		$val = (isSet($data['seitenid']) && $data['seitenid'] == '')?'1':'0';
		$this->form->addElement('Globaler Container', 'useGlobal', 'select', $val, array('Nein','Ja'),false);
		$this->form->addOnChangeReload('useGlobal');
		if((isSet($_GET['changeuseGlobal']) && $_GET['changeuseGlobal'] == '1') || (isset($_POST['useGlobal']) && $_POST['useGlobal'] == 1)){
			addWhere('seitenid', '=', '');
			$updateId = true;
			$this->form->setElement('seitenid', 'value', '0');
			$this->usemainid = false;
		}elseif(isSet($_GET['changeuseGlobal']) || isset($_POST['useGlobal'])){
			addWhere('seitenid', '=', $_GET['m'],'i');
			$updateId = true;
			$this->form->setElement('seitenid', 'value', $_GET['m']);
			$this->usemainid = true;
		}
		if(isset($updateId) && $updateId){
			addWhere('klasse', '=', 'container');
			select('elements');
			$row = getRow();
			if($row) $this->id = $row['id'];
			else{
				$this->id = 0;
			}
		}

		$this->form->addElement('Anordnung', 'orderby', 'select', '0', array('Neuste Unten','Neuste Oben','Eigene'));
		$this->form->removeElement('visible');
		$this->form->removeElement('dontShowOn');
		$this->form->removeElement('eventActions');
		$this->form->useTab('gelöschte Elemente');
//                $seitenId = isset($seitenId)?$data['seitenid']:'';
//                $containerId = $this->id;
//                $searchStr = 's:8:"seitenid";s:'.strlen($seitenId).':"'.$seitenId.'";s:9:"container";s:'.strlen($containerId).':"'.$containerId.'";';
////                addJoin('elements', 'dataId', 'id', $selectWhat)
//                addWhere('data', 'LIKE', '%'.$containerId.'%');
//                select('formBackup');
//		$row = getRow();
//		$ret = unserialize($row['data']);
//		if(isSet($ret['eldata'])){
//			$add = unserialize($ret['eldata']);
//			foreach($add as $i => $d){
//				$ret[$i] = $d;
//			}
//		}
	}

	public function getContent($data = array()){
		$src = '';
                if(isset($this->data['css_class']) && $this->data['css_class'] != ''){
                    $addClass = ' '.$this->data['css_class'];
                }else $addClass = '';
		if(!$this->listStyle){
			if(isAdmin() && $GLOBALS['interface'] != 'ajax'){
				$this->admineig();
				$src = '<div id="cont'.$this->name.'" class="element mainelement container'.$addClass.'">';
			}
			else $src .= '<div class="element mainelement container'.$addClass.'">';
		}
		$src .= $this->src;
		$src .= $this->getInline($data);
		if(!$this->listStyle) $src .= '</div>';
		return $src;
	}

	public function adminEig(){
		$this->src = '';
		if(PageRights::getObj()->isAllowed() && !$this->listStyle){
			$id = (isSet($this->data['id']))? $this->data['id']:$this->id;
			$el_indistr = get_class($this).'-'.$this->container.'-'.$id;
			$this->src = '<div class="admineig" style="width: 32px">';
			if($this->hassub  && rights::getObj()->getContentRights("create")) $this->src .=	'<div class="button sub" id="'.$el_indistr.'-sub" title="Neues Element"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appadd.png" alt="[+]" /></div>';
			if($this->editable && rights::getObj()->getContentRights("edit")) $this->src .=	'<div class="button edit" id="'.$el_indistr.'-edit" title="'.get_class($this).' &auml;ndern"><img src="'.$GLOBALS['cms_rootdir'].'admin/images/icons/appedit.png" alt="[e]" /></div>';
			$this->src .= '</div>';
		}
	}
	public static function getSubName() {
		return '';
	}
	public function formPost(){
		if(checkVar('treeAnordnung'))
			$_POST['orderby'] = 2;

		return parent::formPost();
	}
}
?>