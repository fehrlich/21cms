<?php
/**
 * Loginsystem
 *
 * @author F.Ehrlich
 */
class login {

	public function __construct(){
		
		if(\cms\session::getObj()->isLoggedIn()) return true;
		else {
			if(!$this->html()) exit();
		}
	}

	private function logincheck(){
                
                if (!isset($_POST["username"],$_POST["passwort"])) return false;

                addWhere("loginname","=",$_POST["username"]);
                addWhere("pass","=",md5($_POST["passwort"]));
                setLimit(1);
                select("user","id");

                $data = getRow();

		if(!empty($data)){
			\cms\session::getObj()->setUserId($data["id"]);

			if(isSet($GLOBALS['forum_integration'])  && $GLOBALS['forum_integration'] != ''){
				$api = new phpbb($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['forum_integration'], 'php');
					if($api->user_login(array(
						'username' => $_POST["username"],
						'password' => $_POST["passwort"],
						'admin' => '1',
						'autologin' => true,
				)) != 'SUCCESS') echo ''; //Der Login im Forum ist fehlgeschlagen. Bitte loggen sie sich manuell ein
			}
            return true;
		} else {
			return false;
		}
	}

	private function html(){
		if(checkVar(array('username','passwort')) && $this->logincheck()) return true;
		$tpl = new tpl('admin/tpl/login.html');
		
		$html = '<div id="login">';
		$html .= '<h1>Login</h1>';
		$html .= '<form action="index.php?'.$_SERVER['QUERY_STRING'].'" method="post">';
		$html .= '<label><span>Username:</span> <input type="text" name="username" /></label>';
		$html .= '<label><span>Passwort:</span> <input type="password" name="passwort" /></label>';
		$html .= '<input type="submit" name="loginsub" value="Login" class="loginButton" /><br />';
		$html .= '</form>';
		$html .= '</div>';
		$tpl->setVar('TOOLBAR', $html);
		echo $tpl;
		return false;
	}
}
?>
