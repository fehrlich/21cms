<?php
/**
 * Description of subelement
 *
 * @author F.Ehrlich
 */
abstract class subelement extends element {
	protected $data;
	protected $treeview = false;
	protected $mainelement = array();

	public function __construct($id = 0,$data = array(), $mainid = 0, $skipbuild = false,$deleteable = false,$buildform = true){
		$this->mainid = $mainid;
                parent::__construct($id,$data,'',$mainid,true,$buildform);
		$this->id = $id;
		if(isAdmin()){
			if($GLOBALS['interface'] == 'ajax' && !$skipbuild && $buildform){

				if(isset($_GET['id']) && $_GET['id'] != 'dia' && $mainid == 0) $mid = $_GET['id'];
				else $mid = $mainid;
				$this->form = new formular($this->mysqltab);
				$this->form->setShowHelp(true);
				$this->form->useTab(get_class($this));
				$list = buildSelectArray($this->mysqltab,array('id','title'),$this->treeview);
				$list = array(-1 => '[Nichts]')+$list;
				
				$this->form->addElement('Sichbarkeit', 'visible','select',1, array('unsichtbar', 'normal'));
				if($this->treeview) $this->form->addElement('Unterordnen', 'parent','select',-1, $list);
				
                                if (rights::getObj()->isAdmin()) {
                                    $this->form->useTab('Admin');
                                    $this->form->addElement('Erstellungsdatum', 'erstellt','text');
                                    $this->form->addElement('Anordnung', 'ordernr','text');
                                    $this->form->addElement('MainId', 'mainid','text',$mid);
                                    if($this->treeview){
                                            $this->form->addElement('Level', 'level','text',0);
                                    }

                                } else {
                                    if($this->treeview){
                                            $this->form->addElement('', 'level','hidden',0);
                                    }
                                    $this->form->addElement('', 'mainid','hidden',$mid);
                                    $this->form->addElement('', 'ordernr','hidden');
                                }

				
				$this->form->useTab(get_class($this));
				if($this->getThisTabName() == 'elements'){
				$this->form->addmysqldata('klasse',get_class($this));
					$this->form->startSerialize('eldata');
				}
				$this->formBuild();
			}
		}
	}
	
	public function formPost(){
		if($_GET['get'] == 'sub'){
			if(isSet($_POST['parent']) && $_POST['parent'] != '-1'){
				addWhere('id', '=', $_POST['parent'],'i');
				addWhere('parent', '=', $_POST['parent'],'i','OR');
			}

			select($this->mysqltab,'max(ordernr) as max','',true,true);
			$row = getRow();
			$_POST['ordernr'] = $row['max(ordernr) as max']+1;
			
			if(isSet($_POST['parent']) && $_POST['parent'] != '-1'){
				addWhere('id', '=', $_POST['parent'],'i');
				select($this->mysqltab,'level');
				$parentrow = getRow();
				$_POST['level'] = $parentrow['level']+1;
				
				addWhere('ordernr', '>=', $_POST['ordernr'],'i');
				updatearray($this->mysqltab, array('ordernr' => 'ordernr+1'));
			}
		}
		return parent::formPost();
	}
	
	/**
	 * must return the name of the mainelement
	 */
	abstract public function getMainName();

	public function getThisTabName(){
		return call_user_func_array(array(get_class($this), 'getTabName'),array());
	}

	public static function getTabName(){
		return 'elements';
	}

	public function setMainElement($mainelement){
		$this->mainelement  = $mainelement;
	}

	/**
	 *
	 * @return mainelement Mainelement
	 */
	public function getMainElement(){
            if(!$this->mainelement){
                $this->mainelement = element::load($this->mainid);
            }
            return $this->mainelement;
	}
}