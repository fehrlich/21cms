<?php

/**
 * Description of search
 *
 * @author easteregg
 */
class search {

	private $sql;
	private $searchDb;
	private $searchtext;
	private $result = array();
	private $searchElements = true;

	public function __construct($what) {
        stats::getObj()->registerSearch($what);
        $this->searchtext = mysqli_real_escape_string(mys::getObj(),$what);
		$this->sql = mys::getObj();
	}

	public function searchDbs($where, $what = "id") {
		if (!is_array($where))
			return false;

		if ($what == "id") {
			foreach ($where as $value) {
				$this->searchDb[] = (int) $value;
			}
		} else {
			//todo, datenbank namen übergeben.
			return false;
		}

		return true;
	}

	public function searchElements($bool) {
		$this->searchElements = $bool;
		return true;
	}

	public function exec() {
		$sql = mys::getObj()->cleanup();

		//search through dbs
		if (!empty($this->searchDb)) {

			$dbCon = new DatabaseConnector();
                        addWhere('dbType', '=', '0');
			$dbs = $dbCon->getDatabases();
			$multi = array(1,2,5,10);
			foreach ($dbs as $db) {
				$searchmul = array();
				foreach ($db['fields'] as $field) {
					if (isSet($field['searchmul']) && $field['searchmul'] !== false)
						$searchmul[$field['searchmul']][] = $field['name'];
				}
				$sel_str = '';
				$order = '';
				$allFields = array();
				foreach ($searchmul as $mulId => $fields) {
					$field_str = implode("`,`", $fields);
					$allFields[] = $field_str;
                    
					$sel_str .= ",MATCH(`".$field_str."`) AGAINST('*{$this->searchtext}*') AS score".$mulId;
					$order .= ($order != '')?'+':'';
					$order .= "(score".$mulId."*".$multi[$mulId].")";
				}
				
				$query = "SELECT id".$sel_str." FROM `data_" . $db['name'] . "` WHERE MATCH(`". implode("`,`", $allFields)."`) AGAINST('*{$this->searchtext}*' IN BOOLEAN MODE) > 0.9 ORDER BY ".$order;

				$res = $sql->query($query);
//
				if ($res !== false) {
					while ($row = $res->fetch_assoc()) {
						$row['dbid'] = $db['id'];
						$row['score'] = 0;
						for($i=0;$i<4;$i++)
							if(isSet($row['score'.$i])){
								$row['score'] += $row['score'.$i]*$multi[$i];
							}
						$this->result["db"][$db['id']][] = $row;
					}
				}
			}
		}

		if ($this->searchElements) {
			//$res = $sql->query("SELECT elements.id,menuepunkte.title_intern,MATCH(searchtxt) AGAINST('{$this->searchtext}') AS score FROM elements INNER JOIN menuepunkte ON elements.seitenid = `menuepunkte`.id WHERE MATCH(searchtxt) AGAINST('{$this->searchtext}' IN BOOLEAN MODE) > 0.9");

			$res = $sql->query("SELECT id,MATCH(searchtxt,tags,autor) AGAINST('{$this->searchtext}') AS score FROM elements WHERE MATCH(searchtxt) AGAINST('{$this->searchtext}' IN BOOLEAN MODE) > 0.9");
			if ($res !== false) {
				while ($row = $res->fetch_assoc()) {
					$row['dbid'] = 'elements';
					$this->result["elements"][] = $row;
				}

				$res->close();
			}

			$sql->cleanup();
		}

		return true;
	}

	public function getResult() {
		return $this->result;
	}

	public function getAllResult() {
		$res = array();
		if (isSet($this->result['db']))
			foreach ($this->result['db'] as $resArray)
				$res = array_merge($res, $resArray);
		if (isSet($this->result['elements']))
			$res = array_merge($res, $this->result['elements']);
		uasort($res, 'sortSearchResult');
//		array_multisort($res[0], SORT_ASC, SORT_STRING,
//							$ar[1], SORT_NUMERIC,SORT_DESC );
		return $res;
	}

	public function getResultsHtml() {
		
	}

	public function getContent() {
		
	}

}

function sortSearchResult($a, $b) {
	if ($a['score'] == $b['score'])
		return 0;
	else
		return ($a['score'] > $b['score']) ? -1 : 1;
}

?>
