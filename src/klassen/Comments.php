<?php
/**
 * Description of FrontEndUser
 *
 * @author Franz
 */
class Comments{
	/**
	 * Daten Editor
	 * @var DataEditor
	 */
	private $dbc;

	public function __construct(){
		$this->dbc = new DataEditor('comments');

		$this->dbc->form->addElement('Datenbank', 'dbId', 'database');
		$this->dbc->form->addElement('Datensatz', 'dataId', 'dataRecord','',array(
			'dbformname' => 'dbId'
		));

		$this->dbc->form->addElement('EmailAdresse', 'email', 'email');
		$this->dbc->form->addElement('Name', 'name','text');
		$this->dbc->form->addElement('Url', 'url', 'text');
		$this->dbc->form->addElement('Kommentar', 'comment', 'textarea');
	}

	public static function getUserForm($dbId,$dataId){
		/*
		 * session::getObj()->set("time",time())
		 */
		$form = new formular('comments',$_SERVER['REQUEST_URI']);
		$form->setSpamProtection(true);
		$form->setDoublePostProtection(true);
		$form->addElement('', 'dbId', 'mysql',$dbId);
		$form->addElement('', 'dataId', 'mysql',$dataId);
		$form->addElement('', 'time', 'mysql',time());
		$form->addElement('Ihr Name', 'name', 'text','',array(),true,true);
		$form->addElement('Ihre Email-Adresse', 'email', 'email','',array(),true,true);
		$form->addElement('Kommentar', 'comment', 'textarea','',array(),true,true);
		$form->setSaveButton('Abschicken', true);
                $form->useCaptcha(true);
		$formStr = (string) $form;
		if($formStr == 'Ok')
			$formStr = '<span style="color:green">Danke f&uuml;r Ihr Kommentar.</span>';
		return $formStr;
	}

	public function __toString(){
		if(isSet($_GET['db']) && $_GET['db'] > 0){
		 addWhere('dbId', '=', $_GET['db'],'i');
//			$GLOBALS['mysql_debug'] = true;
		 $this->dbc->getData();
			$GLOBALS['mysql_debug'] = false;
//		 echo $_GET['db'].'!';
		}
		return (string) $this->dbc;
	}

	public function formBuild() {
		$this->form = $this->dbc->form;
	}
        
	public static function getUserHtml($dbId = 0,$dataId = 0) {

                $form = (string) Comments::getUserForm($dbId,$dataId);
                addWhere('dbId', '=', $dbId);
		addWhere('dataId', '=', $dataId);
		select('comments', 'id,name,time,comment', 'time');
		$rows = getRows();
		$new = '<a name="comments"><h3>Kommentare</h3></a>';

		foreach ($rows as $comment) {
			$new .= Comments::buildComment($comment);
		}

		$new .= '<h4>Neuer Kommentar</h4>';
		$new .= $form;
		return $new;
	}

	static public function buildComment($commentRow){
		$com = '<div class="comment">';
		$com .= '<span class="name">' . htmlentities($commentRow['name'], ENT_COMPAT, "UTF-8") . '</span>';
		$com .= '<span class="datum">' . date('d.m.Y H:i:s', $commentRow['time']) . '</span>';
		$com .= '<p class="text">' . htmlentities($commentRow['comment'], ENT_COMPAT, "UTF-8") . '</p>';
		$com .= '</div>';
		return element::createPseudoElement($com, 'Comments', $commentRow['id']);
	}

	public function getDbc() {
	 return $this->dbc;
	}

	static public function formedit(){
		$db = new Comments();
		$dbc = $db->getDbc()->setEdit($_GET['id']);
		return $db;
	}
	public function formPost(){
		$dbc = $this->dbc->setEdit($_GET['id']);
		$this->dbc .= '';
		return 'Ok:RELOAD';
	}
	public function delete(){
		$this->dbc->delete($_GET['id']);
		return 'Ok';
	}
}
?>
