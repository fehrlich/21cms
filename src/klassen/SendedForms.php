<?php

class SendedForms
{

    /**
     * Daten Editor
     * @var DataEditor
     */
    private $dbc;

    public function __construct()
    {
        $this->dbc = new DataEditor('formSubmits');
        $this->dbc->setAllowEdit(false);
        $this->dbc->setAllowNew(false);

        $this->dbc->setLimit(100);

        $this->dbc->form->addElement("Gesendet am", "time", "time");
        $this->dbc->form->addElement("Formular auf Seite", "seitenTitel", "text");
        $this->dbc->form->addElement("FormularId", "formId", "text");
        $this->dbc->form->addElement("Daten", "data", "serTable");
        $this->dbc->setOrderBy('id DESC');
    }

    public function __toString()
    {
        $html = '';
        // addGroupBy('formId');
        // $data = buildSelectArray('formSubmits', array('formId', 'formId'));
        // $form = new formular();
        // if ($form->posted && isset($_POST['formId']) && $_POST['formId'] > 0) {
        //     $this->exportData($_POST['formId']);
        // }
        // $form->addElement('Export Formular: ', 'formId', FormType::SELECT, '', $data);
        // $form->setSaveButton('Export', true);
        // $html .= $form->show(true);
        $html .= (string) $this->dbc;
        return $html;
    }

    public function exportData($formId)
    {
        include($_SERVER['DOCUMENT_ROOT'] . '/klassen/PHPExcel.php');

        $styleArray = array(
            'fixed' => true,
            'locked' => true,
            'font' => array(
                'bold' => true,
            )
        );
        
        addWhere('mainid', '=', $formId);
        select('elements', 'id,eldata');
        $rows = getRows();
        $formRows = array();
        foreach ($rows as $fRow) {
            $d = unserialize($fRow['eldata']);
            $formRows[$fRow['id']]  = $d['title'];
        }
        addWhere('formId', '=', $formId);
        select('formSubmits');
        $rows = getRows();
        
        $phpExcel = new PHPExcel();
        $worksheet = $phpExcel->getActiveSheet();
        $first = true;
        foreach ($rows as $i => $row) {
            $data = unserialize($row['data']);
            if ($first) {
                $first = false;
                $x = 0;
                foreach ($data as $id => $val) {
                    if ($id > 0) {
                        if (isset($formRows[$id])) {
                            $id = $formRows[$id];
                        }
                    }
                    $worksheet->setCellValueByColumnAndRow($x, 1, $id);
                    $worksheet->getColumnDimensionByColumn($x)->setAutoSize(true);
                    $worksheet->getStyleByColumnAndRow($x, 1)->applyFromArray($styleArray);
                    $x++;
                }
            }
            $x = 0;
            foreach ($data as $id => $val) {
                $worksheet->setCellValueByColumnAndRow($x, $i+2, $val);
                $x++;
            }
        }
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"export.xls\"");
        header("Cache-Control: max-age=0");

        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
        $objWriter->save("php://output");
        exit();
    }
}
