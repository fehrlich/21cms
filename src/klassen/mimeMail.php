<?php
    class mimeMail{
	var $parts;
	var $Recipient;
	var $from;
	var $headers;
	var $subject;
	var $body;
	var $type;

	var $debug = false;

	function mime_mail(){
            $this->parts = array();
            $this->Recipient = array();
            $this->from = "";
            $this->subject = "";
            $this->body = "";
            $this->headers = "";
            $this->type = 'normal';
	}

	function setFrom($mail, $name){
            $this->from = trim($name).' <'.trim($mail).'>';
	}
	function setFromComplete($from){
            $this->from = $from;
	}
	function setBody($body){
            $this->body = $body;
	}

	function setSubject($subject){
            $this->subject = (string) $subject;
	}

	function addRecipient($recipient){
            $this->Recipient[] = $recipient;
	}

	/**
	 * Add an attachment to the mail object
	 * @param <type> $message
	 * @param <type> $name
	 * @param <type> $ctype
	 */
	function add_attachment($message, $name = "", $ctype = "application/octet-stream",$encode = ''){
            $this->parts[] = array (
                "ctype" => $ctype,
                "message" => $message,
                "encode" => $encode,
                "name" => $name
            );
	}

	/**
	 * Build message parts of an multipart mail
	 * @param <type> $part
	 * @return <type>
	 */
	function build_message($part){

            $message = $part["message"];
            $message = chunk_split(base64_encode($message));
            $encoding = "base64";

            if ($part["name"]!="") {
                $dispstring = "Content-Disposition: attachment; filename=\"$part[name]\"\r\n";
            }
            return "Content-Type: ".$part["ctype"].
                ($part["name"]?"; name = \"".$part["name"]."\"" : "").
                "\r\nContent-Transfer-Encoding: $encoding\r\n".$dispstring."\r\n$message\r\n";
	}

	/**
	 * Build a multipart mail
	 * @return <type>
	 */
	function build_multipart(){
            if(count($this->parts) > 1) {
                $boundary = "b".md5(uniqid(time()));
                $multipart = "Content-Type: multipart/mixed;\tcharset=utf-8; boundary=$boundary\r\n\r\nThis is a MIME encoded message.\r\n\r\n--$boundary";

                for($i = sizeof($this->parts)-1; $i >= 0; $i--) {
                    $multipart .= "\r\n".$this->build_message($this->parts[$i])."--$boundary";
                }
                $this->type = 'multi';
                return $multipart.= "--\r\n";
            } else {
                return "Content-Type: text/html;\tcharset=utf-8;\r\n";
            }
	}

	/**
	 * Send the mail
	 * @return <type>
	 */
	function send(){
            $mime = "";

            if (!empty($this->from)) $mime .= "Reply-To: ".$this->from."\n";
            if (!empty($this->headers)) $mime .= $this->headers."\r\n";

            if (!empty($this->body))
                $this->add_attachment($this->body, "", "text/html");

            $mime .= "Mime-Version: 1.0\n".$this->build_multipart();
            $txt = $this->body;

            foreach($this->Recipient as $recipient){

                if ( !$this->debug ) {
                    $success = mail($recipient, $this->subject, $txt, $mime);
                    $anz = count($this->parts);
                    $anhang = '';
                } else {
                    $anz = count($this->parts);
                    $anhang = '';

                    for ($i=0;$i<$anz;$i++) $anhang .= ','.$this->parts[$i]['name'];

                    echo '<div style="border: 1px solid black">Email an:'.$recipient."<br />";
                    echo 'Von: '.htmlentities($this->from)."<br />";
                    echo 'Betreff:'.$this->subject."<br />";

                    if ($anhang != '') echo 'Anhaenge: '.substr($anhang, 1)."<br />";
                    echo "Text:".$this->body."</div>";
                    $success = true;
                }
            }
            return $success;
	}
    };
?>
