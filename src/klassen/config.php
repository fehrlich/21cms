<?php

    /**
     * @author F.Ehrlich
     */
    class config {

        static private $handle;
        private $vars;
        private $mustBeWriteable;
        private $mustExist;
        private $install;
        private $statWriteable = false;
        private $statMysqlCon = false;
        private $statTable = false;
        private $statUser = false;
        private $statTpl = false;

        /**
         *
         * @return config
         */
        static public function getObj() {
            if ( config::$handle === null )
                new config();

            return config::$handle;
        }

        public function __construct($install = false) {
            config::$handle = $this;
            $this->install = $install;
            $this->vars = array(
                'mysql_host', 'mysql_user', 'mysql_pass', 'mysql_db', 'mysql_port',
                'cms_rootdir', 'cms_roothtml', 'serverstatus', 'trac_milestone', 'forum_integration', 'piwik_token', 'piwik_siteid'
            );
            $this->mustBeWriteable = array(
                'serverconfig.php', 'dateien/', '.htaccess'
            );
            $this->mustExistTables = array(
                'dataTemplates', 'databases', 'elements', 'menuepunkte', 'user'
            );
            $this->mustExist = array(
                'index.html', 'style.css'
            );
            if ( $this->install )
                $this->mustBeWriteable[] = 'install.sql';
            $GLOBALS['mysql_host'] = 'localhost';
            $GLOBALS['mysql_user'] = 'root';
            $GLOBALS['mysql_pass'] = '';
            $GLOBALS['mysql_port'] = '3306';
            $GLOBALS['mysql_db'] = 'dascms';
            $spl = explode('admin', $_SERVER['PHP_SELF']);
            $GLOBALS['cms_rootdir'] = $spl[0];
            $GLOBALS['cms_roothtml'] = $spl[0];
            $GLOBALS['serverstatus'] = '0';
            $GLOBALS['trac_milestone'] = '';
            $GLOBALS['piwik_token'] = '';
            $GLOBALS['piwik_siteid'] = '';
            $GLOBALS['forum_integration'] = ''; //forum_integration

            if ( file_exists($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'serverconfig.php') )
                include($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'serverconfig.php');
        }

        public function set($name, $value) {
            if ( !in_array($name, $this->vars) )
                $this->vars[] = $name;
            $GLOBALS[$name] = $value;
        }

        public function get($name) {
            if ( isSet($GLOBALS[$name]) )
                return $GLOBALS[$name];
            else
                return false;
        }

        static public function getServerStatus() {
            if ( !isset($GLOBALS['serverstatus']) || $GLOBALS['serverstatus'] == '0' )
                return 'Installation';
            elseif ( $GLOBALS['serverstatus'] == '1' )
                return 'Einpflegen';
            else
                return 'Produktiv';
        }

        private function getVarString() {
            $cfg_txt = '';
            foreach ( $this->vars as $var ) {
                $cfg_txt .= '$GLOBALS[\'' . $var . '\'] = ';
                if ( is_array($GLOBALS[$var]) ) {
                    $cfg_txt .= 'array(';
//				 implode("','", $GLOBALS[$var]);
                    foreach ( $GLOBALS[$var] as $k => $v )
                        $cfg_txt .= "'" . $k . "' => '" . $v . "',";
                    $cfg_txt = substr($cfg_txt, 0, -1);
                    $cfg_txt .= ')';
                } else
                    $cfg_txt .= '\'' . $GLOBALS[$var] . '\'';
                $cfg_txt .= ";\n";
            }
            return $cfg_txt;
        }

        private function getElementString() {
            $cfg_txt = '$GLOBALS[\'elements\'] = array();' . "\n";
            $files = scandir($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'element/');
            foreach ( $files as $file ) {
                //include($_SERVER['DOCUMENT_ROOT'].$GLOBALS['cms_rootdir'].$file);
                if ( substr($file, -4) == '.php' ) {
                    $file = substr($file, 0, -4);
                    $f = new $file();
                    $class = get_parent_class($file);
                    $cfg_txt .= '$GLOBALS[\'elements\'][\'' . $file . '\'] = array(\'' . $class . '\');' . "\n";
                }
            }
            return $cfg_txt;
        }

        public function save() {
            $cfg_txt = '<?php' . "\n";
            if ( $_GET['mm'] == 'install' )
                $cfg_txt .= 'error_reporting(E_ALL);' . "\n";
            $cfg_txt .= $this->getVarString();
            $cfg_txt .= $this->getElementString();
            $cfg_txt .= '?>';
            $ret = file_put_contents($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'serverconfig.php', $cfg_txt);

            if ( $ret === false ) {
                throw new Exception('In Serverconfig konnte nicht geschrieben werden');
                return false;
            } else
                return true;
        }

        private function writeabelStatus() {
            $html = '<h4>Schreibare Verzeichnisse/Dateien</h4>';
            $this->statWriteable = true;
            foreach ( $this->mustBeWriteable as $d ) {
                if ( is_writable('../' . $d) )
                    $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -32px no-repeat;"></div>';
                else {
                    $this->statWriteable = false;
                    $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';
                }
                $html .= $d . ' : ' . $addhtml . '<br />';
            }
            return $html;
        }

        private function mysqlStatus() {
            $html = '<h4>Mysql: Status</h4>';
            $link = @mysqli_connect($GLOBALS['mysql_host'], $GLOBALS['mysql_user'], $GLOBALS['mysql_pass'], $GLOBALS['mysql_db']);

            if ( !mysqli_connect_error() ) {
                $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -32px no-repeat;"></div>';
                if ( $this->install ) {

                }
                $this->statMysqlCon = true;
            } else {
                $this->statMysqlCon = false;
                $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>(' . mysqli_connect_error() . ')';
            }
            $html .= 'Serververbindung:' . $addhtml;
            if ( $this->statMysqlCon ) {
                $res = mysqli_query($link, 'SHOW TABLES');
                $rows[] = array();
                while ( $row = mysqli_fetch_assoc($res) ) {
                    $rows[] = $row['Tables_in_' . $GLOBALS['mysql_db']];
                }
                $check = true;
                foreach ( $this->mustExistTables as $tab ) {
                    if ( !in_array($tab, $rows) )
                        $check = false;
                }
                $html .= '<br />Tabellen getestet: ';
                if ( $check == false )
                    $html .= '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';
                else {
                    $html .= '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -32px no-repeat;"></div>';
                    if ( $this->install ) {
                        if ( @unlink('../install.sql') )
                            $addhtml .='(install.sql wurde gel&ouml;scht)';
                        else
                            $addhtml .='(<span style="color:red">install.sql konnte nicht gel&ouml;scht werden!</span>)';
                        include('../klassen/mysql.php');
                    }
                    $this->statTable = true;

                    $anz = dbCheck('user');
                    if ( $anz == 0 )
                        $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';
                    else
                        $this->statUser = true;
                    $html .= '<br />User angelegt: ' . $addhtml;
                }
            }
            return $html;
        }

        private function tplStatus() {
            $html = '<h4>Template Status (Dateien m&uuml;ssen existieren)</h4>';
            $this->statTpl = true;
            foreach ( $this->mustExist as $d ) {
                if ( file_exists('../tpl/' . $d) )
                    $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -32px no-repeat;"></div>';
                else {
                    $this->statTpl = false;
                    $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';
                }
                $html .= $d . ' : ' . $addhtml . '<br />';
            }
//		if(!file_exists('../tpl/index.php'))  $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';
//		else  $addhtml = '<div style="width:16px;height:16px;display:inline-block;background:url(\'js/themes/apple/icons.png\') -16px -16px no-repeat;"></div>';

            return $html;
        }

        public function htmlStatus() {
            if ( $this->install )
                $status = '<span style="color:red">Aufgesetzt</span>';
            else
                $status = 'unbekannt';
            $html = '<h1>Server Status</h1>';
            $html .= '<h4>Aktueller Zustand: ' . config::getServerStatus() . '</h4>';
            $html .= $this->writeabelStatus();
            $html .= $this->tplStatus();
            $html .= $this->mysqlStatus();
            if ( $this->statWriteable && $this->statMysqlCon && $this->statTpl && $GLOBALS['serverstatus'] < 2 ) {
                $form = new formular();
                $form->addElement('Description', 'asdf', FormType::HTMLEDITOR, '<p><img alt="" class="lightbox" src="/dateien/Bilder/Organigramm.png" style="width: 354px; height: 254px;" /></p>');
                $form->setSaveButton('Auf Produktiv setzen', true);
                if ( $form->posted ) {
                    $hta = "ExpiresActive On\ExpiresDefault \"access plus 10 years\"\nExpiresByType text/html \"access plus 5 minutes\"\nSetOutputFilter DEFLATE\nSetEnvIfNoCase Request_URI \\\n\\.(?:gif|jpe?g|png)\$ no-gzip dont-vary\nFileETag None";
                    $hta .= file_get_contents('../.htaccess');
                    file_put_contents('../.htaccess', $hta);
                    $GLOBALS['serverstatus'] = 2;
                    $this->save();
                }
                $html .= $form;
            }
            return $html;
        }

        public function __toString() {

            $html = '';

            if ( $this->install ) {
                $html .= $this->htmlStatus();
            }

            $html .= '<h1>Server Einstellungen</h1>';

            $form = new formular();
            $form->setFormAction($_SERVER['PHP_SELF'] . '?mm=servereinst');

            foreach ( $this->vars as $var ) {
                if ( $var != 'serverstatus' ) {

                    if ($var == "mysql_pass") {
                        $form->addElement($var, $var, 'text', "****");
                    } else {
                        $form->addElement($var, $var, 'text', $GLOBALS[$var]);
                    }
                }
            }

            if ( $this->install ) {
                $form->addElement('Mysql', 'mysqlinstall', 'check', '', 'Mysqlstrucktur Installieren (Alles vorhande wird gel&ouml;scht)');
            }

            $hta = file_get_contents('../.htaccess');

            if ( strpos($hta, 'AuthType Basic') )
                $protected = true;
            else
                $protected = false;

            $form->addElement('Frontend sperren', 'passprotect', 'check', $protected, 'Frontend sperren');
            $form->setSaveButton('Speichern', true);
            if ( $form->posted ) {
                foreach ( $this->vars as $var ) {

                    if ( $var == "mysql_pass" && $_POST[$var] == "****" ) {
                        continue;
                    }

                    if ($var != 'serverstatus') {
                        $GLOBALS[$var] = addslashes($_POST[$var]);
                    }


                }

                $this->save();
                if ( isset($_POST['passprotect']) ) {
                    $hta = file_get_contents('../.htaccess');
                    $hta .= "\nAuthType Basic\nAuthName \"Login\"\nAuthUserFile " . $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . ".htpasswd\nRequire valid-user";
                    file_put_contents('../.htaccess', $hta);
                }
                if ( isset($_POST['mysqlinstall']) && $this->install ) {
                    $link = @mysqli_connect($GLOBALS['mysql_host'], $GLOBALS['mysql_user'], $GLOBALS['mysql_pass'], $GLOBALS['mysql_db']);
                    if ( !mysqli_connect_error() ) {
                        $mysql_src = file_get_contents('../install.sql');

                        $split_mysql = explode(';', $mysql_src);
                        $anz = count($split_mysql);

                        for ( $i = 0; $i < $anz; $i++ ) {
                            $split_mysql[$i] = trim($split_mysql[$i]);
                            @mysqli_query($link, $split_mysql[$i]) or die(mysqli_error($link));
                        }
                    } else
                        $html .= 'Verbindung fehlgeschlagen';
                }
            }
            if ( $this->install )
                $html .= '<span style="color:red">Achtung bei erfolgreicher Mysql kontrolle wird install.sql gel&ouml;scht!</span>';
            $html .= $form;
            return $html;
        }
    }
?>