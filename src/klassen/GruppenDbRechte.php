<?php
/**
 * Description of User
 *
 * @author R.Heine
 */
class GruppenDbRechte {
	/**
	 *
	 * @var DataEditor
	 */
	private $dbsys;
        private $sql;

	public function  __construct() {

            $yesNo = array(0 => "Nicht erlaubt",1 => "Erlaubt");

            //gruppen holn
            $res = mys::getObj()->cleanup()->query("SELECT id,name FROM userGroups order by name asc");
            $groups = array();
            while($row = $res->fetch_array()) $groups[$row["id"]] = $row["name"];
            $res->close();

            //datenbanken holn
            $res = mys::getObj()->cleanup()->query("SELECT id,name FROM `databases` order by name asc");
            $dbs = array();
            while($row = $res->fetch_array()) $dbs[$row["id"]] = $row["name"];
            $res->close();



            $this->dbsys = new DataEditor('userDbRights');
//            if(!isset($_GET['inter']) || $_GET['inter'] != 'dia')
//                    $this->dbsys->form->setSaveButton('Speichern');

            $this->dbsys->form->setFormAction('ajax.php?kl=GruppenDbRechte'.buildGet('edit,new'));


            //beim bearbeiten kommt sonst nen fehler, keine ahnung warum....

            $this->dbsys->form->addElement('Gruppe', 'groupId','select','0', $groups);
            $this->dbsys->form->addElement('Datenbank', 'dbId','select','0', $dbs);
            

            $this->dbsys->form->addElement('Erstellen', 'create','select','0', $yesNo);
            $this->dbsys->form->addElement('Bearbeiten', 'edit','select','0', $yesNo);
            $this->dbsys->form->addElement('Löschen', 'delete','select','0', $yesNo);
	}

	public function  __toString() {

            if ($GLOBALS["interface"] == "ajax") return (string) $this->dbsys;

//            $this->sql = mys::getObj()->cleanup();
//            $res = $this->sql->query("SELECT r.id,r.create,r.edit,r.release,r.delete,g.name,d.name AS dbname
//                                      FROM userDbRights AS r
//                                      INNER JOIN `databases` AS d ON r.dbId = d.id
//                                      INNER JOIN userGroups AS g ON r.groupId = g.id");

            $html = $this->dbsys->getHtml();
//            $html = '';
//            $html .= '<span class="link newRow" id="GruppenDbRechte-">Neue Berechtigungsrichtlinie anlegen</span><br /><br />';
//            $html .= '<table border="0" cellpadding="3" cellspacing="0">
//                      <thead>
//                        <tr class="headdiv">
//                          <th>id</th>
//                          <th>Gruppenname</th>
//                          <th>Datenbankname</th>
//                          <th>Erstellen</th>
//                          <th>Alle bearbeiten</th>
//                          <th>Alle löschen</th>
//                          <th>Aktionen</th>
//                        </tr>
//                      </thead>
//                      <tbody>';
//
//            if ($res !== false) {
//                $i = 1;
//                while($row = $res->fetch_array()) {
//                    $html .= "<tr". (($i++ % 2 == 0)? ' class="even"':'') .">";
//                    $html .= "<td>{$row["id"]}</td>";
//                    $html .= "<td>{$row["name"]}</td>";
//                    $html .= "<td>{$row["dbname"]}</td>";
//                    $html .= "<td>" . (($row["create"])?'ja':'nein') . "</td>";
//                    $html .= "<td>" . (($row["edit"])?'ja':'nein') . "</td>";
//                    $html .= "<td>" . (($row["delete"])?'ja':'nein') . "</td>";
//                    $html .= '<td style="">
//                                <span class="link editRow" id="GruppenDbRechte--' . $row["id"] .'">Edit</span>
//                                <span class="link delRow" id="GruppenDbRechte--' . $row["id"] .'">Delete</span>
//                              </td>';
//                    $html .= "</tr>";
//                }
//            }
//
//            $html .= "</table>";
            return $html;
	}
}
?>
