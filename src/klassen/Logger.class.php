<?php

/**
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
 */
    class Logger  {

        const ALL       = 'all';
        const EMERGENCY = 'emergency';  // 128
        const ALERT     = 'alert';      // 64
        const CRITICAL  = 'critical';   // 32
        const ERROR     = 'error';      // 16
        const WARNING   = 'warning';    // 8
        const NOTICE    = 'notice';     // 4
        const INFO      = 'info';       // 2
        const DEBUG     = 'debug';      // 1
        const NONE      = 'none';      // 0

        public static function getBitMaskFromLevel($flag) {
            switch ($flag) {
                case self::ALL : return 255;
                case self::EMERGENCY : return 128;
                case self::ALERT : return 64;
                case self::CRITICAL : return 32;
                case self::WARNING : return 16;
                case self::ERROR : return 8;
                case self::NOTICE : return 4;
                case self::INFO : return 2;
                case self::DEBUG : return 1;
                default: return 0;
            }
        }

        static private $logFileHandles = array();

        static private $logLevel = 0;

        private $outputCallback = null;

        public function emergency($message, array $context = array()) { return $this->log(self::EMERGENCY, $message,$context); }
        public function alert($message, array $context = array())     { return $this->log(self::ALERT,     $message,$context); }
        public function critical($message, array $context = array())  { return $this->log(self::CRITICAL,  $message,$context); }
        public function error($message, array $context = array())     { return $this->log(self::ERROR,     $message,$context); }
        public function warning($message, array $context = array())   { return $this->log(self::WARNING,   $message,$context); }
        public function notice($message, array $context = array())    { return $this->log(self::NOTICE,    $message,$context); }
        public function info($message, array $context = array())      { return $this->log(self::INFO,      $message,$context); }
        public function debug($message, array $context = array())     { return $this->log(self::DEBUG,     $message,$context); }

        /**
         * opens the logFile
         * @param type $fileName
         * @return type
         */
        private function openLogFile($fileName) {

            if (!isset(self::$logFileHandles[$fileName])) {
                $filePath =  __DIR__ . "/../logs/$fileName";

                if ( !is_writable(dirname($filePath))) {
                    return false;
                }

                self::$logFileHandles[$fileName] = fopen($filePath, "a");
            }

            return self::$logFileHandles[$fileName];
        }

        public static function setself($flags) {
            self::$logLevel = $flags;
        }

        public function log($level, $message, array $context = array()) {
            if ( !(self::getBitMaskFromLevel(self::$logLevel) & self::getBitMaskFromLevel($level)) ) {
                return false;
            }

            $message = $this->_composeLogString($message, $context);

            if ( $this->outputCallback !== null ) {
                $this->_printToOutputCallback($message,$level);
                return;
            }

            $filename = str_replace("!","",$level);
            $fileHandle = $this->openLogFile($filename);

            if ( $fileHandle !== false ) {
                $this->_writeToLogfile($fileHandle,$message);
            }
        }

        private function _printToOutputCallback($msg,$level = "") {
            $tmp = $this->outputCallback;
            $tmp("[{$level}] {$msg}");
        }

        private function _composeLogString($msg,$context) {

            foreach ($context as $key => $value) {
                $msg = str_replace("{" . $key . "}", $value, $msg);
            }

            $t = microtime(true);
            $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
            $timeStamp = date('Y-m-d H:i:s.' . $micro, $t);

            return $timeStamp . " " . $msg . PHP_EOL;
        }

        private function _writeToLogfile($handle,$msg) {
            fwrite($handle,$msg);
        }

        public function setOutputCallback($func) {
            $this->outputCallback = $func;
            return $this;
        }

        public function __destruct() {
            foreach(static::$logFileHandles as $handle) {
                fclose($handle);
            }
        }
    }