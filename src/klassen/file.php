<?php

namespace cms;

/*
  echo $path_parts['dirname'], "\n";
  echo $path_parts['basename'], "\n";
  echo $path_parts['extension'], "\n";
  echo $path_parts['filename'], "\n"; // since PHP 5.2.0
 */

    class file {

	private $id = 0;
	private $data = null;
	private $fileInfo = array();
	private $created = '';
	private $adminAuthor = 0;
	private $frontEndAuthor = 0;
	private $size = '';
	private $fileType = '';
	private $md5 = '';
	private $comment = '';
	private $time = 0;
	private $updated = false;
	private $deleted = false;
	private static $handle = null;
	private $fileExists = true;
    private $relativePath = '';
    private $pubJson = false;

	public function __construct($path, $absolutepath = false) {

		$this->fullPath = ($absolutepath) ? $path : $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . "/" . $path;

		if (\file_exists($this->fullPath) && \is_readable($this->fullPath)) {

			//cleanup path
			$root = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'];
			$root = \str_replace(array("////", "///", "//"), "/", $root);
			$this->fullPath = \str_replace(array("////", "///", "//"), "/", $this->fullPath);

			$this->fileInfo = \pathinfo($this->fullPath);
			$this->fileInfo["relativedir"] = dirname(\substr($this->fullPath, strlen($root) - 1));
			if (!isSet($this->fileInfo["extension"]))
				$this->fileInfo["extension"] = '';

			$this->fetchData();
		}else{
			echo 'Datei '.$path.' existiert nicht';
			$this->fileExists = false;
			if(isAdmin() && \cms\session::getObj()->getUserId() == 1)
				echo ' '.$this->fullPath;
		}
	}
        
        public function getFileInfo(){
            return $this->fileInfo;
        }

	public function fetchData() {
		\mys::getObj()->cleanup()->clearWhere();
		addWhere("name", "=", $this->fileInfo["basename"]);
		addWhere("path", "=", $this->fileInfo["relativedir"]);
		\addJoin('frontEndUser', 'author2', 'id', 'loginname', 'left', 'frontEndAuthor');
		\addJoin('user', 'author', 'id', 'loginname', 'left', 'adminAuthor');
                \setLimit(1);
		select("files", "id,data,size,author,author2,md5,uploadedTime,filetype,comment,pubJson");


		$result = getRow();

		if (!isset($result["id"])) {
			$this->registerFile();
			return true;
		}

		$this->md5 = $result["md5"];
		$this->fileType = $result["filetype"];
		$this->id = $result["id"];
		$this->size = $result["size"];
		$this->adminAuthor = $result["adminAuthor"];
		$this->frontEndAuthor = $result["frontEndAuthor"];
		$this->time = $result["uploadedTime"];
		$this->data = \unserialize($result["data"]);
		$this->comment = $result["comment"];
        $this->relativePath = ($result["filetype"] == 'directory') ? $this->fileInfo["relativedir"] . "/" . $this->fileInfo["basename"] : $this->fileInfo["basename"];
        $this->pubJson = (bool) $result["pubJson"];
        
        
	}

	public function getId() {
	 return $this->id;
	}
	
	private function registerFile() {
		$this->md5 = \md5_file($this->fullPath);
		$this->size = \filesize($this->fullPath);

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$this->fileType = finfo_file($finfo, $this->fullPath);
		finfo_close($finfo);
		$author = '0';
		$author2 = '0';
		if (session::getObj()->isLoggedIn())
			$author = session::getObj()->getUserId();
		if (\frontendSession::getObj()->isLoggedIn())
			$author2 = \frontendSession::getObj()->getUserId();
		$this->id = insertArray("files", array(
					"name" => $this->fileInfo["basename"],
					"ext" => $this->fileInfo["extension"],
					"path" => $this->fileInfo["relativedir"],
					"filetype" => $this->fileType,
					"md5" => $this->md5,
					"size" => $this->size,
					"author" => $author,
					"author2" => $author2,
					"uploadedTime" => time(),
					"sort" => time(),
						), "sssssiiiii");
	}

	public function delete($databaseOnly = false) {
		if(!$this->fileExists) return;
		//check for possible thumbnail folders
		$folder = dirname($this->fullPath);
		
		$tmp = explode("/", $this->fullPath);
		$filename = array_pop($tmp);

		for ($i = 0; $i <= 10; $i++) {
			$thumbpath = $folder . "/__thumb" . $i;
			if ($filename != '' && file_exists($thumbpath . "/" . $filename)) {
				unlink($thumbpath . "/" . $filename);
			}
		}
		
		if (!$databaseOnly){
			if($this->isDir())
				@rmdir($this->fullPath);
			else
				@unlink($this->fullPath);
		}
		\mys::getObj()->cleanup()->clearWhere();
		addWhere("id", "=", $this->id);
		delete("files");

		$this->deleted = true;
		$this->close();
	}
	
	public function isDir(){
		return ($this->fileType == 'directory');
	}

	/**
	 *
	 * @param <type> $path
	 * @param <type> $absolutepath
	 * @return file
	 */
	public static function load($path, $absolutepath = false) {
		return new self($path, $absolutepath);
	}

	public function get($what = null) {
		if ($what !== null)
			return (isset($this->data[$what])) ? $this->data[$what] : false;
		return $this->data;
	}
        
        public function getData(){
            return $this->data;
        }

	public function checkRights() {
		//needs to be implemented
		return true;
	}

	public function close() {
		$this->__destruct();
	}

	public function set($what, $value) {
            $this->updated = true;
            $this->data[$what] = $value;

            return $this;
	}
        
	public function setData($data) {
            $this->updated = true;
            $this->data = $data;

            return $this;
	}

	public function __destruct() {
		if ($this->updated && !$this->deleted) {
			\mys::getObj()->cleanup()->clearWhere();

			\addWhere('id', '=', $this->id);
			\updateArray('files', array(
				'data' => \serialize($this->data)
					), 's');
		}
	}

	public static function getFolderContent($dir, $onlyFiles = true, $rek = false, $sort = 'id') {

		if ($onlyFiles) {
            addWhere('filetype', '!=', 'directory');
        } 
        
        /* fix for folderselection */
        if (substr($dir,-1) == "/") {
            $dir = substr($dir,0,-1);
        }

		if ($rek) {
            addWhere('SUBSTRING(t.path,1,' . strlen($dir) . ')', '=', $dir, 's', 'AND', '');
        } else {
            addWhere('path', '=', $dir);
        }
			

		\addJoin('frontEndUser', 'author2', 'id', 'loginname', 'left', 'frontEndAuthor');
		\addJoin('user', 'author', 'id', 'loginname', 'left', 'adminAuthor');
		select('files', 'id,name,data,size,author,author2,md5,uploadedTime,filetype,data,path', $sort);

		$rows = array();

		while ($row = \getRow()) {
			if ($row['data'] != '')
				$row['data'] = \unserialize($row['data']);

			$rows[] = $row;
		}

		return $rows;
	}

	public function getCreated() {
		return $this->created;
	}

	public function getAuthor() {
		return $this->adminAuthor;
	}

	public function getAdminAuthor() {
		return $this->adminAuthor;
	}

	public function getFrontEndAuthor() {
		return $this->adminAuthor;
	}

	public function getSize() {
		return $this->size;
	}

	public function getFileType() {
		return $this->fileType;
	}

	public function getMd5() {
		return $this->md5;
	}

	public function getTime() {
		return $this->time;
	}

        public function getcomment() {
		return $this->comment;
	}
        
//        public function move($destination, $absolutepath = false){
//            $destination = ($absolutepath) ? $destination : $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . "/" . $destination;
//            $data = $this->get();
//            $source = $this->fullPath;
//            $this->delete(true);
//            if (rename($source, $destination)){
//                \cms\file::load($destination, true)->setData($data);
//                return true;
//            }else{
//                \cms\file::load($source, true);
//                echo 'Die Datei konnte nicht verschoben werden';
//                return false;
//            }
//        }
        
        public function copy($destination, $absolutepath = false,$move = false,$saveMove = false){
            $destination = ($absolutepath) ? $destination : $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . "/" . $destination;
            if($this->isDir()){
		if(mkdir($destination . '/')){
                    $destDir = \cms\file::load($destination.'/',true)->setData($this->getData());
                    $files = glob($this->fullPath.'/*');
                    if($files){
                        foreach ($files as $x => $file) {
                            $name = substr($file,  strlen($this->fullPath)+1);
                            $source = \cms\file::load($this->fullPath.'/'.$name,true);
                            $source->copy($destination.'/'.$name, true, $move);
                        }                    
                    }
                    if($move)
                        $this->delete();
                    return $destDir;
                }else 
                    return null;
            }else{
                if(copy($this->fullPath, $destination)){
                    if($move)
                        $this->delete ();
                    return \cms\file::load($destination,true)->setData($this->getData());
                }else return null;
            }
        }
        
        public function move($destination, $absolutepath = false,$saveMove = false){
            $this->copy($destination, $absolutepath, true, $saveMove);
        }
        
        public function checkFilePathOccurrence($destination,$absolutepath = false,$updateMysqlData = false,$moveFiles = false){
            $destination = substr($destination,strlen($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'])-1);
            $source = str_replace('&', '&amp;', substr($this->fullPath,strlen($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'])-1));
            $source_pfad = slashFolderFix($source);
            
            $mysqldestination = str_replace('&', '&amp;', slashFolderFix($destination));
//			echo
            $update_tables = array(
                    'elements' => array('eldata'),
                    'dataTemplates' => array('html', 'image'),
                    'menuepunkte' => array('cvars'),
                    'rss' => array('image')
            );
            $serialized = array(
                    'elements' => array('eldata'),
                    'menuepunkte' => array('cvars')
            );

            $dbc = new \DatabaseConnector();
            $dbs = $dbc->getDatabases();
            $dont_replace = array('tags', 'email', 'select');
            $db_tables = array();
            addWhere('preview', '!=', '');
            select('dataTemplates', 'dbid,preview');
            $mainTemplates = getRows();
            $db_byName = array();
            foreach ($dbs as $db) {
                    $update_rows = array();
                    foreach ($db['fields'] as $field) {
                            if (!in_array($field['type'], $dont_replace)) {
                                    $update_rows[] = $field['name'];
                            }
                    }
                    if (count($update_rows) > 0) {
                            if($db['dbType'] == '1') $dbname = 'user';
                            elseif($db['dbType'] == '2') $dbname = 'frontEndUser';
                            else $dbname = 'data_' . $db['name'];
                            
                            $db_tables[$dbname] = $update_rows;
                            $db_byName[$dbname] = $db;
                            $tpl_byDbName[$dbname] = array();
                            foreach ($mainTemplates as $tpl) {
                                    if ($db['id'] == $tpl['dbid']) {
                                            $tpl_byDbName[$dbname][] = $tpl['preview'];
                                    }
                            }
                    }
            }
            $update_tables = $update_tables + $db_tables;

            $count = 0;
            $occurence_db = array();
            $allUpdate = array();
            foreach ($update_tables as $tableName => $cols) {
                    $cols_string = implode(',', $cols);

                    if ($tableName == 'elements') {
                            addJoin('menuepunkte', 'seitenid', 'id', 'title_intern', 'LEFT');
                    }
                    foreach ($cols as $col) {
                            addWhere('`'.$col.'`', 'LIKE', '%' . $source . '%', 's', 'OR');
                    }
                    select($tableName, 'id,' . implode(',', $cols), 'id');
                    $occurence_db[$tableName] = array();
                    $db_links = array();
                    $contents = array();
                    while ($row = getRow()) {
                            $update = array();
                            $check = false;
                            $para = '';
                            foreach ($row as $name2 => $value2) {
                                    if ($name2 != 'id') {
                                            if (isSet($serialized[$tableName]) && in_array($name2, $serialized[$tableName])) {
                                                    $datas = unserialize($value2);
                                                    $doSerialize = true;
                                                    $newdata = $datas;
                                            } else {
                                                    $datas = array_combine(array($name2), array($value2));
                                                    $doSerialize = false;
                                            }
                                            foreach ($datas as $name => $value) {
                                                    $newvalue = str_replace($source, $mysqldestination, $value);
                                                    if ($value != $newvalue) {
                                                            if (!$doSerialize)
                                                                    $update[$name] = $newvalue;
                                                            else
                                                                    $newdata[$name] = $newvalue;
                                                            $count++;
                                                            $check = true;
                                                            $para .= 's';

                                                            if (isset($db_tables[$tableName])) {
                                                                    $db = $db_byName[$tableName];
                                                                    $links = array();
                                                                    $links[] = $dbc->getFieldId($row, $db['fieldid']);
                                                                    foreach ($tpl_byDbName[$tableName] as $tplname) {
                                                                            $links[] = $tplname . '-' . menuepunkte::setTitelIntern($dbc->getFieldId($row, $db['fieldid']));
                                                                    }
                                                                    $occurence_db[$tableName][] = $links;
                                                            } else {
                                                                    $occurence_db['Inhalte'][] = $row['title_intern'];
                                                            }
                                                    } elseif ($doSerialize) {
                                                            $newdata[$name] = $newvalue;
                                                    }
                                            }
                                            if ($doSerialize) {
                                                    $update[$name2] = serialize($newdata);
                                            }
                                    }
                            }
                            if (count($update) > 0) {
                                    $allUpdate[] = array(
                                            'table' => $tableName,
                                            'id' => $row['id'],
                                            'para' => $para,
                                            'updateData' => $update
                                    );
                            }
                            if (!$check)
                                    echo 'FAIL';
                    }
            }
            if($updateMysqlData){
                foreach ($allUpdate as $up) {
                        addWhere('id', '=', $up['id']);
                        updateArray($up['table'], $up['updateData'], $up['para']);
                }
            }
            $html = '<h1>Vorkommen</h1>';
            foreach ($occurence_db as $tableName => $occ) {

                    if ($tableName == 'Inhalte') {
                            $html .= '<b>Inhalte (' . count($occ) . ' vorkommen): </b><br />';
                            foreach ($occ as $o) {
                                    if ($o == '')
                                            $o = 'index';
                                    $html .= ' <a href="http://' . $_SERVER['HTTP_HOST'] . '' . $GLOBALS['cms_roothtml'].'admin/' . $o . '.html" target="_blank">' . $o . '</a><br />';
                            }
                            $show = true;
                    }else {
                            $html .= '<b>Datenbank: ' . $tableName . ' (' . count($occ) . ' vorkommen)</b><br>';
                            $show = false;
                            foreach ($occ as $o) {
                                    $show = true;
                                    $anz = count($o);
                                    $html .= $o[0];
                                    for ($i = 1; $i < $anz; $i++) {
                                            $link = $o[$i];
                                            $html .= ' <a href="http://' . $_SERVER['HTTP_HOST'] . '/' . $GLOBALS['cms_roothtml'].'admin/' . substr($tableName, '5') . '/' . $link . '.html">Vorschau ' . $i . '</a>';
                                    }
                                    if (count($o) > 0)
                                            $html .= '<br />';
                            }
                    }
            }
            
            if($moveFiles){
                $this->move($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'].$destination, true);
            }
            if ($count == 0)
                return 'Es gab keine Vorkommen';
            return $html;            
        }
        
        public function updateFilePathInDatabase($source,$dest){
            
            $source = '/dateien/' . $_POST['src'];
            $source_pfad = slashFolderFix($source);
            
            $destination = $this->pfad . $_POST['to'] . '/' . basename($source);
            $mysqldestination = str_replace('//', '/', '/dateien/' . $_POST['to'] . '/' . basename($_POST['src']));
//			echo
            $update_tables = array(
                    'elements' => array('eldata'),
                    'dataTemplates' => array('html', 'image'),
                    'menuepunkte' => array('cvars'),
                    'rss' => array('image')
            );
            $serialized = array(
                    'elements' => array('eldata'),
                    'menuepunkte' => array('cvars')
            );

            $dbc = new DatabaseConnector();
            $dbs = $dbc->getDatabases();
            $dont_replace = array('tags', 'email', 'select');
            $db_tables = array();
            addWhere('preview', '!=', '');
            select('dataTemplates', 'dbid,preview');
            $mainTemplates = getRows();
            $db_byName = array();
            foreach ($dbs as $db) {
                    $update_rows = array();
                    foreach ($db['fields'] as $field) {
                            if (!in_array($field['type'], $dont_replace)) {
                                    $update_rows[] = $field['name'];
                            }
                    }
                    if (count($update_rows) > 0) {
                        if($db['dbType'] == '1') $dbname = 'user';
                        elseif($db['dbType'] == '2') $dbname = 'frontEndUser';
                        else $dbname = 'data_' . $db['name'];
                            $db_tables[$dbname] = $update_rows;
                            $db_byName[$dbname] = $db;
                            $tpl_byDbName[$dbname] = array();
                            foreach ($mainTemplates as $tpl) {
                                    if ($db['id'] == $tpl['dbid']) {
                                            $tpl_byDbName[$dbname][] = $tpl['preview'];
                                    }
                            }
                    }
            }
            $update_tables = $update_tables + $db_tables;

            $count = 0;
            $occurence_db = array();
            $allUpdate = array();
            foreach ($update_tables as $tableName => $cols) {
                    $cols_string = implode(',', $cols);

                    if ($tableName == 'elements') {
                            addJoin('menuepunkte', 'seitenid', 'id', 'title_intern', 'LEFT');
                    }
                    foreach ($cols as $col) {
                            echo '`'.$col.'`'.'<br>';
                            addWhere('`'.$col.'`', 'LIKE', '%' . $source . '%', 's', 'OR');
                    }
                    select($tableName, 'id,' . implode(',', $cols), 'id');
                    $occurence_db[$tableName] = array();
                    $db_links = array();
                    $contents = array();
                    while ($row = getRow()) {
                            $update = array();
                            $check = false;
                            $para = '';
                            foreach ($row as $name2 => $value2) {
                                    if ($name2 != 'id') {
                                            if (isSet($serialized[$tableName]) && in_array($name2, $serialized[$tableName])) {
                                                    $datas = unserialize($value2);
                                                    $doSerialize = true;
                                                    $newdata = $datas;
                                            } else {
                                                    $datas = array_combine(array($name2), array($value2));
                                                    $doSerialize = false;
                                            }
                                            foreach ($datas as $name => $value) {
                                                    $newvalue = str_replace($source, $mysqldestination, $value);
                                                    if ($value != $newvalue) {
                                                            if (!$doSerialize)
                                                                    $update[$name] = $newvalue;
                                                            else
                                                                    $newdata[$name] = $newvalue;
                                                            $count++;
                                                            $check = true;
                                                            $para .= 's';

                                                            if (isset($db_tables[$tableName])) {
                                                                    $db = $db_byName[$tableName];
                                                                    $links = array();
                                                                    $links[] = $dbc->getFieldId($row, $db['fieldid']);
                                                                    foreach ($tpl_byDbName[$tableName] as $tplname) {
                                                                            $links[] = $tplname . '-' . menuepunkte::setTitelIntern($dbc->getFieldId($row, $db['fieldid']));
                                                                    }
                                                                    $occurence_db[$tableName][] = $links;
                                                            } else {
                                                                    $occurence_db['Inhalte'][] = $row['title_intern'];
                                                            }
                                                    } elseif ($doSerialize) {
                                                            $newdata[$name] = $newvalue;
                                                    }
                                            }
                                            if ($doSerialize) {
                                                    $update[$name2] = serialize($newdata);
                                            }
                                    }
                            }
                            if (count($update) > 0) {
                                    $allUpdate[] = array(
                                            'table' => $tableName,
                                            'id' => $row['id'],
                                            'para' => $para,
                                            'updateData' => $update
                                    );
                            }
                            if (!$check)
                                    echo 'FAIL';
                    }
            }
            foreach ($allUpdate as $up) {
                    addWhere('id', '=', $up['id']);
                    updateArray($up['table'], $up['updateData'], $up['para']);
            }
            echo '<h1>Vorkommen</h1>';
            foreach ($occurence_db as $tableName => $occ) {

                    if ($tableName == 'Inhalte') {
                            $html = '<b>Inhalte ' . count($occ) . ' vorkommen)</b><br />';
                            foreach ($occ as $o) {
                                    if ($o == '')
                                            $o = 'index';
                                    $html .= '<a href="http://' . $_SERVER['HTTP_HOST'] . '/' . $GLOBALS['cms_roothtml'] . $o . '.html" target="_blank">' . $o . '</a><br />';
                            }
                            $show = true;
                    }else {
                            $html = '<b>Datenbank: ' . $tableName . ' (' . count($occ) . ' vorkommen)</b><br>';
                            $show = false;
                            foreach ($occ as $o) {
                                    $show = true;
                                    $anz = count($o);
                                    $html .= $o[0];
                                    for ($i = 1; $i < $anz; $i++) {
                                            $link = $o[$i];
                                            $html .= ' <a href="http://' . $_SERVER['HTTP_HOST'] . '/' . $GLOBALS['cms_roothtml'] . substr($tableName, '5') . '/' . $link . '.html">Vorschau ' . $i . '</a>';
                                    }
                                    if (count($o) > 0)
                                            $html .= '<br />';
                            }
                    }
                    if ($show)
                            echo $html;
            }
            if ($count == 0)
                    echo 'Es gab keine Vorkommen';

            $this->dirmove();
        }
        
        public function getFilesAsJson() {
            
            if (!$this->pubJson) return false;

            \mys::getObj()->cleanup()->clearWhere();
            addWhere("path","LIKE","{$this->relativePath}");
            addWhere("filetype","<>","directory");
            select("files","name,path,comment");
            
            $c = 0;
            $array = array();
            while($row = getRow()) {
               $array[++$c] = array(
                                "path" => $row["path"] ."/" .  $row["name"],
                                "comment" => $row["comment"]
                              );
            }
            
            return json_encode($array);
        }
        
        static public function find($str) {
            if (strlen($str) <= 2) return array();
//            mys::getObj()->cleanup()->clearWhere();
//            $res = mys::getObj()->query("SELECT id,name FROM files WHERE 'name' LIKE '%" . \mysqli_real_escape_string($str,mys::getObj()) . "%' LIMIT 30");
//            $GLOBALS['mysql_debug'] = true;
            addWhere('name', 'LIKE', '%'.$str.'%');
            setLimit(30);
            select('files','id,name,path');
//            $GLOBALS['mysql_debug'] = false;
            return getRows();
        }
    }
?>
