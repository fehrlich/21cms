$(document).ready(function () {

    var slide = $(".LatestGalleryPicutures .content");
    var margin = 0;

    slide.find(".img:first-child").addClass("active");

    setInterval(function () {
        var current = slide.find(".img.active");
        var next = current.next(".img");

        if (next.length == 0) {
            next = slide.find(".img:first-child");
            slide.animate({"margin-left": 0}, 1000);
        } else {
            slide.animate({
                "margin-left": parseInt(slide.css("margin-left")) - current.outerWidth()
            }, 300);
        }

        current.removeClass("active");
        next.addClass("active");

    }, 5000);

});