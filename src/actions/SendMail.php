<?php
namespace action;

class SendMail extends \Action {

	private $to = '';
	private $to_addresses = '';
	private $from = '';
	private $fromEmail = '';
	private $mailSubject = '';
	private $message = '';
	private $attachmets = array();

	function __construct($data,$retObj = null) {
		$this->to = (isSet($data['to']))?$data['to']:'';
		$this->to_addresses = $data['to_addresses'];
		$this->from = $data['from'];
		$this->mailSubject = $data['mailSubject'];
		$this->message = $data['message'];
		if(isSet($data['attachmets']))
			$this->attachmets = $data['attachmets'];
	}

	public function execute() {
		$mail = new \mimeMail();
		
		
		$this->to = $this->parseStr($this->to);
		$this->to_addresses = $this->parseStr($this->to_addresses);
		$groups = explode(",",$this->to);
		$recipients = array();
		$sendToFrontEndUser = false;
		$sendToGroups = false;
		foreach ($groups as $value) {
		  if ($value != '')
			if ( $value != 0) {
				addWhere('groups', 'LIKE', "%,{$value},%","s","OR");
				$sendToGroups = true;
			} else
			  $sendToFrontEndUser = true;
		}
		if ($sendToGroups) {
		  select('addressUser',"email");
		  while($row = getRow()) { if(!empty($row["email"])) $recipients[] = $row["email"]; }
		  $recipients = array_unique($recipients);
		  \mys::getObj()->cleanup()->clearWhere();
		}
		if ($sendToFrontEndUser) {
		  select("frontEndUser","email");
		  while($row = getRow()) { if(!empty($row["email"])) $recipients[] = $row["email"];}
		  \mys::getObj()->cleanup()->clearWhere();
		}
		if($this->to_addresses != ''){
			$addR = explode(',', $this->to_addresses);
			foreach($addR as $r)
				$recipients[] = $r;
		}
		
//		if (is_array($this->to)){
//                new dBug($to);
		foreach ($recipients as $to)
			$mail->addRecipient($to);
//		}else
//			$mail->addRecipient($this->to);
		$mail->setBody($this->parseStr($this->message));
		$mail->setFromComplete($this->from);
		$mail->setSubject($this->mailSubject);
		if(count($this->attachmets) > 0){
			foreach($this->attachmets as $att){
				if(isSet($att['name'])) $att['name'] = '';
				if(isSet($att['ctype'])) $att['ctype'] = 'application/octet-stream';
				if(isSet($att['encode'])) $att['encode'] = '';
				$mail->add_attachment($att['message'], $att['name'], $att['ctype'], $att['encode']);
			}
		}
		$mail->send();
	}
	
	static function getForm(){
		$form = new \formular();
		$groups = array('FrontEndUser')+buildSelectArray("addressGroups", array('id','name'));
//		$groups[] = "FrontEndUser";
		$form->addElement('Zu Gruppen', 'to','multiselect','0',$groups);
		$form->addElement('Zu Emailadressen', 'to_addresses','textarea');
		$form->addElement('Von', 'from');
		$form->addElement('Betreff', 'mailSubject');
		$form->addElement('Nachricht', 'message',  \FormType::HTMLEDITOR);
		
		
		return $form;
	}
}

?>
