<?php
if(!isSet($_GET['el'])) exit();

include('serverconfig.php');
include('klassen/Vars.php');
include "klassen/frontendSession.php";
include "klassen/session.php";
include "funktionen/global.php";
include('klassen/mysql.php');
include('klassen/element.php');

UserConfig::getObj();
$GLOBALS['interface'] = 'frontEndAjax';
$GLOBALS['cms_root'] = $GLOBALS['cms_roothtml'];
$error = '';

if (!isset($_GET["el"])) die("Fehler");

switch($_GET["el"]) {
    
    // filebrowser
    case 'fileBrowser': {
        if (frontendSession::getObj()->isLoggedIn() || \cms\session::getObj()->isLoggedIn()) {
            
            if (checkVar(array('file', 'id'), 'di', true)) {
                if ($_GET['act'] == 'del') {
                    DateiTabelle::deleteFile($_GET['id'], rawurldecode($_GET['file']));
                } elseif ($_GET['act'] == 'download') {
                    DateiTabelle::downloadFile($_GET['id'], rawurldecode($_GET['file']));
                }
            }else
                echo 'Error: Der Fehler wurde an einen Administrator weitergeleitet';
        }else
            echo 'Error: Sie sind nicht mehr eingeloggt';
    } break;
    
    
    case 'userPlugin': {
         if (!isSet($_GET['p'])) {
            echo 'Fehler: Es wurde kein Plugin angegeben'; 
            die();
         }
            
        $p = $_GET['p'];
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'tpl/' . $p . '.php'))
            echo 'Plugin nicht vorhanden';
        else {
            include_once($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['cms_rootdir'] . 'tpl/' . $p . '.php');
            $fkt = "user" . $p;
            ob_start();
            if (!function_exists($fkt))
                echo 'Funktion existiert nicht';
            else {
                $str = $fkt();
            }
            $str .= ob_get_contents();
            ob_end_clean();
            echo $str;
        }
    } break;
    
    
    case 'shop': {
        if (!isset($_GET["act"])) die("Fehler");
        if($_GET['act'] == 'addToCart') {
            shop::getObj()->addProductToCart($_GET['db'], $_GET['title'], $_POST);
        }
        if($_GET['act'] == 'getProduct') {
            if(!isSet($_POST['id'])) echo json_encode (array('error', 'no id given'));
            elseif(!cart::getObj()->getProduct(1)) echo json_encode (array('error', 'no id given'));
            else{
                $arr = cart::getObj()->getProduct($_POST['id'])->toObject();
                echo json_encode($arr);                
            }
        }
        if($_GET['act'] == 'getSettings') {
//            $id = $_POST['id'];
            echo json_encode(array(
                'shipping' => shop::getObj()->getShipment(),
                'payment' => shop::getObj()->getPayment(),
                'shippingPrice' => cart::getObj()->getShippingPrice()
            ));
        }
        if($_GET['act'] == 'addOrChangeArticle') {
            shop::getObj()->addOrChangeArticle($_GET['db'], $_GET['id'], $_GET['title'], $_POST);
        }
        if($_GET['act'] == 'deleteFromCart'){
            cart::getObj()->deleteItem($_GET['id']);
        }
        if($_GET['act'] == 'getCartHtml'){
            echo cart::getObj()->getHtml("Keine Artikel");
        }
        if($_GET['act'] == 'setShipment'){
            echo shop::getObj()->setShipment($_POST['shipment']);
        }
        if($_GET['act'] == 'setPayment'){
            echo shop::getObj()->setPayment($_POST['payment']);
        }
        if($_GET['act'] == 'appendOptions'){
            echo shop::getObj()->appendProductOptions($_GET['id'], $_POST);
        }
        if($_GET['act'] == 'deleteProductOptions'){
            echo shop::getObj()->deleteProductOptions($_GET['id'], $_POST);
        }
        if($_GET['act'] == 'sendMailToAdmin'){
            echo shop::getObj()->sendMailToAdmin();
        }
    }break;
    
    case 'dmJson': {
        if (!isset($_GET['path'])) die("Fehler");
        $f = \cms\file::load($_GET["path"]);
        if (!$f->isDir()) die("Fehler");
        echo $f->getFilesAsJson();
        
    } break;
    
    case 'content':{
        if(!checkVar(array('title','getContent'))) die("Fehler");
         include('klassen/mainelement.php');
         include('klassen/container.php');
         include('klassen/Parser.php');
         include('klassen/Custom_Enum.php');
        $title = $_POST['title'];
        addWhere('title_intern', '=', $title);
        setLimit(1);
        select('menuepunkte','id');
        $row = getRow();
        $mpId = $row['id'];
     //   addWhere('seitenid', '=', $mpId);
     //   addWhere('container', 'getContent');
        $GLOBALS['seitenid'] = getMenuid($title);
        preparation();
        $c = new container($_POST['getContent']);
        echo $c->getInline();

     }break;
    
    default: echo 'Fehler';
}
?>
