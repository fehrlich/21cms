<?php

    define("intern",false);
    //extern
    include("funktionen/constant.php");

    if (file_exists('serverconfig.php') && is_readable('serverconfig.php')) {
      include('serverconfig.php');
    } else { die("Die Serverkonfiguration kann nicht gelesen werden."); }

    include("funktionen/loadclasses.php");

    $logger = new Logger();
    $logger->setOutputCallback(function ($msg) { echo $msg; });

    if ( php_sapi_name() != "cli" ) {
        $logger->debug("called from browser");
        die();
    }


    $runner = new MailQueueRunner;

    $time = microtime(1);

    while($runner->readMailQueueItem()) {

        $logger->debug("read item {id} to mail {to}",$runner->getData());
        $runner->send();
    }

    $logger->debug("finished after " . ( microtime(1) - $time ));


