#!/bin/sh
# wait until MySQL is really available
maxcounter=440
 
counter=1
while ! mysql -u"root" -p"$MYSQL_ROOT_PASSWORD" -e "show databases;" -h db > /dev/null 2>&1; do
    sleep 3
    counter=`expr $counter + 1`
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "We have been waiting for MySQL too long already; failing."
        exit 1
    fi;
done