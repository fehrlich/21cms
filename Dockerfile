FROM php:7.2-apache as app

ENV DBHOST db
ENV DBNAME stickerei
EXPOSE 80
WORKDIR /var/www/html

COPY config/php/apache.conf /etc/apache2/conf-available/user.conf

RUN apt-get update \
    && apt-get -y upgrade \
    && a2enmod rewrite \
        headers \
        expires \
    && a2dismod -f  \
        access_compat \
        alias \
        auth_basic \
        authn_core \
        authn_file \
        authz_user \
        autoindex \
    && a2enconf user \
    && apt-get install -y \
        exim4-base \
        libpng-dev \
        libjpeg-dev \
        libzip-dev \
    && docker-php-ext-configure gd \
        --with-jpeg-dir \
        --with-png-dir \
    && docker-php-ext-install -j$(nproc) mysqli gd zip mbstring exif opcache \
    && docker-php-source delete \
    && mkdir /var/www/html/dateien \
    && chown -R www-data:www-data /var/www/html/dateien \
    && mkdir --mode=777 /tmp/sessions \
    && rm -rf /var/lib/apt/lists/*

COPY src /var/www/html/
COPY config/php/php.ini /usr/local/etc/php/
COPY config/mail/update-exim4.conf.conf /etc/exim4/update-exim4.conf.conf

RUN /etc/init.d/exim4 restart
CMD apache2-foreground


FROM app as appssh
EXPOSE 22

RUN apt-get update && \
    apt-get install -y openssh-server \
    && rm -rf /var/lib/apt/lists/*

COPY config/ssh/* /root/.ssh/
RUN chmod 600 /root/.ssh/*
RUN mkdir /var/run/sshd \
    && chmod 0755 /var/run/sshd \
    && chmod 600 /root/.ssh/*

CMD /etc/init.d/ssh start && apache2-foreground